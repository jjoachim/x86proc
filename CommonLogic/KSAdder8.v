`ifndef _KSADDER8_V_
`define _KSADDER8_V_

`include "CommonLogic/KSAdder32.v"

module kogge_stone_8 (out,cout,in1,in2,cin);
input [7:0] in1,in2;
input cin;
inv1$ inverter_cin(cinv,cin);
output [7:0] out;
output cout;
// Init P and G
wire [7:0] Ginit, Pinit,Pinit0 ;
wire [8:0] sum ;
wire   Ginit_cinv;
assign   Ginit_cinv = cinv;
assign   Pinit0 = Pinit;
PGinit pginit_0 (Ginit[0],Pinit[0],in1[0],in2[0]);
PGinit pginit_1 (Ginit[1],Pinit[1],in1[1],in2[1]);
PGinit pginit_2 (Ginit[2],Pinit[2],in1[2],in2[2]);
PGinit pginit_3 (Ginit[3],Pinit[3],in1[3],in2[3]);
PGinit pginit_4 (Ginit[4],Pinit[4],in1[4],in2[4]);
PGinit pginit_5 (Ginit[5],Pinit[5],in1[5],in2[5]);
PGinit pginit_6 (Ginit[6],Pinit[6],in1[6],in2[6]);
PGinit pginit_7 (Ginit[7],Pinit[7],in1[7],in2[7]);
wire [7:0] G0, P0;
wire   G0_cinv;
assign   G0_cinv = cinv;
wire [31:0]  Pbar0;
//stage:0  start: 0  finish:0
Gunit gunit_0_0 (G0[0],P0[0],Ginit[0],Pinit[0],Ginit_cinv);
PGboxorinv pgbox_0_1 (G0[1],P0[1],Ginit[1],Pinit[1],Ginit[0],Pinit[0],Pbar0[1]);
PGboxorinv pgbox_0_2 (G0[2],P0[2],Ginit[2],Pinit[2],Ginit[1],Pinit[1],Pbar0[2]);
PGboxorinv pgbox_0_3 (G0[3],P0[3],Ginit[3],Pinit[3],Ginit[2],Pinit[2],Pbar0[3]);
PGboxorinv pgbox_0_4 (G0[4],P0[4],Ginit[4],Pinit[4],Ginit[3],Pinit[3],Pbar0[4]);
PGboxorinv pgbox_0_5 (G0[5],P0[5],Ginit[5],Pinit[5],Ginit[4],Pinit[4],Pbar0[5]);
PGboxorinv pgbox_0_6 (G0[6],P0[6],Ginit[6],Pinit[6],Ginit[5],Pinit[5],Pbar0[6]);
PGboxorinv pgbox_0_7 (G0[7],P0[7],Ginit[7],Pinit[7],Ginit[6],Pinit[6],Pbar0[7]);
wire [7:0] G1, P1;
wire   G1_cinv;
assign   G1_cinv = cinv;
//stage:1  start: 1  finish:2
PGbuf pgbuf_1_0 (G1[0],P1[0],G0[0],P0[0]);
Gunit gunit_1_1 (G1[1],P1[1],G0[1],P0[1],G0_cinv);
Gunit gunit_1_2 (G1[2],P1[2],G0[2],P0[2],G0[0]);
PGboxnand pgbox_1_3 (G1[3],P1[3],G0[3],P0[3],G0[1],P0[1],Pbar0[3],Pbar0[1]);
PGboxnand pgbox_1_4 (G1[4],P1[4],G0[4],P0[4],G0[2],P0[2],Pbar0[4],Pbar0[2]);
PGboxnand pgbox_1_5 (G1[5],P1[5],G0[5],P0[5],G0[3],P0[3],Pbar0[5],Pbar0[3]);
PGboxnand pgbox_1_6 (G1[6],P1[6],G0[6],P0[6],G0[4],P0[4],Pbar0[6],Pbar0[4]);
PGboxnand pgbox_1_7 (G1[7],P1[7],G0[7],P0[7],G0[5],P0[5],Pbar0[7],Pbar0[5]);
wire [7:0] G2, P2;
wire   G2_cinv;
assign   G2_cinv = cinv;
wire [31:0]  Pbar2;
//stage:2  start: 3  finish:6
PGbuf pgbuf_2_0 (G2[0],P2[0],G1[0],P1[0]);
PGbuf pgbuf_2_1 (G2[1],P2[1],G1[1],P1[1]);
PGbuf pgbuf_2_2 (G2[2],P2[2],G1[2],P1[2]);
Gunit gunit_2_3 (G2[3],P2[3],G1[3],P1[3],G1_cinv);
Gunit gunit_2_4 (G2[4],P2[4],G1[4],P1[4],G1[0]);
Gunit gunit_2_5 (G2[5],P2[5],G1[5],P1[5],G1[1]);
Gunit gunit_2_6 (G2[6],P2[6],G1[6],P1[6],G1[2]);
PGboxorinv pgbox_2_7 (G2[7],P2[7],G1[7],P1[7],G1[3],P1[3],Pbar2[7]);
//SUM
Sumxor sumxor_0 (sum[0],G2_cinv,Pinit[0]);
Sumxor sumxor_1 (sum[1],G2[0],Pinit[1]);
Sumxor sumxor_2 (sum[2],G2[1],Pinit[2]);
Sumxor sumxor_3 (sum[3],G2[2],Pinit[3]);
Sumxor sumxor_4 (sum[4],G2[3],Pinit[4]);
Sumxor sumxor_5 (sum[5],G2[4],Pinit[5]);
Sumxor sumxor_6 (sum[6],G2[5],Pinit[6]);
Sumxor sumxor_7 (sum[7],G2[6],Pinit[7]);
nor2$ carry_last (Pcarry,cinv,P2[7]);
inv1$ inv_last_g (Ginvlast2,G2[7]);
or2$ sumor_8 (sum[8],Ginvlast2,Pcarry);
assign out=sum[7:0];
assign cout=sum[8];
endmodule

module KSAdder8 (oOut, oCarry, iA, iB, iCarry);
output  [7:0] oOut;
output        oCarry;
input   [7:0] iA;
input   [7:0] iB;
input         iCarry;

kogge_stone_8 adder (oOut, oCarry, iA, iB, iCarry);

endmodule

module KSAdder8NC (oOut, iA, iB);
output  [7:0] oOut;
input   [7:0] iA;
input   [7:0] iB;

wire carry;
kogge_stone_8 adder (oOut, carry, iA, iB, 1'b0);

endmodule

`endif
