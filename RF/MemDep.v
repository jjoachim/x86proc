`ifndef _MEMDEP_V_
`define _MEMDEP_V_

`include "CommonLogic/PipeRegs.v"

`default_nettype none

module MemDep (oBubble, iMemW, iMemR, inStall, inFlush, iClk, inReset);

output  oBubble;
input   iMemW;
input   iMemR;
input   iClk;
input   inReset;
input   inStall;
input   inFlush;

//Stage Track
wire depActive; //calculated later
wire entry0;
mux2$ mux_entry (entry0, iMemW, 1'b0, depActive);

wire oMem;
PipeRegs MEM (oMem, entry0, inStall, inFlush, iClk, inReset);

wire oExe;
PipeRegs EXE (oExe, oMem, inStall, inFlush, iClk, inReset);

wire oWb;
PipeRegs WB (oWb, oExe, inStall, inFlush, iClk, inReset);

//Dependency Check
or3$ or_depActive (depActive, oMem, oExe, oWb);
wire memOp;
or2$ or_memOp (memOp, iMemW, iMemR);

//Bubble Calculation
and2$ and_bubble (oBubble, depActive, memOp);

endmodule

`default_nettype wire
`endif
