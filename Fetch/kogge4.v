`ifndef _KOGGE_4_
`define _KOGGE_4_
module kogge_stone_4 (out,cout,in1,in2,cin);
input [3:0] in1,in2;
input cin;
inv1$ inverter_cin(cinv,cin);
output [3:0] out;
output cout;
// Init P and G
wire [3:0] Ginit, Pinit,Pinit0 ;
wire [4:0] sum ;
wire   Ginit_cinv;
assign   Ginit_cinv = cinv;
assign   Pinit0 = Pinit;
PGinit pginit_0 (Ginit[0],Pinit[0],in1[0],in2[0]);
PGinit pginit_1 (Ginit[1],Pinit[1],in1[1],in2[1]);
PGinit pginit_2 (Ginit[2],Pinit[2],in1[2],in2[2]);
PGinit pginit_3 (Ginit[3],Pinit[3],in1[3],in2[3]);
wire [3:0] G0, P0;
wire   G0_cinv;
assign   G0_cinv = cinv;
wire [31:0]  Pbar0;
//stage:0  start: 0  finish:0
Gunit gunit_0_0 (G0[0],P0[0],Ginit[0],Pinit[0],Ginit_cinv);
PGboxorinv pgbox_0_1 (G0[1],P0[1],Ginit[1],Pinit[1],Ginit[0],Pinit[0],Pbar0[1]);
PGboxorinv pgbox_0_2 (G0[2],P0[2],Ginit[2],Pinit[2],Ginit[1],Pinit[1],Pbar0[2]);
PGboxorinv pgbox_0_3 (G0[3],P0[3],Ginit[3],Pinit[3],Ginit[2],Pinit[2],Pbar0[3]);
wire [3:0] G1, P1;
wire   G1_cinv;
assign   G1_cinv = cinv;
//stage:1  start: 1  finish:2
PGbuf pgbuf_1_0 (G1[0],P1[0],G0[0],P0[0]);
Gunit gunit_1_1 (G1[1],P1[1],G0[1],P0[1],G0_cinv);
Gunit gunit_1_2 (G1[2],P1[2],G0[2],P0[2],G0[0]);
PGboxnand pgbox_1_3 (G1[3],P1[3],G0[3],P0[3],G0[1],P0[1],Pbar0[3],Pbar0[1]);
//SUM
Sumxor sumxor_0 (sum[0],G1_cinv,Pinit[0]);
Sumxor sumxor_1 (sum[1],G1[0],Pinit[1]);
Sumxor sumxor_2 (sum[2],G1[1],Pinit[2]);
Sumxor sumxor_3 (sum[3],G1[2],Pinit[3]);
nor2$ carry_last (Pcarry,cinv,P1[3]);
inv1$ inv_last_g (Ginvlast1,G1[3]);
or2$ sumor_4 (sum[4],Ginvlast1,Pcarry);
assign out=sum[3:0];
assign cout=sum[4];
endmodule
`endif
