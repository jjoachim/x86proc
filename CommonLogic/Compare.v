`ifndef _COMPARE_V_
`define _COMPARE_V_

`include "CommonLogic/Relation32.v"
`include "CommonLogic/Reduce.v"
`include "Fetch/libASE.v"

`default_nettype none

/*****************************************************************************/
// Parameterizable Inverted Comparator
/*****************************************************************************/
module NCompare (oOut, iB, iA);
  parameter width = 4;

  input [width-1:0] iA, iB;
  output oOut;

  generate
    if(width<=8) begin
      wire out;
      Compare #(width) cmp (out, iB, iA);
      not1$ nout (oOut, out);
    end else if(width<=16) begin
      wire [width-1:0] aXb;
      xor2$ cmp [width-1:0] (aXb, iA, iB);
      ReduceOR #(width) reduce (oOut, aXb);
    end else if(width<32) begin
      wire [31-width:0] pad;
      assign pad = 0;
      ase_equal fast_relate 
        (.equal_low(oOut), .in1({pad, iB}), .in2({pad, iA}));
    end else if(width==32) begin
      ase_equal fast_relate 
        (.equal_low(oOut), .in1(iB), .in2(iA));
    end
  endgenerate
endmodule

/*****************************************************************************/
// Parameterizable Comparator
/*****************************************************************************/
module Compare (oOut, iB, iA);
  parameter width = 4;

  input [width-1:0] iA, iB;
  output oOut;
  wire nOut;

  generate
    if(width<8) begin
      wire [7-width:0] pad;
      assign pad = 0;
      compare_8 fast_relate 
        (.out(oOut), .in1({pad, iB}), .in2({pad, iA}));
    end else if(width==8) begin
      compare_8 fast_relate 
        (.out(oOut), .in1(iB), .in2(iA));
    end else if(width<=16) begin
      NCompare #(width) cmp (nOut, iA, iB);
      not1$ notOut (oOut, nOut);
    end else if(width<32) begin
      wire [31-width:0] pad;
      assign pad = 0;
      ase_equal fast_relate 
        (.equal_high(oOut), .in1({pad, iB}), .in2({pad, iA}));
    end else if (width==32) begin
      ase_equal fast_relate 
        (.equal_high(oOut), .in1(iB), .in2(iA));
    end
  endgenerate
endmodule

`default_nettype wire
`endif
