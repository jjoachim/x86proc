`ifndef _PROCESSOR_V_
`define _PROCESSOR_V_

`include "Parameters.v"
`include "Bus/Bus.v"
`include "MainMemory/MainMemory.v"
`include "DMA/DMA.v"
`include "Processor/Pipeline.v"

module Processor(iClk, inReset);
  input iClk, inReset;

  //Instantiate Bus Logic
  wire [`BUS_MSB:0] bus;
  Bus BusLogic (bus, iClk, inReset);

  //Main Memory's bus slave interface
  wire [31:0] MM_BusData, MM_BusAddr, MM_Data;
  wire MM_Valid;
  wire [3:0] MM_BusWr;
  wire MM_BusRd;
  BusSlave #(`BUS_MEM_ADDRL, `BUS_MEM_ADDRH) MM_iBusSlave
    (bus, MM_BusData, MM_BusAddr, MM_BusWr, MM_BusRd, MM_Data, MM_Valid);

  //Instantiate Main Memory
  MainMemory #(`MEM_BANKS) mm
    (MM_Data, MM_Valid, MM_BusData, MM_BusAddr, MM_BusRd, MM_BusWr, iClk, inReset);

  //DMA slave interface
  wire [31:0] DMA_sbusData, DMA_sbusAddr, DMA_sData;
  wire [3:0] DMA_sbusWr;
  wire DMA_sbusRd, DMA_sValid;
  BusSlave #(`BUS_DMA_ADDRL, `BUS_DMA_ADDRH) sbus_dma
    (bus, DMA_sbusData, DMA_sbusAddr, DMA_sbusWr, DMA_sbusRd, DMA_sData, DMA_sValid);

  //DMA master interface
  wire [31:0] DMA_mData, DMA_mAddr, DMA_mbusData;
  wire [3:0] DMA_mWr;
  wire DMA_mRd, DMA_mbusValid;
  BusMaster #(`DMA_MASTER_ID) mbus_dma
    (bus, DMA_mbusData, DMA_mbusValid, DMA_mData, DMA_mAddr, DMA_mWr, DMA_mRd);

  //DMA
  wire DMA_int;
  DMA dma (DMA_mData, DMA_mAddr, DMA_mRd, DMA_mWr, DMA_mbusData, DMA_mbusValid,
           DMA_sValid, DMA_sData, DMA_sbusData, DMA_sbusAddr, DMA_sbusRd, DMA_sbusWr,
           DMA_int, iClk, inReset);
  
//////  DMA INTERRUPT PULSE
	   wire rIntPrev,rIntPrev_prime,IntPulse;
	   PipeRegs  #(1)   dma_reg_pulse
	   (rIntPrev,DMA_int,1'b1,1'b1,iClk,inReset);
	   inv1$ inver_dma(rIntPrev_prime,rIntPrev);
	   and2$ pulse_and(IntPulse,rIntPrev_prime,DMA_int);






///////
  //Instantiate Pipeline
  Pipeline pipe (IntPulse,bus, iClk, inReset);

endmodule

`endif
