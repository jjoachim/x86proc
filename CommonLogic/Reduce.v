`ifndef _REDUCE_V_
`define _REDUCE_V_

`default_nettype none

/*****************************************************************************/
// Parameterizable Reduce
/*****************************************************************************/
`define REDUCE(N, GATE)                           \
module Reduce``N (oOut, iA);                      \
  parameter width = 8;                            \
  parameter depth = $clog2(width);                \
  parameter pad = (1<<depth) - width;             \
  parameter p2Width = width + pad;                \
                                                  \
  output oOut;                                    \
  input [width-1:0] iA;                           \
                                                  \
  wire [p2Width-1:0] adjA;                        \
  assign adjA[width-1:0] = iA;                    \
                                                  \
  genvar i;                                       \
  generate                                        \
    if(pad) assign adjA[p2Width-1:width] = 0;     \
                                                  \
    for(i=0; i<depth; i=i+1) begin : lvl          \
      localparam iWidth = p2Width>>i;             \
      localparam oWidth = iWidth>>1;              \
      wire [oWidth-1:0] tmp;                      \
      if(i==0) begin                              \
        GATE reduce [oWidth-1:0]                  \
          (tmp, adjA[iWidth/2-1:0], adjA[iWidth-1:iWidth/2]);   \
      end else begin                              \
        GATE reduce [oWidth-1:0]                  \
          (tmp, lvl[i-1].tmp[iWidth/2-1:0], lvl[i-1].tmp[iWidth-1: iWidth/2]); \
      end                                         \
    end                                           \
                                                  \
    if(width > 1) begin                           \
      assign oOut = lvl[depth-1].tmp;             \
    end else begin                                \
      assign oOut = iA;                           \
    end                                           \
  endgenerate                                     \
                                                  \
endmodule

`REDUCE(OR, or2$) //ReduceOR
`REDUCE(AND, and2$) //ReduceAND
`REDUCE(XOR, xor2$)

`default_nettype wire
`endif
