###############################################################
# Common Variables
###############################################################
TOP := $(patsubst %/,%,$(dir $(lastword $(MAKEFILE_LIST))))

#LIB  =    /home/projects/courses/spring_14/ee382n-17295/lib/time \
#			 -v /home/projects/courses/spring_14/ee382n-17295/lib/lib1 \
#		 	 -v /home/projects/courses/spring_14/ee382n-17295/lib/lib2 \
#			 -v /home/projects/courses/spring_14/ee382n-17295/lib/lib3 \
#			 -v /home/projects/courses/spring_14/ee382n-17295/lib/lib4 \
#			 -v /home/projects/courses/spring_14/ee382n-17295/lib/lib5 \
#			 -v /home/projects/courses/spring_14/ee382n-17295/lib/lib6

LIB  =    $(TOP)/lib/time \
			 -v $(TOP)/lib/lib1 \
		 	 -v $(TOP)/lib/lib2 \
			 -v $(TOP)/lib/lib3 \
			 -v $(TOP)/lib/lib4 \
			 -v $(TOP)/lib/lib5 \
			 -v $(TOP)/lib/lib6

	
INC  = +incdir+$(TOP)/ +incdir+$(TOP)/libext

       SRC += Top.v
TEMP_FILES += csrc ucli.key simv.daidir session* core.* *.vpd *.mem *.asm *.o
    TARGET  = simv

 VCS_FLAGS += -debug_all +warn=all +v2k -v2005 -v2k_generate +warn=noOBSV2G +v2k

###############################################################
# Submake calls
###############################################################
default: $(TARGET)

.PHONY: $(TARGET)
$(TARGET):
	vcs $(VCS_FLAGS) $(INC) $(LIB) $(SRC) -o $(TARGET)

test: $(TARGET)
	@./$(TARGET)
	chmod 777 top.dump.vpd

clean:
	-rm -rf $(TEMP_FILES) $(TARGET)

###############################################################
# Topmake calls (optional)
###############################################################
SUB_DIRS      = $(shell ls -d */ | sed "s/\///g")
CLEAN_TARGETS = $(patsubst %,%-clean,$(SUB_DIRS))

#Cleaning Submodules
%-clean:
	-$(MAKE) -C $* clean
	-$(MAKE) -C $* all-clean

#Cleaning All Submodules
.PHONY: all-clean
all-clean: $(CLEAN_TARGETS)

#Testing Submodules
%-test:
	$(MAKE) -C $* test
