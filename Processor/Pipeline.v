`ifndef _PIPELINE_V_
`define _PIPELINE_V_

`include "Bus/Bus.v"
`include "Parameters.v"
`include "Fetch/Fetch.v"
`include "Decode/Decode.v"
`include "RF/RF.v"
`include "AGE/AGE.v"
`include "MEM/MEM.v"
`include "EXE/EXE.v"
`include "WB/WB.v"

`default_nettype none

module Pipeline (iInt,ioBus, iClk, inReset);

    //////////////////////////////
    // External control signals //
    //////////////////////////////
    input iInt;
    inout [`BUS_MSB:0] ioBus;
    input  iClk;
    input  inReset;

    ////////////////
    // IF signals //
    ////////////////

    //Fetch (F)
    wire   [ 31:0]  FE_EIP;
    wire            FE_BrTaken;
    wire   [ 31:0]  FE_Stat;
    wire   [255:0]  FE_Data;
    wire            FE_Valid;
    wire   [ 15:0]  FE_CS;
    wire   [ 15:0]  RF_FCS; // -> F

    wire            inFE_Stall;
    wire            inFE_Flush;
    /////////////////
    // CSR signals //
    /////////////////

    //Control Store Read (CSR)
    wire    [`CTRLSZ-1:0]   CSR_ControlStore;
    wire    [       31:0]   CSR_Stat;
    wire    [       63:0]   CSR_InstTail;
    wire    [        7:0]   CSR_ModRM;
    wire    [        7:0]   CSR_SIB;
    wire    [       15:0]   CSR_CS;
    wire    [       31:0]   CSR_EIP,CSR_FEIP;
    wire    [       31:0]   CSR_PC;
    wire    [        3:0]   CSR_ISZ;    //instruction size
    wire    [        2:0]   CSR_FSM;    // state machine state
    wire                    iCSR_Stall;
    wire                    iCSR_Flush;

    ////////////////
    // RF signals //
    ////////////////

    //Register File (RF)
    // RF -> IF
    wire    [       15:0]   RF_CSS;
    // RF -> AGE
    wire    [       31:0]   RF_Stat;
    wire    [`CTRLSZ-1:0]   RF_ControlStore;
    wire                    RF_RegW;
    wire                    RF_SegW;
    wire    [        2:0]   RF_DstR;
    wire    [        2:0]   RF_REG;
    wire    [       63:0]   RF_GPR;
    wire    [       63:0]   RF_RM;
    wire    [        1:0]   RF_Scale;
    wire    [       31:0]   RF_Index;
    wire    [       31:0]   RF_Base;
    wire    [       31:0]   RF_Disp;
    wire    [       15:0]   RF_Seg;
    wire    [       31:0]   RF_SegN;
    wire                    RF_MemR;
    wire                    RF_MemW;
    wire    [       63:0]   RF_AltData;
    wire    [       15:0]   RF_CS;
    wire    [       31:0]   RF_EIP;
    wire    [       31:0]   RF_EIPPLUS;
    wire                    RF_Stall;
    wire                    inRF_Stall;
    wire                    inRF_Flush;

    /////////////////
    // AGE signals //
    /////////////////

    //Address Generation (AGE)
    // AGE -> MEM
    wire    [       31:0]   AGE_Stat;
    wire    [`CTRLSZ-1:0]   AGE_ControlStore;
    wire                    AGE_RegW;
    wire                    AGE_SegW;
    wire    [        2:0]   AGE_DstR;
    wire    [       63:0]   AGE_GPR;
    wire    [       63:0]   AGE_RM;
    wire    [        2:0]   AGE_REG;
    wire    [        1:0]   AGE_RdSz;
    wire    [       31:0]   AGE_Addr;
    wire                    AGE_UA;
    wire    [       31:0]   AGE_AddrP16;
    wire                    AGE_MemR;
    wire                    AGE_MemW;
    wire    [       63:0]   AGE_AltData;
    wire    [       15:0]   AGE_CS;
    wire    [       31:0]   AGE_EIP;
    wire    [       31:0]   AGE_EIPPLUS;
    wire                    inAGE_Stall;
    wire                    inAGE_Flush;

    /////////////////
    // MEM signals //
    /////////////////

    //Memory (MEM)
    // MEM -> EXE
    wire    [`CTRLSZ-1:0]   MEM_ControlStore;
    wire    [       31:0]   MEM_Stat;
    wire                    MEM_Stall;
    wire                    MEM_RegW;
    wire                    MEM_SegW;
    wire                    MEM_MemW;
    wire    [        2:0]   MEM_DstR;
    wire    [        2:0]   MEM_REG;
    wire    [       63:0]   MEM_GPR;
    wire    [       63:0]   MEM_RM;
    wire    [       63:0]   MEM_AltData;
    wire    [       31:0]   MEM_Addr;
    wire                    MEM_UA;
    wire    [       31:0]   MEM_AddrP16;
    wire    [        1:0]   MEM_PCD;
    wire    [       15:0]   MEM_CS;
    wire    [       31:0]   MEM_EIP;
    wire    [       31:0]   MEM_EIPLUS;
    wire                    inMEM_Flush;
    wire                    inMEM_Stall;

    /////////////////
    // EXE signals //
    /////////////////

    //Execute (EXE)
    // EXE -> WB
    wire    [       31:0]   EXE_Stat;
    wire    [`CTRLSZ-1:0]   EXE_ControlStore;
    wire                    EXE_RegW;
    wire                    EXE_SegW;
    wire    [        2:0]   EXE_DstR;
    wire    [       63:0]   EXE_ValE;
    wire    [       31:0]   EXE_FLAGSN;
    wire    [       31:0]   EXE_Addr;
    wire                    EXE_UA;
    wire    [       31:0]   EXE_AddrP16;
    wire    [        1:0]   EXE_PCD;
    wire                    EXE_MemW;
    wire    [        1:0]   EXE_WrSz;
    wire    [       31:0]   EXE_EIPN;
    wire                    EXE_BRTK;
    wire    [       15:0]   EXE_CS;
    wire    [       31:0]   EXE_EIP;
    wire    [       31:0]   EXE_EIPPLUS;
    wire                    inEXE_Flush;
    wire                    inEXE_Stall;

    ////////////////
    // WB signals //
    ////////////////

    // WB -> CSR
    wire    [31:0]  WB_Stat;
    wire            WB_REPE;
    wire    [31:0]  WB_EIPN;
    wire            WB_BRTK;
    // WB -> RF
    wire    [63:0]  WB_ValE;
    wire    [ 2:0]  WB_DstR;
    wire            WB_RegW;
    wire            WB_SegW;
    // WB -> MEM
    wire    [31:0]  WB_Addr;
    wire            WB_UA;   
    wire    [31:0]  WB_AddrP16;
    wire    [ 1:0]  WB_PCD;
    wire            WB_MemW;
    wire    [ 1:0]  WB_WrSz;
    // WB -> EXE
    wire    [31:0]  WB_EFLAGS;
	wire 	[31:0]	WB_ArchEIP;

    //    -> WB
    wire            WB_Flush;
    wire            inWB_Stall;
    wire            inWB_Flush;

    Fetch f (
            .oEIP             (FE_EIP),
            .oBrTaken         (FE_BrTaken),
            .oStat            (FE_Stat),
            .oData            (FE_Data),
            .oValid           (FE_Valid),
            .oCS              (FE_CS),
            .ioBus            (ioBus),

            .iCSR_EIP         (CSR_FEIP),

            .iRF_CS           (RF_FCS),

            .iWB_BrTaken      (WB_BRTK),
            .iWB_EIP          (WB_EIPN),

            .inStall          (inFE_Stall),
            .inFlush          (inFE_Flush),
            .iClk             (iClk),
            .inReset          (inReset)
          );

    Decode csr (
                .oRestOfInst      (CSR_InstTail),
                .oStat            (CSR_Stat),
                .oControlStore    (CSR_ControlStore),
                .oMod_bits        (CSR_ModRM),
                .oSIB_bits        (CSR_SIB),
                .oFSM             (CSR_FSM),
                .oCS              (CSR_CS),
                .oEIP             (CSR_EIP),
                .oEIP_to_Fetch    (CSR_FEIP),
                .oSize            (CSR_ISZ),

                .iFE_EIP          (FE_EIP),
                .iFE_BrTaken      (FE_BrTaken),
                .iStat            (FE_Stat),
                .iFE_Data         (FE_Data),
                .iFE_valid        (FE_Valid),
                .iFE_CS           (FE_CS),
                
                //.iWB_ExceptionSig (WB_Stat[`iEXC]),
                .iWB_Stat (WB_Stat),
                .iInt (1'b0),
                //.iInt (iInt),

                .iClk             (iClk),
                .inReset          (inReset),
                .iStall           (iCSR_Stall),
                .iFlush           (iCSR_Flush)
              );

    RF  rf (.oStall         (RF_Stall), //dependency check

            //to F
            .oCSS           (RF_FCS),

            //to AGE
            .oStat          (RF_Stat),
            .oControlStore  (RF_ControlStore),
            .oRegW          (RF_RegW),
            .oSegW          (RF_SegW),
            .oDstR          (RF_DstR),
            .oREG           (RF_REG),
            .oGPR           (RF_GPR),
            .oRM            (RF_RM),
            .oIndex         (RF_Index),
            .oBase          (RF_Base),
            .oDisp          (RF_Disp),
            .oSeg           (RF_Seg),
            .oSegN          (RF_SegN),
            .oScale         (RF_Scale),
            .oMemR          (RF_MemR),
            .oMemW          (RF_MemW),
            .oAltData       (RF_AltData),
            .oCS            (RF_CS),
            .oEIP           (RF_EIP),
            .oEIPPLUS       (RF_EIPPLUS),

            //From CSR
            .iStat          (CSR_Stat),
            .iControlStore  (CSR_ControlStore),
            .iModRM         (CSR_ModRM),
            .iSIB           (CSR_SIB),
            .iInstTail      (CSR_InstTail),
            .iCS            (CSR_CS),
            .iEIP           (CSR_EIP),
            .iISZ           (CSR_ISZ),
            //From WB
            .iIN            (WB_ValE),
            .iWR            (WB_DstR),
            .iWE            (WB_RegW),
            .iMODEW         (WB_WrSz),
            .iINS           (WB_ValE[15:0]),
            .iSWR           (WB_DstR),
            .iSWE           (WB_SegW),
            //System Inputs
            .iClk           (iClk),
            .iNSysRst       (inReset),
            .iNFlush        (inRF_Flush),
            .iNStall        (inRF_Stall));

    AGE age(.oStat          (AGE_Stat),
            .oControlStore  (AGE_ControlStore),
            .oRegW          (AGE_RegW),
            .oSegW          (AGE_SegW),
            .oDstR          (AGE_DstR),
            .oGPR           (AGE_GPR),
            .oRM            (AGE_RM),
            .oREG           (AGE_REG),
            .oRdSz          (AGE_RdSz),
            .oAddr          (AGE_Addr),
            .oUA            (AGE_UA),
            .oAddrP16       (AGE_AddrP16),
            .oMemR          (AGE_MemR),
            .oMemW          (AGE_MemW),
            .oAltData       (AGE_AltData),
            .oCS            (AGE_CS),
            .oEIP           (AGE_EIP),
            .oEIPPLUS       (AGE_EIPPLUS),
            .iStat          (RF_Stat),
            .iControlStore  (RF_ControlStore),
            .iRegW          (RF_RegW),
            .iSegW          (RF_SegW),
            .iDstR          (RF_DstR),
            .iREG           (RF_REG),
            .iGPR           (RF_GPR),
            .iRM            (RF_RM),
            .iIndex         (RF_Index),
            .iBase          (RF_Base),
            .iDisp          (RF_Disp),
            .iSeg           (RF_Seg),
            .iSegN          (RF_SegN),
            .iScale         (RF_Scale),
            .iMemR          (RF_MemR),
            .iMemW          (RF_MemW),
            .iAltData       (RF_AltData),
            .iCS            (RF_CS),
            .iEIP           (RF_EIP),
            .iEIPPLUS       (RF_EIPPLUS),
            //System
            .iClk           (iClk),
            .iNSysRst       (inReset),
            .inStall        (inAGE_Stall),
            .inFlush        (inAGE_Flush)
    );

    MEM mem(.oStall         (MEM_Stall),
            .oStat          (MEM_Stat),
            .oControlStore  (MEM_ControlStore),
            .oRegW          (MEM_RegW),
            .oSegW          (MEM_SegW),
            .oMemW          (MEM_MemW),
            .oDstR          (MEM_DstR),
            .oGPR           (MEM_GPR),
            .oRM            (MEM_RM),
            .oREG           (MEM_REG),
            .oAddr          (MEM_Addr),
            .oUA            (MEM_UA),
            .oAddrP16       (MEM_AddrP16),
            .oPCD           (MEM_PCD),
            .oAltData       (MEM_AltData),
            .oCS            (MEM_CS),
            .oEIP           (MEM_EIP),
            .oEIPPLUS       (MEM_EIPLUS),

            .ioBus          (ioBus),
            .iStat          (AGE_Stat),
            .iControlStore  (AGE_ControlStore),
            .iRegW          (AGE_RegW),
            .iSegW          (AGE_SegW),
            .iDstR          (AGE_DstR),
            .iGPR           (AGE_GPR),
            .iRM            (AGE_RM),
            .iREG           (AGE_REG),
            .iRdSz          (AGE_RdSz),
            .iAddr          (AGE_Addr),
            .iUA            (AGE_UA),
            .iAddrP16       (AGE_AddrP16),
            .iMemR          (AGE_MemR),
            .iMemW          (AGE_MemW),
            .iAltData       (AGE_AltData),
            .iCS            (AGE_CS),
            .iEIP           (AGE_EIP),
            .iEIPPLUS       (AGE_EIPPLUS),

            .iWB_ValE       (WB_ValE),
            .iWB_Addr       (WB_Addr),
            .iWB_UA         (WB_UA),
            .iWB_AddrP16    (WB_AddrP16),
            .iWB_PCD        (WB_PCD),
            .iWB_OpSz       (WB_WrSz),
            .iWB_MemW       (WB_MemW),
            .iClk           (iClk),
            .iNSysRst       (inReset),
            .inFlush        (inMEM_Flush),
            .inStall        (inMEM_Stall)
    );

    EXE exe(.oStat          (EXE_Stat),
            .oControlStore  (EXE_ControlStore),
            .oRegW          (EXE_RegW),
            .oSegW          (EXE_SegW),
            .oDstR          (EXE_DstR),
            .oValE          (EXE_ValE),
            .oFLAGSN        (EXE_FLAGSN),
            .oAddr          (EXE_Addr),
            .oUA            (EXE_UA),
            .oAddrP16       (EXE_AddrP16),
            .oPCD           (EXE_PCD),
            .oMemW          (EXE_MemW),
            .oWrSz          (EXE_WrSz),
            .oEIPN          (EXE_EIPN),
            .oBRTK          (EXE_BRTK),
            .oCS            (EXE_CS),
            .oEIP           (EXE_EIP),
            .oEIPPLUS       (EXE_EIPPLUS),

            .iStat          (MEM_Stat),
            .iControlStore  (MEM_ControlStore),
            .iRegW          (MEM_RegW),
            .iSegW          (MEM_SegW),
            .iDstR          (MEM_DstR),
            .iGPR           (MEM_GPR),
            .iRM            (MEM_RM),
            .iREG           (MEM_REG),
            .iAddr          (MEM_Addr),
            .iUA            (MEM_UA),
            .iAddrP16       (MEM_AddrP16),
            .iPCD           (MEM_PCD),
            .iMemW          (MEM_MemW),
            .iAltData       (MEM_AltData),
            .iEFLAGS        (WB_EFLAGS),
			.iArchEIP		(WB_ArchEIP),
            .iCS            (MEM_CS),
            .iEIP           (MEM_EIP),
            .iEIPPLUS       (MEM_EIPLUS),

            .iClk           (iClk),
            .iNSysRst       (inReset),
            .inFlush        (inEXE_Flush),
            .inStall        (inEXE_Stall)
    );

    //Writeback (WB)
    WB wb(  // -> CSR
            .oStat          (WB_Stat),
            .oREPE          (WB_REPE),
            .oBRTK          (WB_BRTK),
            .oEIPN          (WB_EIPN),
            // -> MEM and RF
            .oValE          (WB_ValE),
            .oDstR          (WB_DstR),
            .oRegW          (WB_RegW),
            .oSegW          (WB_SegW),
            .oAddr          (WB_Addr),
            .oUA            (WB_UA),
            .oAddrP16       (WB_AddrP16),
            .oPCD           (WB_PCD),
            .oMemW          (WB_MemW),
            .oWrSz          (WB_WrSz),
            // -> EXE
            .oEFLAGS        (WB_EFLAGS),
			.oArchEIP 		(WB_ArchEIP),

            .oFlush         (WB_Flush),

            .iStat          (EXE_Stat),
            .iControlStore  (EXE_ControlStore),
            .iRegW          (EXE_RegW),
            .iSegW          (EXE_SegW),
            .iDstR          (EXE_DstR),
            .iValE          (EXE_ValE),
            .iFLAGSN        (EXE_FLAGSN),
            .iAddr          (EXE_Addr),
            .iUA            (EXE_UA),
            .iAddrP16       (EXE_AddrP16),
            .iPCD           (EXE_PCD),
            .iMemW          (EXE_MemW),
            .iWrSz          (EXE_WrSz),
            .iEIPN          (EXE_EIPN),
            .iBRTK          (EXE_BRTK),
            .iCS            (EXE_CS),
            .iEIP           (EXE_EIP),
            .iEIPPLUS       (EXE_EIPPLUS),

            .iClk           (iClk),
            .iNSysRst       (inReset),
            .inFlush        (inWB_Flush),
            .inStall        (inWB_Stall)
    );

    //Calculate Stalls
    wire nMEM_Stall;
    not1$ not_memstall (nMEM_Stall, MEM_Stall);

    wire nCSR_Stall;
    assign inFE_Stall = nCSR_Stall;
    nor2$ csr_nstall (nCSR_Stall, RF_Stall, MEM_Stall);
    or2$ csr_stall (iCSR_Stall, RF_Stall, MEM_Stall);
    assign inRF_Stall  = nMEM_Stall;
    assign inAGE_Stall = nMEM_Stall;
    assign inMEM_Stall = nMEM_Stall;
    assign inEXE_Stall = nMEM_Stall;
    assign inWB_Stall  = nMEM_Stall;

    //Calculate Flushes
    wire nWB_Flush;
	wire nWB_EXC;
    not1$ not_wbbrtaken (nWB_Flush, WB_Flush);
	not1$ not_wbexc     (nWB_EXC,   WB_Stat[`iEXC]);

    assign inFE_Flush  = nWB_Flush;
    assign iCSR_Flush  =  WB_Flush;
    and2$    rfFlush_or(inRF_Flush,  nWB_Flush, nWB_EXC);
    and2$   ageFlush_or(inAGE_Flush, nWB_Flush, nWB_EXC);
    and2$   memFlush_or(inMEM_Flush, nWB_Flush, nWB_EXC);
    and2$   exeFlush_or(inEXE_Flush, nWB_Flush, nWB_EXC);
    and2$    wbFlush_or(inWB_Flush,  nWB_Flush, nWB_EXC);

endmodule

`default_nettype wire
`endif
