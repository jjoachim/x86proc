mov  $0x0c00, %ax
mov  %ax, %ds
movl $0x00000000, %esi

movl $0xffffffff, %eax    ;eax = 0xffffffff
movl %eax, %ecx           ;ecx = 0xffffffff

andb $0x77, %al           ;eax = 0xffffff77
andw $0x7711, %ax         ;eax = 0xffff7711
andl $0x77771111, %eax    ;eax = 0x77771111
movl $0xffffffff, (%esi)  ;mem0 = 0xffffffff
movl $0xffffffff, %ebx    ;ebx = 0xffffffff
andb $0x77, (%esi)        ;mem0 = 0xffffff77
andb $0x77, %bl           ;ebx = 0xffffff77
andw $0x7711, (%esi)      ;mem0 = 0xffff7711
andw $0x7711, %bx         ;ebx = 0xffff7711
andl $0x77771111, (%esi)  ;mem0 = 0x77771111
andl $0x77771111, %ebx    ;ebx = 0x77771111
movl $0xffffffff, 4(%esi) ;mem4 = 0xffffffff
movl $0xffffffff, %edx    ;edx = 0xffffffff
andw $-2, 4(%esi)         ;mem4 = 0xfffffffe
andw $-2, %dx             ;edx = 0xfffffffe
andl $-128, 4(%esi)       ;mem4 = 0xffffff80
andl $-128, %edx          ;edx = 0xffffff80
movl $0x0f0f0f0f, %ecx    ;ecx = 0x0f0f0f0f
andb %cl, 4(%esi)         ;mem4 = 0xffffff00
andb %cl, %dl             ;edx = 0xffffff00
andw %cx, 4(%esi)         ;mem4 = 0xffff0f00
andw %cx, %dx             ;edx = 0xffff0f00
andl %ecx, 4(%esi)        ;mem4 = 0x0f0f0f00
andl %ecx, %edx           ;edx = 0x0f0f0f00
movl %ecx, 8(%esi)        ;mem8 = 0x0f0f0f0f
movl $0xffffffff, %ecx    ;ecx = 0xffffffff
andb 8(%esi), %cl         ;ecx = 0xffffff0f
andw 8(%esi), %cx         ;ecx = 0xffff0f0f
andl 8(%esi), %ecx        ;ecx = 0x0f0f0f0f
movl 4(%esi), %ebx        ;ebx = 0x0f0f0f00
movl (%esi), %eax         ;eax = 0x77771111
hlt
