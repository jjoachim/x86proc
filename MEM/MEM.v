`ifndef _MEM_V_
`define _MEM_V_

`include "Parameters.v"
`include "Bus/Bus.v"
`include "CommonLogic/PipeRegs.v"

`default_nettype none

module MEM(oStall,
           oStat, oControlStore,
           oRegW, oSegW, oDstR,
           oGPR,  oRM,   oREG,
           oAddr, oUA,   oAddrP16, oPCD,
           oMemW, oAltData, 
           oCS,   oEIP,  oEIPPLUS,

           ioBus,

           iStat,  iControlStore,
           iRegW,  iSegW, iDstR,
           iGPR,   iRM,
           iREG,   iRdSz,
           iAddr,  iUA,   iAddrP16,
           iMemR,  iMemW,
           iAltData,

           iWB_ValE,
           iWB_Addr, iWB_UA, iWB_AddrP16, iWB_PCD,
           iWB_OpSz, iWB_MemW,

           iCS,    iEIP,   iEIPPLUS,
           iClk, iNSysRst, inFlush, inStall);

    // I/O
    // System controls
    input                 iClk;
    input                 iNSysRst;
    input                 inFlush;
    input                 inStall;
    inout   [`BUS_MSB:0]  ioBus;

    // AGE -> MEM
    input   [       31:0] iStat;
    input   [`CTRLSZ-1:0] iControlStore;
    input                 iRegW;
    input                 iSegW;
    input   [        2:0] iDstR;
    input   [       63:0] iGPR;
    input   [       63:0] iRM;
    input   [        2:0] iREG;
    input   [        1:0] iRdSz;
    input   [       31:0] iAddr;
    input                 iUA;
    input   [       31:0] iAddrP16;
    input                 iMemR;
    input                 iMemW;
    input   [       63:0] iAltData;
    input   [       15:0] iCS;
    input   [       31:0] iEIP;
    input   [       31:0] iEIPPLUS;

    // WB -> MEM
    input   [63:0]  iWB_ValE;
    input   [31:0]  iWB_Addr;
    input           iWB_UA;
    input   [31:0]  iWB_AddrP16;
    input   [ 1:0]  iWB_PCD;
    input   [ 1:0]  iWB_OpSz;
    input           iWB_MemW;

    // MEM -> EXE
    output                oStall;
    output  [       31:0] oStat;
    output  [`CTRLSZ-1:0] oControlStore;
    output                oRegW;
    output                oSegW;
    output  [        2:0] oDstR;
    output  [       63:0] oGPR;
    output  [       63:0] oRM;
    output  [        2:0] oREG;
    output  [       31:0] oAddr;
    output                oUA;
    output  [       31:0] oAddrP16;
    output  [        1:0] oPCD;
    output                oMemW;
    output  [       63:0] oAltData;
    output  [       15:0] oCS;
    output  [       31:0] oEIP;
    output  [       31:0] oEIPPLUS;

    // Split control-store signals
    wire [1:0] ALTMUX0;
    wire       ALTMUX1;    // 0=MEMR.AltData 1=TEMP
    wire       LDTEMP;     // Load TEMP register
    wire       USEIMM;
    wire       USEMODRM;

    // Split Control-Store
    assign ALTMUX0    = iControlStore[`iALTMUX0];
    assign ALTMUX1    = iControlStore[`iALTMUX1];
    assign LDTEMP     = iControlStore[`iLDTEMP];
    assign USEIMM     = iControlStore[`iUSEIMM];
    assign USEMODRM   = iControlStore[`iUSEMODRM];

    //DCache
    wire [63:0] cData;
    wire [31:0] pAddr0, pAddr1;
    wire cExceptPF, cExceptWrP;
    wire [1:0] cPCD;

    wire cUA, cRd, cWr;
    wire [31:0] cAddr0, cAddr1;
    DCache #(`DCACHE_MASTER_ID, `DCACHE_SIZE, `DCACHE_BLOCK_SIZE) dcache
    (
      .oData      (cData),
      .oAddr0     (pAddr0),
      .oAddr1     (pAddr1),
      .oExceptPF  (cExceptPF),
      .oExceptWrP (cExceptWrP),
      .oMiss      (oStall),
      .oPCD       (cPCD),
      .iAddr0     (cAddr0),
      .iAddr1     (cAddr1),
      .iUA        (cUA),
      .iRd        (cRd),
      .iWrChk     (cWr),
      .iWr        (iWB_MemW),
      .iDataW     (iWB_ValE),
      .iOpSz      (iWB_OpSz),
      .iPCD       (iWB_PCD),
      .ioBus      (ioBus),
      .iClk       (iClk),
      .inReset    (iNSysRst)
    );

    //Choose Rd and Wr to prevent memory requests during synch flush
    mux2$ mux_cRd (cRd, 1'b0, iMemR, inFlush);
    mux2$ mux_cWr (cWr, 1'b0, iMemW, inFlush);

    //Choose Address
    mux2$ mux_cAddr0 [31:0]
      (cAddr0, iAddr, iWB_Addr, iWB_MemW);
    mux2$ mux_cAddr1 [31:0]
      (cAddr1, iAddrP16, iWB_AddrP16, iWB_MemW);

    //Choose UA
    mux2$ mux_UA (cUA, iUA, iWB_UA, iWB_MemW);

    /* Outputs ******************************************/
    //Stat
    wire [31:0] rStat;
    wire [7:0] myStat;
    mux4$ mux_mystat [7:0]
      (myStat,
       iStat[`iEXCV],
       `PFEXCV,
       `GPREXCV,
       `GPREXCV,
       cExceptPF, cExceptWrP);
    mux2$ mux_stat [7:0] (rStat[`iEXCV], myStat, iStat[`iEXCV], iStat[`iEXC]);
    or3$ or_statexc (rStat[`iEXC], iStat[`iEXC], cExceptPF, cExceptWrP);
    assign rStat[`iHEXCV] = iStat[`iHEXCV];
    assign rStat[`iINT] = iStat[`iINT];
    assign rStat[`iEXCMODE] = iStat[`iEXCMODE];
    assign rStat[`iRSVD] = iStat[`iRSVD];

    //Write signals
    wire rRegW, rSegW, rMemW;
    mux2$ mux_regW (rRegW, iRegW, 1'b0, rStat[`iEXC]);
    mux2$ mux_segW (rSegW, iSegW, 1'b0, rStat[`iEXC]);
    mux2$ mux_memW (rMemW, iMemW, 1'b0, rStat[`iEXC]);

    //Clean up pAddr
    wire [31:0] rAddr0, rAddr1;
    and2$ and_addr0 [31:0] (rAddr0, pAddr0, iMemW);
    and2$ and_addr1 [31:0] (rAddr1, pAddr1, iMemW);

    //RM data
    wire [63:0] rRM;
    mux2$ RM_mux [63:0]
      (rRM, iRM, cData, iMemR);

    //Temp Reg
	wire        useImmMuxSel;
    wire [63:0] useImmMux;
	or2$ useImmSel_or(useImmMuxSel, USEIMM, ALTMUX0[0]);
    mux2$ mux_useImm [63:0]
      (useImmMux, iGPR, iAltData, useImmMuxSel);

    wire rTempMuxSel;
    or2$ or_rTempMuxSel (rTempMuxSel, iMemR, USEMODRM);
    wire [63:0] rTempMux;
    mux2$ mux_rtemp [63:0]
      (rTempMux, useImmMux, rRM, rTempMuxSel);

    wire [63:0] rTemp;
    regs$ #(64) reg_temp 
      (rTemp, rTempMux, LDTEMP, iClk, iNSysRst);

    //AltData
    wire [63:0] rAltData;
    mux2$ mux_altdata [63:0]
      (rAltData, iAltData, rTemp, ALTMUX1);
    
    //Output Pipeline Stage
    PipeRegs #(449) piperegs
      ({oStat, oControlStore, oRegW, oSegW, oDstR,
        oGPR, oRM, oREG, oAddr,  oUA, oAddrP16, oPCD,
        oMemW, oAltData, oCS, oEIP, oEIPPLUS},
       {rStat, iControlStore, rRegW, rSegW, iDstR,
        iGPR, rRM, iREG, rAddr0, iUA, rAddr1,   cPCD,
        iMemW, rAltData, iCS, iEIP, iEIPPLUS},
       inStall, inFlush, iClk, iNSysRst
      );
endmodule

`default_nettype wire
`endif
