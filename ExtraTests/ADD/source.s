mov  $0x0c00, %ax
mov  %ax, %ds
movl $0x00000000, %esi

movl $0x11111111, %eax    ;eax = 0x11111111
movl %eax, %ecx           ;ecx = 0x11111111

addb $0x11, %al           ;eax = 0x11111122
addw $0x1111, %ax         ;eax = 0x11112233
addl $0x11111111, %eax    ;eax = 0x22223344
movl %eax, (%esi)         ;mem0 = 0x22223344
movl %eax, %ebx           ;ebx = 0x22223344
addb $0x11, (%esi)        ;mem0 = 0x22223355
addb $0x11, %bl           ;ebx = 0x22223355
addw $0x1111, (%esi)      ;mem0 = 0x22224466
addw $0x1111, %bx         ;ebx = 0x22224466
addl $0x11111111, (%esi)  ;mem0 = 0x33335577
addl $0x11111111, %ebx    ;ebx = 0x33335577
movl $0x00000000, 4(%esi) ;mem4 = 0x00000000
movl $0x00000000, %edx    ;edx = 0x00000000
addw $-128, 4(%esi)       ;mem4 = 0x0000ff80
addw $-128, %dx           ;edx = 0x0000ff80
addl $-128, 4(%esi)       ;mem4 = 0x0000ff00
addl $-128, %edx          ;edx = 0x0000ff00
addb %cl, 4(%esi)         ;mem4 = 0x0000ff11
addb %cl, %dl             ;edx = 0x0000ff11
addw %cx, 4(%esi)         ;mem4 = 0x00001022
addw %cx, %dx             ;edx = 0x00001022
addl %ecx, 4(%esi)        ;mem4 = 0x11112133
addl %ecx, %edx           ;edx = 0x11112133
movl %ecx, 8(%esi)        ;mem8 = 0x11111111
addb 8(%esi), %cl         ;ecx = 0x11111122
addw 8(%esi), %cx         ;ecx = 0x11112233
addl 8(%esi), %ecx        ;ecx = 0x22223344
movl 4(%esi), %ebx        ;ebx = 0x11122133
movl (%esi), %eax         ;eax = 0x33335577
hlt
