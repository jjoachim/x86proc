`ifndef _FLAGSGEN_VH
`define _FLAGSGEN_VH

`include "libext/defines.v"
`include "libext/libSJQ.v"

// Flag generator unit
module FLAGSGEN(oFLAGSN, iALUOUT, iALUFLAGS, iEFLAGS, FLAGMUX);

    // I/O
    input   [ 7:0]  iALUFLAGS;
    input   [63:0]  iALUOUT;
    input   [31:0]  iEFLAGS;
    input   [ 2:0]  FLAGMUX;
    output  [31:0]  oFLAGSN;

    // Potential FLAGSN
    wire    [31:0]  FLAGMUX000;
    wire    [31:0]  FLAGMUX001;
    wire    [31:0]  FLAGMUX010;
    wire    [31:0]  FLAGMUX011;
    wire    [31:0]  FLAGMUX100;
    wire    [31:0]  FLAGMUX101;
    wire    [31:0]  FLAGMUX110;
    wire    [31:0]  FLAGMUX111;

    // Generate flags
    assign FLAGMUX000 = iEFLAGS;
    assign FLAGMUX001 = {iEFLAGS[31:12],
                         iALUFLAGS[  5],
                         iEFLAGS[10: 8],
                         iALUFLAGS[4:3],
                         iEFLAGS[    5],
                         iALUFLAGS[  2],
                         iEFLAGS[    3],
                         iALUFLAGS[  0],
                         iEFLAGS[    1],
                         iALUFLAGS[  1]};
    assign FLAGMUX010 = {iEFLAGS[31:12],
                         iALUFLAGS[  5],
                         iEFLAGS[10: 8],
                         iALUFLAGS[4:3],
                         iEFLAGS[ 5: 3],
                         iALUFLAGS[  0],
                         iEFLAGS[    1],
                         iALUFLAGS[  1]};
    assign FLAGMUX011 = 32'h00000000;
    assign FLAGMUX100 = {iEFLAGS[31:11],1'b0,iEFLAGS[ 9:0]};
    assign FLAGMUX101 = {iEFLAGS[31:11],1'b1,iEFLAGS[ 9:0]};
    assign FLAGMUX110 = {iEFLAGS[31: 5],iALUFLAGS[1],iEFLAGS[ 3: 1],iALUFLAGS[0]};
    assign FLAGMUX111 = iALUOUT[31:0];

    // Actual FLAGSN generator
    mux8_32$ flagsnmux(oFLAGSN,
                       FLAGMUX000, FLAGMUX001, FLAGMUX010, FLAGMUX011,
                       FLAGMUX100, FLAGMUX101, FLAGMUX110, FLAGMUX111,
                       FLAGMUX);


endmodule

`endif
