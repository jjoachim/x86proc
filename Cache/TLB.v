`ifndef _TLB_V_
`define _TLB_V_

`include "Parameters.v"
`include "CommonLogic/CommonLogic.v"

`default_nettype none

/*****************************************************************************/
// Translation Look-Ahead Buffer (TLB)
/*****************************************************************************/
//Output Bits
`define TLB_PCD 21
`define TLB_WRE 20
`define TLB_A 19

//Tag Bits
`define TLB_V 20

module TLB (oAddr, iAddr, oMiss);
  output [31:0] oAddr; 
  output oMiss;
  input [31:0] iAddr;

  wire [21:0] entry [7:0];
  wire [23:0] tag [7:0];
  assign {entry[0], tag[0]} = `TLB_E0;
  assign {entry[1], tag[1]} = `TLB_E1;
  assign {entry[2], tag[2]} = `TLB_E2;
  assign {entry[3], tag[3]} = `TLB_E3;
  assign {entry[4], tag[4]} = `TLB_E4;
  assign {entry[5], tag[5]} = `TLB_E5;
  assign {entry[6], tag[6]} = `TLB_E6;
  assign {entry[7], tag[7]} = `TLB_E7;

  genvar t, b;
  generate
    //For TLB entry
    wire [7:0] ncmpOut;
    for(t=0; t<8; t=t+1) begin : cmp
      //Compare
      NCompare #(21) addr
        (ncmpOut[t], tag[t][`TLB_V:0], {1'b1, iAddr[31:12]});

      //Load Output
      tristateL$ tbData0 [21:0]
        (.out(oAddr[21:0]), .in(entry[t]), .enbar(ncmpOut[t]));
    end
    
    assign oAddr[31:22] = 0;

    //Calculate hit
    ReduceAND #(8) andMiss (oMiss, ncmpOut);
  endgenerate
endmodule

`default_nettype wire
`endif
