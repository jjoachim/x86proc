`ifndef _ALU_VH
`define _ALU_VH

`include "CommonLogic/KSAdder64.v"
`include "EXE/PUNPCKHBW.v"
`include "EXE/DAA.v"
`include "CommonLogic/arithmetic_shifters.v"
`include "CommonLogic/Reduce.v"

`default_nettype none

// ALU
module ALU(oAluOut, oFlags,
           iA, iB,  iOPSZ,  iEFLAGS,
           iALUMUX, iREG,   iOPSZF);

    // I/O
    input   [63:0]  iA;         // REG/ALT value
    input   [63:0]  iB;         // ModRM/ALT value
    input   [ 1:0]  iOPSZ;      // 8b/16b/32b/64b
    input   [31:0]  iEFLAGS;    // EFLAGS value
    input   [ 3:0]  iALUMUX;    // Operation
    input   [ 2:0]  iREG;       // REG bits
    input   [ 1:0]  iOPSZF;     // 8b/16b/32b/64b

    output  [63:0]  oAluOut;    // Output
    output  [ 7:0]  oFlags;     // Flags


    // Buffered inputs
    wire    [63:0]  ABUF;
    wire    [63:0]  BBUF;
    wire    [ 1:0]  OPSZFBUF;
    wire    [31:0]  EFGLAGSBUF;
    
    /////////////////////////////
    // Functional unit outputs //
    /////////////////////////////

    // ADD
    wire    [32:0]  ADDEROUTA;      // Lower 32-bit adder sum
    wire    [31:0]  ADDERSUMBITS;   // Sum bits of adder outputs
    wire            ADDSBITA;       // Adder A input sign bit
    wire            ADDSBITB;       // Adder B input sign bit
    wire            ADDXNORSBITS;   // XOR of adder input sign bits
    wire            ADDXORSBITS;    // XOR of adder output S and input S
    wire            ADDOFLAG;       // Adder output overflow flag
    wire            ADDSFLAG;       // Adder output sign flag
    wire            ADDZFLAG;       // Adder output zero flag
    wire            ADDAFLAG;       // Adder output adjust flag
    wire            ADDCFLAG;       // Adder output carry flag
	wire 			ADDPFLAGN;
    wire            ADDPFLAG;       // Adder output parity flag
    wire    [ 7:0]  ADDFLAGS;       // Adder flags
    wire    [32:0]  ADDEROUTB;      // Upper 32-bit adder sum
    wire    [ 3:0]  ADD4OUT;

    // AND
    wire    [31:0]  ANDOUT;         // AND output
    wire            ANDOFLAG;       // AND output overflow flag
    wire            ANDSFLAG;       // AND output sign flag
    wire            ANDZFLAG;       // AND output zero flag
    wire            ANDAFLAG;       // AND output adjust flag
    wire            ANDCFLAG;       // AND output carry flag
	wire 			ANDPFLAGN;
    wire            ANDPFLAG;       // AND output parity flag
    wire    [ 7:0]  ANDFLAGS;       // AND flags
    wire    [ 3:0]  AND4OUT;

    // ADD/AND
    wire    [31:0]  ADDANDOUT;      // Select operation output
    wire    [ 7:0]  ADDANDFLAGS;    // Selected operation flags

    // NOT
    wire    [63:0]  SEXTB;          // B sign-extended
    wire    [63:0]  NOTOUT;         // NOT output

    // Subtractor
    wire    [64:0]  SUBOUT;         // Subtractor output
    wire    [31:0]  SUBSUMBITS;     // Relevant subtractor bits
    wire            SUBSBITA;       // Adder A input sign bit
    wire            SUBSBITB;       // Adder B input sign bitt
    wire            SUBXNORSBITS;   // XOR of subtractor input sign bits
    wire            SUBXORSBITS;    // XOR of subtractor output S and input S
    wire            SUBOFLAG;       // Subtractor output overflow flag
    wire            SUBSFLAG;       // Subtractor output sign flag
    wire            SUBZFLAG;       // Subtractor output zero flag
    wire            SUBAFLAG;       // Subtractor output adjust flag
    wire            SUBCFLAG;       // Subtractor output carry flag
	wire 			SUBPFLAGN;
    wire            SUBPFLAG;       // Subtractor output parity flag
    wire    [ 7:0]  SUBFLAGS;       // Subtractor flags
    wire    [ 3:0]  SUB4OUT;

    // OPSZ ADD
    wire    [31:0]  OPSZPSCALED;    // OPSZ+ input
    wire    [32:0]  OPSZADDOUT;     // OPSZ adder output

    // OPSZ SUB
    wire    [31:0]  OPSZMSCALED;    // OPSZ- input
    wire    [32:0]  OPSZSUBOUT;     // OPSZ subtractor output

    // PASS A
    // PASS B

    // DIR Add
    wire    [31:0]  DIRMUXOUT;      // {DIR,OPSZ} 000 = -1 | 001 = -2 | 010 = -4 | 011 = -8
                                    //            100 =  1 | 101 =  2 | 110 =  4 | 111 =  8
    wire    [32:0]  DIRADDEROUT;    // DIR adder output

    // BTS
    wire    [31:0]  BTSOUT;         // BTS output
    wire    [ 7:0]  BTSFLAGS;       // AND flags

    // DAA
    wire    [ 7:0]  DAAOUT;         // DAA output
    wire    [ 7:0]  DAAFLAGS;       // DAA flags

    // PUNPCKHBW
    wire    [63:0]  PUNPCKHBWOUT;   // PUNPCKHBW output

    // SHF B, 1
    // 8-bit
    wire    [ 7:0]  SAL1OUT08;
    wire            SAL1CF08;
    wire            SAL1OF08;
    // 16-bit
    wire    [15:0]  SAL1OUT16;
    wire            SAL1CF16;
    wire            SAL1OF16;
    // 32-bit
    wire    [31:0]  SAL1OUT32;
    wire            SAL1CF32;
    wire            SAL1OF32;
    // 8-bit
    wire    [ 7:0]  SAR1OUT08;
    wire            SAR1CF08;
    wire            SAR1OF08;
    // 16-bit
    wire    [15:0]  SAR1OUT16;
    wire            SAR1CF16;
    wire            SAR1OF16;
    // 32-bit
    wire    [31:0]  SAR1OUT32;
    wire            SAR1CF32;
    wire            SAR1OF32;
    // Final results
    wire    [31:0]  SHF1OUT;
    wire            SHF1OF;
    wire            SHF1CF;
    wire            SHF1ZF;
	wire            SHF1PFN;
    wire            SHF1PF;
    wire            SHF1SF;
    wire    [ 3:0]  SHF14OUT;
    wire    [ 7:0]  SHF1FLAGS;

    // SHF B, A
    // 8-bit
    wire    [ 7:0]  SALAOUT08;
    wire            SALACF08;
    wire            SALAOF08;
    // 16-bit
    wire    [15:0]  SALAOUT16;
    wire            SALACF16;
    wire            SALAOF16;
    // 32-bit
    wire    [31:0]  SALAOUT32;
    wire            SALACF32;
    wire            SALAOF32;
    // 8-bit
    wire    [ 7:0]  SARAOUT08;
    wire            SARACF08;
    wire            SARAOF08;
    // 16-bit
    wire    [15:0]  SARAOUT16;
    wire            SARACF16;
    wire            SARAOF16;
    // 32-bit
    wire    [31:0]  SARAOUT32;
    wire            SARACF32;
    wire            SARAOF32;
    // Final results
    wire    [31:0]  SHFAOUT;
    wire            SHFAOF;
    wire            SHFACF;
    wire            SHFAZF;
	wire            SHFAPFN;
    wire            SHFAPF;
    wire            SHFASF;
    wire    [ 3:0]  SHFA4OUT;
    wire    [ 7:0]  SHFAFLAGS;
	wire 			SHFAMOUNTZERO;

    // Shift flags
    wire    [ 7:0]  SHFFLAGS;


    //////////////////////
    // Buffered  input  //
    //////////////////////

    assign ABUF = iA;
    assign BBUF = iB;
    assign OPSZFBUF = iOPSZF;
    assign EFGLAGSBUF = iEFLAGS;

    //////////////////////
    // Functional units //
    //////////////////////

    /*******************************/
    /*            ADD              */
    /*******************************/

    // Actual adders
    KSAdder32 adderA(ADDEROUTA[31:0], ADDEROUTA[32], ABUF[31:00], BBUF[31:00], 1'b0);
    KSAdder32 adderB(ADDEROUTB[31:0], ADDEROUTB[32], ABUF[63:32], BBUF[63:32], 1'b0);

    // Flag generation
    mux4$ addsfa_mux(ADDSBITA, ABUF[7], ABUF[15], ABUF[31], ABUF[63], OPSZFBUF[0], OPSZFBUF[1]);
    mux4$ addsfb_mux(ADDSBITB, BBUF[7], BBUF[15], BBUF[31], BBUF[63], OPSZFBUF[0], OPSZFBUF[1]);
    mux4$ addosf_mux(ADDSFLAG, 
                     ADDEROUTA[7], ADDEROUTA[15], ADDEROUTA[31], 1'b0,
                     OPSZFBUF[0], OPSZFBUF[1]);
    xnor2$ addisf_xnor(ADDXNORSBITS, ADDSBITA, ADDSBITB);
    xor2$  addiosf_xor(ADDXORSBITS,  ADDSBITA, ADDSFLAG);
    mux2$  addof_mux(ADDOFLAG, 1'b0, ADDXNORSBITS, ADDXORSBITS);
    mux4_32$ addsumbits_mux(ADDERSUMBITS,
                            {24'h0,ADDEROUTA[ 7:0]}, {16'h0,ADDEROUTA[15:0]},
                            ADDEROUTA[31:0],         ADDEROUTA[31:0],
                            OPSZFBUF);
    redORN32$ addzf(ADDZFLAG, ADDERSUMBITS);
    kogge_stone_4 addflag_gen(ADD4OUT, ADDAFLAG, ABUF[3:0], BBUF[3:0], 1'b0);
    mux4$ addcf_mux(ADDCFLAG,
                    ADDEROUTA[8], ADDEROUTA[16], ADDEROUTA[32], 1'b0,
                    OPSZFBUF[0], OPSZFBUF[1]);
	ReduceXOR #(8) addParity(ADDPFLAGN,ADDERSUMBITS[7:0]);
	inv1$ addpf_inv(ADDPFLAG, ADDPFLAGN);
    //parity32$ addParity(ADDPFLAG, ADDERSUMBITS);
    assign ADDFLAGS = {2'b00, ADDOFLAG, ADDSFLAG, ADDZFLAG, ADDAFLAG, ADDCFLAG, ADDPFLAG};


    /*******************************/
    /*             AND             */
    /*******************************/

    and2$ andUnit [31:0] (ANDOUT, iA[31:0], iB[31:0]);
    assign ANDOFLAG = 1'b0;
    assign ANDCFLAG = 1'b0;
    mux4$ andosf_mux(ANDSFLAG, 
                     ANDOUT[7], ANDOUT[15], ANDOUT[31], 1'b0,
                     OPSZFBUF[0], OPSZFBUF[1]);
    redORN32$ andzf(ANDZFLAG, ANDOUT[31:0]);
    assign ANDAFLAG = 1'b0;
	ReduceXOR #(8) andParity(ANDPFLAGN,ANDOUT[7:0]);
	inv1$ andpf_inv(ANDPFLAG, ANDPFLAGN);
    //parity32$ andParity(ANDPFLAG, ANDOUT[31:0]);
    assign ANDFLAGS = {2'b00, ANDOFLAG, ANDSFLAG, ANDZFLAG, ANDAFLAG, ANDCFLAG, ANDPFLAG};

    /*******************************/
    /*           ADD/AND           */
    /*******************************/

    mux2_32$ addandout_mux(ADDANDOUT,   ADDERSUMBITS, ANDOUT,   iREG[2]);
    mux2_8$ addandfout_mux(ADDANDFLAGS, ADDFLAGS,     ANDFLAGS, iREG[2]);

    /*******************************/
    /*            NOT              */
    /*******************************/

    // SEXT input
    mux4_64$ sextB(SEXTB,
                   {{56{ADDSBITB}},BBUF[ 7:0]}, {{48{ADDSBITB}},BBUF[15:0]},
                   {{32{ADDSBITB}},BBUF[31:0]},            BBUF,
                   OPSZFBUF);
    inv1$ notUnit [63:0] (NOTOUT, SEXTB);


    /*******************************/
    /*            SUB              */
    /*******************************/

    // Actual subtractor
    KSAdder64 subtractor(SUBOUT[63:0], SUBOUT[64], ABUF, NOTOUT, 1'b1);

    // Flag generation
    mux4$ subsfa_mux(SUBSBITA, ABUF[7],  ABUF[15],  ABUF[31],  ABUF[63],  OPSZFBUF[0], OPSZFBUF[1]);
    mux4$ subsfb_mux(SUBSBITB, SEXTB[7], SEXTB[15], SEXTB[31], SEXTB[63], OPSZFBUF[0], OPSZFBUF[1]);
    mux4$ subosf_mux(SUBSFLAG, 
                     SUBOUT[7],   SUBOUT[15], SUBOUT[31], 1'b0,
                     OPSZFBUF[0], OPSZFBUF[1]);
    xnor2$ subisf_xnor(SUBXNORSBITS, SUBSBITA, SUBSBITB);
    xor2$  subiosf_xor(SUBXORSBITS,  SUBSBITA, SUBSFLAG);
    mux2$  subof_mux(SUBOFLAG, 1'b0, SUBXNORSBITS, SUBXORSBITS);
    mux4_32$ subsumbits_mux(SUBSUMBITS,
                            {24'h0,SUBOUT[ 7:0]}, {16'h0,SUBOUT[15:0]},
                            SUBOUT[31:0],         SUBOUT[31:0],
                            OPSZFBUF);
    redORN32$ subzf(SUBZFLAG, SUBSUMBITS);
    kogge_stone_4 subflag_gen(ADD4OUT, SUBAFLAG, ABUF[3:0], NOTOUT[3:0], 1'b1);
    mux4$ subcf_mux(SUBCFLAG,
                    SUBOUT[8],   SUBOUT[16], SUBOUT[32], 1'b0,
                    OPSZFBUF[0], OPSZFBUF[1]);
	ReduceXOR #(8) subParity(SUBPFLAGN,SUBSUMBITS[7:0]);
	inv1$ subpf_inv(SUBPFLAG, SUBPFLAGN);
    //parity32$ subParity(SUBPFLAG, SUBSUMBITS);
    assign SUBFLAGS = {2'b00, SUBOFLAG, SUBSFLAG, SUBZFLAG, SUBAFLAG, SUBCFLAG, SUBPFLAG};


    /*******************************/
    /*         OPSZ ADDER          */
    /*******************************/

    // OPSZ scaler
    mux4_32$ opszp_mux(OPSZPSCALED,
                       32'h00000001, 32'h00000002,
                       32'h00000004, 32'h00000008,
                       iOPSZ);
    KSAdder32 opszp_adder(OPSZADDOUT[31:0], OPSZADDOUT[32], 
                          ABUF[31:0],       OPSZPSCALED, 1'b0);

    /*******************************/
    /*       OPSZ SUBTRACTOR       */
    /*******************************/

    // OPSZ scaler
    mux4_32$ opszm_mux(OPSZMSCALED,
                       32'hFFFFFFFF, 32'hFFFFFFFE,
                       32'hFFFFFFFC, 32'hFFFFFFF8,
                       iOPSZ);
    KSAdder32 opszm_adder(OPSZSUBOUT[31:0], OPSZSUBOUT[32],
                          ABUF[31:0],       OPSZMSCALED, 1'b0);


    /******************************/
    /*           DIR ADD          */
    /******************************/

	 // DIR scaler
    mux8_32$ dirmuxout(DIRMUXOUT,
					   32'h00000001, 32'h00000002, 32'h00000004, 32'h00000008,
                       32'hFFFFFFFF, 32'hFFFFFFFE, 32'hFFFFFFFC, 32'hFFFFFFF8,
                       {iEFLAGS[`iDF],iOPSZ});
    KSAdder32 dir_adder(DIRADDEROUT[31:0], DIRADDEROUT[32],
                        ABUF[31:0],        DIRMUXOUT, 1'b0);


    /******************************/
    /*             DAA            */
    /******************************/

    DAA daa(DAAOUT, DAAFLAGS[1], DAAFLAGS[0], ABUF[7:0]);
    assign DAAFLAGS[7:2] = 6'b000000;

    /******************************/
    /*          PUNPCKHBW         */
    /******************************/

    PUNPCKHBW punpckhbw(PUNPCKHBWOUT, BBUF, ABUF);


    /******************************/
    /*            SHF 1           */
    /******************************/

    // Shifters
    arith_barrel_8_1        sal1_08(SAL1OUT08,SAL1CF08,SAL1OF08,BBUF[ 7:0],  3'b001);
    arith_barrel_16_1       sal1_16(SAL1OUT16,SAL1CF16,SAL1OF16,BBUF[15:0], 4'b0001);
    arith_barrel_32_1       sal1_32(SAL1OUT32,SAL1CF32,SAL1OF32,BBUF[31:0],5'b00001);
    right_arith_barrel_8_1  sar1_08(SAR1OUT08,SAR1CF08,SAR1OF08,BBUF[ 7:0],  3'b001);
    right_arith_barrel_16_1 sar1_16(SAR1OUT16,SAR1CF16,SAR1OF16,BBUF[15:0], 4'b0001);
    right_arith_barrel_32_1 sar1_32(SAR1OUT32,SAR1CF32,SAR1OF32,BBUF[31:0],5'b00001);

    // MUX shifter results
    mux8_32$ shf1_mux(SHF1OUT,
                      {24'h000000,SAL1OUT08}, {16'h0000,SAL1OUT16},
                      SAL1OUT32,              32'h00000000,
                      {24'h000000,SAR1OUT08}, {16'h0000,SAR1OUT16},
                      SAR1OUT32,              32'h00000000,
                      {iREG[0],OPSZFBUF});
    mux8$  shf1of_mux(SHF1OF,
                      {1'b0,SAR1OF32,SAR1OF16,SAR1OF08,
                       1'b0,SAL1OF32,SAL1OF16,SAL1OF08},
                      {iREG[0],OPSZFBUF});
    mux8$  shf1cf_mux(SHF1CF,
                      {1'b0,SAR1CF32,SAR1CF16,SAR1CF08,
                       1'b0,SAL1CF32,SAL1CF16,SAL1CF08},
                      {iREG[0],OPSZFBUF});
    mux8$  shf1sf_mux(SHF1SF,
                      {1'b0,SAR1OUT32[31],SAR1OUT16[15],SAR1OUT08[ 7],
                       1'b0,SAL1OUT32[31],SAL1OUT16[15],SAL1OUT08[ 7]},
                      {iREG[0],OPSZFBUF});
    redORN32$ shf1zf(SHF1ZF, SHF1OUT);
	ReduceXOR #(8) shf1Parity(SHF1PFN,SHF1OUT[7:0]);
	inv1$ shf1pf_inv(SHF1PF, SHF1PFN);
    //parity32$ shf1pf(SHF1PF, SHF1OUT);
    assign SHF1FLAGS = {2'b00, SHF1OF, SHF1SF, SHF1ZF, 1'b0, SHF1CF, SHF1PF};

    /******************************/
    /*            SHF A           */
    /******************************/

    // Shifters
    arith_barrel_8_1        sala_08(SALAOUT08,SALACF08,SALAOF08,BBUF[ 7:0],ABUF[2:0]);
    arith_barrel_16_1       sala_16(SALAOUT16,SALACF16,SALAOF16,BBUF[15:0],ABUF[3:0]);
    arith_barrel_32_1       sala_32(SALAOUT32,SALACF32,SALAOF32,BBUF[31:0],ABUF[4:0]);
    right_arith_barrel_8_1  sara_08(SARAOUT08,SARACF08,SARAOF08,BBUF[ 7:0],ABUF[2:0]);
    right_arith_barrel_16_1 sara_16(SARAOUT16,SARACF16,SARAOF16,BBUF[15:0],ABUF[3:0]);
    right_arith_barrel_32_1 sara_32(SARAOUT32,SARACF32,SARAOF32,BBUF[31:0],ABUF[4:0]);

    // MUX shifter results
    mux8_32$ shfa_mux(SHFAOUT,
                      {24'h000000,SALAOUT08}, {16'h0000,SALAOUT16},
                      SALAOUT32,              32'h00000000,
                      {24'h000000,SARAOUT08}, {16'h0000,SARAOUT16},
                      SARAOUT32,              32'h00000000,
                      {iREG[0],OPSZFBUF});
    mux8$  shfaof_mux(SHFAOF,
                      {1'b0,SARAOF32,SARAOF16,SARAOF08,
                       1'b0,SALAOF32,SALAOF16,SALAOF08},
                      {iREG[0],OPSZFBUF});
    mux8$  shfacf_mux(SHFACF,
                      {1'b0,SARACF32,SARACF16,SARACF08,
                       1'b0,SALACF32,SALACF16,SALACF08},
                      {iREG[0],OPSZFBUF});
    mux8$  shfasf_mux(SHFASF,
                      {1'b0,SARAOUT32[31],SARAOUT16[15],SARAOUT08[ 7],
                       1'b0,SALAOUT32[31],SALAOUT16[15],SALAOUT08[ 7]},
                      {iREG[0],OPSZFBUF});
    redORN32$ shfazf(SHFAZF, SHFAOUT);
    ReduceXOR #(8) shfaParity(SHFAPFN,SHFAOUT[7:0]);
	inv1$ shfapf_inv(SHFAPF, SHFAPFN);
    //parity32$ SHFAPF(SHFAPF, SHFAOUT);
	// Don't change flags if shift amount is zero
	redORN32$ shfamountzero_red(SHFAMOUNTZERO, ABUF[31:0]);
    mux2_8$ shfaflags_mux(SHFAFLAGS,
						  {2'b00, SHFAOF, SHFASF, SHFAZF, 1'b0, SHFACF, SHFAPF},
						  {2'b00, EFGLAGSBUF[`iOF],EFGLAGSBUF[`iSF],EFGLAGSBUF[`iZF],1'b0,EFGLAGSBUF[`iCF],EFGLAGSBUF[`iPF]},
						  SHFAMOUNTZERO);

	//SHFAFLAGS = {2'b00, SHFAOF, SHFASF, SHFAZF, 1'b0, SHFACF, SHFAPF};

    // Shift flags
    mux2_8$ shfflags_mux(SHFFLAGS,
                         SHF1FLAGS, SHFAFLAGS,
                         iALUMUX[1]);

	// Final shift flags



    // Function selector
    mux16_64$ alumux(oAluOut,
                     {ADDEROUTB[31:0],ADDERSUMBITS}, {32'h0, ANDOUT},
                     {32'h0, ADDANDOUT},             NOTOUT,
                     {32'h0, SUBSUMBITS},            {32'h0, OPSZADDOUT[31:0]},
                     {32'h0, OPSZSUBOUT[31:0]},      ABUF,
                     BBUF,                           {32'h0, DIRADDEROUT[31:0]},
                     {32'h0, 32'h0},                 {56'h0, DAAOUT},
                     PUNPCKHBWOUT,                   {32'h0, SHF1OUT},
                     {32'h0, SHFAOUT},               {32'h0, 32'h0},
                     iALUMUX);

    // Flag selector
    mux16_8$ flagmux(oFlags,
                     ADDFLAGS,    ANDFLAGS,
                     ADDANDFLAGS, 8'h00,
                     SUBFLAGS,    8'h00,
                     8'h00,       8'h00,
                     8'h00,       8'h00,
                     8'h00,       DAAFLAGS,
                     8'h00,       SHFFLAGS,
                     SHFFLAGS,    8'h00,
                     iALUMUX);


endmodule

`default_nettype wire

`endif
