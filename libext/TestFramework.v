`ifndef _TEST_FRAMEWORK_V_
`define _TEST_FRAMEWORK_V_

//*************************************************************/
// Known Bugs
/**************************************************************/
/*
 * 1. When `TEST_SET() comes before `TEST_WHEN(), `TEST_WHEN()
 * prints the clocks passed before `TEST_SET() prints results.
 */

//*************************************************************/
// Settings
/**************************************************************/
// Please define this in Top.v before the `include!
`ifndef TEST_CLK_CYCLE
`define TEST_CLK_CYCLE 8
`endif

`define TEST_DELAY #(`TEST_CLK_CYCLE/2)

`define TEST_FMT_HEX 0
`define TEST_FMT_DEC 1
`define TEST_FMT_BIN 2

`define TEST_FORMAT_VARNAME _TEST_FORMAT
`define TEST_CLK_VARNAME _TEST_CLK
`define TEST_START_VARNAME _TEST_START
`define TEST_WHENCNT_VARNAME _TEST_WHENCNT_VARNAME
`define TEST_TOTAL_VARNAME _TEST_TOTAL_VARNAME
`define TEST_PASS_VARNAME _TEST_PASS_VARNAME
`define TEST_TMP_VARNAME _TEST_TMP_VARNAME
`define TEST_SET_VARNAME _TEST_SET_VARNAME

/*************************************************************/
// User Calls
/*************************************************************/
//This initializes the test suite global variables.
//Call this before your module instantiations.
`define TEST_GLOBAL_INIT \
  integer `TEST_SET_VARNAME = 0; \
  integer `TEST_TOTAL_VARNAME = 0; \
  integer `TEST_PASS_VARNAME = 0; \
  integer `TEST_WHENCNT_VARNAME = 0; \
  integer `TEST_TMP_VARNAME; \
  integer `TEST_FORMAT_VARNAME = `TEST_FMT_HEX; \
  reg `TEST_CLK_VARNAME = 1

//Hook up your sequential logic to this clock.
`define TEST_CLK `TEST_CLK_VARNAME

//This call allows you to forward the test clock value
//to an external signal if you wish to track its
//waveform values through a different alias.
//WARNING: DO NOT use this new external clk alias to clk 
//your actual modules because it will not sync properly.
`define TEST_ADD_CLK(C) \
  always@(*) \
    C <= `TEST_CLK_VARNAME

//Sets the output format.
//Call this inside initial begin statements
`define TEST_FORMAT(X) `TEST_FORMAT_VARNAME = X

//Allows the user to toggle the test clock.
`define TEST_TOGGLE_CLK \
  `TEST_DELAY `TEST_CLK_VARNAME = 0; \
  `TEST_DELAY `TEST_CLK_VARNAME = 1

//X is the signal to be monitored
//W is the width of that signal
//S is if the signal is signed (Optional, defaults true)
//Call this before your initial begin statements to
//register a test signal.
`define TEST_INIT_COMB(X, W) `TEST_INIT_COMB_SU(X, W, 1)
`define TEST_INIT_UCOMB(X, W) `TEST_INIT_COMB_SU(X, W, 0)
`define TEST_INIT_SCOMB(X, W) `TEST_INIT_COMB_SU(X, W, 1)

`define TEST_INIT_COMB_SU(X, W, S) \
  reg signed [W-S:0] `TEST_VAR(expected,X); \
  reg [4*8-1:0] `TEST_VAR(passfail,X); \
  reg [2*8-1:0] `TEST_VAR(eq,X); \
  time `TEST_VAR(start,X); \
  time `TEST_VAR(stop,X); \
  time `TEST_VAR(diff,X); \
  reg `TEST_VAR(monitor,X); \
  always@(posedge `TEST_CLK_VARNAME) begin \
    if(`TEST_VAR(monitor,X) && (`TEST_WHENCNT_VARNAME == 0)) begin \
      `TEST_TOTAL_VARNAME = `TEST_TOTAL_VARNAME + 1; \
      `TEST_VAR(monitor,X) = 0; \
      if(X === `TEST_VAR(expected,X)) begin \
        `TEST_PASS_VARNAME = `TEST_PASS_VARNAME + 1; \
        `TEST_VAR(passfail,X) = {"PASS"}; \
        `TEST_VAR(eq,X) = {"=="}; \
      end else begin \
        `TEST_VAR(passfail,X) = {"FAIL"}; \
        `TEST_VAR(eq,X) = {"!="}; \
      end \
      case(`TEST_FORMAT_VARNAME) \
        `TEST_FMT_HEX: `TEST_PRINT_CRESULT(X,"0x%x") \
        `TEST_FMT_DEC: `TEST_PRINT_CRESULT(X,"%0d") \
        `TEST_FMT_BIN: `TEST_PRINT_CRESULT(X,"b%b") \
        default:       `TEST_PRINT_CRESULT(X,"0x%x") \
      endcase \
    end \
  end \
  always@(X) \
    `TEST_VAR(stop,X) <= $time

//X is the signal to be monitored
//W is the width of that signal
//S is if the signal is signed (Optional, defaults true)
//Call this before your initial begin statements to
//register a test signal.
`define TEST_INIT_SEQ(X, W) `TEST_INIT_SEQ_SU(X, W, 1)
`define TEST_INIT_USEQ(X, W) `TEST_INIT_SEQ_SU(X, W, 0)
`define TEST_INIT_SSEQ(X, W) `TEST_INIT_SEQ_SU(X, W, 1)

`define TEST_INIT_SEQ_SU(X, W, S) \
  reg signed [W-S:0] `TEST_VAR(expected,X); \
  reg [4*8-1:0] `TEST_VAR(passfail,X); \
  reg [2*8-1:0] `TEST_VAR(eq,X); \
  reg `TEST_VAR(monitor,X); \
  always@(posedge `TEST_CLK_VARNAME) begin \
    if(`TEST_VAR(monitor,X) && (`TEST_WHENCNT_VARNAME == 0)) begin \
      `TEST_TOTAL_VARNAME = `TEST_TOTAL_VARNAME + 1; \
      `TEST_VAR(monitor,X) <= 0; \
      #0.2; \
      if(X === `TEST_VAR(expected,X)) begin \
        `TEST_PASS_VARNAME = `TEST_PASS_VARNAME + 1; \
        `TEST_VAR(passfail,X) = {"PASS"}; \
        `TEST_VAR(eq,X) = {"=="}; \
      end else begin \
        `TEST_VAR(passfail,X) = {"FAIL"}; \
        `TEST_VAR(eq,X) = {"!="}; \
      end \
      case(`TEST_FORMAT_VARNAME) \
        `TEST_FMT_HEX: `TEST_PRINT_SRESULT(X,"0x%x") \
        `TEST_FMT_DEC: `TEST_PRINT_SRESULT(X,"%0d") \
        `TEST_FMT_BIN: `TEST_PRINT_SRESULT(X,"b%b") \
        default:       `TEST_PRINT_SRESULT(X,"0x%x") \
      endcase \
    end \
  end \
  time `TEST_VAR(start,X)

//X is the signal to be evaluated
//A is the expected answer.
//After changing inputs, this will test for the correct
//output values.  Call this within initial begin statements.
`define TEST_EQ(X, A) \
  `TEST_VAR(start,X) <= $time; \
  `TEST_VAR(monitor,X) <= #0.4 1; \
  `TEST_VAR(expected,X) <= #0.4 A

//Use this to group input tests at certain times
`define TEST_SET(X) \
  X; \
  `TEST_SET_VARNAME = `TEST_SET_VARNAME + 1; \
 `TEST_TOGGLE_CLK; \
  $display("Test Set: %0d", `TEST_SET_VARNAME); \
  #0.4;

//Use this to test when a predicate is true
`define TEST_WHEN(P,X) \
  X \
  `TEST_WHENCNT_VARNAME = 1; \
  `TEST_TOGGLE_CLK; \
  while((P) == 0) begin \
    `TEST_TOGGLE_CLK; \
    `TEST_WHENCNT_VARNAME = `TEST_WHENCNT_VARNAME + 1; \
  end \
  `TEST_TMP_VARNAME = `TEST_WHENCNT_VARNAME; \
  `TEST_WHENCNT_VARNAME = 0; \
  $display("%0d cycles have passed. (t = %0d)", `TEST_TMP_VARNAME, $time); \
  #0.4;
  
//Use this to print so that your display messages
//will go in chronological order with the tests.
//The delay is to allow sequential signal monitoring
//messages to print before this new message.
`define TEST_DISPLAY(X) #0.4 $display(X)
`define TEST_DISPLAYF(X) \
  #0.4 $display("%s",$sformatf X)

//Call this at the end of your tests
`define TEST_END \
  $display("\n%0d of %0d tests passed.", `TEST_PASS_VARNAME, `TEST_TOTAL_VARNAME); \
  `TEST_SET_VARNAME = 0; \
  `TEST_PASS_VARNAME = 0; \
  `TEST_TOTAL_VARNAME = 0

/*************************************************************/
// Implementation Calls
/*************************************************************/
`define TEST_VAR(N, T) _TEST_``N``_``T

`define TEST_TOSTR(X) `"X`"

`define TEST_PRINT_SRESULT(X,F) \
  begin \
    $write("%s (", `TEST_VAR(passfail,X)); \
    $write(`TEST_TOSTR(X)); \
    $write(")\t: "); \
    $write(F,X); \
    $write(" %s ", `TEST_VAR(eq,X)); \
    $display(F, `TEST_VAR(expected,X)); \
  end

`define TEST_PRINT_CRESULT(X,F) \
  begin \
    $write("%s (", `TEST_VAR(passfail,X)); \
    $write(`TEST_TOSTR(X)); \
    $write(")\t: "); \
    $write(F,X); \
    $write(" %s ", `TEST_VAR(eq,X)); \
    $write(F, `TEST_VAR(expected,X)); \
    `TEST_VAR(diff,X) = `TEST_VAR(stop,X) - `TEST_VAR(start,X); \
    `TEST_VAR(diff,X) = (`TEST_VAR(diff,X)>{4'b0,-60'd1}) ? 0 : `TEST_VAR(diff,X); \
    $display("\t delay = %0d", `TEST_VAR(diff,X)); \
  end

`endif
