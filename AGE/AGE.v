`ifndef _AGE_VH
`define _AGE_VH

`timescale 1ns/1ps

`include "defines.v"
`include "AGE/AGE_EXC.v"
`include "CommonLogic/PipeRegs.v"

`default_nettype none

// Register file stage
module AGE(iClk,   iNSysRst,
           inFlush, inStall,
           oFlush, oStall,

           oStat,  oControlStore,
           oRegW,  oSegW, oDstR,
           oGPR,   oRM,
           oREG,   oRdSz,
           oAddr,  oUA,   oAddrP16,
           oMemR,  oMemW,
           oAltData,
           oCS,    oEIP,  oEIPPLUS,

           iStat,  iControlStore,
           iRegW,  iSegW, iDstR,
           iREG,
           iGPR,   iRM,
           iIndex, iBase, iDisp, iSeg, iSegN, iScale,
           iMemR,  iMemW,
           iAltData,
           iCS,    iEIP,  iEIPPLUS);

    // I/O
    // System controls
    input           iClk;
    input           iNSysRst;
    input           inFlush;
    input           inStall;
    output          oFlush;
    output          oStall;

    // RF -> AGE
    input   [31:0]  iStat;
    input   [68:0]  iControlStore;
    input           iRegW;
    input           iSegW;
    input   [ 2:0]  iDstR;
    input   [ 2:0]  iREG;
    input   [63:0]  iGPR;
    input   [63:0]  iRM;
    input   [31:0]  iIndex;
    input   [31:0]  iBase;
    input   [31:0]  iDisp;
    input   [15:0]  iSeg;
    input   [31:0]  iSegN;
    input   [ 1:0]  iScale;
    input           iMemR;
    input           iMemW;
    input   [63:0]  iAltData;
    input   [15:0]  iCS;
    input   [31:0]  iEIP;
    input   [31:0]  iEIPPLUS;

    // AGE -> MEM
    output  [31:0]  oStat;
    output  [68:0]  oControlStore;
    output          oRegW;
    output          oSegW;
    output  [ 2:0]  oDstR;
    output  [63:0]  oGPR;
    output  [63:0]  oRM;
    output  [ 2:0]  oREG;
    output  [ 1:0]  oRdSz;
    output  [31:0]  oAddr;
    output          oUA;
    output  [31:0]  oAddrP16;
    output          oMemR;
    output          oMemW;
    output  [63:0]  oAltData;
    output  [15:0]  oCS;
    output  [31:0]  oEIP;
    output  [31:0]  oEIPPLUS;

    // Pipeline stage inputs
    wire    [31:0]  wStat;
    wire    [68:0]  wControlStore;
    wire            wRegW;
    wire            wSegW;
    wire    [ 2:0]  wDstR;
    wire    [63:0]  wGPR;
    wire    [63:0]  wRM;
    wire    [ 2:0]  wREG;
    wire    [ 1:0]  wRdSz;
    wire    [31:0]  wAddr;
    wire            wUA;
    wire    [31:0]  wAddrP16;
    wire            wMemR;
    wire            wMemW;
    wire    [63:0]  wAltData;
    wire    [15:0]  wCS;
    wire    [31:0]  wEIP;
    wire    [31:0]  wEIPPLUS;

    // Split control-store signals
    wire            OPSZOVRPRE; // Operand size override prefix presentw
    wire            TRAPOP;     // TRAP instruction
    wire    [ 1:0]  OPSZ;       // Default operand size
    wire    [ 1:0]  OPSZOVR;    // Overridden operand size
    wire            OPSZOVRS;   // Forced, special operand size override
    wire    [ 1:0]  OPSZS;      // Special operand size

    // Split stat
    wire            EXC;
    wire            EXCBAR;
    wire            INT;
    wire            INTBAR;
    wire    [7:0]   EXCV;
    wire            HEXC;
    wire    [7:0]   HEXCV;
    wire            NANDEXCINT;

    // Operand Sizes
    wire    [ 1:0]  OPSZA;
    wire    [ 1:0]  OPSZB;

    // Forwarding logic
    wire    [63:0]  FGPR;
    wire    [63:0]  FRM;
    wire    [31:0]  FINDEX;
    wire    [31:0]  FBASE;
    wire    [15:0]  FSEG;
    wire    [63:0]  FALT;

    // Scaled index
    wire    [31:0]  SINDEX;

    // Buffered displacement
    wire    [31:0]  DISPBUF;

    //////////////////
    //  ADDER LVL0  //
    //////////////////

    // Index + Base adder logic
    wire            INDEXBASECOUT;
    wire    [31:0]  INDEXPLUSBASE;
    wire    [31:0]  INDEXPLUSEBASEBUF;

    // Disp + SegR<<16 adder logic
    wire            DISPSEGCOUT;
    wire    [31:0]  DISPPLUSSEG;

    // Disp + SegR<<16 + 16 adder logic
    wire            DISPSEG16COUT;
    wire    [31:0]  DISPPLUSSEG16;

    // Segment limit adder logic
    wire            SEGLDISPCOUT;
    wire    [31:0]  SEGLPLUSDISP;

    // UA Adder 0 logic
    wire    [ 2:0]  OPSZMUXOUT;
    wire            UA0ADDERCOUT;
    wire    [31:0]  UA0ADDEROUT;

    // Exception vector adder logic
    wire            EXCADDRCOUT;
    wire    [31:0]  EXCADDR;

    // Exception vector + 8 adder logic
    wire            EXCADDRP16COUT;
    wire    [31:0]  EXCADDRP16;

    // CS exceotuib vector adder logic
    wire            CSEXCADDRCOUT;
    wire    [31:0]  CSEXCADDR;

    //////////////////
    //  ADDER LVL1  //
    //////////////////

    // Address adder logic
    wire            ADDRADDERCOUT;
    wire    [31:0]  ADDRADDEROUT;

    // Address + 16 adder logic
    wire            ADDRADDER16COUT;
    wire    [31:0]  ADDRADDER16OUT;

    // UA Adder 1 logic
    wire            UA1ADDERCOUT;
    wire    [31:0]  UA1ADDEROUT;
	wire			STDUA;
   
    // Offset Adder logic
    wire            OFFSETADDERCOUT;
    wire    [31:0]  OFFSETADDEROUT;

    // Segment overshoot adder logic
    wire            SEGLADDERCOUT;
    wire    [31:0]  SEGLADDEROUT;

    ///////////////
    // EXC LOGIC //
    ///////////////
    wire            MEMRBAR;
    wire            MEMWBAR;
    wire            REGWBAR;
    wire            SEGWBAR;

    // Split Control-Store
    assign OPSZOVRPRE = iControlStore[`iOPSZOVRPRE];
    assign TRAPOP     = iControlStore[`iTRAPOP];
    assign OPSZ       = iControlStore[`iOPSZ];
    assign OPSZOVR    = iControlStore[`iOPSZOVR];
    assign OPSZOVRS   = iControlStore[`iOPSZOVRS];
    assign OPSZS      = iControlStore[`iOPSZS];

    // Split stat
    assign EXC   = iStat[`iEXC];
    assign INT   = iStat[`iINT];
    assign EXCV  = iStat[`iEXCV];
    assign HEXCV = iStat[`iHEXCV];
    inv1$  exc_inv(EXCBAR, EXC);
    inv1$  int_inv(INTBAR, INT);
    nand2$ excint_nand(NANDEXCINT, EXC, INT);

    // Operand sizes
    mux2_2$ opsza_mux(OPSZA, OPSZ,  OPSZOVR, OPSZOVRPRE);
    mux2_2$ opszb_mux(OPSZB, OPSZA, OPSZS,   OPSZOVRS);

    // Forwarding logic
    assign FGPR        = iGPR;
    assign FRM         = iRM;
    assign FINDEX      = iIndex;
    assign FBASE       = iBase;
    assign FSEG        = iSeg;
    assign FALT[63:16] = iAltData[63:16];
    assign FALT[15: 0] = iAltData[15: 0];

    //////////////////
    //  ADDER LVL0  //
    //////////////////

    // Index Barrel Shifter
    mux4_32$ barrelShifter(SINDEX,
                           FINDEX,              {FINDEX[30:0],1'b0},
                           {FINDEX[29:0],2'b0}, {FINDEX[28:0],3'b0},
                           iScale);

    // Displacement buffer
    bufferH16$ displacem_buf [31:0] (DISPBUF, iDisp);

    // Index + Base Adder
    KSAdder32  indexbase_add(INDEXPLUSBASE, INDEXBASECOUT, SINDEX, FBASE, 1'b0);
    bufferH16$ indexbase_buf [31:0] (INDEXPLUSEBASEBUF, INDEXPLUSBASE);

    // Disp + SegR<<16 Adder
    KSAdder32    dispseg_add(DISPPLUSSEG, DISPSEGCOUT,   DISPBUF, {FSEG, 16'h0000}, 1'b0);

    // Disp + SegR<<16 + 16 Adder
    KSAdder32  dispseg16_add(DISPPLUSSEG16, DISPSEG16COUT, DISPBUF, {FSEG, 16'h0010}, 1'b0);

    // Disp - SegL Adder
    KSAdder32  dispnseg_add(SEGLPLUSDISP, SEGLDISPCOUT, DISPBUF, iSegN, 1'b0);

    // UA Adder 0
    mux4_3$    opsz_mux(OPSZMUXOUT,
                        3'b000, 3'b001, 3'b011, 3'b111,
                        OPSZB);
    KSAdder32  ua0_adder(UA0ADDEROUT, UA0ADDERCOUT, DISPBUF, {29'h0,OPSZMUXOUT}, 1'b0);

    // Exception vector adder
    KSAdder32    exc_adder(EXCADDR,    EXCADDRCOUT,    32'h02000000, {24'h0,HEXCV}, 1'b0);

    // Exception vector + 16 adder
    KSAdder32  exc16_adder(EXCADDRP16, EXCADDRP16COUT, 32'h02000010, {24'h0,HEXCV}, 1'b0);

    // CS exception vector adder
    KSAdder32  csexc_adder(CSEXCADDR, CSEXCADDRCOUT, 32'hFFFFB00F, iEIP, 1'b0);

    //////////////////
    //  ADDER LVL1  //
    //////////////////

    // Address adder
    KSAdder32  addr_adder(ADDRADDEROUT, ADDRADDERCOUT, INDEXPLUSEBASEBUF, DISPPLUSSEG, 1'b0);

    // Address + 16 adder
    KSAdder32  addr16_adder(ADDRADDER16OUT, ADDRADDER16COUT, INDEXPLUSEBASEBUF, DISPPLUSSEG16, 1'b0);

    // UA Adder 1
    KSAdder32  ua1_adder(UA1ADDEROUT, UA1ADDERCOUT, INDEXPLUSEBASEBUF, UA0ADDEROUT, 1'b0);
	xor2$ ua_xor(STDUA, UA1ADDEROUT[$clog2(`DCACHE_BLOCK_SIZE)], ADDRADDEROUT[$clog2(`DCACHE_BLOCK_SIZE)]);

    // Offset adder
    KSAdder32  off_adder(OFFSETADDEROUT, OFFSETADDERCOUT, INDEXPLUSEBASEBUF, DISPBUF, 1'b0);

    // Segment overshoot adder
    KSAdder32  ovrshoot_adder(SEGLADDEROUT, SEGLADDERCOUT, INDEXPLUSEBASEBUF, SEGLPLUSDISP, 1'b0);



    /////////////////
    //  EXC LOGIC  //
    /////////////////
    AGE_EXC exc_unit(wStat, 
                     FSEG,  OFFSETADDEROUT, SEGLADDEROUT,
                     iCS,   iEIP,           CSEXCADDR,
                     iStat, TRAPOP, iMemR,  iMemW);

    inv1$ memr_inv(MEMRBAR, iMemR);
    inv1$ memw_inv(MEMWBAR, iMemW);
    inv1$ regw_inv(REGWBAR, iRegW);
    inv1$ segw_inv(SEGWBAR, iSegW);

    //////////////////////
    //  PIPELINE LOGIC  //
    //////////////////////
    assign wControlStore = iControlStore;
    assign wDstR         = iDstR;
    assign wGPR          = FGPR;
    assign wRM           = FRM;
    assign wREG          = iREG;
    assign wRdSz         = OPSZB;
    mux2_32$   addr_mux(wAddr,    ADDRADDEROUT,   EXCADDR, TRAPOP);
    mux2$    ua_mux(wUA, STDUA, 1'b0, TRAPOP);
    mux2_32$ addr16_mux(wAddrP16, ADDRADDER16OUT, EXCADDRP16, TRAPOP);
    nor2$ memr_nor(wMemR, wStat[`iEXC], MEMRBAR);
    nor2$ memw_nor(wMemW, wStat[`iEXC], MEMWBAR);
    nor2$ regw_nor(wRegW, wStat[`iEXC], REGWBAR);
    nor2$ segw_nor(wSegW, wStat[`iEXC], SEGWBAR);
    assign wAltData      = FALT;
    assign wCS           = iCS;
    assign wEIP          = iEIP;
    assign wEIPPLUS      = iEIPPLUS;

    assign oFlush = 0;
    assign oStall = 0;

    // Pipeline register
    PipeRegs #(450) age2mem({oStat,oControlStore,oRegW,oSegW,oDstR,
                             oGPR,oRM,oREG,oRdSz,
                             oAddr,oUA,oAddrP16,oMemR,oMemW,
                             oAltData,oCS,oEIP,oEIPPLUS},

                            {wStat,wControlStore,wRegW,wSegW,wDstR,
                             wGPR,wRM,wREG,wRdSz,
                             wAddr,wUA,wAddrP16,wMemR,wMemW,
                             wAltData,wCS,wEIP,wEIPPLUS},

                             inStall, inFlush,
                             iClk,   iNSysRst);

endmodule

`default_nettype wire

`endif
