`ifndef _FETCH_V_
`define _FETCH_V_

`include "Parameters.v"
`include "Cache/Cache.v"
`include "CommonLogic/CommonLogic.v"

`default_nettype none

module Fetch (oEIP, oBrTaken, oStat, oData, oValid, 
              oCS, ioBus,
              iCSR_EIP, iRF_CS,
	            iWB_BrTaken, iWB_EIP,
              inStall, inFlush, iClk, inReset);

output [31:0]   oEIP;
output [31:0]   oStat;
output [255:0]  oData;
output          oValid;
output [15:0]   oCS;
output          oBrTaken;

input [31:0]    iCSR_EIP;
input [15:0]    iRF_CS;
input           iWB_BrTaken;
input [31:0]    iWB_EIP;

input           inStall;
input           inFlush;
input           iClk;
input           inReset;

inout [`BUS_MSB:0] ioBus;

//FSM Signals
wire FSM_AddrSel;
wire FSM_nBrRegRst;
wire FSM_Valid;

//Latch branching signals
wire [31:0] brEIPraw;
PipeRegs #(32) brt_reg
  (brEIPraw, iWB_EIP, iWB_BrTaken, 1'b1, iClk, inReset);

wire rbrTaken, brTaken;
PipeRegs br_reg
  (rbrTaken, 1'b1, iWB_BrTaken, FSM_nBrRegRst, iClk, inReset);
and2$ and_brTaken (brTaken, rbrTaken, FSM_nBrRegRst);
  

//Address Calculation
wire [31:0] nxEIP, nxEIP16;
wire [31:0] brEIP, brEIP16;


KSAdder32NC add_csrAddr
  (nxEIP, {iRF_CS, 16'b0000}, iCSR_EIP);
KSAdder32NC add_csrAddr16
  (nxEIP16, {iRF_CS, 16'h0010}, iCSR_EIP);
KSAdder32NC add_brAddr
  (brEIP, {iRF_CS, 16'b0000}, brEIPraw);
KSAdder32NC add_brAddr16
  (brEIP16, {iRF_CS, 16'h0010}, brEIPraw);

//Select Address
wire loBrTaken;

wire [31:0] cAddr,cAddr32noCS, cAddr16;
mux2$ eip_mux_noCS [31:0] 
  (cAddr32noCS, iCSR_EIP, brEIPraw, FSM_AddrSel);
mux2$ eip_mux [31:0] 
  (cAddr, nxEIP, brEIP, FSM_AddrSel);
mux2$ eip_mux16 [31:0] 
  (cAddr16, nxEIP16, brEIP16, FSM_AddrSel);

//ICache
wire [255:0] cDataReverse, cData;
wire cMiss, cHit;
wire cExceptPF;
ICache #(`ICACHE_MASTER_ID, `ICACHE_SIZE, `ICACHE_BLOCK_SIZE) icache
  (cDataReverse, cExceptPF, cMiss, cHit,
   cAddr, cAddr16, 1'b1, 1'b1,
   ioBus, iClk, inReset);

ByteReverse #(32) reverse
  (cData, cDataReverse);

//Stat Check
wire [31:0] rStat;
and2$ and_stat [31:0] (rStat, 32'h00027000, cExceptPF);

//FSM
localparam stateSize = 6;
wire [stateSize-1:0] FSM_state;
wire [stateSize-1:0] FSM_nstate;
wire [3:0] FSM_input;

wire brTakenNow;
or2$ or_brTakenNow (brTakenNow, brTaken, iWB_BrTaken);
assign FSM_input = {brTakenNow, cHit, FSM_state[1:0]};

//FSM lookup
wire [25:0] fsm_garbage;
rom32b32w$ fsm_lookup ({1'b0, FSM_input}, 1'b1, {fsm_garbage, FSM_nstate});
initial begin
  //IDLE
  fsm_lookup.mem[4'b01_00] = 32'b0110_00;
  
  fsm_lookup.mem[4'b00_00] = 32'b0010_11;
  fsm_lookup.mem[4'b10_00] = 32'b0010_11;

  fsm_lookup.mem[4'b11_00] = 32'b0011_01;

  //BR
  fsm_lookup.mem[4'b00_01] = 32'b0011_01;
  fsm_lookup.mem[4'b10_01] = 32'b0011_01;

  fsm_lookup.mem[4'b01_01] = 32'b1111_10;
  fsm_lookup.mem[4'b11_01] = 32'b1111_10;

  //CD
  fsm_lookup.mem[4'b01_10] = 32'b0100_00;
  fsm_lookup.mem[4'b00_10] = 32'b0100_00;
  fsm_lookup.mem[4'b11_10] = 32'b0100_00;
  fsm_lookup.mem[4'b10_10] = 32'b0100_00;

  //MISS
  fsm_lookup.mem[4'b00_11] = 32'b0010_11;
  fsm_lookup.mem[4'b10_11] = 32'b0010_11;

  fsm_lookup.mem[4'b01_11] = 32'b0110_00;

  fsm_lookup.mem[4'b11_11] = 32'b0011_01;
end

/*
always@(*) begin
  casex(FSM_input)
  // 'bBH_s                     'bBVHA_s
    4'b01_00: FSM_nstate <= #2 6'b0110_00; //IDLE -> IDLE
    4'bx0_00: FSM_nstate <= #2 6'b0010_11; //IDLE -> MISS
    4'b11_00: FSM_nstate <= #2 6'b0011_01; //IDLE -> BR

    4'bx0_01: FSM_nstate <= #2 6'b0011_01; //BR -> BR
    4'bx1_01: FSM_nstate <= #2 6'b1111_10; //BR -> CD
    
    4'bxx_10: FSM_nstate <= #2 6'b0101_00; //CD -> IDLE

    4'bx0_11: FSM_nstate <= #2 6'b0010_11; //MISS -> MISS
    4'b01_11: FSM_nstate <= #2 6'b0110_00; //MISS -> IDLE
    4'b11_11: FSM_nstate <= #2 6'b0011_01; //MISS -> BR
  endcase
end
*/

//State Latching
regs$ #(stateSize) state_regs 
  (FSM_state, FSM_nstate, 1'b1, iClk, inReset);
assign FSM_AddrSel = FSM_state[2];
assign FSM_nBrRegRst = FSM_state[3];

wire nWB_BrTaken;
not1$ not_BrTaken (nWB_BrTaken, iWB_BrTaken);
and3$ and_valid(FSM_Valid, FSM_nstate[4], cHit, nWB_BrTaken);

wire brValid;
assign brValid = FSM_nstate[5];
//and2$ and_brValid (brValid, brTaken, FSM_Valid);

//Pipeline Stage
wire [255:0] rData;
mux2$ mux_Data [255:0] (rData, 1'b0, cData, FSM_Valid);

/*
PipeRegs #(338) stage_regs
  ({oEIP, oBrTaken, oStat, oData, oValid, oCS},
   {cAddr, brValid, rStat, rData, FSM_Valid, iRF_CS},
    1'b1, iWB_BrTaken, iClk, inReset);
*/

//Stall, Flush
regs$ #(338, 0, 2) stage_regs
  ({oEIP, oBrTaken, oStat, oData, oValid, oCS},
   {cAddr32noCS, brValid, rStat, rData, FSM_Valid, iRF_CS},
    1'b1, iClk, inReset);

endmodule

`default_nettype wire
`endif
