`timescale 1ns/1ps

`define CYCLE_TIME (100)
`define HALF_CYCLE_TIME (`CYCLE_TIME/2)

// Register file
module regfilex86(IN, WR, WE,

                  RD0, OUT0,
                  RD1, OUT1,
                  RD2, OUT2,
                  RD3, OUT3,

                  INS,  SWR, SWE,
                  SRD0, SGO0,
                  SRD1, SGO1,

                  CSO,

                  MODER, MODEB, MODEW,
                  CLK, CLR, PRE);

    // GPR Write ports
    input   [63:0]  IN;     // Data-In 0
    input   [ 2:0]  WR;     // Write register 0
    input           WE;     // Write-enable 0

    // GPR Read ports
    input   [ 2:0]  RD0;    // Read register 0
    output  [63:0]  OUT0;   // Data-Out 0
    input   [ 2:0]  RD1;    // Read register 1
    output  [63:0]  OUT1;   // Data-Out 1
    input   [ 2:0]  RD2;    // Read register 2
    output  [31:0]  OUT2;   // Data-Out 2
    input   [ 2:0]  RD3;    // Read register 3
    output  [31:0]  OUT3;   // Data-Out 3

    // SegR Write Ports
    input   [15:0]  INS;    // Segment Data-In
    input   [ 2:0]  SWR;    // Segment write register
    input           SWE;    // Segment write-enable

    // SegR Read Ports
    input   [ 2:0]  SRD0;   // Segment read register 0
    output  [15:0]  SGO0;   // Segment data-out 0
    input   [ 2:0]  SRD1;   // Segment read register 1
    output  [15:0]  SGO1;   // Segment data-out 1

    // Special output
    output  [15:0]  CSO;    // Code segment output

    // Controls
    input   [ 1:0]  MODER;  // GPR Read Mode:  GPR8/GPR16/GPR32/MMX
    input           MODEB;  // BASE Read Mode: GPR8/GPR32
    input   [ 1:0]  MODEW;  // GPR Write Mode: GPR8/GPR16/GPR32/MMX
    input           CLK;    // System clock
    input           CLR;    // System reset
    input           PRE;    // System preset

    // Register enable signals
    wire    [ 7:0]  ENGPR;      // GPR enable
    wire    [ 7:0]  ENMMX;      // MMX enable
    wire    [ 7:0]  ENSGR;      // Segment enable

    // GPR and MMX register inputs
    wire    [31:0]  GPRIN[0:7];
    wire    [63:0]  MMXIN[0:7];

    // Register outputs
    wire    [31:0]  GPR   [0:7];
    wire    [31:0]  GPRBAR[0:7];
    wire    [63:0]  MMX   [0:7];
    wire    [63:0]  MMXBAR[0:7];
    wire    [31:0]  SEG   [0:5];
    wire    [31:0]  SEGBAR[0:5];

    // Selected register read outputs
    wire    [63:0]  R64_SELECTED_RD0;   // Chosen R64 for read port 0
    wire    [31:0]  R32_SELECTED_RD0;   // Chosen R32 for read port 0
    wire    [15:0]  R16_SELECTED_RD0;   // Chosen R16 for read port 0
    wire    [ 7:0]  R8_SELECTED_RD0;    // Chosen R8 for read port 0

    wire    [63:0]  R64_SELECTED_RD1;   // Chosen R64 for read port 1
    wire    [31:0]  R32_SELECTED_RD1;   // Chosen R32 for read port 1
    wire    [15:0]  R16_SELECTED_RD1;   // Chosen R16 for read port 1
    wire    [ 7:0]  R8_SELECTED_RD1;    // Chosen R8 for read port 1

    wire    [31:0]  R32_SELECTED_RD2;   // Chosen R32 for read port 2

    wire    [31:0]  R32_SELECTED_RD3;   // Chosen R32 for read port 3
    wire    [ 7:0]  R8_SELECTED_RD3;    // Chosen R8 for read port 3

    // Potential register write inputs
    wire    [63:0]  R64_WR;             // R64 write data 0
    wire    [31:0]  R32_WR;             // R32 write data 0
    wire    [31:0]  R16_WR[0:7];        // R16 write data 0
    wire    [31:0]  R8L_WR[0:7];        // R8 low write data
    wire    [31:0]  R8H_WR[0:7];        // R8 high write data

    // Write-enable logic
    wire            MODE_8BAR;          // Not writing to 8b registers?
    wire    [ 2:0]  WRMASK;             // Write destination masked
    wire    [ 7:0]  WR_DECODED;         // WRMASk decoded
    wire    [ 7:0]  WR_DECODEDBAR;      // WRMASk decoded and inverted
    wire            GPR_MODE;           // GPR mode?
    wire            MMX_MODE;           // MMX mode?
    wire    [ 7:0]  REG_EN;             // Register enable
    wire    [31:0]  R8_WR[0:7];    
    wire    [ 7:0]  SRW_DECODED;        // SRW decoded 
    wire    [ 7:0]  SRW_DECODEDBAR;     // SRW decoded and inverted

    // Generate variable
    genvar          i;

    // Generate GPRs and MMX registers
    generate
        for(i = 0; i < 8; i = i + 1) begin : reg_gen
            reg32e$ gpr_reg(CLK, GPRIN[i], GPR[i], GPRBAR[i], CLR, PRE, ENGPR[i]);
            reg64e$ mmx_reg(CLK, MMXIN[i], MMX[i], MMXBAR[i], CLR, PRE, ENMMX[i]);
        end
    endgenerate

    // Generate segment registers
    generate
        for(i = 0; i < 6; i = i + 1) begin : seg_gen
            reg32e$ seg_reg(CLK, {16'h0000, INS}, SEG[i], SEGBAR[i], CLR, PRE, ENSGR[i]);
        end
    endgenerate


    // Select register outputs
    // Read port 0
    mux8_64$ muxR64_RD0(R64_SELECTED_RD0, 
                        MMX[0], MMX[1], MMX[2], MMX[3],
                        MMX[4], MMX[5], MMX[6], MMX[7],
                        RD0);
    mux8_32$ muxR32_RD0(R32_SELECTED_RD0, 
                        GPR[0], GPR[1], GPR[2], GPR[3],
                        GPR[4], GPR[5], GPR[6], GPR[7],
                        RD0);
    mux8_16$ muxR16_RD0(R16_SELECTED_RD0, 
                        GPR[0][15:0], GPR[1][15:0], GPR[2][15:0], GPR[3][15:0],
                        GPR[4][15:0], GPR[5][15:0], GPR[6][15:0], GPR[7][15:0],
                        RD0);
    mux8_8$  muxR08_RD0(R8_SELECTED_RD0, 
                        GPR[0][ 7:0], GPR[1][ 7:0], GPR[2][ 7:0], GPR[3][ 7:0],
                        GPR[0][15:8], GPR[1][15:8], GPR[2][15:8], GPR[3][15:8],
                        RD0);
    // Read port 1
    mux8_64$ muxR64_RD1(R64_SELECTED_RD1, 
                        MMX[0], MMX[1], MMX[2], MMX[3],
                        MMX[4], MMX[2], MMX[6], MMX[7],
                        RD1);
    mux8_32$ muxR32_RD1(R32_SELECTED_RD1, 
                        GPR[0], GPR[1], GPR[2], GPR[3],
                        GPR[4], GPR[5], GPR[6], GPR[7],
                        RD1);
    mux8_16$ muxR16_RD1(R16_SELECTED_RD1, 
                        GPR[0][15:0], GPR[1][15:0], GPR[2][15:0], GPR[3][15:0],
                        GPR[4][15:0], GPR[5][15:0], GPR[6][15:0], GPR[7][15:0],
                        RD1);
    mux8_8$  muxR08_RD1(R8_SELECTED_RD1, 
                        GPR[0][ 7:0], GPR[1][ 7:0], GPR[2][ 7:0], GPR[3][ 7:0],
                        GPR[0][15:8], GPR[1][15:8], GPR[2][15:8], GPR[3][15:8],
                        RD1);
    // Read port 2
    mux8_32$ muxR32_RD2(R32_SELECTED_RD2, 
                        GPR[0], GPR[1], GPR[2], GPR[3],
                        GPR[4], GPR[5], GPR[6], GPR[7],
                        RD2);
    // Read port 3
    mux8_32$ muxR32_RD3(R32_SELECTED_RD3, 
                        GPR[0], GPR[1], GPR[2], GPR[3],
                        GPR[4], GPR[5], GPR[6], GPR[7],
                        RD3);
    mux8_8$  muxR08_RD3(R8_SELECTED_RD3, 
                        GPR[0][ 7:0], GPR[1][ 7:0], GPR[2][ 7:0], GPR[3][ 7:0],
                        GPR[0][15:8], GPR[1][15:8], GPR[2][15:8], GPR[3][15:8],
                        RD3);

    // Output to read ports based on mode
    mux4_64$ mux_RD0(OUT0,
                     {56'd0, R8_SELECTED_RD0},
                     {48'd0, R16_SELECTED_RD0},
                     {32'd0, R32_SELECTED_RD0},
                     R64_SELECTED_RD0,
                     MODER);
    mux4_64$ mux_RD1(OUT1,
                     {56'd0, R8_SELECTED_RD1},
                     {48'd0, R16_SELECTED_RD1},
                     {32'd0, R32_SELECTED_RD1},
                     R64_SELECTED_RD1,
                     MODER);
    assign OUT2 = R32_SELECTED_RD2;
    mux2_32$ mux_RD3(OUT3,
                     {24'd0, R8_SELECTED_RD3},
                     R32_SELECTED_RD3,
                     MODEB);

    // Segment read port 0
    mux8_16$ muxSeg_Reg0(SGO0,
                         SEG[0][15:0], SEG[1][15:0], SEG[2][15:0], SEG[3][15:0],
                         SEG[4][15:0], SEG[5][15:0], 16'hZZZZ,     16'hZZZZ,
                         SRD0);
    // Segment read port 1
    mux8_16$ muxSeg_Reg1(SGO1,
                         SEG[0][15:0], SEG[1][15:0], SEG[2][15:0], SEG[3][15:0],
                         SEG[4][15:0], SEG[5][15:0], 16'hZZZZ,     16'hZZZZ,
                         SRD1);

    // Potential input data
    assign R64_WR = IN;
    assign R32_WR = IN[31:0];
    generate
        for(i = 0; i < 8; i = i + 1) begin: potin_gen0
            assign R16_WR[i] = {GPR[i][31:16], IN[15: 0]};
            assign R8L_WR[i] = {GPR[i][31: 8], IN[ 7: 0]};
            assign R8H_WR[i] = {GPR[i][31:16], IN[ 7: 0], GPR[i][7:0]};
        end
    endgenerate

    
    // Write-enable logic
    // GPR
    or2$        orMODE_8BAR(MODE_8BAR, MODEW[0], MODEW[1]);
    and2$       andwrmask(WRMASK[2], MODE_8BAR, WR[2]);
    assign      WRMASK[1:0] = WR[1:0];
    decoder3_8$ decWR(WRMASK, WR_DECODED, WR_DECODEDBAR);
    and2_8$     and_regen(REG_EN, WR_DECODED, {8{WE}});
    nand2$      nand_gprmode(GPR_MODE, MODEW[0], MODEW[1]);
    and2$       and_mmxmode(MMX_MODE, MODEW[0], MODEW[1]);
    and2_8$     and_gpr_en(ENGPR, REG_EN, {8{GPR_MODE}});
    and2_8$     and_mmx_en(ENMMX, REG_EN, {8{MMX_MODE}});
    // SGR
    decoder3_8$ decSegWR(SWR, SRW_DECODED, SRW_DECODEDBAR);
    and2_8$     andSegWR(ENSGR, SRW_DECODED, {8{SWE}});

    // Generate register inputs
    generate 
        for(i = 0; i < 8; i = i + 1) begin: regin_gen
            mux2_32$ muxwrin8 (R8_WR[i], R8L_WR[i], R8H_WR[i], WR[2]);
            mux4_32$ muxwrin  (GPRIN[i], R8_WR[i], R16_WR[i], R32_WR, 32'hZZZZZZZZ, MODEW);
            assign MMXIN[i] = IN;
        end
    endgenerate


    // Generate special outputs
    assign CSO  = SEG[1][15:0];

endmodule


/*
module RegisterFile_TB;

    // GPR Write ports
    reg     [63:0]  IN;     // Data-In 0
    reg     [ 2:0]  WR;     // Write register 0
    reg             WE;     // Write-enable 0

    // GPR Read ports
    reg     [ 2:0]  RD0;    // Read register 0
    wire    [63:0]  OUT0;   // Data-Out 0
    reg     [ 2:0]  RD1;    // Read register 1
    wire    [63:0]  OUT1;   // Data-Out 1
    reg     [ 2:0]  RD2;    // Read register 2
    wire    [31:0]  OUT2;   // Data-Out 2
    reg     [ 2:0]  RD3;    // Read register 3
    wire    [31:0]  OUT3;   // Data-Out 3

    // SegR Write Ports
    reg     [15:0]  INS;    // Segment Data-In
    reg     [ 2:0]  SWR;    // Segment write register
    reg             SWE;    // Segment write-enable

    // SegR Read Ports
    reg     [ 2:0]  SRD0;   // Segment read register 0
    wire    [15:0]  SGO0;   // Segment data-out 0
    reg     [ 2:0]  SRD1;   // Segment read register 1
    wire    [15:0]  SGO1;   // Segment data-out 1

    // Special output
    wire    [15:0]  CSO;    // Code segment output

    // Controls
    reg     [ 1:0]  MODER;  // GPR Read Mode:  GPR8/GPR16/GPR32/MMX
    reg             MODEB;  // BASE Read Mode: GPR8/GPR32
    reg     [ 1:0]  MODEW;  // GPR Write Mode: GPR8/GPR16/GPR32/MMX
    reg             CLK;    // System clock
    reg             CLR;    // System reset
    reg             PRE;    // System preset

    integer i, j, k , l;

    // Setup clock
    initial begin
        CLK <= 1'b0;
        CLR <= 1'b0;
        PRE <= 1'b1;
        forever
            #`HALF_CYCLE_TIME CLK <= ~CLK;
    end

    initial begin

        // Test GPR8
        #`HALF_CYCLE_TIME
        #`HALF_CYCLE_TIME
        IN <= 64'h0123456789ABCDEF;
        WE <= 1'b1;
        CLR <= 1'b1;
        #`HALF_CYCLE_TIME

        for(i = 0; i < 4; i = i + 1) begin
            MODEW = i;
            MODER = i;
            MODEB = i%2;
            for(j = 0; j < 8; j = j + 1) begin
                RD0 = j;
                RD1 = j;
                RD2 = j;
                RD3 = j;
                for(k = 0; k < 8; k = k + 1) begin
                    #`CYCLE_TIME
                    WR = k;
                end
            end
        end
        
        MODE <= 2'b00;
        IN0 <= 64'h0000000012345678;
        IN1 <= 64'h000000009ABCDEF0;
        WE0 <= 1'b1;
        WE1 <= 1'b1;
        for(i = 0; i < 8; i = i + 1) begin
            for(j = 0; j < 8; j = j + 1) begin
                #`CYCLE_TIME
                WR0 = i;
                WR1 = j;
            end
        end
        

        #`CYCLE_TIME
        $finish;
    end

    regfilex86 regfile(IN, WR, WE,

                       RD0, OUT0,
                       RD1, OUT1,
                       RD2, OUT2,
                       RD3, OUT3,

                       INS,  SWR, SWE,
                       SRD0, SGO0,
                       SRD1, SGO1,

                       CSO,

                       MODER, MODEB, MODEW,
                       CLK, CLR, PRE);

    // Dump simulation
    initial begin
        $dumpfile("RegisterFile.vcd");
        $dumpvars(0, RegisterFile_TB);
    end

endmodule

*/
