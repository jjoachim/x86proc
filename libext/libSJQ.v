`ifndef _LIBSJQ_VH
`define _LIBSJQ_VH

// 2-way 8-bit AND
module and2_8$(y, a, b);

    // I/O
    input   [7:0]  a;
    input   [7:0]  b;
    output  [7:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 8; i = i + 1) begin : and_gen
            and2$ and_gate(y[i], a[i], b[i]);
        end
    endgenerate
endmodule

// 2-way 16-bit AND
module and2_16$(y, a, b);

    // I/O
    input   [15:0]  a;
    input   [15:0]  b;
    output  [15:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 16; i = i + 1) begin : and_gen
            and2$ and_gate(y[i], a[i], b[i]);
        end
    endgenerate
endmodule

// 2-way 24-bit AND
module and2_24$(y, a, b);

    // I/O
    input   [23:0]  a;
    input   [23:0]  b;
    output  [23:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 24; i = i + 1) begin : and_gen
            and2$ and_gate(y[i], a[i], b[i]);
        end
    endgenerate
endmodule

// 2-way 32-bit AND
module and2_32$(y, a, b);

    // I/O
    input   [31:0]  a;
    input   [31:0]  b;
    output  [31:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 32; i = i + 1) begin : and_gen
            and2$ and_gate(y[i], a[i], b[i]);
        end
    endgenerate
endmodule

// 2-way 48-bit AND
module and2_48$(y, a, b);

    // I/O
    input   [47:0]  a;
    input   [47:0]  b;
    output  [47:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 48; i = i + 1) begin : and_gen
            and2$ and_gate(y[i], a[i], b[i]);
        end
    endgenerate
endmodule

// 2-way 8-bit OR
module or2_8$(y, a, b);

    // I/O
    input   [7:0]  a;
    input   [7:0]  b;
    output  [7:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 8; i = i + 1) begin : or_gen
            or2$ or_gate(y[i], a[i], b[i]);
        end
    endgenerate
endmodule

// 2-way 16-bit OR
module or2_16$(y, a, b);

    // I/O
    input   [15:0]  a;
    input   [15:0]  b;
    output  [15:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 16; i = i + 1) begin : or_gen
            or2$ or_gate(y[i], a[i], b[i]);
        end
    endgenerate
endmodule

// 2-way 24-bit OR
module or2_24$(y, a, b);

    // I/O
    input   [23:0]  a;
    input   [23:0]  b;
    output  [23:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 24; i = i + 1) begin : or_gen
            or2$ or_gate(y[i], a[i], b[i]);
        end
    endgenerate
endmodule

// 2-Wway 32-bit OR
module or2_32$(y, a, b);

    // I/O
    input   [31:0]  a;
    input   [31:0]  b;
    output  [31:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 32; i = i + 1) begin : or_gen
            or2$ or_gate(y[i], a[i], b[i]);
        end
    endgenerate
endmodule



// 2-way 48-bit OR
module or2_48$(y, a, b);

    // I/O
    input   [47:0]  a;
    input   [47:0]  b;
    output  [47:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 48; i = i + 1) begin : or_gen
            or2$ or_gate(y[i], a[i], b[i]);
        end
    endgenerate
endmodule

// 2-bit Inverter
module inv2$(y, a);

    // I/O
    input   [1:0]  a;
    output  [1:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 2; i = i + 1) begin : inv_gen
            inv1$ inverter(y[i], a[i]);
        end
    endgenerate
endmodule

// 32-bit Inverter
module inv32$(y, a);

    // I/O
    input   [31:0]  a;
    output  [31:0]  y;
    
    // Structural model
    genvar  i;
    generate
        for(i = 0; i < 32; i = i + 1) begin : inv_gen
            inv1$ inverter(y[i], a[i]);
        end
    endgenerate
endmodule

// 8-way 1-bit multiplexer
module mux8$(outb, in, s);

    // I/O
    input   [ 7:0]  in;
    input   [ 2:0]   s;
    output          outb;
    
    // Intermediate selected bits
    wire    [ 1:0]  in2;
    
    // Level one
    mux4$ mux00_03(in2[0], in[ 0], in[ 1], in[ 2], in[ 3], s[0], s[1]);
    mux4$ mux04_07(in2[1], in[ 4], in[ 5], in[ 6], in[ 7], s[0], s[1]);
    
    // Level two
    mux2$ muxfinal(outb, in2[0], in2[1], s[2]);

endmodule

// 16-way 1-bit multiplexer
module mux16$(outb, in, s);

    // I/O
    input   [15:0]  in;
    input   [ 3:0]   s;
    output          outb;
    
    // Intermediate selected bits
    wire    [ 3:0]  in4;
    
    // Level one
    mux4$ mux00_03(in4[0], in[ 0], in[ 1], in[ 2], in[ 3], s[0], s[1]);
    mux4$ mux04_07(in4[1], in[ 4], in[ 5], in[ 6], in[ 7], s[0], s[1]);
    mux4$ mux08_11(in4[2], in[ 8], in[ 9], in[10], in[11], s[0], s[1]);
    mux4$ mux12_15(in4[3], in[12], in[13], in[14], in[15], s[0], s[1]);
    
    // Level two
    mux4$ muxfinal(outb,   in4[0], in4[1], in4[2], in4[3], s[2], s[3]);

endmodule

// 32-way 1-bit multiplexer
module mux32$(outb, in, s);

    // I/O
    input   [31:0]  in;
    input   [4:0]   s;
    output          outb;
    
    // Intermediate selected bits
    wire    [ 7:0]  in8;
    wire    [ 1:0]  in2;
    
    // Level one
    mux4$ mux00_03(in8[0], in[ 0], in[ 1], in[ 2], in[ 3], s[0], s[1]);
    mux4$ mux04_07(in8[1], in[ 4], in[ 5], in[ 6], in[ 7], s[0], s[1]);
    mux4$ mux08_11(in8[2], in[ 8], in[ 9], in[10], in[11], s[0], s[1]);
    mux4$ mux12_15(in8[3], in[12], in[13], in[14], in[15], s[0], s[1]);
    mux4$ mux16_19(in8[4], in[16], in[17], in[18], in[19], s[0], s[1]);
    mux4$ mux20_23(in8[5], in[20], in[21], in[22], in[23], s[0], s[1]);
    mux4$ mux24_27(in8[6], in[24], in[25], in[26], in[27], s[0], s[1]);
    mux4$ mux28_31(in8[7], in[28], in[29], in[30], in[31], s[0], s[1]);
    
    // Level two
    mux4$ mux0_3(in2[0], in8[0], in8[1], in8[2], in8[3], s[2], s[3]);
    mux4$ mux4_7(in2[1], in8[4], in8[5], in8[6], in8[7], s[2], s[3]);
    
    // Level three
    mux2$ muxfinal(outb, in2[0], in2[1], s[4]);

endmodule

// 64-way 1-bit multiplexer
module mux64$(outb, in, s);

    // I/O
    input   [63:0]  in;
    input   [5:0]   s;
    output          outb;
    
    // Intermediate selected bits
    wire    [15:0]  in16;
    wire    [ 3:0]  in4;
    
    // Level one
    mux4$ mux00_03(in16[ 0], in[ 0], in[ 1], in[ 2], in[ 3], s[0], s[1]);
    mux4$ mux04_07(in16[ 1], in[ 4], in[ 5], in[ 6], in[ 7], s[0], s[1]);
    mux4$ mux08_11(in16[ 2], in[ 8], in[ 9], in[10], in[11], s[0], s[1]);
    mux4$ mux12_15(in16[ 3], in[12], in[13], in[14], in[15], s[0], s[1]);
    mux4$ mux16_19(in16[ 4], in[16], in[17], in[18], in[19], s[0], s[1]);
    mux4$ mux20_23(in16[ 5], in[20], in[21], in[22], in[23], s[0], s[1]);
    mux4$ mux24_27(in16[ 6], in[24], in[25], in[26], in[27], s[0], s[1]);
    mux4$ mux28_31(in16[ 7], in[28], in[29], in[30], in[31], s[0], s[1]);
    mux4$ mux32_35(in16[ 8], in[32], in[33], in[34], in[35], s[0], s[1]);
    mux4$ mux36_39(in16[ 9], in[36], in[37], in[38], in[39], s[0], s[1]);
    mux4$ mux40_43(in16[10], in[40], in[41], in[42], in[43], s[0], s[1]);
    mux4$ mux44_47(in16[11], in[44], in[45], in[46], in[47], s[0], s[1]);
    mux4$ mux48_51(in16[12], in[48], in[49], in[50], in[51], s[0], s[1]);
    mux4$ mux52_55(in16[13], in[52], in[53], in[54], in[55], s[0], s[1]);
    mux4$ mux56_59(in16[14], in[56], in[57], in[58], in[59], s[0], s[1]);
    mux4$ mux60_63(in16[15], in[60], in[61], in[62], in[63], s[0], s[1]);
    
    // Level two
    mux4$ mux00_03l2(in4[0], in16[ 0], in16[ 1], in16[ 2], in16[ 3], s[2], s[3]);
    mux4$ mux04_07l2(in4[1], in16[ 4], in16[ 5], in16[ 6], in16[ 7], s[2], s[3]);
    mux4$ mux08_11l2(in4[2], in16[ 8], in16[ 9], in16[10], in16[11], s[2], s[3]);
    mux4$ mux12_15l2(in4[3], in16[12], in16[13], in16[14], in16[15], s[2], s[3]);
    
    // Level three
    mux4$ muxfinal(  outb,    in4[ 0],  in4[ 1],  in4[ 2],  in4[ 3], s[4], s[5]);

endmodule

// 2-way 2-bit multiplexer
module mux2_2$(Y, IN0, IN1, S);
    // I/O
    input   [ 1:0]  IN0, IN1;
    input           S;
    output  [ 1:0]  Y;
    // Generate variable
    genvar i;
    // Multiplex
    generate
        for(i = 0; i < 2; i = i + 1) begin: multiplex2_2
            mux2$ mux(Y[i], IN0[i], IN1[i], S);
        end
    endgenerate
endmodule

// 2-way 3-bit multiplexer
module mux2_3$(Y, IN0, IN1, S);
    // I/O
    input   [ 2:0]  IN0, IN1;
    input           S;
    output  [ 2:0]  Y;
    // Generate variable
    genvar i;
    // Multiplex
    generate
        for(i = 0; i < 3; i = i + 1) begin: multiplex2_3
            mux2$ mux(Y[i], IN0[i], IN1[i], S);
        end
    endgenerate
endmodule

// 2-way 32-bit multiplexer
module mux2_32$(Y, IN0, IN1, S);
    // I/O
    input   [31:0]  IN0, IN1;
    input           S;
    output  [31:0]  Y;
    // Generate variable
    genvar i;
    // Multiplex
    generate
        for(i = 0; i < 32; i = i + 1) begin: multiplex2_32
            mux2$ mux(Y[i], IN0[i], IN1[i], S);
        end
    endgenerate
endmodule

// 2-way 48-bit multiplexer
module mux2_48$(Y, IN0, IN1, S);
    // I/O
    input   [47:0]  IN0, IN1;
    input           S;
    output  [47:0]  Y;
    // Generate variable
    genvar i;
    // Multiplex
    generate
        for(i = 0; i < 48; i = i + 1) begin: multiplex2_48
            mux2$ mux(Y[i], IN0[i], IN1[i], S);
        end
    endgenerate
endmodule

// 2-way 64-bit multiplexer
module mux2_64$(Y, IN0, IN1, S);
    // I/O
    input   [63:0]  IN0, IN1;
    input           S;
    output  [63:0]  Y;
    // Generate variable
    genvar i;
    // Multiplex
    generate
        for(i = 0; i < 64; i = i + 1) begin: multiplex2_64
            mux2$ mux(Y[i], IN0[i], IN1[i], S);
        end
    endgenerate
endmodule

// 4-way 3-bit multiplexer
module mux4_3$(Y, IN0, IN1, IN2, IN3, S);
    // I/O
    input   [ 2:0]  IN0, IN1, IN2, IN3;
    input   [ 1:0]  S;
    output  [ 2:0]  Y;
    // Generate variable
    genvar i;
    // Multiplex
    generate
        for(i = 0; i < 3; i = i + 1) begin: multiplex4_3
            mux4$ mux(Y[i], IN0[i], IN1[i], IN2[i], IN3[i], S[0], S[1]);
        end
    endgenerate
endmodule

// 4-way 32-bit multiplexer
module mux4_32$(Y, IN0, IN1, IN2, IN3, S);
    // I/O
    input   [31:0]  IN0, IN1, IN2, IN3;
    input   [ 1:0]  S;
    output  [31:0]  Y;
    // Generate variable
    genvar i;
    // Multiplex
    generate
        for(i = 0; i < 32; i = i + 1) begin: multiplex4_32
            mux4$ mux(Y[i], IN0[i], IN1[i], IN2[i], IN3[i], S[0], S[1]);
        end
    endgenerate
endmodule

// 4-way 48-bit multiplexer
module mux4_48$(Y, IN0, IN1, IN2, IN3, S);
    // I/O
    input   [47:0]  IN0, IN1, IN2, IN3;
    input   [ 1:0]  S;
    output  [47:0]  Y;
    // Generate variable
    genvar i;
    // Multiplex
    generate
        for(i = 0; i < 48; i = i + 1) begin: multiplex4_48
            mux4$ mux(Y[i], IN0[i], IN1[i], IN2[i], IN3[i], S[0], S[1]);
        end
    endgenerate
endmodule

// 4-way 64-bit multiplexer
module mux4_64$(Y, IN0, IN1, IN2, IN3, S);
    // I/O
    input   [63:0]  IN0, IN1, IN2, IN3;
    input   [ 1:0]  S;
    output  [63:0]  Y;
    // Generate variable
    genvar i;
    // Multiplex
    generate
        for(i = 0; i < 64; i = i + 1) begin: multiplex4_64
            mux4$ mux(Y[i], IN0[i], IN1[i], IN2[i], IN3[i], S[0], S[1]);
        end
    endgenerate
endmodule

// 8-way 3-bit multiplexer
module mux8_3$(Y,
               IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07,
               S);
    
    // I/O
    input   [ 2:0]  IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07;
    input   [ 2:0]  S;
    output  [ 2:0]  Y;
    
    // Organized bits
    wire    [ 7:0]  BORG[0:2];
    
    // Generate variable
    genvar          i;
    
    // Organize bits
    generate
        for(i = 0; i < 3; i = i+1) begin : org8_3
            assign BORG[i] = {IN07[i],IN06[i],IN05[i],IN04[i],IN03[i],IN02[i],IN01[i],IN00[i]};
        end
    endgenerate
                  
    // Multiplex
    generate
        for(i = 0; i < 3; i = i+1) begin : multiplex8_3
            mux8$ mux(Y[i], BORG[i], S);
        end
    endgenerate

endmodule

// 8-way 8-bit multiplexer
module mux8_8$(Y,
               IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07,
               S);
    
    // I/O
    input   [ 7:0]  IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07;
    input   [ 2:0]  S;
    output  [ 7:0]  Y;
    
    // Organized bits
    wire    [ 7:0]  BORG[0:7];
    
    // Generate variable
    genvar          i;
    
    // Organize bits
    generate
        for(i = 0; i < 8; i = i+1) begin : org8_8
            assign BORG[i] = {IN07[i],IN06[i],IN05[i],IN04[i],IN03[i],IN02[i],IN01[i],IN00[i]};
        end
    endgenerate
                  
    // Multiplex
    generate
        for(i = 0; i < 8; i = i+1) begin : multiplex8_8
            mux8$ mux(Y[i], BORG[i], S);
        end
    endgenerate

endmodule


// 8-way 16-bit multiplexer
module mux8_16$(Y,
                IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07,
                S);
    
    // I/O
    input   [15:0]  IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07;
    input   [ 2:0]  S;
    output  [15:0]  Y;
    
    // Organized bits
    wire    [ 7:0]  BORG[0:15];
    
    // Generate variable
    genvar          i;
    
    // Organize bits
    generate
        for(i = 0; i < 16; i = i+1) begin : org8_16
            assign BORG[i] = {IN07[i],IN06[i],IN05[i],IN04[i],IN03[i],IN02[i],IN01[i],IN00[i]};
        end
    endgenerate
                  
    // Multiplex
    generate
        for(i = 0; i < 16; i = i+1) begin : multiplex8_16
            mux8$ mux(Y[i], BORG[i], S);
        end
    endgenerate

endmodule

// 8-way 32-bit multiplexer
module mux8_32$(Y,
                IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07,
                S);
    
    // I/O
    input   [31:0]  IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07;
    input   [ 2:0]  S;
    output  [31:0]  Y;
    
    // Organized bits
    wire    [ 7:0]  BORG[0:31];
    
    // Generate variable
    genvar          i;
    
    // Organize bits
    generate
        for(i = 0; i < 32; i = i+1) begin : org8_32
            assign BORG[i] = {IN07[i],IN06[i],IN05[i],IN04[i],IN03[i],IN02[i],IN01[i],IN00[i]};
        end
    endgenerate
                  
    // Multiplex
    generate
        for(i = 0; i < 32; i = i+1) begin : multiplex8_32
            mux8$ mux(Y[i], BORG[i], S);
        end
    endgenerate

endmodule

// 8-way 64-bit multiplexer
module mux8_64$(Y,
                IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07,
                S);
    
    // I/O
    input   [63:0]  IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07;
    input   [ 2:0]  S;
    output  [63:0]  Y;
    
    // Organized bits
    wire    [ 7:0]  BORG[0:63];
    
    // Generate variable
    genvar          i;
    
    // Organize bits
    generate
        for(i = 0; i < 64; i = i+1) begin : org8_64
            assign BORG[i] = {IN07[i],IN06[i],IN05[i],IN04[i],IN03[i],IN02[i],IN01[i],IN00[i]};
        end
    endgenerate
                  
    // Multiplex
    generate
        for(i = 0; i < 64; i = i+1) begin : multiplex8_64
            mux8$ mux(Y[i], BORG[i], S);
        end
    endgenerate

endmodule

// 16-way 8-bit multiplexer
module mux16_8$(Y,
                 IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07,
                 IN08, IN09, IN10, IN11, IN12, IN13, IN14, IN15,
                 S);
    
    // I/O
    input   [ 7:0]  IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07;
    input   [ 7:0]  IN08, IN09, IN10, IN11, IN12, IN13, IN14, IN15;
    input   [ 3:0]  S;
    output  [ 7:0]  Y;
    
    // Organized bits
    wire    [15:0]  BORG[0:7];
    
    // Generate variable
    genvar          i;
    
    // Organize bits
    generate
        for(i = 0; i < 8; i = i+1) begin : org16_8
            assign BORG[i] = {IN15[i],IN14[i],IN13[i],IN12[i],IN11[i],IN10[i],IN09[i],IN08[i],
                              IN07[i],IN06[i],IN05[i],IN04[i],IN03[i],IN02[i],IN01[i],IN00[i]};
        end
    endgenerate
                  
    // Multiplex
    generate
        for(i = 0; i < 8; i = i+1) begin : multiplex16_8
            mux16$ mux(Y[i], BORG[i], S);
        end
    endgenerate

endmodule

// 16-way 32-bit multiplexer
module mux16_32$(Y,
                 IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07,
                 IN08, IN09, IN10, IN11, IN12, IN13, IN14, IN15,
                 S);
    
    // I/O
    input   [31:0]  IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07;
    input   [31:0]  IN08, IN09, IN10, IN11, IN12, IN13, IN14, IN15;
    input   [ 3:0]  S;
    output  [31:0]  Y;
    
    // Organized bits
    wire    [15:0]  BORG[0:31];
    
    // Generate variable
    genvar          i;
    
    // Organize bits
    generate
        for(i = 0; i < 32; i = i+1) begin : org16_32
            assign BORG[i] = {IN15[i],IN14[i],IN13[i],IN12[i],IN11[i],IN10[i],IN09[i],IN08[i],
                              IN07[i],IN06[i],IN05[i],IN04[i],IN03[i],IN02[i],IN01[i],IN00[i]};
        end
    endgenerate
                  
    // Multiplex
    generate
        for(i = 0; i < 32; i = i+1) begin : multiplex16_32
            mux16$ mux(Y[i], BORG[i], S);
        end
    endgenerate

endmodule

// 16-way 64-bit Multiplexer
module mux16_64$(Y,
                 IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07,
                 IN08, IN09, IN10, IN11, IN12, IN13, IN14, IN15,
                 S);
    
    // I/O
    input   [63:0]  IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07;
    input   [63:0]  IN08, IN09, IN10, IN11, IN12, IN13, IN14, IN15;
    input   [ 3:0]  S;
    output  [63:0]  Y;
    
    // Organized bits
    wire    [15:0]  BORG[0:63];
    
    // Generate variable
    genvar          i;
    
    // Organize bits
    generate
        for(i = 0; i < 64; i = i+1) begin : org16_64
            assign BORG[i] = {IN15[i],IN14[i],IN13[i],IN12[i],IN11[i],IN10[i],IN09[i],IN08[i],
                              IN07[i],IN06[i],IN05[i],IN04[i],IN03[i],IN02[i],IN01[i],IN00[i]};
        end
    endgenerate
                  
    // Multiplex
    generate
        for(i = 0; i < 64; i = i+1) begin : multiplex16_64
            mux16$ mux(Y[i], BORG[i], S);
        end
    endgenerate

endmodule

// 32-way 32-bit Multiplexer
module mux32_32$(Y,
                 IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07,
                 IN08, IN09, IN10, IN11, IN12, IN13, IN14, IN15,
                 IN16, IN17, IN18, IN19, IN20, IN21, IN22, IN23,
                 IN24, IN25, IN26, IN27, IN28, IN29, IN30, IN31,
                 S);
    
    // I/O
    input   [31:0]  IN00, IN01, IN02, IN03, IN04, IN05, IN06, IN07;
    input   [31:0]  IN08, IN09, IN10, IN11, IN12, IN13, IN14, IN15;
    input   [31:0]  IN16, IN17, IN18, IN19, IN20, IN21, IN22, IN23;
    input   [31:0]  IN24, IN25, IN26, IN27, IN28, IN29, IN30, IN31;
    input   [ 4:0]  S;
    output  [31:0]  Y;
    
    // Organized bits
    wire    [31:0]  BORG[0:31];
    
    genvar          i;
    
    // Organize bits
    generate
        for(i = 0; i < 32; i = i + 1) begin : org32_32
            assign BORG[i] = {IN31[i],IN30[i],IN29[i],IN28[i],IN27[i],IN26[i],IN25[i],IN24[i],
                              IN23[i],IN22[i],IN21[i],IN20[i],IN19[i],IN18[i],IN17[i],IN16[i],
                              IN15[i],IN14[i],IN13[i],IN12[i],IN11[i],IN10[i],IN09[i],IN08[i],
                              IN07[i],IN06[i],IN05[i],IN04[i],IN03[i],IN02[i],IN01[i],IN00[i]};
        end
    endgenerate
    
    // Multiplex
    generate
        for(i = 0; i < 32; i = i+1) begin : multiplex32_32
            mux32$ mux(Y[i], BORG[i], S);
        end
    endgenerate

endmodule

// 32-bit Decoder
module decoder5_32$(SEL,Y,YBAR);

    // I/O
    input   [ 4:0]  SEL;
    output  [31:0]  Y;
    output  [31:0]  YBAR;
    
    // Intermediate decoded signals
    wire    [31:0]  DECSIG;
    wire    [31:0]  DECSIGBAR;
    
    // Selection wires
    wire    [ 4:0]  SELBAR;
    wire    [ 3:0]  SELDEC;
    wire    [ 3:0]  SELDECBAR;
    
    // Masking wires
    wire    [31:0]  MASK;
    wire    [31:0]  UNMASK;   
    
    // Decoders
    decoder3_8$ dec0(SEL[2:0], DECSIG[ 7: 0], DECSIGBAR[ 7: 0]);
    decoder3_8$ dec1(SEL[2:0], DECSIG[15: 8], DECSIGBAR[15: 8]);
    decoder3_8$ dec2(SEL[2:0], DECSIG[23:16], DECSIGBAR[23:16]);
    decoder3_8$ dec3(SEL[2:0], DECSIG[31:24], DECSIGBAR[31:24]);
    
    // Decoder selction logic
    inv1$ invSel0(SELBAR[0], SEL[0]);
    inv1$ invSel1(SELBAR[1], SEL[1]);
    inv1$ invSel2(SELBAR[2], SEL[2]);
    inv1$ invSel3(SELBAR[3], SEL[3]);
    inv1$ invSel4(SELBAR[4], SEL[4]);
    and2$ andSelDec0(SELDEC[0], SELBAR[4], SELBAR[3]);
    and2$ andSelDec1(SELDEC[1], SELBAR[4],    SEL[3]);
    and2$ andSelDec2(SELDEC[2],    SEL[4], SELBAR[3]);
    and2$ andSelDec3(SELDEC[3],    SEL[4],    SEL[3]);
    inv1$ invSelDec0(SELDECBAR[0], SELDEC[0]);
    inv1$ invSelDec1(SELDECBAR[1], SELDEC[1]);
    inv1$ invSelDec2(SELDECBAR[2], SELDEC[2]);
    inv1$ invSelDec3(SELDECBAR[3], SELDEC[3]);
    
    // Create bit masks
    assign MASK =   {{8{SELDEC[3]}},   {8{SELDEC[2]}},   {8{SELDEC[1]}},   {8{SELDEC[0]}}};
    assign UNMASK = {{8{SELDECBAR[3]}},{8{SELDECBAR[2]}},{8{SELDECBAR[1]}},{8{SELDECBAR[0]}}};
    
    // Finalize output
    and2_32$ Y_OUT(Y,       DECSIG,    MASK);
    or2_32$  YBAR_OUT(YBAR, DECSIGBAR, UNMASK);
    
endmodule

// Black cell
module black_cell$(Gkj, Pik, Gik, Pkj, G, P);
    input Gkj, Pik, Gik, Pkj;
    output G, P;
    wire Y;
    and2$ and0(Y, Gkj, Pik);
    or2$  or0(G, Gik, Y);
    and2$ and1(P, Pkj, Pik); 
endmodule

// Gray cell
module gray_cell$(Gkj, Pik, Gik, G);
    input Gkj, Pik, Gik;
    output G;
    wire Y; 
    and2$ and0(Y, Gkj, Pik);
    or2$  or0(G, Y, Gik);
endmodule

// AND with XOR
module and_xor$(a, b, p, g);
    input a, b;
    output p, g;
    xor2$ xor0(p, a, b);
    and2$ and0(g, a, b);
endmodule

// 2-bit equivalency checker
module equal2$(equal, x, y);
    input   [1:0]   x, y;
    output          equal;
    wire    [1:0]   eqbits;
    xnor2$ xnorb0(eqbits[0], x[0], y[0]);
    xnor2$ xnorb1(eqbits[1], x[1], y[1]);
    and2$  andeq(equal, eqbits[0], eqbits[1]);
endmodule

// 3-bit equivalency checker
module equal3$(equal, x, y);
    input   [2:0]   x, y;
    output          equal;
    wire    [2:0]   eqbits;
    xnor2$ xnorb0(eqbits[0], x[0], y[0]);
    xnor2$ xnorb1(eqbits[1], x[1], y[1]);
    xnor2$ xnorb2(eqbits[2], x[2], y[2]);
    and3$  andeq(equal, eqbits[0], eqbits[1], eqbits[2]);
endmodule

// 16-bit reduction OR
module redOR16$(res, x);

    // I/O
    output          res;
    input   [15:0]  x;

    // Level 1
    wire    nor0_0;
    wire    nor1_0;
    wire    nor2_0;
    wire    nor3_0;

    // Level 2
    wire    nand0_1;

    // Level 1
    nor4$   nor0(nor0_0, x[ 0], x[ 1], x[ 2], x[ 3]);
    nor4$   nor1(nor1_0, x[ 4], x[ 5], x[ 6], x[ 7]);
    nor4$   nor2(nor2_0, x[ 8], x[ 9], x[10], x[11]);
    nor4$   nor3(nor3_0, x[12], x[13], x[14], x[15]);

    // Level 2
    nand4$ nandF(nand0_1, nor0_0, nor1_0, nor2_0, nor3_0);


endmodule


// Inverted 16-bit reduction OR
module redORN16$(res, x);

    // I/O
    output          res;
    input   [15:0]  x;

    // Level 1
    wire    nor0_0;
    wire    nor1_0;
    wire    nor2_0;
    wire    nor3_0;

    // Level 1
    nor4$   nor0(nor0_0, x[ 0], x[ 1], x[ 2], x[ 3]);
    nor4$   nor1(nor1_0, x[ 4], x[ 5], x[ 6], x[ 7]);
    nor4$   nor2(nor2_0, x[ 8], x[ 9], x[10], x[11]);
    nor4$   nor3(nor3_0, x[12], x[13], x[14], x[15]);

    // Level 2
    and4$ andF(res, nor0_0, nor1_0, nor2_0, nor3_0);


endmodule

// Inverted 32-bit reduction OR
module redORN32$(res, x);

    // I/O
    output          res;
    input   [31:0]  x;

    // Level 1
    wire    nor0_0;
    wire    nor1_0;
    wire    nor2_0;
    wire    nor3_0;
    wire    nor4_0;
    wire    nor5_0;
    wire    nor6_0;
    wire    nor7_0;

    // Level 2
    wire    nand0_1;
    wire    nand1_1;

    // Level 1
    nor4$   nor0(nor0_0, x[ 0], x[ 1], x[ 2], x[ 3]);
    nor4$   nor1(nor1_0, x[ 4], x[ 5], x[ 6], x[ 7]);
    nor4$   nor2(nor2_0, x[ 8], x[ 9], x[10], x[11]);
    nor4$   nor3(nor3_0, x[12], x[13], x[14], x[15]);
    nor4$   nor4(nor4_0, x[16], x[17], x[18], x[19]);
    nor4$   nor5(nor5_0, x[20], x[21], x[22], x[23]);
    nor4$   nor6(nor6_0, x[24], x[25], x[26], x[27]);
    nor4$   nor7(nor7_0, x[28], x[29], x[30], x[31]);

    // Level 2
    nand4$ nand0(nand0_1, nor0_0, nor1_0, nor2_0, nor3_0);
    nand4$ nand1(nand1_1, nor4_0, nor5_0, nor6_0, nor7_0);

    // Level 3
    nor2$  norF(res, nand0_1, nand1_1);

endmodule

// 32-bit Parity Detector
module parity32$(p, x);

    // I/O
    input   [31:0]  x;
    output          p;

    // Level 0
    wire            XOR00_0;
    wire            XOR01_0;
    wire            XOR02_0;
    wire            XOR03_0;
    wire            XOR04_0;
    wire            XOR05_0;
    wire            XOR06_0;
    wire            XOR07_0;
    wire            XOR08_0;
    wire            XOR09_0;
    wire            XOR10_0;
    wire            XOR11_0;
    wire            XOR12_0;
    wire            XOR13_0;
    wire            XOR14_0;
    wire            XOR15_0;

    // Level 1
    wire            XOR00_1;
    wire            XOR01_1;
    wire            XOR02_1;
    wire            XOR03_1;
    wire            XOR04_1;
    wire            XOR05_1;
    wire            XOR06_1;
    wire            XOR07_1;

    // Level 2
    wire            XOR00_2;
    wire            XOR01_2;
    wire            XOR02_2;
    wire            XOR03_2;

    // Level 3
    wire            XOR00_3;
    wire            XOR01_3;

    // Level 0
    xor2$ xor00_0(XOR00_0, x[ 0], x[ 1]);
    xor2$ xor01_0(XOR01_0, x[ 2], x[ 3]);
    xor2$ xor02_0(XOR02_0, x[ 4], x[ 5]);
    xor2$ xor03_0(XOR03_0, x[ 6], x[ 7]);
    xor2$ xor04_0(XOR04_0, x[ 8], x[ 9]);
    xor2$ xor05_0(XOR05_0, x[10], x[11]);
    xor2$ xor06_0(XOR06_0, x[12], x[13]);
    xor2$ xor07_0(XOR07_0, x[14], x[15]);
    xor2$ xor08_0(XOR08_0, x[16], x[17]);
    xor2$ xor09_0(XOR09_0, x[18], x[19]);
    xor2$ xor10_0(XOR10_0, x[20], x[21]);
    xor2$ xor11_0(XOR11_0, x[22], x[23]);
    xor2$ xor12_0(XOR12_0, x[24], x[25]);
    xor2$ xor13_0(XOR13_0, x[26], x[27]);
    xor2$ xor14_0(XOR14_0, x[28], x[29]);
    xor2$ xor15_0(XOR15_0, x[30], x[31]);

    // Level 1
    xor2$ xor00_1(XOR00_1, XOR00_0, XOR01_0);
    xor2$ xor01_1(XOR01_1, XOR02_0, XOR03_0);
    xor2$ xor02_1(XOR02_1, XOR04_0, XOR05_0);
    xor2$ xor03_1(XOR03_1, XOR06_0, XOR07_0);
    xor2$ xor04_1(XOR04_1, XOR08_0, XOR09_0);
    xor2$ xor05_1(XOR05_1, XOR10_0, XOR11_0);
    xor2$ xor06_1(XOR06_1, XOR12_0, XOR13_0);
    xor2$ xor07_1(XOR07_1, XOR14_0, XOR15_0);

    // Level 2
    xor2$ xor00_2(XOR00_2, XOR00_1, XOR01_1);
    xor2$ xor01_2(XOR01_2, XOR02_1, XOR03_1);
    xor2$ xor02_2(XOR02_2, XOR04_1, XOR05_1);
    xor2$ xor03_2(XOR03_2, XOR06_1, XOR07_1);

    // Level 3
    xor2$ xor00_3(XOR00_3, XOR00_2, XOR01_2);
    xor2$ xor01_3(XOR01_3, XOR02_2, XOR03_2);

    // Level 4
    xnor2$ xnor_4(p, XOR00_3, XOR01_3);

endmodule

`endif
