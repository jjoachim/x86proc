`timescale 1ns/1ps

`include "Parameters.v"
`include "TestFramework.v"

`include "Cache/Cache.v"
`include "MainMemory/MainMemory.v"

module TOP;
  integer i, hwi, sz;
  reg nreset;

  `TEST_GLOBAL_INIT;

  //Instantiate Bus Logic
  wire [`BUS_MSB:0] bus; 
  Bus #(2) bus_logic (bus, `TEST_CLK, nreset);

  //Give Main Memory a bus interface
  wire [31:0] mmbusdata, mmbusaddr, mmdata;
  wire mmvalid;
  wire [3:0] mmbuswr;
  wire mmbusrd;
  BusSlave #(`BUS_MEM_ADDRL, `BUS_MEM_ADDRH) bus_mm
    (bus, mmbusdata, mmbusaddr, mmbuswr, mmbusrd, mmdata, mmvalid);
  MainMemory #(8) mm 
    (mmdata, mmvalid, mmbusdata, mmbusaddr, mmbusrd, mmbuswr, `TEST_CLK, nreset);

  //Hardware Address
  reg [31:0] hwdata;
  reg hwvalid;

  wire [31:0] hwbusdata, hwbusaddr;
  wire [3:0] hwbuswr;
  wire hwbusrd;

  reg [3:0] hwvalidbytes;
  always@(posedge `TEST_CLK) begin
    for(hwi=0; hwi<4; hwi=hwi+1) begin
      if(hwbuswr[hwi])
        hwdata[hwi*8 +: 8] <= hwbusdata[hwi*8 +: 8];
    end

    hwvalid <= 1;
  end

  BusSlave #(32'h10000000, 32'h20000000) bus_hw
    (bus, hwbusdata, hwbusaddr, hwbuswr, hwbusrd, hwdata, hwvalid);

  //Instantiate Cache
  wire [63:0] odata;
  wire [31:0] odata32;
  wire [15:0] odata16;
  wire [7:0] odata8;
  assign odata8 = odata[7:0];
  assign odata16 = odata[15:0];
  assign odata32 = odata[31:0];

  wire [31:0] oaddr0, oaddr1;
  wire oexcPF, oexcWrP, omiss;
  wire [1:0] pcd, opcd;

  reg [63:0] data;
  reg [31:0] addr0;
  reg wr, rd;
  reg [1:0] opsz;

  wire [63:0] rdata;
  wire [31:0] raddr0, raddr1;
  wire rua, rwr;
  wire [1:0] ropsz, rpcd;
  
  //inferred signals
  wire [31:0] addr1;
  wire ua;
  assign addr1 = addr0 + 16;
  assign ua = ((addr0 % 16) + ((1<<opsz)-1)) >= 16;
 
  //Control signals
  wire [31:0] caddr0, caddr1;
  wire cua;

  assign caddr0 = rwr ? raddr0 : addr0;
  assign caddr1 = rwr ? raddr1 : addr1;
  assign cua = rwr ? rua : ua;

  DCache #(0, `DCACHE_SIZE, `DCACHE_BLOCK_SIZE) dcache 
               (odata, oaddr0, oaddr1, oexcPF, oexcWrP, omiss, opcd,
                caddr0, caddr1, cua, rd | wr, wr, rwr,
                rdata, ropsz, rpcd,
                bus, `TEST_CLK, nreset);

  //Cache Pipestage
  regs$ #(64+64+1+2+1+2) pipestage
    ({rdata, raddr0, raddr1, rua, rpcd, rwr, ropsz},
     {data, oaddr0, oaddr1,  ua, opcd,  wr,  opsz},
      ~omiss, `TEST_CLK, nreset);

  `TEST_INIT_COMB(odata, 64);
  `TEST_INIT_COMB(odata32, 32);
  `TEST_INIT_COMB(odata16, 16);
  `TEST_INIT_COMB(odata8, 8);
  `TEST_INIT_COMB(oexcWrP, 1);
  `TEST_INIT_COMB(oexcPF, 1);
  `TEST_INIT_SEQ(omiss, 1);
  `TEST_INIT_COMB(hwdata, 32);

  initial begin
    `TEST_TOGGLE_CLK;
    wr = 0;
    rd = 0;
    addr0 = 0;
    opsz = 0;
    data = 0;
    `TEST_TOGGLE_CLK;
    nreset = 0;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    nreset = 1;
    `TEST_TOGGLE_CLK;

    `TEST_DISPLAY("\n>> BEGIN MMIO TESTS <<\n");
    opsz = 2;
    wr = 1;
    data = 64'hABCDEF01;
    addr0 = 32'h10000000;
    `TEST_TOGGLE_CLK;
    data = 0;
    wr = 0;
    `TEST_WHEN(omiss==0, 
      `TEST_EQ(hwdata, 32'hABCDEF01);
    )

    rd = 1;
    `TEST_WHEN(omiss==0,
      addr0 = 32'h10000000;
      `TEST_EQ(odata32, 32'hABCDEF01);
    )
    rd = 0;

    opsz = 1;
    wr = 1;
    data = 64'hFFFF0000;
    addr0 = 32'h10000012;
    `TEST_TOGGLE_CLK;
    data = 0;
    wr = 0;
    `TEST_WHEN(omiss==0, 
      `TEST_EQ(hwdata, 32'hABCD0000);
    )

    rd = 1;
    `TEST_WHEN(omiss==0,
      addr0 = 32'h10000012;
      `TEST_EQ(odata32, 32'hABCD0000);
    )
    rd = 0;
   
    `TEST_DISPLAY("\n>> BEGIN ALIGNED QWORD TESTS <<\n");
    `TEST_DISPLAYF(("Writes at %0d...", $time));
    opsz = 3;

    wr = 1;
    data = 64'h11111111_11111111;
    `TEST_WHEN(omiss==0, addr0 = 0;)
    wr = 0;
    `TEST_TOGGLE_CLK;

    wr = 1;
    data = 64'h22222222_22222222;
    `TEST_WHEN(omiss==0, addr0 = 8;)
    wr = 0;
    `TEST_TOGGLE_CLK;

    wr = 1;
    data = 64'h33333333_33333333;
    `TEST_WHEN(omiss==0, addr0 = 16;)
    wr = 0;
    `TEST_TOGGLE_CLK;
    
    wr = 1;
    data = 64'h44444444_44444444;
    `TEST_WHEN(omiss==0, addr0 = 24;)
    wr = 0;
    `TEST_TOGGLE_CLK;
 
    wr = 1;
    data = 64'h55555555_55555555;
    `TEST_WHEN(omiss==0, addr0 = 32'hF00;)
    wr = 0;
    `TEST_TOGGLE_CLK;

    `TEST_DISPLAYF(("\nReads at %0d...", $time));
    rd = 1;
    `TEST_WHEN(omiss==0,
      addr0 = 0;
      `TEST_EQ(odata, 64'h11111111_11111111);
    )
    `TEST_WHEN(omiss==0,
      addr0 = 8;
      `TEST_EQ(odata, 64'h22222222_22222222);
    )
    `TEST_WHEN(omiss==0,
      addr0 = 16;
      `TEST_EQ(odata, 64'h33333333_33333333);
    )
    `TEST_WHEN(omiss==0,
      addr0 = 24;
      `TEST_EQ(odata, 64'h44444444_44444444);
    )
    `TEST_WHEN(omiss==0,
      addr0 = 32'hF00;
      `TEST_EQ(odata, 64'h55555555_55555555);
    )

    `TEST_DISPLAY("\n>> BEGIN MISALIGNED QWORD TESTS <<\n");
    `TEST_DISPLAYF(("Writes at %0d...", $time));
    rd = 0;
    opsz = 3;

    wr = 1;
    data = 64'h11111111_11111111;
    `TEST_WHEN(omiss==0, addr0 = 8;)
    wr = 0;
    `TEST_TOGGLE_CLK;

    wr = 1;
    `TEST_WHEN(omiss==0, addr0 = 35;)
    wr = 0;
    `TEST_TOGGLE_CLK;

    wr = 1;
    `TEST_WHEN(omiss==0, addr0 = 16;)
    wr = 0;
    `TEST_TOGGLE_CLK;
    
    wr = 1;
    data = 64'h22222222_22222222;
    `TEST_WHEN(omiss==0, addr0 = 12;)
    wr = 0;
    `TEST_TOGGLE_CLK;
    
    `TEST_DISPLAYF(("\nReads at %0d...", $time));
    `TEST_WHEN(omiss==0,
      addr0 = 8;
      `TEST_EQ(odata, 64'h22222222_11111111);
    )
    `TEST_WHEN(omiss==0,
      addr0 = 10;
      `TEST_EQ(odata, 64'h22222222_22221111);
    )
    `TEST_WHEN(omiss==0,
      addr0 = 12;
      `TEST_EQ(odata, 64'h22222222_22222222);
    )
    `TEST_WHEN(omiss==0,
      addr0 = 14;
      `TEST_EQ(odata, 64'h11112222_22222222);
    )
    `TEST_WHEN(omiss==0,
      addr0 = 16;
      `TEST_EQ(odata, 64'h11111111_22222222);
    )

    `TEST_DISPLAY("\n>> BEGIN WORD TESTS<<\n");
    opsz = 1;
    
    `TEST_DISPLAYF(("Writes at %0d...", $time));
    wr = 1;
    data = 64'h3333;
    `TEST_WHEN(omiss==0, addr0 = 13;)
    wr = 0;
    `TEST_TOGGLE_CLK;

    wr = 1;
    data = 64'h4444;
    `TEST_WHEN(omiss==0, addr0 = 14;)
    wr = 0;
    `TEST_TOGGLE_CLK;

    wr = 1;
    data = 64'h5555;
    `TEST_WHEN(omiss==0, addr0 = 15;)
    wr = 0;
    `TEST_TOGGLE_CLK;

    `TEST_DISPLAYF(("\nReads at %0d...", $time));
    opsz = 3;
    rd = 1;
    `TEST_WHEN(omiss==0,
      addr0 = 12;
      `TEST_EQ(odata, 64'h22222255_55443322);
    )

    `TEST_DISPLAY("\n>> BEGIN HIT ALIGNED WORD TESTS<<\n");
    `TEST_DISPLAYF(("Writes at %0d...", $time));
    rd = 0;
    opsz = 1;
    for(i=0; i<32; i=i+1) begin
      wr = 1;
      data = {~48'b0, i[7:0], i[7:0]};
      `TEST_WHEN(omiss==0, addr0 = i*2;)
      wr = 0;
      `TEST_TOGGLE_CLK;
    end

    `TEST_DISPLAYF(("\nReads at %0d...", $time));
    data = 0;
    rd = 1;
    for(i=0; i<32; i=i+1) begin
      `TEST_WHEN(omiss==0,
        addr0 = i*2;
        `TEST_EQ(odata16, {i[7:0], i[7:0]});
      ) 
    end



    `TEST_DISPLAY("\n>> BEGIN MASS ALIGNED QWORD TESTS<<\n");
    `TEST_DISPLAYF(("Writes at %0d...", $time));
    rd = 0;
    opsz = 3;
    for(i=0; i<64; i=i+1) begin
      wr = 1;
      data = {8{i[7:0]}};
      `TEST_WHEN(omiss==0, addr0 = i*8;)
      wr = 0;
      `TEST_TOGGLE_CLK;
    end

    `TEST_DISPLAYF(("\nReads at %0d...", $time));
    data = 0;
    rd = 1;
    for(i=0; i<64; i=i+1) begin
      `TEST_WHEN(omiss==0,
        addr0 = i*8;
        `TEST_EQ(odata, {8{i[7:0]}});
      ) 
    end

    `TEST_DISPLAY("\n>> BEGIN MASS MISALIGNED QWORD TESTS<<\n");
    `TEST_DISPLAYF(("Writes at %0d...", $time));
    rd = 0;
    opsz = 3;
    for(i=32; i<96; i=i+1) begin
      wr = 1;
      data = {8{i[7:0]}};
      `TEST_WHEN(omiss==0, addr0 = i*8 + 3;)
      wr = 0;
      `TEST_TOGGLE_CLK;
    end

    `TEST_DISPLAYF(("\nReads at %0d...", $time));
    data = 0;
    rd = 1;
    for(i=32; i<96; i=i+1) begin
      `TEST_WHEN(omiss==0,
        addr0 = i*8 + 3;
        `TEST_EQ(odata, {8{i[7:0]}});
      ) 
    end

    `TEST_DISPLAY("\n>> BEGIN MASS ALIGNED WORD TESTS<<\n");
    `TEST_DISPLAYF(("Writes at %0d...", $time));
    rd = 0;
    opsz = 1;
    for(i=200; i<255; i=i+1) begin
      wr = 1;
      data = {~48'b0, i[7:0], i[7:0]};
      `TEST_WHEN(omiss==0, addr0 = i*2;)
      wr = 0;
      `TEST_TOGGLE_CLK;
    end

    `TEST_DISPLAYF(("\nReads at %0d...", $time));
    data = 0;
    rd = 1;
    for(i=200; i<255; i=i+1) begin
      `TEST_WHEN(omiss==0,
        addr0 = i*2;
        `TEST_EQ(odata16, {i[7:0], i[7:0]});
      ) 
    end

    `TEST_DISPLAY("\n>> BEGIN MASS MISALIGNED WORD TESTS<<\n");
    `TEST_DISPLAYF(("Writes at %0d...", $time));
    rd = 0;
    opsz = 1;
    for(i=0; i<100; i=i+1) begin
      wr = 1;
      data = {~48'b0, i[7:0], i[7:0]};
      `TEST_WHEN(omiss==0, addr0 = i*2 + 1;)
      wr = 0;
      `TEST_TOGGLE_CLK;
    end

    `TEST_DISPLAYF(("\nReads at %0d...", $time));
    data = 0;
    rd = 1;
    for(i=0; i<64; i=i+1) begin
      `TEST_WHEN(omiss==0,
        addr0 = i*2 + 1;
        `TEST_EQ(odata16, {2{i[7:0]}});
      )
    end

    `TEST_DISPLAY("\n>> BEGIN MASS BYTE TESTS<<\n");
    `TEST_DISPLAYF(("Writes at %0d...", $time));
    rd = 0;
    opsz = 0;
    for(i=0; i<32; i=i+1) begin
      wr = 1;
      data = {~58'b0, i[7:0]};
      `TEST_WHEN(omiss==0, addr0 = i;)
      wr = 0;
      `TEST_TOGGLE_CLK;
    end

    `TEST_DISPLAYF(("\nReads at %0d...", $time));
    data = 0;
    rd = 1;
    for(i=0; i<32; i=i+1) begin
      `TEST_WHEN(omiss==0,
        addr0 = i;
        `TEST_EQ(odata8, i[7:0]);
      ) 
    end

    `TEST_DISPLAY("\n>> RETENTION MMIO TESTS <<\n");
    opsz = 2;
    rd = 1;
    `TEST_WHEN(omiss==0,
      addr0 = 32'h10000021;
      `TEST_EQ(hwdata, 32'hABCD0000);
      `TEST_EQ(odata32, 32'hABCD0000);
    )
    rd = 0;

    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_END;
    `TEST_DISPLAY("\n>> END TESTS <<\n");

    $finish;
  end
  
  initial begin
    $vcdplusfile("top.dump.vpd");
    $vcdpluson(0, TOP);
    $vcdpluson(0, dcache.wrDataAlign);
    $vcdpluson(0, dcache.wrBAlign);
    $vcdpluson(0, dcache.rdDataAlign);
    $vcdpluson(0, dcache.wrDataOrder);
    $vcdpluson(0, dcache.wrBOrder);
    $vcdpluson(0, dcache.rdAddr);
  end

endmodule
