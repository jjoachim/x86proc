// Bit Test and Set Module
module BTS(oSetBits, oCF, iBitBase, iBitOffset, iOPSZ);

    // I/O
    input   [31:0]  iBitBase;   // Bit base
    input   [31:0]  iBitOffset; // Bit offset
    input           iOPSZ;      // 16-bit or 32-bit?
    output  [31:0]  oSetBits;   // Bit output
    output          oCF;        // Carry flag
    
    wire    [ 4:0]  bSel;       // Selected bit
    wire    [31:0]  decSel;     // Decoded selected bit
    wire    [31:0]  decSelBar;  // Decoded selected bit
    
    // Mask off MSB of bSel for 16b iOPSZ
    and bSelHigh(bSel[4], iOPSZ, iBitOffset[4]);
    assign bSel[3:0] = iBitOffset[3:0];
    
    mux32$ oCFmux(oCF, iBitBase, bSel);
    decoder5_32$ dec(bSel, decSel, decSelBar);
    or2_32$ orout(oSetBits, decSel, iBitBase);

endmodule

// Exhaustive testing
module BTS_TB;

    reg     [31:0]  iBitBase;
    reg     [31:0]  iBitOffset;
    reg             iOPSZ;
    wire            oCF;
    wire    [31:0]  oSetBits;
    integer         i, j, k;

    // instantiate the 8-bit Kogge-Stone adder
    BTS bts(oSetBits, oCF, iBitBase, iBitOffset, iOPSZ);

    initial
     begin
        $dumpfile("test.vcd");
        $dumpvars(0,BTS_TB);
     end

    // exhaustive checking
    initial begin

        // loop through all possible cases and record the results
        for (i = 0; i < 50; i = i + 1) begin
            iBitBase = {$random} % 4294967296;
            iBitOffset = {$random} % 4294967296;
            iOPSZ = {$random} % 2;
              
            // compute and check the product
            #10 
            $display("iBitBase:   %B", iBitBase);
            $display("iBitOffset: %B %d", iBitOffset, iBitOffset[4:0]);
            $display("iOPSZ:    %B", iOPSZ);
            $display("oCF:      %B", oCF);
            $display("oSetBits: %B\n", oSetBits);
        end
     
    end

endmodule
