movl $0x00001000, %esp
movl $0x0, %edx

mov  $0x0c00, %ax
mov  %ax, %ds
mov  %ax, %es
mov  %ax, %fs
mov  %ax, %gs
mov  $0x0200, %ax
mov  %ax, %ss
movl $0xDEADBEEF, 100(%edx)

pushw 100(%edx)
push 100(%edx)

push %ax
push %eax

push %cs
push %ss
push %ds
push %es
push %fs
push %gs
push  $0x08
pushw $0x1616
pushl $0x32323232

mov  $0x0000, %ax
mov  %ax, %ds
mov  %ax, %es
mov  %ax, %fs
mov  %ax, %gs

pop %eax                  ;eax = 0x32323232
pop %ax                   ;eax = 0x32321616
pop %eax                  ;eax = 0x00000008
pop %gs
pop %fs
pop %es
pop %ds
pop %ss
pop %eax                  ;eax = 0x00000000

pop %eax                  ;eax = 0x00000200
movl $0x0, %ebx
pop %bx                   ;ebx = 0x00000200

movl $0x00000000, %eax
movl 100(%edx), %eax      ;eax = 0xdeadbeef

movl $0x00000000, %eax
pop 90(%edx)
movl 90(%edx), %eax       ;eax = 0xdeadbeef

movl $0x00000000, %eax
popw 94(%edx)
movw 94(%edx), %ax        ;eax = 0x0000beef

hlt
