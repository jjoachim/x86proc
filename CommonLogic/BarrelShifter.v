`ifndef _BARREL_SHIFTER_V_
`define _BARREL_SHIFTER_V_
`default_nettype none

/*****************************************************************************/
// BitBarrel L-Shifter
/*****************************************************************************/
module BitBarrelLS (oOut, iA, iShft);
  parameter selWidth = 4;
  parameter dataWidth = 1<<selWidth;

  input [dataWidth-1:0] iA;
  input [selWidth-1:0] iShft;
  output [dataWidth-1:0] oOut;

  //assign oOut = iA << iShft;
  genvar i;
  generate
    for(i=0; i<selWidth; i=i+1) begin : shftMux
      localparam shftAmnt = 1<<i;
      wire [dataWidth-1:0] out, in;
      if(i==0) begin
        assign in = {iA, 1'b0};
        mux2$ lvl [dataWidth-1:0]
          (out, iA, in, iShft[0]);
      end else begin
        assign in = {shftMux[i-1].out, {shftAmnt{1'b0}}};
        mux2$ lvl [dataWidth-1:0]
          (out, shftMux[i-1].out, in, iShft[i]);
      end
    end
    
    if(selWidth)
      assign oOut = shftMux[selWidth-1].out;
    else
      assign oOut = iA;
  endgenerate


endmodule

/*****************************************************************************/
// Barrel L-Shifter
/*****************************************************************************/
module BarrelLS (oOut, iA, iShft);
  parameter blockWidth = 8;
  parameter selWidth = 4;
  
  parameter numBlocks = 1<<selWidth;
  parameter dataWidth = numBlocks*blockWidth;

  input [dataWidth-1:0] iA;
  input [selWidth-1:0] iShft;
  output [dataWidth-1:0] oOut;
  
  wire [dataWidth-1:0] bytesIn, bytesOut;
  
  genvar i, j;
  generate
    for(i=0; i<blockWidth; i=i+1) begin : inbyte
      for(j=0; j<numBlocks; j=j+1) begin : inbit
        assign bytesIn[i*numBlocks+j] = iA[j*blockWidth+i];
        assign oOut[j*blockWidth+i] = bytesOut[i*numBlocks+j];
      end
    end

    for(i=0; i<blockWidth; i=i+1) begin : shftArr
      BitBarrelLS #(selWidth) byteShft
        (bytesOut[i*numBlocks +: numBlocks], bytesIn[i*numBlocks +: numBlocks], iShft);
    end

  endgenerate

endmodule

/*****************************************************************************/
// Barrel R-Shifter
/*****************************************************************************/
module BarrelRS (oOut, iA, iShft);
  parameter blockWidth = 8;
  parameter selWidth = 4;

  parameter numBlocks = 1<<selWidth;
  parameter dataWidth = numBlocks*blockWidth;
  
  input [dataWidth-1:0] iA;
  input [selWidth-1:0] iShft;
  output [dataWidth-1:0] oOut;
  
  wire [dataWidth-1:0] iAr, oOutr;
  genvar i;

  //reverse inputs to reuse L-Shifter
  generate
    for(i=0; i<dataWidth; i=i+1) begin : reverse
      assign iAr[i] = iA[dataWidth-i-1];
      assign oOut[i] = oOutr[dataWidth-i-1];
    end
  endgenerate

  BarrelLS #(blockWidth, selWidth) shft 
    (oOutr, iAr, iShft);

endmodule

`default_nettype wire
`endif
