`ifndef _DECODE_V_
`define _DECODE_V_

`include "Cache/Cache.v"
`include "CommonLogic/KSAdder32.v"
`include "CommonLogic/CommonLogic.v"
`include "Fetch/libASE.v"
`include "libext/defines.v"

`default_nettype none
// Used its own pc reg in order to calc the PC
module Decode (iFE_valid,iFE_EIP,iFE_CS,iFE_BrTaken,
	iFE_Data, iInt, iWB_Stat,iStat,
	oEIP,oCS,oSize, oRestOfInst, oStat, oControlStore,oEIP_to_Fetch,
	oMod_bits,oSIB_bits,oFSM,
	       iClk,inReset,iStall,iFlush
);
input [255:0] iFE_Data;
input iClk,inReset;
input iInt;
input iStall,iFlush;
input [31:0] iFE_EIP;
input [15:0] iFE_CS;
input iFE_valid;
input iFE_BrTaken;
input [31:0] iWB_Stat;
output [`CTRLSZ-1:0] oControlStore;
wire [63:0] decControlStore;
output [2:0] oFSM;
output [63:0] oRestOfInst;
output [31:0] oStat;
input [31:0] iStat;
output [31:0] oEIP_to_Fetch ;





wire  ExceptionSig;

wire except_int;
wire iInt_noRop;
wire rRopSig;
wire rRopSig_prime,rINT;
wire STARTINTERRUPT;
wire ExceptionSig_prime;
wire Interrupt,Interrupt_not_exc,mInterrupt;
wire [31:0] iWB_Stat_prime;
inv1$ inverWBstat [31:0]  (iWB_Stat_prime,iWB_Stat);

or2$ intor_asdf(Interrupt,iInt,rINT);
nand3$ int_or2222 (Interrupt_not_exc,ExceptionSig_prime,rRopSig_prime,Interrupt);
mux2$  muxinterr  (mInterrupt,1'b0,Interrupt,Interrupt_not_exc);
//mux2$  muxinterr  (mInterrupt,iWB_Stat_prime[`iINT],Interrupt,Interrupt_not_exc);
inv1$  ivert_enable_int (STARTINTERRUPT,Interrupt_not_exc);


// STAT AND EXC
wire [31:0] wStat;
wire [31:0] zeros;
assign zeros = 32'b0;
//Exception Signal coming from Fetch
// If WB is coming, erate it, no nesting
wire wEXC;
mux2$ muxEXC (wEXC,iStat[`iEXC],zeros[`iEXC],iWB_Stat[`iEXCMODE]);
or2$ exc_or(wStat[`iEXC] ,wEXC, STARTINTERRUPT);
// Exeption Vector passed through
// If WB is coming, erase it, no nesting
wire [`iEXCV] wEXCV;
mux2$ muxEXCV [`iEXCV] (wEXCV,iStat[`iEXCV],zeros[`iEXCV],iWB_Stat[`iEXC]);
assign wStat[`iEXCV] =iStat[`iEXCV];
// Interrupt is always zero for now
assign wStat[`iINT] = STARTINTERRUPT;
// Tells if Exception is coming from WB
assign wStat[`iEXCMODE] = iWB_Stat[`iEXCMODE];
// Getting the Exception / Interrupt vector to be handled at the moment
// Stuff Comes from Writeback
wire [`iHEXCV] exception_vector;
wire [`iEXCV] iWB_ExcVector;
assign iWB_ExcVector=iWB_Stat[`iEXCV];
mux2_8$ mux_exc_vect(exception_vector,8'h10,iWB_ExcVector,iWB_Stat[`iEXCMODE]);
assign wStat[`iHEXCV] =exception_vector;
// Garbage
assign wStat[`iRSVD] =iStat[`iRSVD];





assign ExceptionSig=iWB_Stat[`iEXC];
inv1$ rRop_inv (rRopSig_prime,rRopSig);
and2$ and_inter      (iInt_noRop,rRopSig_prime,iInt);
or2$  exception_orgate (except_int,ExceptionSig,iInt_noRop);




// WHAT THE HELL IS EXCMODE?? need to add that??
// i guess its a register
output [31:0] oEIP;
output [15:0] oCS;
output [3:0] oSize;
output [7:0] oSIB_bits,oMod_bits;
// My regs used as input and output
wire [31:0]		rEIP;
wire [15:0]		rCS;
wire [3:0] 		rSize;
wire iStallp;
inv1$ invert_stall (iStallp,iStall);
wire iFlushp;
inv1$ invert_flush (iFlushp,iFlush);

//  INT SIGNALS

inv1$ except_inv (ExceptionSig_prime,ExceptionSig);


  wire [31:0] muxEIP,muxEIP_Fetch;
  wire [31:0] adderEIP,adderEIP_plus1,iFE_EIP_plus1;
KSAdder32NC add_EIPg
  (adderEIP, rEIP, {28'b0, rSize});
KSAdder32NC add_EIPgp1
  (adderEIP_plus1, rEIP, {28'b1, 4'b0});
mux2$ EIPsel [31:0]	
  (muxEIP,adderEIP,iFE_EIP,iFE_BrTaken);

KSAdder32NC add_EIP_fetch_p1
  (iFE_EIP_plus1, iFE_EIP, {28'b1, 4'b0});

wire accum_overflow;
wire Overflow;
wire rAccumOverflow;
or2$ overflowor (Overflow,accum_overflow,rAccumOverflow);
mux4$ EIPsel_for_Fetch [31:0]		
  (muxEIP_Fetch,rEIP,adderEIP_plus1,iFE_EIP,iFE_EIP_plus1,Overflow,iFE_BrTaken);
  //(muxEIP_Fetch,rEIP,adderEIP_plus1,iFE_EIP_plus1,iFE_EIP_plus1,Overflow,iFE_BrTaken);
  //(muxEIP_Fetch,rEIP,adderEIP_plus1,iFE_EIP,iFE_EIP,Overflow,iFE_BrTaken);



// Select PC based on Branch
  wire [3:0] calcSize,calcSize_rom;




//Select Size
// Accumulator
  //When the accumulator generate carry, enable shift signal register for next cycle/instruction until cache hits

wire [4:0] selectSize;
wire [3:0] rAccum;
// this follwoing mux might be wrong
wire [ 2:0] FSM_state;
wire [2:0] FSM_state_prime;
wire flush_ctrl,flush_inc,flush_rop;
wire flush_init_ctrl,flush_init_rop,flush_init_inc;
wire load_ctrl,load_inc,load_rop;


inv1$ FSM_prime [2:0] (FSM_state_prime,FSM_state);
ase_mux2_5bit mux_accum_4(selectSize, {1'b0,rAccum}, {1'b0,iFE_EIP[3:0]},iFE_BrTaken);
wire [3:0] shift_accum;
// accumlator logic and the mux to reset accumulator wehn done
accumulator size_accum (shift_accum,calcSize,selectSize[3:0],accum_overflow);

//wire [3:0] shift_accum_mux;
//mux2$ mux_shift_accum [3:0] (shift_accum_mux,shift_accum,iFE_EIP[3:0],iFE_BrTaken);


// THE DECODE LOGIC IS HERE
//DECODE SHIFTER
wire [255:0]shiftedData;
barrel_256_8 shift_data256 (shiftedData,iFE_Data,selectSize);


// get the bytes in diff slots
wire [127:0] Bytes16ForDecode;
assign Bytes16ForDecode=shiftedData[255:128];
wire [7:0] byte0,byte1,byte2,byte3,byte4,byte5,byte6,byte7,byte8,byte9,byte10,byte11,byte12,byte13,byte14,byte15;
assign byte0=Bytes16ForDecode[127:120];assign byte1=Bytes16ForDecode[119:112];assign byte2=Bytes16ForDecode[111:104];
assign byte3=Bytes16ForDecode[103:96];assign byte4=Bytes16ForDecode[95:88];assign byte5=Bytes16ForDecode[87:80];
assign byte6=Bytes16ForDecode[79:72];assign byte7=Bytes16ForDecode[71:64];assign byte8=Bytes16ForDecode[63:56];
assign byte9=Bytes16ForDecode[55:48];assign byte10=Bytes16ForDecode[47:40];assign byte11=Bytes16ForDecode[39:32];
assign byte12=Bytes16ForDecode[31:24];assign byte13=Bytes16ForDecode[23:16];assign byte14=Bytes16ForDecode[15:8];
assign byte15=Bytes16ForDecode[7:0];



wire [2:0] prefix_one_hot;
wire [1:0] prefix_num;
wire [2:0] encoded_segment;
wire encoded_rep;
prefix_check prefix_checker(byte0,byte1,byte2,prefix_one_hot,encoded_rep,encoded_segment,prefix_num);
wire opsPFX,segPFX,repPFX;
assign opsPFX= prefix_one_hot[0];
assign segPFX= prefix_one_hot[1];
assign repPFX= prefix_one_hot[2];
//OFFSET 
//Prefix+Opcode extra byte offseted by 1 (since it is used for mod)
wire [1:0] adjusted_prefix;
wire coutha1_grabage,coutha2_garbage;
ase_ha ha1 ({coutha1_grabage,adjusted_prefix[0]},prefix_num[0],1'b1);
ase_ha ha2 ({coutha2_garbage,adjusted_prefix[1]},prefix_num[1],prefix_num[0]);
// mux to select which byte is the potential second bute
wire [7:0]potential_1st_byte;
mux4_8$ mux_adjusted_pfx(potential_1st_byte, byte0, byte1, byte2, byte3,prefix_num[0], prefix_num[1]);
wire extendOPC;

compare_8 compare2ndByteg (extendOPC,potential_1st_byte,8'h0F);

wire [1:0] modoffset_comp;
//DECODE
wire [`CTRLSZ-`CSSZ-1:0] additionalControl;
wire [63:0] RestOfInst;
ase_mux2_2bit mux_offset(modoffset_comp,prefix_num,adjusted_prefix,extendOPC);
wire [7:0]OPcode;
wire [5:0] newRop;
assign newRop = oControlStore[`iIROP];
decoder_full  decoder_full(prefix_num,prefix_one_hot,Bytes16ForDecode,decControlStore,OPcode,newRop,rRopSig,ExceptionSig,ExceptionSig);
wire [63:0] rRestOfInst;
  assign oRestOfInst=rRestOfInst;

wire sib_present;
//MODRM
wire cs_mod_bit,cs_rop_bit;
assign cs_mod_bit=decControlStore[`iUSEMODRM];
assign cs_rop_bit=decControlStore[`iROP];
wire[7:0] SIB_byte, Mod_byte;
wire [1:0] dispoffset_comp;
modrm_full modrm_unit (modoffset_comp,dispoffset_comp,byte0,byte1,byte2,byte3,byte4,byte5,cs_mod_bit,sib_present,SIB_byte,Mod_byte); 
// IMM LOGIC
//SIZE CALC
wire [1:0] imm_compensated ;
//ase, will use saddams bit i guess, assign imm_compensated[1]= csbits_out[imm1_csbit];
		//opsize should only be on wehn the mode is 32 bit, otherwise insturction is bad
//ase mux2$ mux2_inv_imm (imm_compensated[0],csbits_out[imm0_csbit],1'b0,prefix_out[0]);//might need to and prefix_out[0] (opsize) with the imm1cs as a sanity check (if the instructions are perfect, should never happen.
//saddams imlemenation
wire [1:0] sex_mux_out;
//wire [1:0] opz_mux_out;
wire [1:0] cs_iopszovr,cs_iopsz;
wire  cs_isextimm,cs_iuseimm,cs_iopszovrs;
assign cs_iopszovr=decControlStore[`iOPSZOVR];
assign cs_iopsz=decControlStore[`iOPSZ];
assign cs_isextimm=decControlStore[`iSEXTIMM];
assign cs_iuseimm=decControlStore[`iUSEIMM];
assign cs_iopszovrs=decControlStore[`iOPSZOVRS];
//mux2_2$ mux_comp (opz_mux_out,cs_iopsz,cs_iopszovr,cs_iopszovrs);
//mux2_2$ mux_comp2 (sex_mux_out,opz_mux_out,2'b00,cs_isextimm);
//mux2_2$ mux_comp2 (sex_mux_out,cs_iopsz,2'b00,cs_isextimm);
//mux4$ mux_comp3 [1:0] (imm_compensated,2'b11,2'b11,sex_mux_out,2'b01,opsPFX,cs_iuseimm);


mux4$ mux_comp3 [1:0] (sex_mux_out,2'b11,2'b11,cs_iopsz,2'b01,opsPFX,cs_iuseimm);
mux2_2$ mux_comp2 (imm_compensated,sex_mux_out,2'b00,cs_isextimm);

// SIZE ROM


wire [10:0] fused_size_bits;
rom128x4 rom_size(fused_size_bits,{sib_present,modoffset_comp,imm_compensated,dispoffset_comp});


assign calcSize_rom=fused_size_bits[10:7];


wire jmp_ptr,call_ptr;
wire [3:0] size_jmp_ptr;

compare_8 comp_jmp_ptr  (jmp_ptr,OPcode,8'hEA);
mux2$ muxjmp_ptr [3:0] (size_jmp_ptr,4'h5,4'h7,opsPFX);
compare_8 comp_call_ptr  (call_ptr,OPcode,8'h9A);
// F is illegal!!!
mux4$ mux_size [3:0] (calcSize,calcSize_rom,size_jmp_ptr,4'h7,4'hF,jmp_ptr,call_ptr);

//assign calcSize=calcSize_rom;


wire [2:0] disp_select;
assign disp_select=fused_size_bits[6:4];
ase_mux8_64bit restmux(RestOfInst,Bytes16ForDecode[119:56],Bytes16ForDecode[111:48],Bytes16ForDecode[103:40],Bytes16ForDecode[95:32],Bytes16ForDecode[87:24],Bytes16ForDecode[79:16],64'hFADEFADEFADEFADE,64'hFADEFADEFADEFADE,disp_select);

wire [7:0] rMod_bits,rSIB_bits;

  //PC REG CSR from PC REG FE
  assign oEIP=rEIP;
  assign oCS=rCS;
  assign oSize=rSize;
  assign oMod_bits=rMod_bits;
  assign oSIB_bits=rSIB_bits;



// ROP
PipeRegs #(1) Ropreg
  (rRopSig,decControlStore[`iROP],load_ctrl,flush_inc,iClk,inReset);
// Incremental
PipeRegs #(4) Sizereg
  (rSize, calcSize, load_inc,flush_inc, iClk, inReset);
PipeRegs #(4) accum_reg
  (rAccum,shift_accum,load_inc,flush_inc,iClk,inReset);
// POINTERS
PipeRegs #(16) CSreg
  (rCS, iFE_CS, load_inc,1'b1, iClk, inReset);
  wire load_EIP_or_ROPen;
  or2$ EIP_enable (load_EIP_or_ROPen,load_inc,iFE_BrTaken);
  wire load_EIP_and_not_rop;
  and2$ EIP_fetch_enable (load_EIP_and_not_rop, load_EIP_or_ROPen, rRopSig_prime);
PipeRegs #(32) EIPreg
  (rEIP, muxEIP, load_EIP_or_ROPen,1'b1, iClk, inReset);
  wire [31:0] rEIP_to_Fetch;
PipeRegs #(32) EIPreg_jeremy
  (rEIP_to_Fetch, muxEIP_Fetch, load_EIP_and_not_rop,1'b1, iClk, inReset);
  assign oEIP_to_Fetch = rEIP_to_Fetch;
  // CONTROL
PipeRegs #(8) modbyte_g
  (rMod_bits, Mod_byte, load_inc,flush_inc, iClk, inReset);
PipeRegs #(8) sibbyte_g
  (rSIB_bits, SIB_byte, load_inc,flush_inc, iClk, inReset);
PipeRegs #(64) rest_instr_mux
  //(rRestOfInst,RestOfInst,rop_keep,flush_ctrl,iClk,inReset);
(rRestOfInst,RestOfInst,load_inc,flush_inc,iClk,inReset);
assign additionalControl = {opsPFX,segPFX,repPFX,encoded_segment,OPcode[2:0],sib_present};
  wire [`CTRLSZ-1:0] ibControlStore;
  //assign ibControlStore[`CTRLSZ-1:`CSSZ-1]=additionalControl;
  assign ibControlStore[`iOPSZOVRPRE]= opsPFX;
  assign ibControlStore[`iSEGOVRPRE]= segPFX;
  assign ibControlStore[`iREPEPRE]= repPFX;
  assign ibControlStore[`iSEGOVR]= encoded_segment;
  assign ibControlStore[`iRPLUS]= OPcode[2:0];
  assign ibControlStore[`iUSESIB]= sib_present;
  assign ibControlStore[`CSSZ-1:0]=decControlStore[`CSSZ-1:0];

  wire [`CSSZ-1:0] rControlStore_cs,ibControlStore_cs;
  wire [`CTRLSZ-`CSSZ-1:0] rControlStore_add,ibControlStore_add;

  assign ibControlStore_add[`iOPSZOVRPRE-`CSSZ]= opsPFX;
  assign ibControlStore_add[`iSEGOVRPRE-`CSSZ]= segPFX;
  assign ibControlStore_add[`iREPEPRE-`CSSZ]= repPFX;
  assign ibControlStore_add[-`CSSZ+`iSEGOVR-`CSSZ]= encoded_segment;
  assign ibControlStore_add[-`CSSZ+`iRPLUS-`CSSZ]= OPcode[2:0];
  assign ibControlStore_add[`iUSESIB-`CSSZ]= sib_present;
  assign ibControlStore_cs[`CSSZ-1:0]=decControlStore[`CSSZ-1:0];
PipeRegs #(`CSSZ) controlStoreBitsReg_cs
  (rControlStore_cs,ibControlStore_cs,load_ctrl,flush_ctrl,iClk,inReset);
PipeRegs #(`CTRLSZ-`CSSZ) controlStoreBitsReg_additional
  (rControlStore_add,ibControlStore_add,load_inc,flush_rop,iClk,inReset);
  wire [`CTRLSZ-1:0] oControlStore_concat;
  assign oControlStore_concat ={rControlStore_add,rControlStore_cs};
  assign oControlStore=oControlStore_concat;
// Flag
PipeRegs #(1) accum_reg_overflow
  (rAccumOverflow,accum_overflow,load_EIP_and_not_rop,flush_inc,iClk,inReset);

// ALWAYS
PipeRegs #(1) int_reg
  (rINT,mInterrupt,1'b1,1'b1,iClk,inReset);
PipeRegs #(32) statregs
(oStat, wStat, load_ctrl, iFlushp, iClk, inReset);
//FSM




  //Master FSM
  //localparam stateWidth = 3;
  localparam numStates = 17;
  localparam iStateWidth = 8;
  localparam oStateWidth = 9;
  wire [oStateWidth-1:0] FSM_nstate;

  wire [numStates*iStateWidth-1:0] conditions, mask;
  assign conditions = 
    {
     8'b00xxx_000,8'b01xxx_000,8'b1x0x0_000,8'b1x1x0_000,8'b1xxx1_000,
     8'b00xxx_001,8'b01xxx_001,8'b1x0x0_001,8'b1x1x0_001,8'b1xxx1_001,
     8'bxxxxx_011,
     8'b00xx0_010,8'b01xxx_010,8'b1x0x0_010,8'b1x1x0_010,8'b1xxx1_010,8'b00xx1_010
     };

  assign mask = 
    {
     8'b11000_111,8'b11000_111,8'b10101_111,8'b10101_111,8'b10001_111,
     8'b11000_111,8'b11000_111,8'b10101_111,8'b10101_111,8'b10001_111,
     8'b00000_111,
     8'b11001_111,8'b11000_111,8'b10101_111,8'b10101_111,8'b10001_111,8'b11001_111
     };
  
  wire [numStates*oStateWidth-1:0] outputs;
  assign outputs = 
    {
    9'b001010_000,9'b000000_000,9'b111111_001,9'b111111_011,9'b111111_010,
    9'b001010_000,9'b000000_000,9'b111111_001,9'b111111_011,9'b111111_010,
    9'b001010_000,
    9'b111011_000,9'b111011_000,9'b111011_000,9'b111011_011,9'b111011_010,9'b111011_010
     };

  wire [iStateWidth-1:0] FSM_input;
assign FSM_input = {iFE_valid, iFE_BrTaken,accum_overflow, except_int, cs_rop_bit, FSM_state};
  
  FSMLogic #(oStateWidth, iStateWidth, numStates) fsmlogic
    (FSM_nstate, FSM_input, conditions, mask, outputs);






//
//
//always@(*) begin
//	casex(FSM_inputs)
//	8'b00xxx_000: FSM_nstate <= #2 9'b001010_000;  //Wait to Wait
//	8'b01xxx_000: FSM_nstate <= #2 9'b000000_000;  //Wait to Wait/Br
//	//8'b1xxx0_000: FSM_nstate <= #2 9'b001010_001;  //Wait to Stream
//	8'b1x0x0_000: FSM_nstate <= #2 9'b111111_001;  //Wait to Stream
//	8'b1x1x0_000: FSM_nstate <= #2 9'b111111_011;  //Wait to Wait2
//	8'b1xxx1_000: FSM_nstate <= #2 9'b111111_010;  //Wait to ROP
//
//	8'b00xxx_001: FSM_nstate <= #2 9'b001010_000;  //Stream to Wait
//	8'b01xxx_001: FSM_nstate <= #2 9'b000000_000;  //Stream to Wait/Br
//	8'b1x0x0_001: FSM_nstate <= #2 9'b111111_001;  //Stream to Stream
//	8'b1x1x0_001: FSM_nstate <= #2 9'b111111_011;  //Stream to Wait2
//	8'b1xxx1_001: FSM_nstate <= #2 9'b111111_010;  //Stream to ROP
//
//	8'bxxxxx_011: FSM_nstate <= #2 9'b001010_000;  //Wait2 to Wait
//
//	8'b00xx0_010: FSM_nstate <= #2 9'b111011_000;  //ROP to Wait     // added a zero at the end instead of x to let rops go
//	8'b01xxx_010: FSM_nstate <= #2 9'b111011_000;  //ROP to Wait/Br
//  //8'b1x0x0_010: FSM_nstate <= #2 9'b111111_001;  //ROP to Stream
//	8'b1x0x0_010: FSM_nstate <= #2 9'b111011_000;  //ROP to Stream
//	8'b1x1x0_010: FSM_nstate <= #2 9'b111011_011;  //ROP to Wait2
//	8'b1xxx1_010: FSM_nstate <= #2 9'b111011_010;  //ROP to ROP
//	8'b00xx1_010: FSM_nstate <= #2 9'b111011_010;  //ROP to ROP    // to see if invalid will work with rop
//
//	default: FSM_nstate <= #2 9'bxxxxxxxxx;
//endcase
//end
//
wire[1:0]  ctrlEn,incEnb,ropEnb;
// rop and inc go hand in hand except in rop, then rop takes precednce and keeps/

assign {ctrlEn,incEnb, ropEnb } = FSM_nstate[8:3];
and2$ andbubble [2:0] ({flush_init_ctrl,flush_init_inc,flush_init_rop},{ctrlEn[1],incEnb[1],ropEnb[1]},iFlushp);
mux2$ muxbubble [2:0] ({flush_ctrl,flush_inc,flush_rop},{flush_init_ctrl,flush_init_inc,flush_init_rop},1'b1,iStall);
and2$ andload   [2:0] ({load_ctrl,load_inc,load_rop},{ctrlEn[0],incEnb[0],ropEnb[0]},iStallp);
PipeRegs #(3) fsm_state (FSM_state, FSM_nstate[2:0], iStallp, iFlushp, iClk, inReset);
assign oFSM=FSM_state[2:0];

endmodule





















module decoder_full (iPrefixNum,iPrefixEncoded,iInstLine,oControlStore,oOPC_byte0,inew_uop,iUop,iExceptionSig,iInt);
input [5:0] inew_uop;
input [1:0] iPrefixNum;

input [2:0] iPrefixEncoded;
input iExceptionSig;
output [7:0] oOPC_byte0;
input iUop,iInt;
input [127:0] iInstLine;
parameter control_store_bit_size=64;
output [control_store_bit_size-1:0] oControlStore;
wire [7:0] byte0,byte1,byte2,byte3,byte4,byte5,byte6,byte7,byte8,byte9,byte10,byte11,byte12,byte13,byte14,byte15;
assign byte0=iInstLine[127:120];assign byte1=iInstLine[119:112];assign byte2=iInstLine[111:104];
assign byte3=iInstLine[103:96];assign byte4=iInstLine[95:88];assign byte5=iInstLine[87:80];
assign byte6=iInstLine[79:72];assign byte7=iInstLine[71:64];assign byte8=iInstLine[63:56];
assign byte9=iInstLine[55:48];assign byte10=iInstLine[47:40];assign byte11=iInstLine[39:32];
assign byte12=iInstLine[31:24];assign byte13=iInstLine[23:16];assign byte14=iInstLine[15:8];
assign byte15=iInstLine[7:0];




wire [control_store_bit_size-1:0] ControlStore0,ControlStore1, ControlStore2, ControlStore3;
decoder_slice decSlice0(byte0,byte1,ControlStore0,inew_uop,iUop,iExceptionSig,iInt);
decoder_slice decSlice1(byte1,byte2,ControlStore1,inew_uop,iUop,iExceptionSig,iInt);
decoder_slice decSlice2(byte2,byte3,ControlStore2,inew_uop,iUop,iExceptionSig,iInt);
decoder_slice decSlice3(byte3,byte4,ControlStore3,inew_uop,iUop,iExceptionSig,iInt);


ase_mux4_64bit mux_control_store(oControlStore,ControlStore0,ControlStore1,ControlStore2,ControlStore3,iPrefixNum[0],iPrefixNum[1]);
mux4$        mux_byte0  [7:0]  (oOPC_byte0,byte0,byte1,byte2,byte3,iPrefixNum[0],iPrefixNum[1]);





endmodule



module decoder_slice (byte0,byte1,csbits_out,iNew_uop,in_Uop,exception_sig,iInt);
parameter control_store_bit_size=64;
input in_Uop,iInt;
input [7:0] byte0,byte1;
input [5:0] iNew_uop;
output [control_store_bit_size-1:0] csbits_out;
input exception_sig;
wire opc_size;
wire [control_store_bit_size-1:0] out_byte0a_rom0,out_byte0b_rom0,out_risc;

rom_byte0_rom256x64 first_byte_rom0(out_byte0a_rom0,byte0);
rom_byte1_rom128x64 second_byte_rom0(out_byte0b_rom0,byte1[6:0]);

wire [6:0] input_to_risc;
wire [1:0] src_reg_bits;
assign src_reg_bits = byte1[5:4];
mux2$ mux_rop_ff [6:0] (input_to_risc,{5'b0,src_reg_bits},{1'b0,iNew_uop},in_Uop);

risc_rom128x64 RISC_byte_rom0(out_risc,input_to_risc);// add byte select, need to look more into it/
					// state machined too
compare_8 comp_OF  (opc_size,byte0,8'h0F);
wire specialFF;
compare_8 comp_FF  (specialFF,byte0,8'hFF);

wire exception_or_int;
wire [63:0] excint_rom;
wire [63:0] exception_op,interrupt_op;
assign exception_op =  64'h02b5600002000100;
assign interrupt_op =  64'h02b5600002000100;

ase_mux2_64bit  muxfor_exint (excint_rom,interrupt_op,exception_op,exception_sig);
or2_mux$ or_opc  (exception_or_int,iInt,exception_sig);
wire [63:0] csbits_opc;
ase_mux2_64bit opcmux (csbits_opc,out_byte0a_rom0,out_byte0b_rom0,opc_size);
wire index_to_rop;
or2_mux$  mux_risc   (index_to_rop,specialFF,in_Uop);
ase_mux4_64bit set1mux(csbits_out,csbits_opc,out_risc,excint_rom,excint_rom,index_to_rop,exception_or_int);

endmodule


module check_risc(out,in);
//dummy needs fixin


//this dummy will most likely be a rom, 32 inputs.  5 bits addressing
// will be adressed by part of opcode (1 or 2 bits) other 3 are used for pointer / modrm based on 
// next uop flag
//
input [7:0] in;
assign out =1'b0;
output out;
endmodule


module modrm_full (iMod_offset,omoddisp_compensated,byte0,byte1,byte2,byte3,byte4,byte5,iMod_csbit,oSIB_check,oSIB_bits,oMod_bits);  // will add other modrm functionality, as in calc
input [7:0] byte0,byte1,byte2,byte3,byte4,byte5;
input [1:0] iMod_offset;
input  iMod_csbit;
output oSIB_check;
output [7:0] oSIB_bits;
output [7:0] oMod_bits;
output [1:0] omoddisp_compensated;

wire sib_check0,sib_check1,sib_check2,sib_check3;
modrm_slice slice0(byte1,sib_check0);  // will add other modrm functionality, as in calc
modrm_slice slice1(byte2,sib_check1);  // will add other modrm functionality, as in calc
modrm_slice slice2(byte3,sib_check2);  // will add other modrm functionality, as in calc
modrm_slice slice3(byte4,sib_check3);  // will add other modrm functionality, as in calc


wire [1:0] omoddisp_compensated;
wire nodea0,nodea1,nodeb0,nodeb1;
wire [7:0]oMod_bits_p;
inv1$ invermodbit [7:0] (oMod_bits_p,oMod_bits);
nand2$ gate0a(nodea0,oMod_bits[6],iMod_csbit);
nand2$ gate1a(nodea1,oMod_bits_p[7],iMod_csbit);

nand2$ gate0b(nodeb0,oMod_bits_p[6],iMod_csbit);
nand2$ gate1b(nodeb1,oMod_bits[7],iMod_csbit);
wire disp_only,don1,don2;
nand2$ disponly1(don1,oMod_bits_p[7],oMod_bits_p[6]);
nand3$ disponly2(don2,oMod_bits[2],oMod_bits_p[1],oMod_bits[0]);
	nor2$  disponly3(disp_only,don1,don2);

wire [1:0] omoddisp_compensated_normal;
nand2$ ormoddisp0(omoddisp_compensated_normal[0],nodea0,nodea1);
nand2$ ormoddisp1(omoddisp_compensated_normal[1],nodeb0,nodeb1);  //check these gates, will ya?
mux4$  muxdisp_only [1:0] (omoddisp_compensated,2'b0,2'b0,omoddisp_compensated_normal,2'b10,disp_only,iMod_csbit);
wire SIB_checkmaybe;
mux4_8$ mux_modbits (oMod_bits,byte1,byte2,byte3,byte4,iMod_offset[0],iMod_offset[1]);
mux4_8$ mux_sibbits (oSIB_bits,byte2,byte3,byte4,byte5,iMod_offset[0],iMod_offset[1]);
mux4$ mux_sib_check (SIB_checkmaybe,sib_check0,sib_check1,sib_check2,sib_check3,iMod_offset[0],iMod_offset[1]);
and2$ andssib(oSIB_check,SIB_checkmaybe,iMod_csbit);

endmodule





module modrm_slice (byte_in,sib_check);  // will add other modrm functionality, as in calc
input [7:0] byte_in;
output sib_check;//might need to add one to it for further logic
sib_check sib_checker(byte_in,sib_check);// can SIB be used without modrm???
endmodule


module risc_rom128x64(out,in);
input [6:0] in;
output [63:0] out;
wire [63:0] csbits0,csbits1,csbits2,csbits3,csbits4;


initial begin
	$readmemb("../rom/risc/risc_0.rom", risc_rom0.mem);
	$readmemb("../rom/risc/risc_1.rom", risc_rom1.mem);
	$readmemb("../rom/risc/risc_2.rom", risc_rom2.mem);
	$readmemb("../rom/risc/risc_3.rom", risc_rom3.mem);
end


rom64b32w$ risc_rom0(in[4:0],1'b1,csbits0);
rom64b32w$ risc_rom1(in[4:0],1'b1,csbits1);
rom64b32w$ risc_rom2(in[4:0],1'b1,csbits2);
rom64b32w$ risc_rom3(in[4:0],1'b1,csbits3);
//need to add buffers here and orangize it. these 2 muxes will fanout to a total of 8.
//a buffer of 4 is already in the mux, thats to buffer delays.
//better to put the buffer outside, so use on 8 buffer instead
//by extrapolation, the higher up u go, the bigger the buffer and the smaller the delay
//therefore just use a big buffer right from the shifter
ase_mux4_64bit   firstbyte_mux4_0(out,csbits0,csbits1,csbits2,csbits3,in[5],in[6]);

endmodule



module rom_byte1_rom128x64(out,in);
input [6:0] in;
output [63:0] out;
wire [63:0] csbits0,csbits1,csbits2,csbits3,csbits4;


initial begin
	$readmemb("../rom/risc/risc_0.rom", b1_rom0.mem);
	$readmemb("../rom/risc/risc_1.rom", b1_rom1.mem);
	$readmemb("../rom/risc/risc_2.rom", b1_rom2.mem);
	$readmemb("../rom/risc/risc_3.rom", b1_rom3.mem);
end


rom64b32w$ b1_rom0(in[4:0],1'b1,csbits0);
rom64b32w$ b1_rom1(in[4:0],1'b1,csbits1);
rom64b32w$ b1_rom2(in[4:0],1'b1,csbits2);
rom64b32w$ b1_rom3(in[4:0],1'b1,csbits3);
//need to add buffers here and orangize it. these 2 muxes will fanout to a total of 8.
//a buffer of 4 is already in the mux, thats to buffer delays.
//better to put the buffer outside, so use on 8 buffer instead
//by extrapolation, the higher up u go, the bigger the buffer and the smaller the delay
//therefore just use a big buffer right from the shifter
ase_mux4_64bit   firstbyte_mux4_0(out,csbits0,csbits1,csbits2,csbits3,in[5],in[6]);

endmodule




module rom_byte0_rom256x64(out,in);
input [7:0] in;
wire [7:0] byte0;
assign byte0=in[7:0];
//assign byte1=in[7:0];
output [63:0] out;
wire [63:0] csbits0,csbits1,csbits2,csbits3,csbits4,csbits5,csbits6,csbits7,csbits8,csbits1a,csbits2a;


initial begin
	$readmemb("../rom/cs_rom/control_store_0.rom", firstbyte_rom0.mem);
	$readmemb("../rom/cs_rom/control_store_1.rom", firstbyte_rom1.mem);
	$readmemb("../rom/cs_rom/control_store_2.rom", firstbyte_rom2.mem);
	$readmemb("../rom/cs_rom/control_store_3.rom", firstbyte_rom3.mem);
	$readmemb("../rom/cs_rom/control_store_4.rom", firstbyte_rom4.mem);
	$readmemb("../rom/cs_rom/control_store_5.rom", firstbyte_rom5.mem);
	$readmemb("../rom/cs_rom/control_store_6.rom", firstbyte_rom6.mem);
	$readmemb("../rom/cs_rom/control_store_7.rom", firstbyte_rom7.mem);
end


rom64b32w$ firstbyte_rom0(byte0[4:0],1'b1,csbits0);
rom64b32w$ firstbyte_rom1(byte0[4:0],1'b1,csbits1);
rom64b32w$ firstbyte_rom2(byte0[4:0],1'b1,csbits2);
rom64b32w$ firstbyte_rom3(byte0[4:0],1'b1,csbits3);
rom64b32w$ firstbyte_rom4(byte0[4:0],1'b1,csbits4);
rom64b32w$ firstbyte_rom5(byte0[4:0],1'b1,csbits5);
rom64b32w$ firstbyte_rom6(byte0[4:0],1'b1,csbits6);
rom64b32w$ firstbyte_rom7(byte0[4:0],1'b1,csbits7);
//need to add buffers here and orangize it. these 2 muxes will fanout to a total of 8.
//a buffer of 4 is already in the mux, thats to buffer delays.
//better to put the buffer outside, so use on 8 buffer instead
//by extrapolation, the higher up u go, the bigger the buffer and the smaller the delay
//therefore just use a big buffer right from the shifter
ase_mux4_64bit   firstbyte_mux4_0(csbits1a,csbits0,csbits1,csbits2,csbits3,byte0[5],byte0[6]);
ase_mux4_64bit   firstbyte_mux4_1(csbits2a,csbits4,csbits5,csbits6,csbits7,byte0[5],byte0[6]);
ase_mux2_64bit   firstbyte_mux4_2(out,csbits1a,csbits2a,byte0[7]);

endmodule


module rom_byte1_rom256x64(out,in);
input [7:0] in;
wire [7:0] byte0;
assign byte0=in[7:0];
//assign byte1=in[7:0];
output [63:0] out;
wire [63:0] csbits0,csbits1,csbits2,csbits3,csbits4,csbits5,csbits6,csbits7,csbits8,csbits1a,csbits2a;


initial begin
	$readmemb("../rom/cs_rom/control_store_0.rom", firstbyte_rom0.mem);
	$readmemb("../rom/cs_rom/control_store_1.rom", firstbyte_rom1.mem);
	$readmemb("../rom/cs_rom/control_store_2.rom", firstbyte_rom2.mem);
	$readmemb("../rom/cs_rom/control_store_3.rom", firstbyte_rom3.mem);
	$readmemb("../rom/cs_rom/control_store_4.rom", firstbyte_rom4.mem);
	$readmemb("../rom/cs_rom/control_store_5.rom", firstbyte_rom5.mem);
	$readmemb("../rom/cs_rom/control_store_6.rom", firstbyte_rom6.mem);
	$readmemb("../rom/cs_rom/control_store_7.rom", firstbyte_rom7.mem);
end


rom64b32w$ firstbyte_rom0(byte0[4:0],1'b1,csbits0);
rom64b32w$ firstbyte_rom1(byte0[4:0],1'b1,csbits1);
rom64b32w$ firstbyte_rom2(byte0[4:0],1'b1,csbits2);
rom64b32w$ firstbyte_rom3(byte0[4:0],1'b1,csbits3);
rom64b32w$ firstbyte_rom4(byte0[4:0],1'b1,csbits4);
rom64b32w$ firstbyte_rom5(byte0[4:0],1'b1,csbits5);
rom64b32w$ firstbyte_rom6(byte0[4:0],1'b1,csbits6);
rom64b32w$ firstbyte_rom7(byte0[4:0],1'b1,csbits7);
//need to add buffers here and orangize it. these 2 muxes will fanout to a total of 8.
//a buffer of 4 is already in the mux, thats to buffer delays.
//better to put the buffer outside, so use on 8 buffer instead
//by extrapolation, the higher up u go, the bigger the buffer and the smaller the delay
//therefore just use a big buffer right from the shifter
ase_mux4_64bit   firstbyte_mux4_0(csbits1a,csbits0,csbits1,csbits2,csbits3,byte0[5],byte0[6]);
ase_mux4_64bit   firstbyte_mux4_1(csbits2a,csbits4,csbits5,csbits6,csbits7,byte0[5],byte0[6]);
ase_mux2_64bit   firstbyte_mux4_2(out,csbits1a,csbits2a,byte0[7]);

endmodule



`default_nettype wire
`endif
