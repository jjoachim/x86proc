mov  $0x00000200, %ebx
mov  %bx, %ds
mov  $0x00000000, %ebx
movl $0xffffffff, %eax    ;eax = 0xffffffff

notb %al                  ;eax = 0xffffff00
notw %ax                  ;eax = 0xffff00ff
notl %eax                 ;eax = 0x0000ff00
movl %eax, (%ebx)         ;mem0= 0x0000ff00
notb (%ebx)               ;mem0= 0x0000ffff
notw (%ebx)               ;mem0= 0x00000000
notl (%ebx)               ;mem0= 0xffffffff
movl (%ebx), %eax         ;eax = 0xffffffff
hlt
