`ifndef _DMA_V_
`define _DMA_V_

`include "CommonLogic/CommonLogic.v"

`default_nettype none

module DMA(omData, omAddr, omRd, omWr, imData, imV,
           osV, osData, isData, isAddr, isRd, isWr,
           oIntDone, iClk, inReset);

//System Signals
input         iClk;
input         inReset;
output        oIntDone;

//Master Interface
output [31:0] omData;
output [31:0] omAddr;
output        omRd;
output [ 3:0] omWr;

input  [31:0] imData; //Never used since never reads
input         imV;

//Slave Interface
output        osV;
output [31:0] osData;

input  [31:0] isData;
input  [31:0] isAddr;
input         isRd;
input  [ 3:0] isWr;

//FSM outputs
wire        FSM_BusW;
wire [31:0] FSM_Addr;

//Bus outputs
assign omData = 32'hABCDABCD; //Just write garbage
assign omAddr = FSM_Addr;
assign omRd   = 0; //Never reads
assign omWr   = {4{FSM_BusW}}; //Always write in words

assign osV    = 1; //Always 1-cycle transactions
assign osData = 0; //Never reads data directly

//Source Address Register (where to get data)
wire        iSrcAddrWr;
wire [31:0] rSrcAddr;
regs$ #(32) srcaddr_reg
  (rSrcAddr, isData, iSrcAddrWr, iClk, inReset);

Compare #(3) cmp_srcAddr
  (iSrcAddrWr, {isWr[0], isAddr[3:2]}, 3'b100);

//Destination Address Register (where to put data)
wire        iDstAddrWr;
wire [31:0] rDstAddr;
regs$ #(32) dstaddr_reg
  (rDstAddr, isData, iDstAddrWr, iClk, inReset);

Compare #(3) cmp_dstAddr
  (iDstAddrWr, {isWr[0], isAddr[3:2]}, 3'b101);

//Size Register (how much data to transfer)
wire        iSizeWr;
wire [31:0] rSize;
regs$ #(32) size_reg
  (rSize, isData, iSizeWr, iClk, inReset);

Compare #(3) cmp_size
  (iSizeWr, {isWr[0], isAddr[3:2]}, 3'b110);

wire [31:0] endAddr; //go ahead and calculate this
KSAdder32NC end_adder (endAddr, rSize, rDstAddr);

//Start Register (start the transfer)
wire start;
Compare #(3) cmp_start
  (start, {isWr[0], isAddr[3:2]}, 3'b111);

//Address Counter
wire [31:0] cCount;
wire        nFSM_countRst;
Counter #(32) addr_counter
  (cCount, imV, iClk, nFSM_countRst);

KSAdder32NC currentAddr //write to this addr
  (FSM_Addr, {cCount[29:0], 2'b0}, rDstAddr);

wire        countDone;
Compare #(32) cmp_done
  (countDone, FSM_Addr, endAddr);

//Disk
reg diskDone;
always@(posedge iClk) begin
  if(inReset==0)    diskDone <= 0;
  else if(start==1) diskDone <= #750 1;
  //else              diskDone <= 0;
end

//FSM Logic
localparam stateWidth  = 2;
localparam numStates   = 7;
localparam inputWidth  = 5;
localparam outputWidth = 5;
wire [1:0] FSM_state;
wire [4:0] FSM_nstate;

wire [inputWidth-1:0] FSM_inputs;
assign FSM_inputs = {start, diskDone, countDone, FSM_state};

//FSM next state logic
wire [numStates*inputWidth-1:0] conditions, mask;
assign conditions =
  {5'b0xx00, 5'b1xx00,
   5'bx0x01, 5'bx1x01,
   5'bxx010, 5'bxx110,
   5'bxxx11};

assign mask = 
  {5'b10011, 5'b10011,
   5'b01011, 5'b01011,
   5'b00111, 5'b00111,
   5'b00011};

wire [numStates*outputWidth-1:0] outputs;
assign outputs = 
  {5'b10000, 5'b10001,
   5'b10001, 5'b10010,
   5'b01010, 5'b00011,
   5'b10100};

FSMLogic #(outputWidth, inputWidth, numStates) fsmlogic
  (FSM_nstate, FSM_inputs, conditions, mask, outputs);

/*
always@(*) begin
  casex(FSM_inputs)
   //'bSDC_s                     'bRWI_s
    5'b0xx_00: FSM_nstate <= #2 5'b100_00;
    5'b1xx_00: FSM_nstate <= #2 5'b100_01;
    
    5'bx0x_01: FSM_nstate <= #2 5'b100_01;
    5'bx1x_01: FSM_nstate <= #2 5'b100_10;
    
    5'bxx0_10: FSM_nstate <= #2 5'b010_10;
    5'bxx1_10: FSM_nstate <= #2 5'b000_11;

    5'bxxx_11: FSM_nstate <= #2 5'b101_00;
  endcase
end
*/

//FSM current state
regs$ #(2) fsm_state (FSM_state, FSM_nstate[1:0], 1'b1, iClk, inReset);

not1$ not_fsmcrst (nFSM_countRst, FSM_nstate[4]);
assign FSM_BusW = FSM_nstate[3];

wire FSM_Int;
assign FSM_Int = FSM_nstate[2];

//Interrupt register
wire nWr;
not1$ not_Wr (nWr, isWr[0]);
wire nintRst;
and2$ and_rst (nintRst, inReset, nWr);
regs$ int (oIntDone, 1'b1, FSM_Int, iClk, nintRst);

endmodule

`default_nettype wire
`endif
