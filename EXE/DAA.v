`ifndef _DAA_V_
`define _DAA_V_

`include "CommonLogic/KSAdder8.v"
`include "CommonLogic/LibExt.v"

/*****************************************************************************/
// Decimal Add and Adjust (daa)
/*****************************************************************************/
module DAA(oByte, oAF, oCF, iByte);
  output  [7:0] oByte;
  output        oAF;
  output        oCF;
  input   [7:0] iByte;

  /*
  CF_old = CF
  IF (al AND 0Fh > 9) or (the Auxilliary Flag is set)
    al = al+6
    CF = CF or CF_old
    AF set
  ENDIF
  IF (al > 99h) or (Carry Flag is set)
    al = al + 60h
    CF set
  ENDIF
  */

  wire [7:0] alP06;
  KSAdder8NC plus06 (alP06, {4'b0, iByte[3:0]}, 8'h06);
  assign oAF = alP06[4];

  wire [7:0] alP6X;
  KSAdder8 plus6X (alP6X, oCF, {iByte[7:4], 4'b0}, {3'b011, oAF, 4'b0}, 1'b0);
  
  wire [7:0] alP0X, carry;
  KSAdder8NC plus0X (alP0X, {iByte[7:4], 4'b0}, {3'b000, oAF, 4'b0});

  mux2$ mux_lnib [3:0] (oByte[3:0], iByte[3:0], alP06[3:0], oAF);
  mux2$ mux_hnib [3:0] (oByte[7:4], alP0X[7:4], alP6X[7:4], oCF);

endmodule

`endif
