`ifndef _PUNPCKHBW_VH
`define _PUNPCKHBW_VH

// 64-bit byte unpack and interleave
module PUNPCKHBW(y, src, dst);

	// I/O
	input	[63:0]	src;
	input	[63:0]	dst;
	output	[63:0]	y;

	// Pack and interleave
	assign y[63:56] = src[63:56];
	assign y[55:48] = dst[63:56];
	assign y[47:40] = src[55:48];
	assign y[39:32] = dst[55:48];
	assign y[31:24] = src[47:40];
	assign y[23:16] = dst[47:40];
	assign y[15: 8] = src[39:32];
	assign y[ 7: 0] = dst[39:32];

endmodule

`endif