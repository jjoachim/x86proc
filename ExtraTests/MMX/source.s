mov  $0x00000a00, %ecx
mov  %cx, %ds

mov  $0x0, %esi
mov  $0x0, %ecx

movl $0x11111111, 1(%esi, %ecx, 4)  ;mem1 = 0x11111111
movw $0x2222, 5(%esi, %ecx, 4)      ;mem5 = 0x2222
movw $0x2222, 7(%esi, %ecx, 4)      ;mem7 = 0x2222
movl $0x33333333, 9(%esi, %ecx, 4)  ;mem9 = 0x33333333

add  $0x1, %ecx                          ;ecx = 0x00000001

movq -3(%esi, %ecx, 4), %mm0             ;mm0 = 0x2222222211111111
movq  1(%esi, %ecx, 4), %mm1             ;mm1 = 0x3333333322222222
paddd  %mm0, %mm1                        ;mm1 = 0x5555555533333333
movq   %mm1, %mm2                        ;mm2 = 0x5555555533333333
movq   %mm0,  5(%esi, %ecx, 4)           ;mem9 = 0x2222222211111111
movq  5(%esi, %ecx, 4), %mm3             ;mm3 = 0x2222222211111111

punpckhbw %mm0, %mm2                     ;mm2 = 0x2255225522552255

hlt
