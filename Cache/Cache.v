`ifndef _CACHE_V_
`define _CACHE_V_

`include "Cache/CacheSlice.v"

`default_nettype none

/*****************************************************************************/
// DCache
/*****************************************************************************/
module DCache (oData, oAddr0, oAddr1, oExceptPF, oExceptWrP, oMiss, oPCD,
               iAddr0, iAddr1, iUA, iRd, iWrChk, iWr, //Generic inputs
               iDataW, iOpSz, iPCD, //Additional Write inputs
               ioBus, iClk, inReset);

  parameter busMasterID = 0;
  parameter totalSize = 512;
  parameter blockSize = 16;

  //Block size must be at least 64-bits for QWORD operations
  `STATIC_ASSERT((blockSize % 8) == 0, blockSize_divisible_by_8);
  //totalSize has to be divisible by blockSize
  `STATIC_ASSERT((totalSize % blockSize) == 0, totalSize_divisible_by_blockSize);

  parameter numBlocks = totalSize/blockSize;
  parameter blockBytes = blockSize;
  parameter blockBits = blockBytes*8;

  parameter byteAddrSize = $clog2(8);
  parameter byteAddrLSB = 0;
  parameter byteAddrMSB = byteAddrSize - 1;

  parameter qwordAddrSize = $clog2(blockSize) - byteAddrSize;
  parameter qwordAddrLSB  = byteAddrMSB + 1;
  parameter qwordAddrMSB  = qwordAddrLSB + qwordAddrSize - 1;

  parameter shiftSize = $clog2(blockSize) + 1;
  parameter shiftMSB = qwordAddrMSB;
  parameter sliceAddr = $clog2(blockSize);

  genvar i;

  inout [`BUS_MSB:0] ioBus;

  output [63:0] oData;
  output [31:0] oAddr0, oAddr1;
  output [1:0] oPCD;
  output oExceptPF, oExceptWrP, oMiss;

  input [63:0] iDataW;
  input [31:0] iAddr0, iAddr1;
  input [1:0] iOpSz, iPCD;
  input iUA, iRd, iWrChk, iWr, iClk, inReset;

  //Write Size Decode
  //BYTE (1B), SHORT (2B), DWORD (4B), QWORD (8B)
  wire [blockBytes-1:0] wrBNoAlign [1:0];

  mux4$ wrBDecodeMux [7:0]
    (wrBNoAlign[0][7:0], 8'b01, 8'h03, 8'h0F, 8'hFF, iOpSz[0], iOpSz[1]);
  assign wrBNoAlign[0][blockBytes-1:8] = 0;
  assign wrBNoAlign[1] = 0;

  //Write Bytes Alignment
  wire [blockBytes-1:0] wrBOrder [1:0];
  wire [blockBytes-1:0] wrBAlign [1:0];

  BarrelLS #(1, shiftSize) wrBAlignShft
    ({wrBAlign[1], wrBAlign[0]}, 
     {wrBNoAlign[1], wrBNoAlign[0]}, 
     {1'b0, iAddr0[shiftMSB:0]}
    );

  mux4$ muxWrBOrder0 [blockBytes-1:0]
    (wrBOrder[0], wrBAlign[0], wrBAlign[1], wrBNoAlign[0], wrBNoAlign[0],
     iAddr0[sliceAddr], iPCD[0]);
  mux4$ muxwrBOrder1 [blockBytes-1:0]
    (wrBOrder[1], wrBAlign[1], wrBAlign[0], 1'b0, 1'b0,
     iAddr0[sliceAddr], iPCD[0]);

  //Write Data Alignment
  wire [blockBits-1:0] wrDataOrder [1:0];
  wire [blockBits-1:0] wrDataAlign [1:0];

  BarrelLS #(8, shiftSize) wrDataAlignShft
    ({wrDataAlign[1], wrDataAlign[0]}, 
     {{(2*blockBits-64){1'b0}}, iDataW}, 
     {1'b0, iAddr0[shiftMSB:0]}
    );

  mux4$ muxWrDataOrder0 [blockBits-1:0]
    (wrDataOrder[0], wrDataAlign[0], wrDataAlign[1], 
     {{(blockBits-64){1'b0}}, iDataW}, 
     {{(blockBits-64){1'b0}}, iDataW},
     iAddr0[sliceAddr], iPCD[0]);
  mux4$ muxWrDataOrder1 [blockBits-1:0]
    (wrDataOrder[1], wrDataAlign[1], wrDataAlign[0], 1'b0, 1'b0,
     iAddr0[sliceAddr], iPCD[0]);

  //Read Data Alignment
  wire [blockBits-1:0] rdData [1:0];
  wire [blockBits-1:0] rdDataOrder [1:0];
  wire [blockBits-1:0] rdDataAlign [1:0];

  mux2$ muxDataOrder0 [blockBits-1:0]
    (rdDataOrder[0], rdData[0], rdData[1], iAddr0[sliceAddr]);
  mux2$ muxDataOrder1 [blockBits-1:0]
    (rdDataOrder[1], rdData[1], rdData[0], iAddr0[sliceAddr]);

  BarrelRS #(8, shiftSize) rdDataAlignShft
    ({rdDataAlign[1], rdDataAlign[0]}, 
     {rdDataOrder[1], rdDataOrder[0]}, 
     {1'b0, iAddr0[shiftMSB:0]}
    );

  mux2$ mux_odata [63:0]
    (oData, rdDataAlign[0][63:0], rdData[0][63:0], oPCD[0]); 

  //Input Addresses
  wire [31:0] rdAddr [2:0];

  wire pcdio;
  nor2$ nor_pcdio (pcdio, oPCD[0], iPCD[0]);
  mux4$ muxiAddrOrder0 [31:0] //reroute all PCD requests to slice[0]
    (rdAddr[0], iAddr0, iAddr0, iAddr0, iAddr1, iAddr0[sliceAddr], pcdio);
  mux2$ muxiAddrOrder1 [31:0]
    (rdAddr[1], iAddr1, iAddr0, iAddr0[sliceAddr]);

  //Input Signals
  wire rdAbort; //abort read to due bus write
  and2$ and_rdAbort (rdAbort, iWrChk, oPCD[0]);

  wire rdua, slice0FR, slice0FRua;
  and2$ and_fr (slice0FR, oPCD[0], iRd);
  and2$ and_rdua (rdua, iRd, iUA);
  or2$ or_slice0FRua (slice0FRua, rdua, slice0FR);

  wire [1:0] rd;
  mux4$ rdOrder0
    (rd[0], iRd, slice0FRua, 1'b0, 1'b0, iAddr0[sliceAddr], rdAbort);
  mux4$ rdOrder1
    (rd[1], rdua, iRd, 1'b0, 1'b0, iAddr0[sliceAddr], oPCD[0]);

  wire wrua;
  and2$ and_wrua (wrua, iWr, iUA);
  wire [1:0] wr;
  mux4$ wrOrder0
    (wr[0], iWr, wrua, iWr, iWr, iAddr0[sliceAddr], iPCD[0]);
  mux4$ wrOrder1
    (wr[1], wrua, iWr, 1'b0, 1'b0, iAddr0[sliceAddr], iPCD[0]);

  //Output Addresses
  wire [31:0] pAddr [1:0];

  mux4$ muxoAddrOrder0 [31:0]
    (oAddr0, pAddr[0], pAddr[1], pAddr[0], pAddr[0], iAddr0[sliceAddr], oPCD[0]);
  mux4$ muxoAddrOrder1 [31:0]
    (oAddr1, pAddr[1], pAddr[0], pAddr[0], pAddr[0], iAddr0[sliceAddr], oPCD[0]);

  //Output Signals
  wire [1:0] exceptPF;
  or2$ reduceExceptPF (oExceptPF, exceptPF[0], exceptPF[1]);
  wire [1:0] exceptWrP;
  wire orExceptWrP;
  or2$ reduceExceptWrP (orExceptWrP, exceptWrP[0], exceptWrP[1]);
  and2$ and_WrPWrChk (oExceptWrP, orExceptWrP, iWrChk);

  wire [1:0] miss;
  or2$ reduceMiss (oMiss, miss[0], miss[1]);

  //two caches facilitate misaligned access
  generate
    for(i=0; i<2; i=i+1) begin : sloop
      wire [blockBytes-1:0] wrMask;
      and2$ and_wrMask [blockBytes-1:0] (wrMask, iWr, wrBOrder[i]);

      CacheSlice #(busMasterID+i, totalSize/2, blockSize) slice
        (rdData[i], pAddr[i], exceptPF[i], exceptWrP[i], miss[i], oPCD[i],
         rdAddr[i], rd[i], wr[i], wrDataOrder[i], wrMask, iPCD[i],
         ioBus, iClk, inReset);

      defparam slice.sliceShift = 1;
    end
  endgenerate

endmodule

/*****************************************************************************/
// ICache
/*****************************************************************************/
module ICache (oData, oExceptPF, oMiss, oHit,
               iAddr0, iAddr1, iUA, iRd, //Generic inputs
               ioBus, iClk, inReset);

  parameter busMasterID = 0;
  parameter totalSize = 512;
  parameter blockSize = 16;

  //Block size must be at least 64-bits for QWORD operations
  `STATIC_ASSERT((blockSize % 8) == 0, blockSize_divisible_by_8);
  //totalSize has to be divisible by blockSize
  `STATIC_ASSERT((totalSize % blockSize) == 0, totalSize_divisible_by_blockSize);

  parameter numBlocks = totalSize/blockSize;
  parameter blockBytes = blockSize;
  parameter blockBits = blockBytes*8;

  parameter byteAddrSize = $clog2(8);
  parameter byteAddrLSB = 0;
  parameter byteAddrMSB = byteAddrSize - 1;

  parameter qwordAddrSize = $clog2(blockSize) - byteAddrSize;
  parameter qwordAddrLSB  = byteAddrMSB + 1;
  parameter qwordAddrMSB  = qwordAddrLSB + qwordAddrSize - 1;

  parameter shiftSize = $clog2(blockSize) + 1;
  parameter shiftMSB = qwordAddrMSB;
  parameter sliceAddr = $clog2(blockSize);

  genvar i;

  inout [`BUS_MSB:0] ioBus;

  output [blockBits*2-1:0] oData;
  output oExceptPF, oMiss, oHit;

  input [31:0] iAddr0, iAddr1;
  input iUA, iRd, iClk, inReset;

  //Read Data
  wire [blockBits-1:0] rdData [1:0];
  mux2$ muxDataOrder0 [2*blockBits-1:0]
    (oData,
     {rdData[1], rdData[0]},
     {rdData[0], rdData[1]},
     iAddr0[sliceAddr]);

  //Input Addresses
  wire [31:0] rdAddr [2:0];
 
  mux2$ muxiAddrOrder0 [31:0] //reroute all PCD requests to slice[0]
    (rdAddr[0], iAddr0, iAddr1, iAddr0[sliceAddr]);
  mux2$ muxiAddrOrder1 [31:0]
    (rdAddr[1], iAddr1, iAddr0, iAddr0[sliceAddr]);

  //Input Signals
  wire rdua;
  and2$ and_rdua (rdua, iRd, iUA);

  wire [1:0] rd;
  mux2$ rdOrder0
    (rd[0], iRd, rdua, iAddr0[sliceAddr]);
  mux2$ rdOrder1
    (rd[1], rdua, iRd, iAddr0[sliceAddr]);

  //Output Signals
  wire [1:0] exceptPF;
  or2$ reduceExceptPF (oExceptPF, exceptPF[0], exceptPF[1]);

  wire [1:0] miss;
  or2$ reduceMiss (oMiss, miss[0], miss[1]);
  nor2$ reduceHit (oHit, miss[0], miss[1]);

  //two caches facilitate misaligned access
  generate
    for(i=0; i<2; i=i+1) begin : sloop
      wire [31:0] pAddr;
      wire oPCD, exceptWrP;
      CacheSlice #(busMasterID+i, totalSize/2, blockSize) slice
        (rdData[i], pAddr, exceptPF[i], exceptWrP, miss[i], oPCD,
         rdAddr[i], rd[i], 1'b0, 
         {blockBits{1'b0}}, {blockBytes{1'b0}}, 1'b0,
         ioBus, iClk, inReset);

      defparam slice.sliceShift = 1;
    end
  endgenerate

endmodule

`default_nettype wire
`endif
