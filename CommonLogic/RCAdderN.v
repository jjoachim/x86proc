`ifndef _RC_ADDERN_V_
`define _RC_ADDERN_V_
`default_nettype none

/*****************************************************************************/
// Full Adder
/*****************************************************************************/
module FullAdder (oS, oCarry, iA, iB, iCin);
  input iA, iB, iCin;
  output oS, oCarry;

  //using NAND full adder found here:
  //http://students.cs.tamu.edu/wanglei/csce350/handout/lab6.html
  wire t1_0;
  wire t2_0, t2_1;
  wire t3_0;
  wire t4_0;
  wire t5_0, t5_1;
 
  nand2$ nand1(t1_0, iA, iB),
         nand2(t2_0, iA, t1_0),
         nand3(t2_1, iB, t1_0),
         nand4(t3_0, t2_0, t2_1),
         nand5(t4_0, t3_0, iCin),
         nand6(t5_0, t3_0, t4_0),
         nand7(t5_1, t4_0, iCin),
         nand9(oCarry, t4_0, t1_0),
         nand8(oS, t5_0, t5_1);

endmodule

/*****************************************************************************/
// Parameterizable Ripple-Carry Adder
/*****************************************************************************/
module RCAdderN (oOut, oCarry, iA, iB, iCarry);
  parameter width = 32;

  input [width-1:0] iA;
  input [width-1:0] iB;
  input iCarry;
  output [width-1:0] oOut;
  output oCarry;

  wire [width:0] carryProp;

  assign carryProp[0] = iCarry;
  assign oCarry = carryProp[width];

  genvar i;
  generate
    for (i=0; i<width; i=i+1) begin : FA_array
      FullAdder FA(.oS(oOut[i]),
                   .oCarry(carryProp[i+1]),
                   .iA(iA[i]),
                   .iB(iB[i]),
                   .iCin(carryProp[i])
                  );
    end
  endgenerate

endmodule

`default_nettype wire
`endif
