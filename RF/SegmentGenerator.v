`ifndef _SEGMENTGENERATOR_VH
`define _SEGMENTGENERATOR_VH

`timescale 1ns/1ps

module SegmentGenerator(SEGID0, MODRM, SIB, CONTROL_STORE);
    
    // I/O
    input   [ 7:0]  MODRM;
    input   [ 7:0]  SIB;
    input   [68:0]  CONTROL_STORE;
    output  [ 2:0]  SEGID0;

    // Control-store
    wire            USEMODRM;
    wire            USESIB;
    wire            USESIBBAR;
    wire            SEGOVRPRE;
    wire    [ 2:0]  SEGOVR;
    wire            SEGOVRDIS;
    wire            SEGOVREN;
    wire            WSTACK;
    wire            RSTACK;
    wire            CMPSMUX;
    wire            CMPSR;

    // ModR/M and SIB values
    wire            MOD00, MOD01, MOD10, MOD11, XORMOD, MOD1XBAR, MODX1BAR;
    wire            RM101, RM1XXBAR, RMXX1BAR;
    wire            BASE100, BASE100BAR, BASE101, BASE1XXBAR, BASEXX1BAR;
    wire            SIBDISPO, SIBDISPOBAR;

    // Selection signals
    wire            MRMSSMODE;
    wire            SIBSSMODE;
    wire            SSSEL;
    wire            ESSEL;
    wire            MSKSEGOVREN;

    // MUX outputs
    wire    [ 2:0]  MRMMUXOUT;
    wire    [ 2:0]  ESMUXOUT;

    // Control-store
    assign USEMODRM  = CONTROL_STORE[`iUSEMODRM];
    assign USESIB    = CONTROL_STORE[`iUSESIB];
    inv1$  usesib_inv(USESIBBAR, USESIB);
    assign SEGOVRPRE = CONTROL_STORE[`iSEGOVRPRE];
    assign SEGOVR    = CONTROL_STORE[`iSEGOVR];
    assign SEGOVRDIS = CONTROL_STORE[`iSEGOVRDIS];
    inv1$  segovren_inv(SEGOVREN, SEGOVRDIS);
    assign WSTACK    = CONTROL_STORE[`iWSTACK];
    assign RSTACK    = CONTROL_STORE[`iRSTACK];
    assign CMPSMUX   = CONTROL_STORE[`iCMPSMUX];
    assign CMPSR     = CONTROL_STORE[`iCMPSR];

    // ModR/M + SIB values
    inv1$ mod1xbar_inv(MOD1XBAR, MODRM[7]);
    inv1$ modx1bar_inv(MODX1BAR, MODRM[6]);
    nor2$ mod00_nor(MOD00, MODRM[7], MODRM[6]);
    nor2$ mod01_nor(MOD01, MODRM[7], MODX1BAR);
    nor2$ mod10_nor(MOD10, MOD1XBAR, MODRM[6]);
    nor2$ mod11_and(MOD11, MOD1XBAR, MODX1BAR);
    xor2$ xormod_xor(XORMOD, MODRM[7], MODRM[6]);

    inv1$ rm1xxbar_inv(RM1XXBAR, MODRM[2]);
    inv1$ rmxx1bar_inv(RMXX1BAR, MODRM[0]);
    nor3$ rm101_nor(RM101, RM1XXBAR, MODRM[1], RMXX1BAR);

    inv1$ base1xxbar_inv(BASE1XXBAR, SIB[2]);
    inv1$ basexx1bar_inv(BASEXX1BAR, SIB[0]);
    nor3$ base100_nor(BASE100, BASE1XXBAR, SIB[1], SIB[0]);
    nor3$ base101_nor(BASE101, BASE1XXBAR, SIB[1], BASEXX1BAR);

    // SIB DISPO = USESIB & MOD00 & BASE101
    and3$  sibdispo_and(SIBDISPO, USESIB, MOD00, BASE101);
    inv1$  sibdispbar_inv(SIBDISPOBAR, SIBDISPO);

    // Generate SS select bits
    and3$ mrmssmode_and(MRMSSMODE, USEMODRM, XORMOD, RM101);
    and3$ sibssmode_and(SIBSSMODE, USESIB,   BASE100, SIBDISPOBAR);

    // Generate segment select bits
    or4$  sssel_or(SSSEL, WSTACK, RSTACK, SIBSSMODE, MRMSSMODE);
    and2$ essel_and(ESSEL, CMPSMUX, CMPSR);
    and2$ msksovr_and(MSKSEGOVREN, SEGOVRPRE, SEGOVREN);

    // Output segment ID
    mux2_3$ mrm_mux(MRMMUXOUT, 3'b011,    3'b010, SSSEL);
    mux2_3$ ess_mux(ESMUXOUT,  MRMMUXOUT, 3'b000, ESSEL);
    mux2_3$ segid0_mux(SEGID0, ESMUXOUT, SEGOVR, MSKSEGOVREN);

endmodule

`endif
