`ifndef _COMMON_LOGIC_V_
`define _COMMON_LOGIC_V_

`include "CommonLogic/RCAdderN.v"
`include "CommonLogic/KSAdder64.v"
`include "CommonLogic/KSAdder32.v"
`include "CommonLogic/KSAdder8.v"
`include "CommonLogic/Relation32.v"
`include "CommonLogic/Compare.v"
`include "CommonLogic/BarrelShifter.v"
`include "CommonLogic/FSMLogic.v"
`include "CommonLogic/Reduce.v"
`include "CommonLogic/Counter.v"
`include "CommonLogic/Skew.v"
`include "CommonLogic/ByteReverse.v"
`include "CommonLogic/LibExt.v"
`include "CommonLogic/PipeRegs.v"

`endif
