`timescale 1ns/1ps

`include "Parameters.v"
`include "TestFramework.v"
`include "CommonLogic/CommonLogic.v"

//Counter
`define SIZE 2

//Shifter
`define CHUNK 32
`define SELW 2
`define BSIZE (`CHUNK<<`SELW)

module TOP;
  wire [`SIZE-1:0] state;
  reg inc, reset;
  
  `TEST_GLOBAL_INIT;

  //Shifter
  reg [`BSIZE-1:0] barrelIn;
  reg [`SELW-1:0] shft;
  wire [`BSIZE-1:0] barrelOut;
  BarrelLS #(`CHUNK, `SELW)  barrel 
    (barrelOut, barrelIn, shft);
  `TEST_INIT_COMB(barrelOut, `BSIZE);

  //ReduceOR
  reg [23:0] orIn;
  wire orOut;
  ReduceOR #(24) reducer
    (orOut, orIn);

  `TEST_INIT_COMB(orOut, 1);

  //Adder
  reg [31:0] addA, addB;
  reg addci;
  wire [31:0] addOut;
  wire addco;

  KSAdder32 adder (addOut, addco, addA, addB, addci);

  `TEST_INIT_COMB(addOut, 32);
  `TEST_INIT_COMB(addco, 1);
  
  //Byte Reverse
  reg [63:0] brA;
  wire [63:0] brO;
  ByteReverse #(8) br (brO, brA);

  `TEST_INIT_COMB(brO, 64);

  integer i;

  initial begin
    reset = 0;
    inc = 0;
    barrelIn = 0;
    shft = 0;
    `TEST_TOGGLE_CLK;
    reset = 1;
    `TEST_TOGGLE_CLK;
    
    `TEST_DISPLAY("\n>> ADD TESTS <<\n");
    `TEST_SET(
      addA = 32'h1;
      addB = 32'hFFFF_FFFF;
      addci = 0;
      `TEST_EQ(addOut, 0);
      `TEST_EQ(addco, 1);
     )

    `TEST_DISPLAY("\n>> OR TESTS <<\n");
    for(i=0; i<7; i=i+1) begin
      `TEST_SET(
        orIn = 15'b0;
        `TEST_EQ(orOut, |orIn);
      );
      `TEST_SET(
        orIn = $random;
        `TEST_EQ(orOut, |orIn);
      );
    end

    `TEST_DISPLAY("\n>> SHIFTER TESTS <<\n");
    `TEST_SET(
      barrelIn = 127'h00000000_0000FFFF;
      shft = 2;
      `TEST_EQ(barrelOut, barrelIn<<(shft*`CHUNK));
    );

    `TEST_SET(
      barrelIn = 127'h000000FF_0000FFFF;
      shft = 2;
      `TEST_EQ(barrelOut, barrelIn<<(shft*`CHUNK));
    );
 
    `TEST_SET(
      barrelIn = 127'h00000000_0000FFFF;
      shft = 15;
      `TEST_EQ(barrelOut, barrelIn<<(shft*`CHUNK));
    );
 
    `TEST_SET(
      barrelIn = 127'hABFFABFF_ABFFABFF;
      shft = 1;
      `TEST_EQ(barrelOut, barrelIn<<(shft*`CHUNK));
    );
 
    `TEST_SET(
      barrelIn = 127'h12345678_12345678;
      shft = 4;
      `TEST_EQ(barrelOut, barrelIn<<(shft*`CHUNK));
    );

    for(i=0; i<15; i=i+1) begin
      `TEST_SET(
        barrelIn = $random;
        shft = $random;
        `TEST_EQ(barrelOut, barrelIn<<(shft*`CHUNK));
      );
    end

    `TEST_DISPLAY("\n>> BYTE REVERSE TESTS <<\n");
 
    `TEST_SET(
      brA = #1 64'h010203_04050607;
      `TEST_EQ(brO, 64'h070605_0403020100);
    );

    `TEST_SET(
      brA = #1 64'h00000000_11223344;
      `TEST_EQ(brO, 64'h44332211_00000000);
    );

    `TEST_SET(
      brA = #1 64'h76543210_76543210;
      `TEST_EQ(brO, 64'h10325476_10325476);
    );

    `TEST_DISPLAY("\n>> END TESTS <<\n");
    `TEST_END;
    
    $finish;
  end

  initial begin
    $vcdplusfile("top.dump.vpd");
    $vcdpluson(0, TOP);
  end

endmodule
