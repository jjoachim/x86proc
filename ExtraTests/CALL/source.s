mov  $0x00000200, %eax
mov  %ax, %ss
movl $0x00000c00, %eax
movw %ax, %ds

movl $0x00001000, %esp
movl $0x00000000, %eax
movl $0xffffffff, %ebx

call lbl_j8
movl $0x0, %ebx
movl $0xffff0000, %eax
movl $0x12345678, %ecx

call lbl_fast

hlt

lbl_fast:
ret

.skip 0x50
lbl_j8:
movl $0xfffffff0, %eax
call lbl_jx
movl $0xffff1234, %ebx
ret

.skip 0x25
lbl_jx:
movl $0xf123456f, %ecx
ret 

