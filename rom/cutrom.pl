use strict;
my $file=$ARGV[0];
$file=~/(.*)\.(.*)/;
my $new_filename=$1;
my $new_fileext=$2;
open(my $in_fh, "<", "$file")
or die "cannot open > output.txt: $!";
my $j=0;
my $i=0;
	my $fh;
	my $zero_length=0;
while (<$in_fh>){
	my $line=$_;
	chomp($line);
	if ($i == 0){
		open($fh, ">", "$new_filename\_$j.$new_fileext")
		or die "cannot open > output.txt: $!";
		my $length_line=length($line);
		if ($length_line<=4){
			$zero_length=4-$length_line;
		}elsif($length_line<=32){
			$zero_length=32-$length_line;
		}else{
			$zero_length=64-$length_line;
		}		
			print "length=$length_line   ;padding= $zero_length\n";
	

	}
	print $fh "0" x $zero_length;
	print $fh "$line";
	print $fh "\n";
	#print "$i $j\n";
	$i=$i+1;


	if ($i==32){
		$i=0;
		close ($fh);
		$j=$j+1;
	}
}
close $in_fh;
`chmod -R 777 *`;
