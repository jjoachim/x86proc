`ifndef _SKEW_V_
`define _SKEW_V_

`default_nettype none

/*****************************************************************************/
// Parameterizable Clock Skew
/*****************************************************************************/
module Skew (oOut, iA);
  parameter skew = 4;

  output oOut;
  input iA;

  wire [skew:0] delay;
  assign delay[0] = iA;

  genvar i;
  generate
    for(i=1; i<=skew; i=i+1) begin : buff
        not1$ inv (delay[i], delay[i-1]);
    end
  endgenerate

  assign oOut = delay[skew];
endmodule

`default_nettype wire
`endif
