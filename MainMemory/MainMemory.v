`ifndef _MAIN_MEMORY_V_
`define _MAIN_MEMORY_V_

`include "Parameters.v"
`include "CommonLogic/CommonLogic.v"

`default_nettype none

module MainMemory (oData, oValid, iData, iAddr, iRd, iWr, iClk, inReset);
  parameter sramBanks = 4;
  parameter sramBlocks = (1<<15)/(32*4); //32KB
  parameter bankAddrSize = $clog2(sramBanks);
  parameter dataSize = sramBanks*32;
  parameter dataBytes = dataSize/8;
  parameter dwordAddrSize = $clog2(dataBytes) - 2;
  
  parameter sramDelay = 60;
  parameter waitClks = (sramDelay/`CLK_CYCLE) + ((sramDelay % `CLK_CYCLE) != 0) - 1;
  parameter stateBits = $clog2(waitClks+1);
  parameter blocksPerBank = sramBlocks/sramBanks;

  parameter lineAddrSize = $clog2(32);
  parameter lineAddrLSB = $clog2(sramBanks) + 2;
  parameter lineAddrMSB = lineAddrLSB + lineAddrSize - 1;

  parameter blockAddrSize = $clog2(blocksPerBank);
  parameter blockAddrLSB = lineAddrMSB + 1;
  parameter blockAddrMSB = blockAddrLSB + blockAddrSize - 1;

  parameter tagSize = blockAddrSize + lineAddrSize + 1; //valid bit
  parameter tagValid = tagSize - 1;
  parameter tagLineLSB = 0;
  parameter tagLineMSB = tagLineLSB + lineAddrSize - 1;
  parameter tagBlockLSB = tagLineMSB + 1;
  parameter tagBlockMSB = tagValid - 1;
  `STATIC_ASSERT(tagValid == (tagBlockLSB + $clog2(blocksPerBank)), tagBlockAddr_does_not_align);

  parameter addrTagLSB = lineAddrLSB;
  parameter addrTagMSB = blockAddrMSB;

  output [31:0] oData;
  output oValid;
  input [31:0] iAddr, iData;
  input iRd, iClk, inReset;
  input [3:0] iWr;

  genvar i, j;

  //State Machine Outputs
  wire FSM_countInc, FSM_ceEn, FSM_memWR, FSM_fill, FSM_idle;
  
  //Input Buffer/Adjusting
  wire [dataSize-1:0] data, rawData;
  wire [31:0] addr;
  wire rd, wr, reqRdWr, inBuffWrEn;
  wire [65:0] iInputBufferData, oInputBufferData;
  wire [65:0] inputData;
  
  or2$ reqRdRw$ (reqRdWr, rd, wr);
  and2$ inBuffWrEn$ (inBuffWrEn, reqRdWr, FSM_idle);
  assign iInputBufferData = {iData, iAddr, iRd, iWr[0]};

  regs$ inBuffer [65:0]
    (oInputBufferData, iInputBufferData, inBuffWrEn, iClk, inReset);
  mux2$ inputMux [65:0]
    (inputData, oInputBufferData, iInputBufferData, FSM_idle);
  
  assign rawData = {{(dataSize-32){1'b0}}, inputData[65:34]};
  assign {addr, rd, wr} = inputData[33:0];

  BarrelLS #(32, dwordAddrSize) dataShft
    (data, rawData, addr[dwordAddrSize+1 : 2]);

  wire [dataBytes-1:0] iWrAdj;
  BarrelLS #(4, dwordAddrSize) wrShft
    (iWrAdj, {{(sramBanks*4-4){1'b0}}, {4{wr}}}, addr[dwordAddrSize+1 : 2]);

  //Data Buffer
  wire [dataSize-1:0] cdb; //data bus to SRAM
  wire [dataSize-1:0] ibdata;
  wire [dataSize-1:0] obdata;
  wire [dataBytes-1:0] ibdataWr, dataWrOVR, wrAdj;
  wire wrhit, hit; //hit comes from tag compare

  and2$ wrAndHit (wrhit, wr, hit);
  BarrelLS #(4, dwordAddrSize) wrBarrelAdj
    (wrAdj, {{(dataBytes-4){1'b0}}, {4{wrhit}}}, addr[dwordAddrSize+1 :2]);

  assign dataWrOVR = {(dataBytes){FSM_fill}};
  or2$ dataWrLogic [dataBytes-1:0]
    (ibdataWr, dataWrOVR, wrAdj);

  mux2$ bdataMux [dataSize-1:0]
    (ibdata, data, cdb, FSM_fill);
  
  generate
    for(i=0; i<dataBytes; i=i+1) begin : dataStore
      regs$ dbit [7:0]
        (obdata[i*8 +: 8], ibdata[i*8 +: 8], ibdataWr[i], iClk, inReset);
    end
  endgenerate

  //Dirty Bit
  wire ibdirty, obdirty, ibdirtyWr;
  mux2$ dirtyValMux
    (ibdirty, wr, 1'b0, FSM_fill);
  mux2$ dirtyWrMux
    (ibdirtyWr, wr, 1'b1, FSM_fill);
  regs$ dirtyStore
    (obdirty, ibdirty, ibdirtyWr, iClk, inReset);

  //Tag Store
  wire [tagSize-1:0] obtag;
  wire dirty;
  regs$ #(tagSize) tagStore
    (obtag, {1'b1, addr[addrTagMSB:addrTagLSB]}, FSM_fill, iClk, inReset);
  and2$ lineDirty
    (dirty, obtag[tagSize-1], obdirty);

  //Tag Compare
  Compare #(tagSize) tagCmp
    (hit, obtag, {1'b1, addr[addrTagMSB:addrTagLSB]});

  //SRAM
  wire [blocksPerBank-1:0] FSM_ceEnAdj, nFSM_ceEnAdj, nFSM_ceEn;
  assign nFSM_ceEn[blocksPerBank-1:1] = 0;
  not1$ nCeEn (nFSM_ceEn[0], FSM_ceEn);

  BarrelLS #(1, blockAddrSize) ceEnAdjShft
    (nFSM_ceEnAdj, nFSM_ceEn, obtag[tagBlockMSB:tagBlockLSB]);
  not1$ not_ceEnAdj [blocksPerBank-1:0]
    (FSM_ceEnAdj, nFSM_ceEnAdj);

  generate
    for(i=0; i<sramBanks; i=i+1) begin : bank
      sram32x32$ block [blocksPerBank-1:0]
        (obtag[tagLineMSB:tagLineLSB], 
         cdb[i*32 +: 32], 1'b0, 
         FSM_memWR, FSM_ceEnAdj
        );
    end
  endgenerate

  //Data line structure
  tristateL$ tribuff [dataSize-1:0]
    (.enbar(FSM_memWR), .in(obdata), .out(cdb));

  //Counter
  wire [stateBits-1:0] oCount, constWaitClks;
  wire countSaturate, ncounterRst;
  assign constWaitClks = waitClks;
  Compare #(stateBits) cmp_count
    (countSaturate, oCount, constWaitClks);
  
  and2$ countRst (ncounterRst, FSM_countInc, inReset);
  Counter #(stateBits) FSMcounter
    (oCount, FSM_countInc, iClk, ncounterRst);

  //Controller FSM
  localparam stateWidth = 3;
  localparam numStates = 10;
  localparam inputWidth = 7;
  localparam outputWidth = 7;
  wire [stateWidth-1:0] FSM_state;
  wire [3:0] FSM_output;
  wire [outputWidth-1:0] FSM_nextout;
  wire FSM_valid;
  
  wire [inputWidth-1:0] FSM_input;
  assign FSM_input = {reqRdWr, hit, dirty, countSaturate, FSM_state};

  wire [numStates*inputWidth-1:0] conditions, mask;
  assign conditions = 
    {7'b0xxx_000, 7'b11xx_000, 7'b101x_000, 7'b100x_000,
     7'bxxx0_001, 7'bxxx1_001, 7'bxxx0_010, 7'bxxx1_010,
     7'bxxxx_011, 7'bxxxx_100};

  assign mask = 
    {7'b1000_111, 7'b1100_111, 7'b1110_111, 7'b1110_111,
     7'b0001_111, 7'b0001_111, 7'b0001_111, 7'b0001_111,
     7'b0000_111, 7'b0000_111};
  
  wire [numStates*outputWidth-1:0] outputs;
  assign outputs = 
    {7'b0011_000, 7'b1011_100, 7'b0010_001, 7'b0011_010,
     7'b0100_001, 7'b0011_010, 7'b0101_010, 7'b0011_011,
     7'b1011_100, 7'b0011_000};

  FSMLogic #(outputWidth, inputWidth, numStates) fsmlogic
    (FSM_nextout,
    {reqRdWr, hit, dirty, countSaturate, FSM_state},
    conditions, mask, outputs);

  /*
  always@(*) begin
    casex(FSM_input)
      7'b0xxx_000: FSM_nextout <= #2 7'b0011_000; //IDLE -> IDLE
      7'b11xx_000: FSM_nextout <= #2 7'b1011_100; //IDLE -> COOL
      7'b101x_000: FSM_nextout <= #2 7'b0010_001; //IDLE -> WMEM
      7'b100x_000: FSM_nextout <= #2 7'b0011_010; //IDLE -> RMEM

      7'bxxx0_001: FSM_nextout <= #2 7'b0100_001; //WMEM -> WMEM
      7'bxxx1_001: FSM_nextout <= #2 7'b0011_010; //WMEM -> RMEM
      
      7'bxxx0_010: FSM_nextout <= #2 7'b0101_010; //RMEM -> RMEM
      7'bxxx1_010: FSM_nextout <= #2 7'b0011_011; //RMEM -> WBUF

      7'bxxxx_011: FSM_nextout <= #2 7'b1011_100; //WBUF -> COOL

      7'bxxxx_100: FSM_nextout <= #2 7'b0011_000; //COOL -> IDLE
      default: $display("BAD FSM STATE");
    endcase
  end
  */

  //State registers
  regs$ #(stateWidth) stateRegs
    (FSM_state, FSM_nextout[stateWidth-1:0], 1'b1, iClk, inReset);
  
  regs$ #(3) moorRegs //output regs
    (FSM_output[3:1], FSM_nextout[outputWidth-1:stateWidth+1], 1'b1, iClk, inReset);
  regs$ #(1, 1) reg_FSM_memWR //memWR has special reset behavior
    (FSM_output[0], FSM_nextout[stateWidth], 1'b1, iClk, inReset);

  //FSM outputs
  assign {FSM_valid, FSM_countInc, FSM_ceEn, FSM_memWR} = FSM_output;
  nor3$ cmp_s0 (FSM_idle, FSM_state[0], FSM_state[1], FSM_state[2]);
  assign FSM_fill = FSM_state[1];

  //Outputs  
  wire [dataSize-1:0] oDataRaw;
  BarrelRS #(32, dwordAddrSize) dataOutputShft
    (oDataRaw, obdata, addr[dwordAddrSize+1 : 2]);
  assign oData = oDataRaw[31:0];
  and2$ oValidEn (oValid, FSM_valid, hit);

endmodule

`default_nettype wire
`endif
