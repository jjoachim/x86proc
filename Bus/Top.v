`timescale 1ns/1ps

`include "Parameters.v"
`include "TestFramework.v"

`include "MainMemory/MainMemory.v"
`include "Bus.v"

`define MEM_SZ 16

module TOP;
  reg nrst;
  
  `TEST_GLOBAL_INIT;

  //Instantiate Bus Logic
  wire [`BUS_MSB:0] bus; 
  Bus #(1) bus_logic (bus, `TEST_CLK, nrst);

  //Give Main Memory a bus interface
  wire [31:0] mmbusdata, mmbusaddr, mmdata;
  wire mmvalid;
  wire [3:0] mmbuswr;
  wire mmbusrd;
  BusSlave #(0, 32'h0FFFFFFF) bus_mm
    (bus, mmbusdata, mmbusaddr, mmbuswr, mmbusrd, mmdata, mmvalid);
  MainMemory #(8, 32) mm 
    (mmdata, mmvalid, mmbusdata, mmbusaddr, mmbusrd, mmbuswr, `TEST_CLK, nrst);

  //Give us a bus interface
  wire [31:0] databus;
  wire valid;
  reg [31:0] data, addr;
  reg [3:0] wr;
  reg rd, rst;
  BusMaster #(0) bus_master
    (bus, databus, valid, data, addr, wr, rd);
  
  `TEST_INIT_COMB(databus, 32);

  integer i;
  
  initial begin
    //initialize signals
    data = 0;
    addr = 0;
    rd = 0;
    wr = 0;
    nrst = 0;
    `TEST_TOGGLE_CLK;
    nrst = 1;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;

    `TEST_DISPLAY("\n>> BEGIN TESTS <<\n");
    //Write to memory
    rd = 0;
    for(i=0; i<`MEM_SZ; i=i+1) begin
      data = 32'hFFFF0000 + i;
      addr = i<<2;
      wr = 4'b1111;
      `TEST_TOGGLE_CLK;
      while(valid != 1) begin
        `TEST_TOGGLE_CLK;
      end
    end
    
    //Read from memory
    wr = 0;
    data = 0;
    for(i=0; i<`MEM_SZ; i=i+1) begin
      addr = i<<2;
      rd = 1;
      `TEST_WHEN(valid,
        `TEST_EQ(databus, 32'hFFFF0000 + i);
      )
    end

    //Write to memory
    rd = 0;
    for(i=0; i<`MEM_SZ; i=i+1) begin
      data = 32'h00FF0000 + i;
      addr = i<<2;
      wr = 4'b1111;
      `TEST_TOGGLE_CLK;
      while(valid != 1) begin
        `TEST_TOGGLE_CLK;
      end
    end
    
    //Read from memory
    wr = 0;
    data = 0;
    for(i=0; i<`MEM_SZ; i=i+1) begin
      addr = i<<2;
      rd = 1;
      `TEST_WHEN(valid,
        `TEST_EQ(databus, 32'h00FF0000 + i);
      )
    end
    `TEST_DISPLAY("\n>> END TESTS <<");
    
    `TEST_END;

    $finish;
  end
  
  initial begin
    $vcdplusfile("top.dump.vpd");
    $vcdpluson(0, TOP);
    $vcdpluson(0, mm.bank[0].block[0].mem);
    $vcdpluson(0, mm.bank[1].block[0].mem);
    //$vcdpluson(0, mm.bank[2].block[0].mem);
    //$vcdpluson(0, mm.bank[3].block[0].mem);
  end

endmodule
