`timescale 1ns/1ps

`include "Parameters.v"
`include "TestFramework.v"
`include "EXE/DAA.v"

module TOP;
  `TEST_GLOBAL_INIT;

  reg [7:0] byte;
  wire [7:0] obyte;
  wire af, cf;

  DAA daa (obyte, af, cf, byte);

  `TEST_INIT_COMB(obyte, 8);
  `TEST_INIT_COMB(af, 1);
  `TEST_INIT_COMB(cf, 1);

  initial begin
    `TEST_DISPLAY("\n>>BEGIN DAA TESTS<<\n");
    `TEST_SET(
      byte = 8'h11 + 8'h19;
      `TEST_EQ(obyte, 8'h30);
      `TEST_EQ(af, 1);
      `TEST_EQ(cf, 0);
    )

    `TEST_SET(
      byte = 8'h55 + 8'h45;
      `TEST_EQ(obyte, 8'h00);
      `TEST_EQ(af, 1);
      `TEST_EQ(cf, 1);
    )

    `TEST_SET(
      byte = 8'h75 + 8'h75;
      `TEST_EQ(obyte, 8'h50);
      `TEST_EQ(af, 1);
      `TEST_EQ(cf, 1);
    )

    `TEST_SET(
      byte = 8'h27 + 8'h72;
      `TEST_EQ(obyte, 8'h99);
      `TEST_EQ(af, 0);
      `TEST_EQ(cf, 0);
    )

    `TEST_SET(
      byte = 8'h02 + 8'h70;
      `TEST_EQ(obyte, 8'h72);
      `TEST_EQ(af, 0);
      `TEST_EQ(cf, 0);
    )

    `TEST_SET(
      byte = 8'h90 + 8'h15;
      `TEST_EQ(obyte, 8'h05);
      `TEST_EQ(af, 0);
      `TEST_EQ(cf, 1);
    )

    `TEST_SET(
      byte = 8'h9a;
      `TEST_EQ(obyte, 8'h0);
      `TEST_EQ(af, 1);
      `TEST_EQ(cf, 1);
    )

    `TEST_END;
    `TEST_DISPLAY("\n>>END DAA TESTS<<\n");
  end

endmodule
