`ifndef _INDEX_GENERATOR_VH
`define _INDEX_GENERATOR_VH

module IndexGenerator(INDEX, OUT2, MODRM, SIB, CONTROL_STORE);

    // I/O
    input   [31:0]  OUT2;           // Register file output
    input   [ 7:0]  MODRM;          // ModR/M byte
    input   [ 7:0]  SIB;            // SIB byte
    input   [68:0]  CONTROL_STORE;  // Control store
    output  [31:0]  INDEX;          // Immediate


    // Control-store
    wire            USEMODRM;
    wire            USESIB;
    wire            RSTACK;
    wire            WSTACK;
    wire            RWSTACK;
    wire            OPSZOVRPRE; // Operand size override prefix present
    wire    [ 1:0]  OPSZ;       // Default operand size
    wire    [ 1:0]  OPSZOVR;    // Overridden operand size
    wire            OPSZOVRS;   // Forced, special operand size override
    wire    [ 1:0]  OPSZS;      // Special operand size

    // Operand Sizes
    wire    [ 1:0]  OPSZA;
    wire    [ 1:0]  OPSZB;

    // ModR/M and SIB values
    wire            MOD00, MOD01, MOD10, MOD11, MOD1XBAR, MODX1BAR;
    wire            RM101, RM1XXBAR, RMXX1BAR;
    wire            BASE101, BASE1XXBAR, BASEXX1BAR;

    // Displacement options
    wire            MRMDISPO;       // ModR/M displacement-only mode
    wire            SIBDISPO;       // SIB displacement-only mode
    wire            DISPOMODE;      // Displacement-only mode

    // MUXing
    wire    [31:0]  DISPOMUXOUT;

    // Control-store values
    assign OPSZOVRPRE = CONTROL_STORE[`iOPSZOVRPRE];
	assign OPSZ       = CONTROL_STORE[`iOPSZ];
	assign OPSZOVR    = CONTROL_STORE[`iOPSZOVR];
    assign OPSZOVRS   = CONTROL_STORE[`iOPSZOVRS];
    assign OPSZS      = CONTROL_STORE[`iOPSZS];
    assign USEMODRM   = CONTROL_STORE[`iUSEMODRM];
    assign USESIB     = CONTROL_STORE[`iUSESIB];
    assign RSTACK     = CONTROL_STORE[`iRSTACK];
    assign WSTACK     = CONTROL_STORE[`iWSTACK];
    or2$   rwstack_or(RWSTACK, RSTACK, WSTACK);

    // Operand sizes
    mux2_2$ opsza_mux(OPSZA, OPSZ,  OPSZOVR, OPSZOVRPRE);
    mux2_2$ opszb_mux(OPSZB, OPSZA, OPSZS,   OPSZOVRS);

    // ModR/M + SIB values
    inv1$ mod1xbar_inv(MOD1XBAR, MODRM[7]);
    inv1$ modx1bar_inv(MODX1BAR, MODRM[6]);
    nor2$ mod00_nor(MOD00, MODRM[7], MODRM[6]);
    nor2$ mod01_nor(MOD01, MODRM[7], MODX1BAR);
    nor2$ mod10_nor(MOD10, MOD1XBAR, MODRM[6]);
    nor2$ mod11_and(MOD11, MOD1XBAR, MODX1BAR);

    inv1$ rm1xxbar_inv(RM1XXBAR, MODRM[2]);
    inv1$ rmxx1bar_inv(RMXX1BAR, MODRM[0]);
    nor3$ rm101_nor(RM101, RM1XXBAR, MODRM[1], RMXX1BAR);

    inv1$ base1xxbar_inv(BASE1XXBAR, SIB[2]);
    inv1$ basexx1bar_inv(BASEXX1BAR, SIB[0]);
    nor3$ base101_nor(BASE101, BASE1XXBAR, SIB[1], BASEXX1BAR);

    // Determine displacement option
    nand2$ stddisp32_nand(STDDISP32, USEMODRM, MOD10);
    and3$ mrmdispo_and(MRMDISPO,   USEMODRM, MOD00, RM101);
    and3$ sibdispo_and(SIBDISPO,   USESIB,   MOD00, BASE101);
    or2$   dispo_or    (DISPOMODE,  MRMDISPO, SIBDISPO);

    // MUXing
    mux2_32$ dispo_mux(DISPOMUXOUT, OUT2, 32'h00000000, DISPOMODE);
    mux2_32$ rwstack_mux(INDEX, DISPOMUXOUT, OUT2, RWSTACK);


endmodule

`endif
