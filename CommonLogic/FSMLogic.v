`ifndef _FSMLOGIC_V_
`define _FSMLOGIC_V_

`include "CommonLogic/Compare.v"

`default_nettype none

/*****************************************************************************/
// Parameterizable FSMLogic
/*****************************************************************************/
module FSMLogic (oOut, iInputs, iConditions, iMask, iOutputs);
  parameter outputWidth = 1;
  parameter inputWidth = 1;
  parameter numStates = 1;

  output [outputWidth-1:0] oOut;
  input [inputWidth-1:0] iInputs;
  input  [numStates*inputWidth-1:0] iConditions, iMask;
  input [numStates*outputWidth-1:0] iOutputs;

  genvar i;
  generate
    for(i=0; i<numStates; i=i+1) begin : state
      wire [inputWidth-1:0] conditions, mask, inputs;
      wire [outputWidth-1:0] outputs;
      assign mask = iMask[inputWidth*i +: inputWidth];

      and2$ dcmask [inputWidth-1:0]
        (conditions, iConditions[inputWidth*i +: inputWidth], mask);
      and2$ dcinputs [inputWidth-1:0]
        (inputs, iInputs, mask);

      wire nStateEn;
      NCompare #(inputWidth) statecmp
        (nStateEn, inputs, conditions);

      assign outputs = iOutputs[outputWidth*i +: outputWidth];
      tristateL$ stateout [outputWidth-1:0]
        (.enbar(nStateEn),
         .in(outputs),
         .out(oOut)
        );
    end
  endgenerate

endmodule

`default_nettype wire
`endif
