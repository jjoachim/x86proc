movl $0x00000000, %esi
movl $0x00000c00, %eax
mov %ax, %ds

movb $0x11, %dl               ;edx = 0x00000011
movw $0x2222, %bx             ;ebx = 0x00002222
movl $0x33333333, %ecx        ;ecx = 0x33333333
movb %dl, %dh                 ;edx = 0x00001111
movl %ecx, %eax               ;eax = 0x33333333
movw %bx, %cx                 ;ecx = 0x33332222
movl %eax, (%esi)             ;m1000 = 0x33333333
movl %ecx, 4(%esi)            ;m1004 = 0x33332222
addl $0x8, %esi               ;esi = 0x02000008
movl %eax, (%esi)             ;m1008 = 0x33333333
addl %eax, (%esi)             ;m1008 = 0x66666666
addl (%esi), %eax             ;eax = 0x99999999
andw %dx, %ax                 ;eax = 0x99991111
andl $0x8888FFFF, %eax        ;eax = 0x88881111
movl -8(%esi), %eax           ;eax = 0x33333333
movl -4(%esi), %eax           ;eax = 0x33332222
movl (%esi), %eax             ;eax = 0x66666666
movl -1(%esi), %eax           ;eax = 0x66666633
hlt
