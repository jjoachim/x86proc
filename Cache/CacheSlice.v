`ifndef _CACHE_SLICE_V_
`define _CACHE_SLICE_V_

`include "Parameters.v"
`include "CommonLogic/CommonLogic.v"
`include "Bus/Bus.v"
`include "Cache/TLB.v"
`include "Cache/RAM.v"

`default_nettype none

/*****************************************************************************/
// Cache Slice
/*****************************************************************************/
module CacheSlice (oData, oAddr, oExceptPF, oExceptWrP, oMiss, oPCD,
                   iAddr, iRd, iWr, //Generic inputs
                   iDataW, iWrB, iPCD, //Additional Write inputs
                   ioBus, iClk, inReset);

  parameter busMasterID = 0;
  parameter totalSize = 256;
  parameter blockSize = 16;

  //Block size must be at least WORD operations
  `STATIC_ASSERT((blockSize % 8) == 0, blockSize_divisible_by_wordSize);
  //totalSize has to be divisible by blockSize
  `STATIC_ASSERT((totalSize % blockSize) == 0, totalSize_divisible_by_blockSize);
  
  parameter numBlocks = totalSize/blockSize;
  parameter blockBytes = blockSize;
  parameter blockBits = blockBytes*8;

  parameter sliceShift = 0;

  parameter lineAddrSize = $clog2(numBlocks);
  parameter lineAddrLSB = $clog2(blockSize) + sliceShift;
  parameter lineAddrMSB = lineAddrLSB + lineAddrSize - 1;

  parameter tagAddrSize = 32 - $clog2(blockSize) + 2; // {dirty, valid}
  parameter tagAddrLSB = $clog2(blockSize);
  parameter tagAddrMSB = 31;
  `STATIC_ASSERT((tagAddrMSB - tagAddrLSB + 3) == tagAddrSize, tagAddrSize_check);
  
  parameter tagPadSize = (8 - (tagAddrSize % 8) % 8);
  parameter ptagAddrSize = tagAddrSize + tagPadSize;
  parameter ptagAddrBytes = ptagAddrSize/8;
  parameter ptagAddrLSB = 0;
  parameter ptagAddrMSB = tagAddrSize-3;
  parameter ptagValid = tagAddrSize-2;
  parameter ptagDirty = tagAddrSize-1;
  `STATIC_ASSERT((ptagAddrSize % 8) == 0, ptagAddrSize_check);

  //Special bus dimensions for 32-bits
  parameter bwordAddrSize = $clog2(blockSize) - 2;
  parameter bwordAddrLSB  = 2;
  parameter bwordAddrMSB  = bwordAddrLSB + bwordAddrSize - 1;

  genvar i;

  wire [tagPadSize-1:0] tagPad;
  assign tagPad = 0;

  inout [`BUS_MSB:0] ioBus;

  output [blockBits-1:0] oData;
  output [31:0] oAddr;
  output oPCD, oExceptPF, oExceptWrP, oMiss;

  input [blockBits-1:0] iDataW;
  input [31:0] iAddr;
  input [blockBytes-1:0] iWrB;
  input iRd, iWr, iPCD, iClk, inReset;

  //FSM forward declarations
  wire [31:0] oFSM_Addr;
  wire oFSM_BusW, oFSM_BusR, oFSM_CRst, oFSM_BusRW, oFSM_Miss, oFSM_PCDWR;

  //Bus Interface
  wire [31:0] obusData, ibusData;
  wire [31:0] ibusAddr;
  wire [3:0] ibusW;
  wire obusV, ibusR;
  BusMaster #(busMasterID) ibus 
    (ioBus, obusData, obusV, ibusData, ibusAddr, ibusW, ibusR);

  //Memory
  wire [blockBits-1:0] ramDataOut;
  wire [lineAddrSize-1:0] ramAddrIn;
  wire [blockBits-1:0] ramDataIn;
  wire [blockBytes-1:0] ramWrEnIn;
  RAM #(totalSize, blockSize) ram
    (ramDataOut,
     ramAddrIn,
     ramDataIn,
     ramWrEnIn,
     iClk
    );

  //Tag Store
  wire [ptagAddrSize-1:0] tagDataOut;
  wire [lineAddrSize-1:0] tagAddrIn;
  wire [ptagAddrSize-1:0] tagDataIn;
  wire [ptagAddrBytes-1:0] tagWrEnIn;
  RAM #(numBlocks*(ptagAddrSize/8), ptagAddrSize/8) tags
    (tagDataOut,
     tagAddrIn,
     tagDataIn,
     tagWrEnIn,
     iClk
    );
  
  //TLB
  wire [31:0] tlbAddr;
  wire tlbMiss, tlbHit;
  TLB tlb (tlbAddr, iAddr, tlbMiss);
  not1$ not_tlbHit (tlbHit, tlbMiss);

  wire [31:0] pcdAddr;
  mux2$ mux_pcdAddr [31:0] (pcdAddr, iAddr, oAddr, iRd);

  //PCD select
  wire pcd, npcd, oPCDSkew;
  Skew #(4) skew_opcd (oPCDSkew, tlbAddr[`TLB_PCD]);
  mux4$ opPCDsel (pcd, oPCDSkew, iPCD, 1'b0, iPCD, iWr, tlbMiss);
  //mux2$ opPCDsel (pcd, oPCD, iPCD, iWr);
  not1$ not_pcd (npcd, pcd);

  //Some preliminary signals
  wire [31:0] dramAddr;
  mux2$ dramAddrMux [31:0]
    (dramAddr, iAddr, oFSM_Addr, oFSM_BusRW);

  //Bus connections
  wire [blockBits-1:0] busData; //align ramData for bus writes
  BarrelRS #(32, bwordAddrSize) busiDataShift
    (busData, ramDataOut, dramAddr[bwordAddrMSB:bwordAddrLSB]);

  wire busEnRW, onbusValid;
  not1$ not_busvalid (onbusValid, obusV);
  and2$ and_busEnRW (busEnRW, oFSM_PCDWR, onbusValid);

  mux4$ busRsel
    (ibusR, 1'b0, iRd, oFSM_BusR, oFSM_BusR, busEnRW, oFSM_BusR);
  mux4$ busWsel [3:0]
    (ibusW, 1'b0, iWrB[3:0], oFSM_BusW, oFSM_BusW, busEnRW, oFSM_BusW);
  mux4$ busAddrSel [31:0]
    (ibusAddr, 32'b0, pcdAddr, oFSM_Addr, oFSM_Addr, pcd, oFSM_BusRW);
  mux4$ busDataSel [31:0]
    (ibusData, 32'b0, iDataW[31:0], busData[31:0], busData[31:0], pcd, oFSM_BusRW);

  wire [blockBits-1:0] oBusDataShft; //bus data needs an extra shift due to word-size
  BarrelLS #(32, bwordAddrSize) busoDataShift
    (oBusDataShft, 
     {{(blockBits-32){1'b0}}, obusData}, 
     dramAddr[bwordAddrMSB:bwordAddrLSB]
    );

  wire [blockBytes-1:0] oBusWrShft;
  BarrelLS #(4, bwordAddrSize) busWrShft
    (oBusWrShft, 
     {{(blockBytes-4){1'b0}}, 4'b1111}, 
     dramAddr[bwordAddrMSB:bwordAddrLSB]
    );

  //TLB Read
  and2$ and_ExceptPF (oExceptPF, iRd, tlbMiss); //Page Fault Catch

  wire nExceptPF;
  nand2$ nand_ExceptPF (nExceptPF, iRd, tlbMiss);

  wire tlbWrE;
  and2$ and_tlbWrE (tlbWrE, tlbAddr[`TLB_WRE], tlbHit);
  //and3$ and_ExceptWrP (oExceptWrP, iRd, nExceptPF, tlbAddr[`TLB_WRE]); //Write Protect only if TLB page is found
  and3$ and_ExceptWrP (oExceptWrP, iRd, nExceptPF, tlbWrE);

  mux4$ pcdmux 
    (oPCD, pcd, 1'b0, 1'b0, 1'b0, tlbMiss, iWr);
  assign oAddr = {tlbAddr[`TLB_A:0], iAddr[30-`TLB_A:0]};

  //Tag Read
  assign tagAddrIn = iAddr[lineAddrMSB:lineAddrLSB];

  //Tag Compare
  wire ntagHit;
  NCompare #(ptagValid+1) ntagcmp
    (ntagHit, tagDataOut[ptagValid:0],
    {1'b1, oAddr[tagAddrMSB:tagAddrLSB]}
    );

  wire tagMissRd, tagMissWr, tagMiss;
  and2$ and_tagMissRd (tagMissRd, ntagHit, iRd);
  and2$ and_tagMissWr (tagMissWr, iWr, iPCD);
  or2$ or_tagMiss (tagMiss, tagMissRd, tagMissWr); //goes to FSM

  wire oMissNormal;
  or3$ or_oMissNormal (oMissNormal, tagMissRd, tagMissWr, oFSM_Miss);

  //Read/Write Address
  assign ramAddrIn = iAddr[lineAddrMSB:lineAddrLSB];

  //Read/Write Data Bus/Input Mux
  mux2$ dramDataMux [blockBits-1:0]
    (ramDataIn, iDataW, oBusDataShft, oFSM_BusR);
  
  mux4$ dramWrEnMux [blockBytes-1:0] //don't ramWr during busOps
    (ramWrEnIn, iWrB, oBusWrShft, 1'b0, 1'b0, oFSM_BusR, pcd);

  mux2$ mux_oData [blockBits-1:0]
    (oData, ramDataOut, {{(blockBits-32){1'b0}}, obusData}, pcd);

  //Write Tag (MSBs are {dirty, valid})
  mux4$ tagWrEnMux [ptagAddrBytes-1:0] //don't tagWr during busOps
    (tagWrEnIn, iWr, 1'b1, 1'b0, 1'b0, oFSM_CRst, pcd);

  mux2$ tagWrDataMux [ptagAddrSize-1:0]
    (tagDataIn, 
     {tagPad, iWr, tagDataOut[ptagValid:0]},
     {tagPad, 1'b0, 1'b1, oAddr[tagAddrMSB:tagAddrLSB]}, 
     oFSM_CRst
    );

  //Counter for cache-line write-back and read
  wire countDone, countRst;
  wire ncountRst;
  wire [bwordAddrSize-1:0] countVal;

  not1$ not_countRst (ncountRst, countRst);
  Counter #(bwordAddrSize+1) 
    wordCount ({countDone, countVal}, obusV, iClk, ncountRst);

  //Dirty tag to help skip a phase if we can
  wire dirty;
  assign dirty = tagDataOut[ptagDirty];

  //Master FSM
  localparam stateWidth = 4;
  localparam numStates = 14;
  localparam iStateWidth = 9;
  localparam oStateWidth = stateWidth+2;
  wire [stateWidth-1:0] FSM_state;
  wire [oStateWidth-1:0] FSM_nstateRaw;
  wire [oStateWidth-1:0] FSM_nstate;

  wire [numStates*iStateWidth-1:0] conditions, mask;
  assign conditions = 
    {9'bxxxxx_0000, 
     9'b10xxx_1011, 9'b01xxx_1011, 9'b001xx_1011, 9'b000xx_1011,
     9'bxxx0x_1000, 9'b10x1x_1000, 9'b01x1x_1000,
     9'bxxxxx_0011,
     9'bxxxx0_0001, 9'bxxxx1_0001,
     9'bxxxxx_0100,
     9'bxxxx0_0010, 9'bxxxx1_0010};

  assign mask = 
    {9'b00000_1111, 
     9'b11000_1111, 9'b11000_1111, 9'b11100_1111, 9'b11100_1111,
     9'b00010_1111, 9'b11010_1111, 9'b11010_1111,
     9'b00000_1111,
     9'b00001_1111, 9'b00001_1111,
     9'b00000_1111,
     9'b00001_1111, 9'b00001_1111};
  
  wire [numStates*oStateWidth-1:0] outputs;
  assign outputs = 
    {6'b01_0000, 
     6'b11_1000, 6'b11_1000, 6'b11_0001, 6'b11_0100,
     6'b11_1000, 6'b11_0011, 6'b01_0000,
     6'b01_0000,
     6'b10_0001, 6'b10_0100,
     6'b11_0010,
     6'b10_0010, 6'b00_0000};

  wire [iStateWidth-1:0] FSM_input;
  assign FSM_input = {iPCD, oPCD, dirty, obusV, countDone, FSM_state};
  
  FSMLogic #(oStateWidth, iStateWidth, numStates) fsmlogic
    (FSM_nstateRaw, FSM_input, conditions, mask, outputs);

  /*
  always@(*) begin
    casex(FSM_input)
     //'bMPDVC_s                         'bMC_s
      9'bxxxxx_0000: FSM_nstateRaw <= #2 6'b01_0000; //IDLE -> IDLE

      //Transition from IDLE to root is done with tagMiss masking

      9'b10xxx_1011: FSM_nstateRaw <= #2 6'b11_1000; //ROOT -> BRQ
      9'b01xxx_1011: FSM_nstateRaw <= #2 6'b11_1000; //ROOT -> BRQ
      9'b001xx_1011: FSM_nstateRaw <= #2 6'b11_0001; //ROOT -> CWR
      9'b000xx_1011: FSM_nstateRaw <= #2 6'b11_0100; //ROOT -> CRDI

      9'bxxx0x_1000: FSM_nstateRaw <= #2 6'b11_1000; //BRQ -> BRQ
      9'b10x1x_1000: FSM_nstateRaw <= #2 6'b11_0011; //BRQ -> BRQD //bus writes need extra cycle to advance
      9'b01x1x_1000: FSM_nstateRaw <= #2 6'b01_0000; //BRQ -> IDLE
      
      9'bxxxxx_0011: FSM_nstateRaw <= #2 6'b01_0000; //BRQD -> IDLE

      9'bxxxx0_0001: FSM_nstateRaw <= #2 6'b10_0001; //CWR -> CWR
      9'bxxxx1_0001: FSM_nstateRaw <= #2 6'b10_0100; //CWR -> CRDI

      9'bxxxxx_0100: FSM_nstateRaw <= #2 6'b11_0010; //CRDI -> CRD

      9'bxxxx0_0010: FSM_nstateRaw <= #2 6'b10_0010; //CRD -> CRD
      9'bxxxx1_0010: FSM_nstateRaw <= #2 6'b00_0000; //CRD -> IDLE

      default: $display("BAD CACHE STATE INPUTS: 0x%0x", FSM_input);
    endcase
  end
  */

  //FSM nstate cleanup
  wire idleState, missDetect;
  nor4$ nand_idle (idleState, FSM_state[0], FSM_state[1], FSM_state[2], FSM_state[3]);
  //Compare #(16) cmp_idle (idleState, {4{FSM_state[3:0]}}, 16'b0);
  and3$ and_missDetect (missDetect, idleState, tagMiss, nExceptPF);

  or2$ or_nstateMask [2:0] //fast force into ROOT state
    ({FSM_nstate[3], FSM_nstate[1:0]}, missDetect, {FSM_nstateRaw[3], FSM_nstateRaw[1:0]});

  mux4$ mux_omiss (oMiss, 1'b0, 1'b0, oFSM_Miss, missDetect, idleState, nExceptPF);
  assign FSM_nstate[5:4] = FSM_nstateRaw[5:4];
  assign FSM_nstate[2] = FSM_nstateRaw[2];

  //State registers
  regs$ #(stateWidth) stateRegs
    (FSM_state, FSM_nstate[stateWidth-1:0], 1'b1, iClk, inReset);

  //FSM outputs
  assign {oFSM_Miss, countRst} = FSM_nstate[stateWidth+1:stateWidth];

  wire [tagAddrMSB:tagAddrLSB] oFSM_AddrTag;
  mux2$ fsm_addrTag [tagAddrMSB:tagAddrLSB]
    (oFSM_AddrTag, 
     oAddr[tagAddrMSB:tagAddrLSB],
     tagDataOut[ptagAddrMSB:ptagAddrLSB],
     oFSM_BusW);
  //assign oFSM_AddrTag = tagDataOut[ptagAddrMSB:ptagAddrLSB];
  assign oFSM_Addr = {oFSM_AddrTag, countVal, 2'b0};

  assign oFSM_CRst = FSM_state[2];
  //assign oFSM_PCDWR = FSM_state[3];
 
  wire ncountDone;
  not1$ ncntDone (ncountDone, countDone);

  wire busRdRaw, busWrRaw;
  and3$ buswrawstate (busRdRaw, FSM_state[1], ncountDone, npcd);
  and3$ busrrawstate (busWrRaw, FSM_state[0], ncountDone, npcd);
  xor2$ or_fsmActive (oFSM_BusRW, busRdRaw, busWrRaw);
  and2$ buswstate (oFSM_BusW, busWrRaw, oFSM_BusRW);
  and2$ busrstate (oFSM_BusR, busRdRaw, oFSM_BusRW);
  
  wire fsm_3zeros;
  nor3$ nor3_zeros (fsm_3zeros, FSM_state[2], FSM_state[1], FSM_state[0]);
  and2$ and_pcdwr (oFSM_PCDWR, FSM_state[3], fsm_3zeros);

endmodule

`default_nettype wire
`endif
