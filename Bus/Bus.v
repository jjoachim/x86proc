`ifndef _BUS_V_
`define _BUS_V_

`include "Parameters.v"
`include "CommonLogic/CommonLogic.v"

`default_nettype none

/*****************************************************************************/
// Bus Layout:
// {32'data, 32'addr, 4'wr, 1'rd, 1'v,
//  state, masterNum, masters{mreq}}
/*****************************************************************************/
//Since we only have an 8->3 decoder, we restrict masters to 7 (+1 reserved).  
//For better flexibility, write a parameterizable decoder for BusLogic.
`define BUS_MADDR_SZ $clog2(8)
`define BUS_ADDR_RSVD 32'hFFFFFFFF
`define BUS_MADDR_RSVD 7

`define BUS_ADDR_SZ 32
`define BUS_DATA_SZ 32
`define BUS_WR_SZ 4
`define BUS_RD_SZ 1
`define BUS_V_SZ 1
`define BUS_S_SZ 2

`define BUS_PACKET_SZ (`BUS_DATA_SZ + \
                       `BUS_WR_SZ + `BUS_RD_SZ + `BUS_V_SZ)

`define BUS_SZ (`BUS_ADDR_SZ + `BUS_PACKET_SZ + `BUS_S_SZ + \
                `BUS_MADDR_SZ + `BUS_NUM_MASTERS)

`define BUS_ADDR_MSB (`BUS_SZ-1)
`define BUS_ADDR_LSB (`BUS_ADDR_MSB-`BUS_ADDR_SZ+1)
`define BUS_DATA_MSB (`BUS_ADDR_LSB-1)
`define BUS_DATA_LSB (`BUS_DATA_MSB-`BUS_DATA_SZ+1)
`define BUS_WR_MSB   (`BUS_DATA_LSB-1)
`define BUS_WR_LSB   (`BUS_WR_MSB-`BUS_WR_SZ+1)
`define BUS_RD_BIT   (`BUS_WR_LSB-1)
`define BUS_V_BIT    (`BUS_RD_BIT-1)
`define BUS_S_MSB    (`BUS_V_BIT-1)
`define BUS_S_LSB    (`BUS_S_MSB-`BUS_S_SZ+1)
`define BUS_MADDR_MSB   (`BUS_S_LSB-1)
`define BUS_MADDR_LSB   (`BUS_MADDR_MSB-`BUS_MADDR_SZ+1)

`define BUS_PACKET_MSB `BUS_DATA_MSB
`define BUS_PACKET_LSB `BUS_V_BIT
`define BUS_MSB (`BUS_SZ-1)

/*****************************************************************************/
// Bus Master
/*****************************************************************************/
module BusMaster (ioBus, oData, oV, iData, iAddr, iWr, iRd);
  parameter masterBusID = 0;

  //Crashes VCS?!
  //`STATIC_ASSERT(masterBusID != `BUS_MADDR_RSVD, master_ID_is_reserved);

  inout [`BUS_MSB:0] ioBus;
  input [31:0] iData, iAddr;
  input [3:0] iWr;
  input iRd;

  output [31:0] oData;
  output oV;

  wire [`BUS_MADDR_SZ-1:0] masterID;
  assign masterID = masterBusID;

  //Read control from bus
  wire myAddr;
  wire [`BUS_MADDR_SZ-1:0] busMAddr;
  assign busMAddr = ioBus[`BUS_MADDR_MSB:`BUS_MADDR_LSB];
  Compare #(`BUS_MADDR_SZ) cmp_busMAddr 
    (myAddr, busMAddr, masterID);

  //Read the bus state
  wire [`BUS_S_SZ-1:0] busState;
  assign busState = ioBus[`BUS_S_MSB:`BUS_S_LSB];
  
  //Generate control signals
  wire masterRd, masterWr, mgen, msel;
  Compare #(`BUS_S_SZ) slvRdCmp
    (masterRd, busState, 2'b10);
  not1$ not_masterWr (masterWr, busState[1]);
  
  and2$ andEn_ssel (msel, masterRd, myAddr);
  and2$ andEn_sgen (mgen, masterWr, myAddr);

  //Output command package to bus
  wire nmgen, nmsel;
  not1$ not_mgen (nmgen, mgen);
  not1$ not_msel (nmsel, msel);
  tristateL$ tbData [`BUS_PACKET_SZ-1:0]
    (.enbar(nmgen), 
     .in({iData, iWr, iRd, 1'b0}),
     .out(ioBus[`BUS_PACKET_MSB:`BUS_PACKET_LSB])
    );

  wire naddrEn, nmgenDelay;
  Skew #(4) skew_nmgendelay (nmgenDelay, nmgen);
  and2$ and_naddrEn (naddrEn, nmsel, nmgenDelay);
  tristateL$ tbAddr [`BUS_ADDR_SZ-1:0]
    (.enbar(naddrEn), 
     .in(iAddr),
     .out(ioBus[`BUS_ADDR_MSB:`BUS_ADDR_LSB])
    );
  
  //Output control to bus
  wire wrbool, mreq;
  or4$ or_wrbool (wrbool, iWr[0], iWr[1], iWr[2], iWr[3]);
  or2$ or_mreq (mreq, wrbool, iRd);
  assign ioBus[masterBusID] = mreq;

  //Output to interface
  and2$ and_ovalid (oV, ioBus[`BUS_V_BIT], msel);
  assign oData = ioBus[`BUS_DATA_MSB:`BUS_DATA_LSB];

endmodule

/*****************************************************************************/
// Bus Slave
/*****************************************************************************/
module BusSlave (ioBus, oData, oAddr, oWr, oRd, iData, iV);
  parameter slaveAddrL = 32'h00000000;
  parameter slaveAddrH = 32'hFFFFFFF0;

  //Crashes VCS?!
  `STATIC_ASSERT(`BUS_ADDR_RSVD > slaveAddrH ||
                 `BUS_ADDR_RSVD < slaveAddrL, 
                 slave_contains_reserved_address);

  inout [`BUS_MSB:0] ioBus;
  output [31:0] oData, oAddr;
  output [3:0] oWr;
  output oRd;

  input [31:0] iData;
  input iV;

  //Read the slave address select
  wire [31:0] busAddr;
  assign busAddr = ioBus[`BUS_ADDR_MSB:`BUS_ADDR_LSB];

  //Read the bus state
  wire [`BUS_S_SZ-1:0] busState;
  assign busState = ioBus[`BUS_S_MSB:`BUS_S_LSB];
 
  //Range check the address
  wire myAddr, gte, lte;
  
  Relation32 alu_gte (.gte(gte), .in1(busAddr),  .in2(slaveAddrL));
  Relation32 alu_lte (.lte(lte), .in1(busAddr),  .in2(slaveAddrH));
  and2$ inrange (myAddr, lte, gte);
  //assign myAddr = (busAddr >= slaveAddrL) && (busAddr <= slaveAddrH);
 
  //Generate control signals
  wire slaveRd, slaveWr, ssel, sgen;
  Compare #(`BUS_S_SZ) slvRdCmp
    (slaveRd, busState, 2'b01);
  Compare #(`BUS_S_SZ) slvWrCmp
    (slaveWr, busState, 2'b10);
  and2$ andEn_ssel (ssel, slaveRd, myAddr);
  and2$ andEn_sgen (sgen, slaveWr, myAddr);

  //Output to bus
  wire nsgen;
  not1$ not_sgen (nsgen, sgen);
  tristateL$ tbData [`BUS_DATA_SZ-1:0]
    (.enbar(nsgen), 
     .in(iData),
     .out(ioBus[`BUS_DATA_MSB:`BUS_DATA_LSB])
    );
  tristateL$ tbValid
    (.enbar(nsgen), 
     .in(iV),
     .out(ioBus[`BUS_V_BIT])
    );

  //Output to interface
  and2$ oDataEn [`BUS_DATA_SZ-1:0]
    (oData, ioBus[`BUS_DATA_MSB:`BUS_DATA_LSB], ssel);
  and2$ oAddrEn [`BUS_ADDR_SZ-1:0]
    (oAddr, ioBus[`BUS_ADDR_MSB:`BUS_ADDR_LSB], ssel);

  and2$ oWrEn [`BUS_WR_SZ-1:0]
    (oWr, ioBus[`BUS_WR_MSB:`BUS_WR_LSB], ssel);
  and2$ oRdEn
    (oRd, ioBus[`BUS_RD_BIT], ssel);

endmodule

/*****************************************************************************/
// Bus Logic
/*****************************************************************************/
module Bus (ioBus, iClk, inReset);
  parameter numMasters = `BUS_NUM_MASTERS;

  inout [`BUS_MSB:0] ioBus;
  input iClk, inReset;

  wire [7:0] iReq;
  wire iSlaveValid;

  assign iSlaveValid = ioBus[`BUS_V_BIT];
  assign iReq = {{(8-numMasters){1'b0}}, ioBus[numMasters-1:0]};

  //FSM signals
  wire FSM_buffWrEn;

  //Priorty master requests
  wire masterValid;
  wire [`BUS_MADDR_SZ-1:0] masterAddr;
  pencoder8_3v$ encoder_master
    (.Y(masterAddr), .valid(masterValid), .X(iReq), .enbar(1'b0));

  //Buffer priority master requests
  wire [`BUS_MADDR_SZ-1:0] buffMasterAddr;
  regs$ #(`BUS_MADDR_SZ) regs_masterAddr
    (buffMasterAddr, masterAddr, FSM_buffWrEn, iClk, 1'b1);

  //Bus FSM
  wire slaveValid;
  assign slaveValid = iSlaveValid;
  wire [`BUS_S_SZ-1:0] FSM_state;
  wire [`BUS_S_SZ:0] FSM_nextState;
  
  FSMLogic #(`BUS_S_SZ+1, 4, 5) fsmlogic
    (FSM_nextState, {masterValid, slaveValid, FSM_state},
    {4'b0x00, 4'b1x00, 4'bxx01, 4'bx010, 4'bx110},
    {4'b1011, 4'b1011, 4'b0011, 4'b0111, 4'b0111},
    {3'b100,  3'b101,  3'b010,  3'b010,  3'b000 });

  /*
  always@(*) begin
    casex({masterValid, slaveValid, FSM_state})
      4'b0x_00: FSM_nextState <= 4'b1_00; //IDLE -> IDLE
      4'b1x_00: FSM_nextState <= 4'b1_01; //IDLE -> MAST

      4'bxx_01: FSM_nextState <= 4'b0_10; //MAST -> SLAV

      4'bx0_10: FSM_nextState <= 4'b0_10; //SLAVE -> SLAVE
      4'bx1_10: FSM_nextState <= 4'b0_00; //SLAVE -> IDLE

    endcase
  end
  */
  
  //State registers
  regs$ #(`BUS_S_SZ) stateRegs
    (FSM_state, FSM_nextState[`BUS_S_SZ-1:0], 1'b1, iClk, inReset);
  nor2$ nor_buffwrEn (FSM_buffWrEn, FSM_state[1], FSM_state[0]);
  /*
  regs$ #(1, 1) buffWrEn
    (FSM_buffWrEn, FSM_nextState[`BUS_S_SZ], 1'b1, iClk, inReset);
  */

  /*
  always@(posedge iClk) begin
    FSM_state <= FSM_nextState[`BUS_S_SZ-1:0];
    FSM_buffWrEn <= FSM_nextState[`BUS_S_SZ];
  end
  */

  //Calculate outputs
  wire [`BUS_MADDR_SZ-1:0] mAddrMux, oMAddr;
  wire addrClear, naddrClear, nmasterValid;
  
  not1$ not_masterValid (nmasterValid, masterValid);
  and2$ and_addrClear (addrClear, nmasterValid, /*FSM_buffWrEn,*/ FSM_nextState[`BUS_S_SZ]);
  nand2$ nand_naddrClear (naddrClear, nmasterValid, FSM_buffWrEn);

  mux2$ mux_mAddr [`BUS_MADDR_SZ-1:0]
    (mAddrMux, buffMasterAddr, masterAddr, FSM_buffWrEn);
  or2$ or_oMAddr [`BUS_MADDR_SZ-1:0]
    (oMAddr, mAddrMux, addrClear);
  assign ioBus[`BUS_MADDR_MSB:`BUS_MADDR_LSB] = oMAddr;

  tristateL$ tbAddr [`BUS_ADDR_SZ-1:0]
    (.enbar(naddrClear), 
     .in(`BUS_ADDR_RSVD),
     .out(ioBus[`BUS_ADDR_MSB:`BUS_ADDR_LSB])
    );
  tristateL$ tbValid
    (.enbar(naddrClear),
     .in(1'b0),
     .out(ioBus[`BUS_V_BIT])
    );

  assign ioBus[`BUS_S_MSB:`BUS_S_LSB] = FSM_state;
endmodule

`default_nettype wire
`endif
