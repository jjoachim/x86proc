mov $0x0c00, %dx
mov %dx, %ds
mov %dx, %es

movl $0, %esi
movl $4, %edi
movl $0, %eax
movl $0, %ebx

movl $1, (%esi)
movl $-1,(%edi)
cmpsb
jne lbl_test0
addl $1, %eax
lbl_test0:
addl $1, %ebx

;eax = 0
;ebx = 1

movl $1, (%esi)
movl $1, (%edi)
cmpsb
jne lbl_test1
addl $1, %eax
lbl_test1:
addl $1, %ebx

;eax = 1
;ebx = 2

movl $0x1234, (%esi)
movl $0x1234, (%edi)
cmpsw
jne lbl_test2
addl $1, %eax
lbl_test2:
addl $1, %ebx

;eax = 2
;ebx = 3

movl $0x1235, (%esi)
movl $0x1234, (%edi)
cmpsw
jnbe lbl_test3
addl $1, %eax
lbl_test3:
addl $1, %ebx

;eax = 2 
;ebx = 4

movl $0xdeadbeef, (%esi)
movl $0xdeadbeff, (%edi)
cmpsl
jnbe lbl_test4
addl $1, %eax
lbl_test4:
addl $1, %ebx

;eax = 3 
;ebx = 5

hlt
