`ifndef _KS_ADDER32_V_
`define _KS_ADDER32_V_

module KSAdder32 (out,cout,in1,in2,cin);
input [31:0] in1,in2;
input cin;
inv1$ inverter_cin(cinv,cin);
output [31:0] out;
output cout;
// Init P and G
wire [31:0] Ginit, Pinit,Pinit0 ;
wire [32:0] sum ;
wire   Ginit_cinv;
assign   Ginit_cinv = cinv;
assign   Pinit0 = Pinit;
PGinit pginit_0 (Ginit[0],Pinit[0],in1[0],in2[0]);
PGinit pginit_1 (Ginit[1],Pinit[1],in1[1],in2[1]);
PGinit pginit_2 (Ginit[2],Pinit[2],in1[2],in2[2]);
PGinit pginit_3 (Ginit[3],Pinit[3],in1[3],in2[3]);
PGinit pginit_4 (Ginit[4],Pinit[4],in1[4],in2[4]);
PGinit pginit_5 (Ginit[5],Pinit[5],in1[5],in2[5]);
PGinit pginit_6 (Ginit[6],Pinit[6],in1[6],in2[6]);
PGinit pginit_7 (Ginit[7],Pinit[7],in1[7],in2[7]);
PGinit pginit_8 (Ginit[8],Pinit[8],in1[8],in2[8]);
PGinit pginit_9 (Ginit[9],Pinit[9],in1[9],in2[9]);
PGinit pginit_10 (Ginit[10],Pinit[10],in1[10],in2[10]);
PGinit pginit_11 (Ginit[11],Pinit[11],in1[11],in2[11]);
PGinit pginit_12 (Ginit[12],Pinit[12],in1[12],in2[12]);
PGinit pginit_13 (Ginit[13],Pinit[13],in1[13],in2[13]);
PGinit pginit_14 (Ginit[14],Pinit[14],in1[14],in2[14]);
PGinit pginit_15 (Ginit[15],Pinit[15],in1[15],in2[15]);
PGinit pginit_16 (Ginit[16],Pinit[16],in1[16],in2[16]);
PGinit pginit_17 (Ginit[17],Pinit[17],in1[17],in2[17]);
PGinit pginit_18 (Ginit[18],Pinit[18],in1[18],in2[18]);
PGinit pginit_19 (Ginit[19],Pinit[19],in1[19],in2[19]);
PGinit pginit_20 (Ginit[20],Pinit[20],in1[20],in2[20]);
PGinit pginit_21 (Ginit[21],Pinit[21],in1[21],in2[21]);
PGinit pginit_22 (Ginit[22],Pinit[22],in1[22],in2[22]);
PGinit pginit_23 (Ginit[23],Pinit[23],in1[23],in2[23]);
PGinit pginit_24 (Ginit[24],Pinit[24],in1[24],in2[24]);
PGinit pginit_25 (Ginit[25],Pinit[25],in1[25],in2[25]);
PGinit pginit_26 (Ginit[26],Pinit[26],in1[26],in2[26]);
PGinit pginit_27 (Ginit[27],Pinit[27],in1[27],in2[27]);
PGinit pginit_28 (Ginit[28],Pinit[28],in1[28],in2[28]);
PGinit pginit_29 (Ginit[29],Pinit[29],in1[29],in2[29]);
PGinit pginit_30 (Ginit[30],Pinit[30],in1[30],in2[30]);
PGinit pginit_31 (Ginit[31],Pinit[31],in1[31],in2[31]);
wire [31:0] G0, P0;
wire   G0_cinv;
assign   G0_cinv = cinv;
wire [31:0]  Pbar0;
//stage:0  start: 0  finish:0
Gunit gunit_0_0 (G0[0],P0[0],Ginit[0],Pinit[0],Ginit_cinv);
PGboxorinv pgbox_0_1 (G0[1],P0[1],Ginit[1],Pinit[1],Ginit[0],Pinit[0],Pbar0[1]);
PGboxorinv pgbox_0_2 (G0[2],P0[2],Ginit[2],Pinit[2],Ginit[1],Pinit[1],Pbar0[2]);
PGboxorinv pgbox_0_3 (G0[3],P0[3],Ginit[3],Pinit[3],Ginit[2],Pinit[2],Pbar0[3]);
PGboxorinv pgbox_0_4 (G0[4],P0[4],Ginit[4],Pinit[4],Ginit[3],Pinit[3],Pbar0[4]);
PGboxorinv pgbox_0_5 (G0[5],P0[5],Ginit[5],Pinit[5],Ginit[4],Pinit[4],Pbar0[5]);
PGboxorinv pgbox_0_6 (G0[6],P0[6],Ginit[6],Pinit[6],Ginit[5],Pinit[5],Pbar0[6]);
PGboxorinv pgbox_0_7 (G0[7],P0[7],Ginit[7],Pinit[7],Ginit[6],Pinit[6],Pbar0[7]);
PGboxorinv pgbox_0_8 (G0[8],P0[8],Ginit[8],Pinit[8],Ginit[7],Pinit[7],Pbar0[8]);
PGboxorinv pgbox_0_9 (G0[9],P0[9],Ginit[9],Pinit[9],Ginit[8],Pinit[8],Pbar0[9]);
PGboxorinv pgbox_0_10 (G0[10],P0[10],Ginit[10],Pinit[10],Ginit[9],Pinit[9],Pbar0[10]);
PGboxorinv pgbox_0_11 (G0[11],P0[11],Ginit[11],Pinit[11],Ginit[10],Pinit[10],Pbar0[11]);
PGboxorinv pgbox_0_12 (G0[12],P0[12],Ginit[12],Pinit[12],Ginit[11],Pinit[11],Pbar0[12]);
PGboxorinv pgbox_0_13 (G0[13],P0[13],Ginit[13],Pinit[13],Ginit[12],Pinit[12],Pbar0[13]);
PGboxorinv pgbox_0_14 (G0[14],P0[14],Ginit[14],Pinit[14],Ginit[13],Pinit[13],Pbar0[14]);
PGboxorinv pgbox_0_15 (G0[15],P0[15],Ginit[15],Pinit[15],Ginit[14],Pinit[14],Pbar0[15]);
PGboxorinv pgbox_0_16 (G0[16],P0[16],Ginit[16],Pinit[16],Ginit[15],Pinit[15],Pbar0[16]);
PGboxorinv pgbox_0_17 (G0[17],P0[17],Ginit[17],Pinit[17],Ginit[16],Pinit[16],Pbar0[17]);
PGboxorinv pgbox_0_18 (G0[18],P0[18],Ginit[18],Pinit[18],Ginit[17],Pinit[17],Pbar0[18]);
PGboxorinv pgbox_0_19 (G0[19],P0[19],Ginit[19],Pinit[19],Ginit[18],Pinit[18],Pbar0[19]);
PGboxorinv pgbox_0_20 (G0[20],P0[20],Ginit[20],Pinit[20],Ginit[19],Pinit[19],Pbar0[20]);
PGboxorinv pgbox_0_21 (G0[21],P0[21],Ginit[21],Pinit[21],Ginit[20],Pinit[20],Pbar0[21]);
PGboxorinv pgbox_0_22 (G0[22],P0[22],Ginit[22],Pinit[22],Ginit[21],Pinit[21],Pbar0[22]);
PGboxorinv pgbox_0_23 (G0[23],P0[23],Ginit[23],Pinit[23],Ginit[22],Pinit[22],Pbar0[23]);
PGboxorinv pgbox_0_24 (G0[24],P0[24],Ginit[24],Pinit[24],Ginit[23],Pinit[23],Pbar0[24]);
PGboxorinv pgbox_0_25 (G0[25],P0[25],Ginit[25],Pinit[25],Ginit[24],Pinit[24],Pbar0[25]);
PGboxorinv pgbox_0_26 (G0[26],P0[26],Ginit[26],Pinit[26],Ginit[25],Pinit[25],Pbar0[26]);
PGboxorinv pgbox_0_27 (G0[27],P0[27],Ginit[27],Pinit[27],Ginit[26],Pinit[26],Pbar0[27]);
PGboxorinv pgbox_0_28 (G0[28],P0[28],Ginit[28],Pinit[28],Ginit[27],Pinit[27],Pbar0[28]);
PGboxorinv pgbox_0_29 (G0[29],P0[29],Ginit[29],Pinit[29],Ginit[28],Pinit[28],Pbar0[29]);
PGboxorinv pgbox_0_30 (G0[30],P0[30],Ginit[30],Pinit[30],Ginit[29],Pinit[29],Pbar0[30]);
PGboxorinv pgbox_0_31 (G0[31],P0[31],Ginit[31],Pinit[31],Ginit[30],Pinit[30],Pbar0[31]);
wire [31:0] G1, P1;
wire   G1_cinv;
assign   G1_cinv = cinv;
//stage:1  start: 1  finish:2
PGbuf pgbuf_1_0 (G1[0],P1[0],G0[0],P0[0]);
Gunit gunit_1_1 (G1[1],P1[1],G0[1],P0[1],G0_cinv);
Gunit gunit_1_2 (G1[2],P1[2],G0[2],P0[2],G0[0]);
PGboxnand pgbox_1_3 (G1[3],P1[3],G0[3],P0[3],G0[1],P0[1],Pbar0[3],Pbar0[1]);
PGboxnand pgbox_1_4 (G1[4],P1[4],G0[4],P0[4],G0[2],P0[2],Pbar0[4],Pbar0[2]);
PGboxnand pgbox_1_5 (G1[5],P1[5],G0[5],P0[5],G0[3],P0[3],Pbar0[5],Pbar0[3]);
PGboxnand pgbox_1_6 (G1[6],P1[6],G0[6],P0[6],G0[4],P0[4],Pbar0[6],Pbar0[4]);
PGboxnand pgbox_1_7 (G1[7],P1[7],G0[7],P0[7],G0[5],P0[5],Pbar0[7],Pbar0[5]);
PGboxnand pgbox_1_8 (G1[8],P1[8],G0[8],P0[8],G0[6],P0[6],Pbar0[8],Pbar0[6]);
PGboxnand pgbox_1_9 (G1[9],P1[9],G0[9],P0[9],G0[7],P0[7],Pbar0[9],Pbar0[7]);
PGboxnand pgbox_1_10 (G1[10],P1[10],G0[10],P0[10],G0[8],P0[8],Pbar0[10],Pbar0[8]);
PGboxnand pgbox_1_11 (G1[11],P1[11],G0[11],P0[11],G0[9],P0[9],Pbar0[11],Pbar0[9]);
PGboxnand pgbox_1_12 (G1[12],P1[12],G0[12],P0[12],G0[10],P0[10],Pbar0[12],Pbar0[10]);
PGboxnand pgbox_1_13 (G1[13],P1[13],G0[13],P0[13],G0[11],P0[11],Pbar0[13],Pbar0[11]);
PGboxnand pgbox_1_14 (G1[14],P1[14],G0[14],P0[14],G0[12],P0[12],Pbar0[14],Pbar0[12]);
PGboxnand pgbox_1_15 (G1[15],P1[15],G0[15],P0[15],G0[13],P0[13],Pbar0[15],Pbar0[13]);
PGboxnand pgbox_1_16 (G1[16],P1[16],G0[16],P0[16],G0[14],P0[14],Pbar0[16],Pbar0[14]);
PGboxnand pgbox_1_17 (G1[17],P1[17],G0[17],P0[17],G0[15],P0[15],Pbar0[17],Pbar0[15]);
PGboxnand pgbox_1_18 (G1[18],P1[18],G0[18],P0[18],G0[16],P0[16],Pbar0[18],Pbar0[16]);
PGboxnand pgbox_1_19 (G1[19],P1[19],G0[19],P0[19],G0[17],P0[17],Pbar0[19],Pbar0[17]);
PGboxnand pgbox_1_20 (G1[20],P1[20],G0[20],P0[20],G0[18],P0[18],Pbar0[20],Pbar0[18]);
PGboxnand pgbox_1_21 (G1[21],P1[21],G0[21],P0[21],G0[19],P0[19],Pbar0[21],Pbar0[19]);
PGboxnand pgbox_1_22 (G1[22],P1[22],G0[22],P0[22],G0[20],P0[20],Pbar0[22],Pbar0[20]);
PGboxnand pgbox_1_23 (G1[23],P1[23],G0[23],P0[23],G0[21],P0[21],Pbar0[23],Pbar0[21]);
PGboxnand pgbox_1_24 (G1[24],P1[24],G0[24],P0[24],G0[22],P0[22],Pbar0[24],Pbar0[22]);
PGboxnand pgbox_1_25 (G1[25],P1[25],G0[25],P0[25],G0[23],P0[23],Pbar0[25],Pbar0[23]);
PGboxnand pgbox_1_26 (G1[26],P1[26],G0[26],P0[26],G0[24],P0[24],Pbar0[26],Pbar0[24]);
PGboxnand pgbox_1_27 (G1[27],P1[27],G0[27],P0[27],G0[25],P0[25],Pbar0[27],Pbar0[25]);
PGboxnand pgbox_1_28 (G1[28],P1[28],G0[28],P0[28],G0[26],P0[26],Pbar0[28],Pbar0[26]);
PGboxnand pgbox_1_29 (G1[29],P1[29],G0[29],P0[29],G0[27],P0[27],Pbar0[29],Pbar0[27]);
PGboxnand pgbox_1_30 (G1[30],P1[30],G0[30],P0[30],G0[28],P0[28],Pbar0[30],Pbar0[28]);
PGboxnand pgbox_1_31 (G1[31],P1[31],G0[31],P0[31],G0[29],P0[29],Pbar0[31],Pbar0[29]);
wire [31:0] G2, P2;
wire   G2_cinv;
assign   G2_cinv = cinv;
wire [31:0]  Pbar2;
//stage:2  start: 3  finish:6
PGbuf pgbuf_2_0 (G2[0],P2[0],G1[0],P1[0]);
PGbuf pgbuf_2_1 (G2[1],P2[1],G1[1],P1[1]);
PGbuf pgbuf_2_2 (G2[2],P2[2],G1[2],P1[2]);
Gunit gunit_2_3 (G2[3],P2[3],G1[3],P1[3],G1_cinv);
Gunit gunit_2_4 (G2[4],P2[4],G1[4],P1[4],G1[0]);
Gunit gunit_2_5 (G2[5],P2[5],G1[5],P1[5],G1[1]);
Gunit gunit_2_6 (G2[6],P2[6],G1[6],P1[6],G1[2]);
PGboxorinv pgbox_2_7 (G2[7],P2[7],G1[7],P1[7],G1[3],P1[3],Pbar2[7]);
PGboxorinv pgbox_2_8 (G2[8],P2[8],G1[8],P1[8],G1[4],P1[4],Pbar2[8]);
PGboxorinv pgbox_2_9 (G2[9],P2[9],G1[9],P1[9],G1[5],P1[5],Pbar2[9]);
PGboxorinv pgbox_2_10 (G2[10],P2[10],G1[10],P1[10],G1[6],P1[6],Pbar2[10]);
PGboxorinv pgbox_2_11 (G2[11],P2[11],G1[11],P1[11],G1[7],P1[7],Pbar2[11]);
PGboxorinv pgbox_2_12 (G2[12],P2[12],G1[12],P1[12],G1[8],P1[8],Pbar2[12]);
PGboxorinv pgbox_2_13 (G2[13],P2[13],G1[13],P1[13],G1[9],P1[9],Pbar2[13]);
PGboxorinv pgbox_2_14 (G2[14],P2[14],G1[14],P1[14],G1[10],P1[10],Pbar2[14]);
PGboxorinv pgbox_2_15 (G2[15],P2[15],G1[15],P1[15],G1[11],P1[11],Pbar2[15]);
PGboxorinv pgbox_2_16 (G2[16],P2[16],G1[16],P1[16],G1[12],P1[12],Pbar2[16]);
PGboxorinv pgbox_2_17 (G2[17],P2[17],G1[17],P1[17],G1[13],P1[13],Pbar2[17]);
PGboxorinv pgbox_2_18 (G2[18],P2[18],G1[18],P1[18],G1[14],P1[14],Pbar2[18]);
PGboxorinv pgbox_2_19 (G2[19],P2[19],G1[19],P1[19],G1[15],P1[15],Pbar2[19]);
PGboxorinv pgbox_2_20 (G2[20],P2[20],G1[20],P1[20],G1[16],P1[16],Pbar2[20]);
PGboxorinv pgbox_2_21 (G2[21],P2[21],G1[21],P1[21],G1[17],P1[17],Pbar2[21]);
PGboxorinv pgbox_2_22 (G2[22],P2[22],G1[22],P1[22],G1[18],P1[18],Pbar2[22]);
PGboxorinv pgbox_2_23 (G2[23],P2[23],G1[23],P1[23],G1[19],P1[19],Pbar2[23]);
PGboxorinv pgbox_2_24 (G2[24],P2[24],G1[24],P1[24],G1[20],P1[20],Pbar2[24]);
PGboxorinv pgbox_2_25 (G2[25],P2[25],G1[25],P1[25],G1[21],P1[21],Pbar2[25]);
PGboxorinv pgbox_2_26 (G2[26],P2[26],G1[26],P1[26],G1[22],P1[22],Pbar2[26]);
PGboxorinv pgbox_2_27 (G2[27],P2[27],G1[27],P1[27],G1[23],P1[23],Pbar2[27]);
PGboxorinv pgbox_2_28 (G2[28],P2[28],G1[28],P1[28],G1[24],P1[24],Pbar2[28]);
PGboxorinv pgbox_2_29 (G2[29],P2[29],G1[29],P1[29],G1[25],P1[25],Pbar2[29]);
PGboxorinv pgbox_2_30 (G2[30],P2[30],G1[30],P1[30],G1[26],P1[26],Pbar2[30]);
PGboxorinv pgbox_2_31 (G2[31],P2[31],G1[31],P1[31],G1[27],P1[27],Pbar2[31]);
wire [31:0] G3, P3;
wire   G3_cinv;
assign   G3_cinv = cinv;
//stage:3  start: 7  finish:14
PGbuf pgbuf_3_0 (G3[0],P3[0],G2[0],P2[0]);
PGbuf pgbuf_3_1 (G3[1],P3[1],G2[1],P2[1]);
PGbuf pgbuf_3_2 (G3[2],P3[2],G2[2],P2[2]);
PGbuf pgbuf_3_3 (G3[3],P3[3],G2[3],P2[3]);
PGbuf pgbuf_3_4 (G3[4],P3[4],G2[4],P2[4]);
PGbuf pgbuf_3_5 (G3[5],P3[5],G2[5],P2[5]);
PGbuf pgbuf_3_6 (G3[6],P3[6],G2[6],P2[6]);
Gunit gunit_3_7 (G3[7],P3[7],G2[7],P2[7],G2_cinv);
Gunit gunit_3_8 (G3[8],P3[8],G2[8],P2[8],G2[0]);
Gunit gunit_3_9 (G3[9],P3[9],G2[9],P2[9],G2[1]);
Gunit gunit_3_10 (G3[10],P3[10],G2[10],P2[10],G2[2]);
Gunit gunit_3_11 (G3[11],P3[11],G2[11],P2[11],G2[3]);
Gunit gunit_3_12 (G3[12],P3[12],G2[12],P2[12],G2[4]);
Gunit gunit_3_13 (G3[13],P3[13],G2[13],P2[13],G2[5]);
Gunit gunit_3_14 (G3[14],P3[14],G2[14],P2[14],G2[6]);
PGboxnand pgbox_3_15 (G3[15],P3[15],G2[15],P2[15],G2[7],P2[7],Pbar2[15],Pbar2[7]);
PGboxnand pgbox_3_16 (G3[16],P3[16],G2[16],P2[16],G2[8],P2[8],Pbar2[16],Pbar2[8]);
PGboxnand pgbox_3_17 (G3[17],P3[17],G2[17],P2[17],G2[9],P2[9],Pbar2[17],Pbar2[9]);
PGboxnand pgbox_3_18 (G3[18],P3[18],G2[18],P2[18],G2[10],P2[10],Pbar2[18],Pbar2[10]);
PGboxnand pgbox_3_19 (G3[19],P3[19],G2[19],P2[19],G2[11],P2[11],Pbar2[19],Pbar2[11]);
PGboxnand pgbox_3_20 (G3[20],P3[20],G2[20],P2[20],G2[12],P2[12],Pbar2[20],Pbar2[12]);
PGboxnand pgbox_3_21 (G3[21],P3[21],G2[21],P2[21],G2[13],P2[13],Pbar2[21],Pbar2[13]);
PGboxnand pgbox_3_22 (G3[22],P3[22],G2[22],P2[22],G2[14],P2[14],Pbar2[22],Pbar2[14]);
PGboxnand pgbox_3_23 (G3[23],P3[23],G2[23],P2[23],G2[15],P2[15],Pbar2[23],Pbar2[15]);
PGboxnand pgbox_3_24 (G3[24],P3[24],G2[24],P2[24],G2[16],P2[16],Pbar2[24],Pbar2[16]);
PGboxnand pgbox_3_25 (G3[25],P3[25],G2[25],P2[25],G2[17],P2[17],Pbar2[25],Pbar2[17]);
PGboxnand pgbox_3_26 (G3[26],P3[26],G2[26],P2[26],G2[18],P2[18],Pbar2[26],Pbar2[18]);
PGboxnand pgbox_3_27 (G3[27],P3[27],G2[27],P2[27],G2[19],P2[19],Pbar2[27],Pbar2[19]);
PGboxnand pgbox_3_28 (G3[28],P3[28],G2[28],P2[28],G2[20],P2[20],Pbar2[28],Pbar2[20]);
PGboxnand pgbox_3_29 (G3[29],P3[29],G2[29],P2[29],G2[21],P2[21],Pbar2[29],Pbar2[21]);
PGboxnand pgbox_3_30 (G3[30],P3[30],G2[30],P2[30],G2[22],P2[22],Pbar2[30],Pbar2[22]);
PGboxnand pgbox_3_31 (G3[31],P3[31],G2[31],P2[31],G2[23],P2[23],Pbar2[31],Pbar2[23]);
wire [31:0] G4, P4;
wire   G4_cinv;
assign   G4_cinv = cinv;
wire [31:0]  Pbar4;
//stage:4  start: 15  finish:30
PGbuf pgbuf_4_0 (G4[0],P4[0],G3[0],P3[0]);
PGbuf pgbuf_4_1 (G4[1],P4[1],G3[1],P3[1]);
PGbuf pgbuf_4_2 (G4[2],P4[2],G3[2],P3[2]);
PGbuf pgbuf_4_3 (G4[3],P4[3],G3[3],P3[3]);
PGbuf pgbuf_4_4 (G4[4],P4[4],G3[4],P3[4]);
PGbuf pgbuf_4_5 (G4[5],P4[5],G3[5],P3[5]);
PGbuf pgbuf_4_6 (G4[6],P4[6],G3[6],P3[6]);
PGbuf pgbuf_4_7 (G4[7],P4[7],G3[7],P3[7]);
PGbuf pgbuf_4_8 (G4[8],P4[8],G3[8],P3[8]);
PGbuf pgbuf_4_9 (G4[9],P4[9],G3[9],P3[9]);
PGbuf pgbuf_4_10 (G4[10],P4[10],G3[10],P3[10]);
PGbuf pgbuf_4_11 (G4[11],P4[11],G3[11],P3[11]);
PGbuf pgbuf_4_12 (G4[12],P4[12],G3[12],P3[12]);
PGbuf pgbuf_4_13 (G4[13],P4[13],G3[13],P3[13]);
PGbuf pgbuf_4_14 (G4[14],P4[14],G3[14],P3[14]);
Gunit gunit_4_15 (G4[15],P4[15],G3[15],P3[15],G3_cinv);
Gunit gunit_4_16 (G4[16],P4[16],G3[16],P3[16],G3[0]);
Gunit gunit_4_17 (G4[17],P4[17],G3[17],P3[17],G3[1]);
Gunit gunit_4_18 (G4[18],P4[18],G3[18],P3[18],G3[2]);
Gunit gunit_4_19 (G4[19],P4[19],G3[19],P3[19],G3[3]);
Gunit gunit_4_20 (G4[20],P4[20],G3[20],P3[20],G3[4]);
Gunit gunit_4_21 (G4[21],P4[21],G3[21],P3[21],G3[5]);
Gunit gunit_4_22 (G4[22],P4[22],G3[22],P3[22],G3[6]);
Gunit gunit_4_23 (G4[23],P4[23],G3[23],P3[23],G3[7]);
Gunit gunit_4_24 (G4[24],P4[24],G3[24],P3[24],G3[8]);
Gunit gunit_4_25 (G4[25],P4[25],G3[25],P3[25],G3[9]);
Gunit gunit_4_26 (G4[26],P4[26],G3[26],P3[26],G3[10]);
Gunit gunit_4_27 (G4[27],P4[27],G3[27],P3[27],G3[11]);
Gunit gunit_4_28 (G4[28],P4[28],G3[28],P3[28],G3[12]);
Gunit gunit_4_29 (G4[29],P4[29],G3[29],P3[29],G3[13]);
Gunit gunit_4_30 (G4[30],P4[30],G3[30],P3[30],G3[14]);
PGboxorinv pgbox_4_31 (G4[31],P4[31],G3[31],P3[31],G3[15],P3[15],Pbar4[31]);
//SUM
Sumxor sumxor_0 (sum[0],G4_cinv,Pinit[0]);
Sumxor sumxor_1 (sum[1],G4[0],Pinit[1]);
Sumxor sumxor_2 (sum[2],G4[1],Pinit[2]);
Sumxor sumxor_3 (sum[3],G4[2],Pinit[3]);
Sumxor sumxor_4 (sum[4],G4[3],Pinit[4]);
Sumxor sumxor_5 (sum[5],G4[4],Pinit[5]);
Sumxor sumxor_6 (sum[6],G4[5],Pinit[6]);
Sumxor sumxor_7 (sum[7],G4[6],Pinit[7]);
Sumxor sumxor_8 (sum[8],G4[7],Pinit[8]);
Sumxor sumxor_9 (sum[9],G4[8],Pinit[9]);
Sumxor sumxor_10 (sum[10],G4[9],Pinit[10]);
Sumxor sumxor_11 (sum[11],G4[10],Pinit[11]);
Sumxor sumxor_12 (sum[12],G4[11],Pinit[12]);
Sumxor sumxor_13 (sum[13],G4[12],Pinit[13]);
Sumxor sumxor_14 (sum[14],G4[13],Pinit[14]);
Sumxor sumxor_15 (sum[15],G4[14],Pinit[15]);
Sumxor sumxor_16 (sum[16],G4[15],Pinit[16]);
Sumxor sumxor_17 (sum[17],G4[16],Pinit[17]);
Sumxor sumxor_18 (sum[18],G4[17],Pinit[18]);
Sumxor sumxor_19 (sum[19],G4[18],Pinit[19]);
Sumxor sumxor_20 (sum[20],G4[19],Pinit[20]);
Sumxor sumxor_21 (sum[21],G4[20],Pinit[21]);
Sumxor sumxor_22 (sum[22],G4[21],Pinit[22]);
Sumxor sumxor_23 (sum[23],G4[22],Pinit[23]);
Sumxor sumxor_24 (sum[24],G4[23],Pinit[24]);
Sumxor sumxor_25 (sum[25],G4[24],Pinit[25]);
Sumxor sumxor_26 (sum[26],G4[25],Pinit[26]);
Sumxor sumxor_27 (sum[27],G4[26],Pinit[27]);
Sumxor sumxor_28 (sum[28],G4[27],Pinit[28]);
Sumxor sumxor_29 (sum[29],G4[28],Pinit[29]);
Sumxor sumxor_30 (sum[30],G4[29],Pinit[30]);
Sumxor sumxor_31 (sum[31],G4[30],Pinit[31]);
nor2$ carry_last (Pcarry,cinv,P4[31]);
inv1$ inv_last_g (Ginvlast4,G4[31]);
or2_mux$ sumor_32 (sum[32],Ginvlast4,Pcarry);
assign out=sum[31:0];
assign cout=sum[32];
endmodule

module Gunit (Gout,Pout,G,P,Gprev);
input G,Gprev,P;
output Gout,Pout;
nor2$ and2g(node1,P,Gprev);
inv1$ inv(Gp,G);
nor2$ or2g2(Gout,node1,Gp);
assign Pout=P;
endmodule




module PGbox (Gout,Pout,G,P,Gprev,Pprev);
input G,Gprev,P,Pprev;
output Gout,Pout;
or2_mux$ and1g(Pout,P,Pprev);
nor2$ and2g(node1,P,Gprev);
inv1$ inv(Gp,G);
nor2$ or2g2(Gout,node1,Gp);
endmodule

module PGboxorinv (Gout,Pout,G,P,Gprev,Pprev,Pbarout);
input G,Gprev,P,Pprev;
inout Gout,Pout,Pbarout;//not really inout, vcs stupid
nor2$ and1g(Pbarout,P,Pprev);
inv1$ inv1g(Pout,Pbarout);
nor2$ and2g(node1,P,Gprev);
inv1$ inv(Gp,G);
nor2$ or2g2(Gout,node1,Gp);
endmodule


module PGboxnand (Gout,Pout,G,P,Gprev,Pprev,Pbar,Pbarprev);
input G,Gprev,P,Pprev,Pbar,Pbarprev;
output Gout,Pout;
//or2$ and1g(Pout,P,Pprev);
nand2$ and1g(Pout,Pbar,Pbarprev);
nor2$ and2g(node1,P,Gprev);
inv1$ inv(Gp,G);
nor2$ or2g2(Gout,node1,Gp);
endmodule



module PGbuf (Gout,Pout,G,P);
input G,P;
output Gout,Pout;
assign Gout=G;
assign Pout=P;
endmodule


module Sumxor (sum,in1,in2);
input in1,in2;
output sum;
xor2$ xorg(sum,in1,in2);
endmodule



module PGinit (G,P,in1,in2);
input in1,in2;
output G,P;
xnor2$ xorg(P,in1,in2);
nand2$ andg(G,in1,in2);
endmodule

module KSAdder32NC (out,in1,in2);
  output [31:0] out;
  input [31:0] in1, in2;

  wire cout;
  KSAdder32 adder (out, cout, in1, in2, 1'b0);
endmodule
module or2_mux$ (out,in1,in2);
input in1,in2;
output out;
//input 2 is the critical path
mux2$ muxor(out,in1,1'b1,in2);

endmodule

`endif
