movl $0x00000200, %eax
movw %ax, %ds

movl $0x00000000, %esi
movl $0x00000000, %eax
movl $0xffffffff, %ebx

movl $lbl_reg, (%esi)
movl $lbl_exit, 0x4(%esi)

jmp lbl_j8
movl $0x0, %ebx

lbl_halt:
hlt
movl $0x0, %ebx

lbl_exit:
jmp lbl_halt
movl $0x0, %ebx

lbl_mem:
movl $0xfff00000, %eax
jmp  *0x4(%esi)
movl $0x0, %ebx

lbl_reg:
movl $0xffff0000, %eax
movl $lbl_mem, %ecx
jmp  %ecx
movl $0x0, %ebx

.skip 0x50
lbl_j8:
movl $0xfffffff0, %eax
.long 0x0200e966
movl $0x0, %ebx

.skip 0x1fb
lbl_j16:
movl $0xffffff00, %eax
jmp lbl_j32
movl $0x0, %ebx

.skip 0x500
lbl_j32:
movl $0xfffff000, %eax
jmp *0x0
movl $0x0, %ebx
