`ifndef _BASEGENERATOR_VH
`define _BASEGENERATOR_VH

`timescale 1ns/1ps

module BaseGenerator(BASE, USEBASE, OUT3, MODRM, SIB, CONTROL_STORE);

    // I/O
    input   [31:0]  OUT3;
    input   [ 7:0]  MODRM;
    input   [ 7:0]  SIB;
    input   [68:0]  CONTROL_STORE;
    output          USEBASE;
    output  [31:0]  BASE;

    // Control-store
    wire            USEMODRM;
    wire            USESIB;

    // ModR/M and SIB values
    wire            MOD00, MOD01, MOD10, MOD11, MOD1XBAR, MODX1BAR;
    wire            BASE100, BASE100BAR, BASE101, BASE1XXBAR, BASEXX1BAR;
    wire            SIBDISPO, SIBDISPOBAR;

    // Control-store
    assign USEMODRM  = CONTROL_STORE[`iUSEMODRM];
    assign USESIB    = CONTROL_STORE[`iUSESIB];

    // ModR/M + SIB values
    inv1$ mod1xbar_inv(MOD1XBAR, MODRM[7]);
    inv1$ modx1bar_inv(MODX1BAR, MODRM[6]);
    nor2$ mod00_nor(MOD00, MODRM[7], MODRM[6]);
    nor2$ mod01_nor(MOD01, MODRM[7], MODX1BAR);
    nor2$ mod10_nor(MOD10, MOD1XBAR, MODRM[6]);
    nor2$ mod11_and(MOD11, MOD1XBAR, MODX1BAR);

    inv1$ base1xxbar_inv(BASE1XXBAR, SIB[2]);
    inv1$ basexx1bar_inv(BASEXX1BAR, SIB[0]);
    nor3$ base100_nor(BASE100, BASE1XXBAR, SIB[1], SIB[0]);
    nor3$ base101_nor(BASE101, BASE1XXBAR, SIB[1], BASEXX1BAR);

    // SIB DISPO = USESIB & MOD00 & BASE101
    and3$ sibdispo_and(SIBDISPO, USESIB, MOD00, BASE101);
    inv1$  sibdispbar_inv(SIBDISPOBAR, SIBDISPO);

    // Generate
    and2$      usebase_and(USEBASE, USESIB, SIBDISPOBAR);
    
    // Mask base register output
    mux2_32$   base_mux(BASE, 32'h0, OUT3, USEBASE);

endmodule

`endif
