use strict;
my $debug=0;
my $our_input;
my $BANK=8;
my $Outfile=$ARGV[0];
if (defined $ARGV[1]){
	$BANK=$ARGV[1];
}
my $SIZE=32*1024;
my $NUMWORDS=4;
my $ENTRIES=32;
my $BLOCK=$SIZE/($NUMWORDS*$ENTRIES);
my $BLOCKPB=$BLOCK/$BANK;
print "Banks: $BANK\n";
print "Block: $BLOCK\n";
print "Block/BANK: ",$BLOCKPB,"\n";



my %HASH;
open INFILE, "< $Outfile" or die $!;
while (<INFILE>){
	my $line=$_;
	chomp $line;
	my ($rest,@comment)=split(/[;\/]/,$line);
	unless ($rest=~/:/){
		next;
	}

	my ($address,$content_str)=split(":",$rest);
			if ($debug){
				print "$address\t\t$line   \n";		
			}
	$address=~s/^0x//;
	$address=~s/\s+//;
		$content_str=~s/^\s+//;
		$content_str=~s/\s+$//;
		my (@content)=split(/\s+/,$content_str);
		my $dec_adr=hex2dec($address);
		my $current_address=$dec_adr;
			if ($debug){
				print "$address   $content_str\n";		
			}
		foreach my $cont (@content){
			$HASH{$current_address}=$cont;
			$current_address++;


		}


	}
	close INFILE;

	my $start2=0;


	for my $banknum (0 .. $BANK-1){
		for my $block_num (0 .. $BLOCKPB-1){
			open OUTFILE, "> memfile\_$banknum\_$block_num.mem" or die $!;
			my $start=0;
			for (my $i=$block_num*$NUMWORDS*$ENTRIES*$BANK+$banknum*$NUMWORDS;                                                                                                                                  $i<$block_num*$NUMWORDS*$ENTRIES*$BANK+$banknum*$NUMWORDS+$ENTRIES*$NUMWORDS*$BANK;                                                                                                                $i=$i+                                             $NUMWORDS*$BANK){
				#print "bank $banknum block $block_num $i   ",$i%$NUMWORDS,"\n";
				#for (my $index=$i;$index<$i+$NUMWORDS;$index++){
					#if ($index % $NUMWORDS ==0){
				for (my $index=$i+$NUMWORDS-1;$index>=$i;$index--){

					if ($index % $NUMWORDS ==3){
						my $hexindex=dec2hex($index-3);
						if ($start==1){
							if ($debug){
								print OUTFILE"\n$hexindex:\t";
							}else{
								#print OUTFILE "\n0x";
								print OUTFILE "\n";
							}
						}else{
							if ($debug){
								print OUTFILE "$hexindex:\t";
							}else{
								#print OUTFILE "0x";
							}
							$start=1;
						}
					}
					if (defined $HASH{$index}){
						print OUTFILE $HASH{$index};
						#print "bank $banknum block $block_num $i   ",$HASH{$index},"\n";
					}else{
						print OUTFILE  "00";
					}


				}
			}

		}
		close OUTFILE;
	}



	sub dec2hex{
		my $in= shift;
	return ( unpack("H8", pack("N", $in)));
}


sub hex2dec{
	my $in= shift;
	return  (hex $in);
}
