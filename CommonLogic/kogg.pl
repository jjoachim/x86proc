
use strict;
# add the buffers for cin and for the intermaediate stage 
	my $invert=1;
my $i=0;
my $bits=32;
my $bit_odd=$bits-1;
my $stages=log($bits)/log(2);
my $start;
my $finish;
my $pattern;
my $module_with_inst;
print "module kogge_stone_$bits (out,cout,in1,in2,cin);\n";
print "input [$bit_odd:0] in1,in2;\n";
print "input cin;\n";
if ($invert==1){
	print"inv1\$ inverter_cin(cinv,cin);\n";
}else{
	print "assign   cinv = cin;\n";
}
print "output [$bit_odd:0] out;\n";
print "output cout;\n";
print "// Init P and G\n";
print "wire [$bit_odd:0] Ginit, Pinit,Pinit0 ;\n";
print "wire [$bits:0] sum ;\n";
print "wire   Ginit_cinv;\n";
print "assign   Ginit_cinv = cinv;\n";
print "assign   Pinit0 = Pinit;\n";
for (my $bit=0;$bit<$bits;$bit++){
	$module_with_inst="PGinit pginit";
	print	"$module_with_inst\_$bit (Ginit[$bit],Pinit[$bit],in1[$bit],in2[$bit]);\n";
}

for (my $stage=0;$stage<$stages;$stage++){
	print "wire [$bit_odd:0] G$stage, P$stage;\n";
	print "wire   G$stage\_cinv;\n";
	print "assign   G$stage\_cinv = cinv;\n";
			if ($stage%2){
			}else{
	print "wire [31:0]  Pbar$stage;\n";
			}
	$start=2**$stage-1;
	$finish=$start+$start;
	if ($stage==0){
		$pattern="init";
	}else{
		$pattern="";
	}
	print "//stage:$stage  start: $start  finish:$finish\n";
	for (my $bit=0;$bit<$bits;$bit++){
		my $prevbitnum=$bit-2**$stage;
		my $prevbit="[$prevbitnum]";
		if ($prevbitnum<0){
			$prevbit="_cinv";
		}
		my $prevstage=$stage-1;
		if ($prevstage<0){
			$prevstage="";
		}
		if ($bit<$start){
			$module_with_inst="PGbuf pgbuf";
			print	"$module_with_inst\_$stage\_$bit (G$stage\[$bit],P$stage\[$bit],G$pattern$prevstage\[$bit],P$pattern$prevstage\[$bit]);\n";
		}elsif(($bit>=$start)and($bit<=$finish)){
			$module_with_inst="Gunit gunit";
			print	"$module_with_inst\_$stage\_$bit (G$stage\[$bit],P$stage\[$bit],G$pattern$prevstage\[$bit],P$pattern$prevstage\[$bit],G$pattern$prevstage$prevbit);\n";
		}elsif($bit>$finish){
			if ($stage%2){
			#use the nand)
			$module_with_inst="PGboxnand pgbox";
			print	"$module_with_inst\_$stage\_$bit (G$stage\[$bit],P$stage\[$bit],G$pattern$prevstage\[$bit],P$pattern$prevstage\[$bit],G$pattern$prevstage$prevbit,P$pattern$prevstage$prevbit,Pbar$pattern$prevstage\[$bit],Pbar$pattern$prevstage$prevbit);\n";

		}else{
			#use the or (nor +inv)
			$module_with_inst="PGboxorinv pgbox";
			print	"$module_with_inst\_$stage\_$bit (G$stage\[$bit],P$stage\[$bit],G$pattern$prevstage\[$bit],P$pattern$prevstage\[$bit],G$pattern$prevstage$prevbit,P$pattern$prevstage$prevbit,Pbar$stage\[$bit]);\n";

		}
		$module_with_inst="PGbox pgbox";
		#print	"$module_with_inst\_$stage\_$bit (G$stage\[$bit],P$stage\[$bit],G$pattern$prevstage\[$bit],P$pattern$prevstage\[$bit],G$pattern$prevstage$prevbit,P$pattern$prevstage$prevbit);\n";
	}


}
}
my $maxstage=$stages-1;
print "//SUM\n";
for (my $bit=0;$bit<=$bits;$bit++){
	my $prevbitnum=$bit-1;
	my $prevbit="[$prevbitnum]";
	if ($prevbitnum<0){
		$prevbit="_cinv";
	}
	if ($bit==$bits){
		if ($invert==1){
			print	"nor2\$ carry_last (Pcarry,cinv,P$maxstage$prevbit);\n";
			print	"inv1\$ inv_last_g (Ginvlast$maxstage,G$maxstage$prevbit);\n";
			$module_with_inst="or2\$ sumor";
			print	"$module_with_inst\_$bit (sum[$bit],Ginvlast$maxstage,Pcarry);\n";


		}else{
			print	"and2\$ carry_last (Pcarry,cinv,P$maxstage$prevbit);\n";
			$module_with_inst="or2\$ sumor";
			print	"$module_with_inst\_$bit (sum[$bit],G$maxstage$prevbit,Pcarry);\n";
		}
	}else{
		$module_with_inst="Sumxor sumxor";
		print	"$module_with_inst\_$bit (sum[$bit],G$maxstage$prevbit,Pinit[$bit]);\n";
	}
}


print "assign out=sum[$bit_odd:0];\n";
print "assign cout=sum[$bits];\n";


print "endmodule\n";
