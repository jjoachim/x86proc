`timescale 1ns/1ps

`include "Parameters.v"
`include "TestFramework.v"

`include "Cache/Cache.v"
`include "MainMemory/MainMemory.v"
`include "DMA/DMA.v"

`define DMA_WRITES 64

module TOP;
  integer i, sz;
  reg nreset;

  `TEST_GLOBAL_INIT;

  //Instantiate Bus Logic
  wire [`BUS_MSB:0] bus; 
  Bus #(3) bus_logic (bus, `TEST_CLK, nreset);

  //Main Memory
  wire [31:0] mmbusdata, mmbusaddr, mmdata;
  wire mmvalid;
  wire [3:0] mmbuswr;
  wire mmbusrd;
  BusSlave #(`BUS_MEM_ADDRL, `BUS_MEM_ADDRH) bus_mm
    (bus, mmbusdata, mmbusaddr, mmbuswr, mmbusrd, mmdata, mmvalid);
  MainMemory #(8) mm 
    (mmdata, mmvalid, mmbusdata, mmbusaddr, mmbusrd, mmbuswr, `TEST_CLK, nreset);

  //DMA
  wire [31:0] DMA_sbusData, DMA_sbusAddr, DMA_sData;
  wire [3:0] DMA_sbusWr;
  wire DMA_sbusRd, DMA_sValid;
  BusSlave #(`BUS_DMA_ADDRL, `BUS_DMA_ADDRH) sbus_dma
    (bus, DMA_sbusData, DMA_sbusAddr, DMA_sbusWr, DMA_sbusRd, DMA_sData, DMA_sValid);

  wire [31:0] DMA_mData, DMA_mAddr, DMA_mbusData;
  wire [3:0] DMA_mWr;
  wire DMA_mRd, DMA_mbusValid;
  BusMaster #(0) mbus_dma
    (bus, DMA_mbusData, DMA_mbusValid, DMA_mData, DMA_mAddr, DMA_mWr, DMA_mRd);

  wire dmaint;
  DMA dma (DMA_mData, DMA_mAddr, DMA_mRd, DMA_mWr, DMA_mbusData, DMA_mbusValid,
           DMA_sValid, DMA_sData, DMA_sbusData, DMA_sbusAddr, DMA_sbusRd, DMA_sbusWr,
           dmaint, `TEST_CLK, nreset);

  //Instantiate Cache
  wire [63:0] odata;
  wire [31:0] odata32;
  wire [15:0] odata16;
  wire [7:0] odata8;
  assign odata8 = odata[7:0];
  assign odata16 = odata[15:0];
  assign odata32 = odata[31:0];

  wire [31:0] oaddr0, oaddr1;
  wire oexcPF, oexcWrP, omiss;
  wire [1:0] pcd, opcd;

  reg [63:0] data;
  reg [31:0] addr0;
  reg wr, rd;
  reg [1:0] opsz;

  wire [63:0] rdata;
  wire [31:0] raddr0, raddr1;
  wire rua, rwr;
  wire [1:0] ropsz, rpcd;
  
  //inferred signals
  wire [31:0] addr1;
  wire ua;
  assign addr1 = addr0 + 16;
  assign ua = ((addr0 % 16) + ((1<<opsz)-1)) >= 16;
 
  //Control signals
  wire [31:0] caddr0, caddr1;
  wire cua;

  assign caddr0 = rwr ? raddr0 : addr0;
  assign caddr1 = rwr ? raddr1 : addr1;
  assign cua = rwr ? rua : ua;

  DCache #(1) dcache 
               (odata, oaddr0, oaddr1, oexcPF, oexcWrP, omiss, opcd,
                caddr0, caddr1, cua, rd | wr, wr, rwr,
                rdata, ropsz, rpcd,
                bus, `TEST_CLK, nreset);

  //Cache Pipestage
  regs$ #(64+64+1+2+1+2) pipestage
    ({rdata, raddr0, raddr1, rua, rpcd, rwr, ropsz},
     {data, oaddr0, oaddr1,  ua, opcd,  wr,  opsz},
      ~omiss, `TEST_CLK, nreset);

  `TEST_INIT_COMB(odata, 64);
  `TEST_INIT_COMB(odata32, 32);
  `TEST_INIT_COMB(odata16, 16);
  `TEST_INIT_COMB(odata8, 8);
  `TEST_INIT_COMB(oexcPF, 1);
  `TEST_INIT_COMB(oexcWrP, 1);
  `TEST_INIT_SEQ(omiss, 1);
  `TEST_INIT_SEQ(dmaint, 1);

  initial begin
    `TEST_TOGGLE_CLK;
    wr = 0;
    rd = 0;
    addr0 = 0;
    opsz = 0;
    data = 0;
    `TEST_TOGGLE_CLK;
    nreset = 0;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    nreset = 1;
    `TEST_TOGGLE_CLK;

    `TEST_DISPLAY("\n>> Write to the DMA <<\n");
    //Write the Source Address
    opsz = 2;
    wr = 1;
    data  = 64'hFFFFFFFF;
    addr0 = 32'h10000000;
    `TEST_TOGGLE_CLK;
    data = 0;
    wr = 0;
    `TEST_WHEN(omiss==0, )

    //Write Destination Address
    wr = 1;
    data  = 64'h00000000;
    addr0 = 32'h10000004;
    `TEST_TOGGLE_CLK;
    data = 0;
    wr = 0;
    `TEST_WHEN(omiss==0, )

    //Write Size Register
    wr = 1;
    data  = `DMA_WRITES*4;
    addr0 = 32'h10000008;
    `TEST_TOGGLE_CLK;
    data = 0;
    wr = 0;
    `TEST_WHEN(omiss==0, )

    //Write Start Register
    wr = 1;
    data  = 64'hFFFFFFFF;
    addr0 = 32'h1000000C;
    `TEST_TOGGLE_CLK;
    data = 0;
    wr = 0;
    `TEST_WHEN(omiss==0, )

    `TEST_DISPLAY("\n>> Aligned QWords <<\n");
    `TEST_DISPLAYF(("Writes at %0d...", $time));
    rd = 0;
    opsz = 3;
    for(i=0; i<64; i=i+1) begin
      wr = 1;
      data = {8{i[7:0]}};
      addr0 = i*8 + 32'h02000000;
      `TEST_WHEN(omiss==0, )
      wr = 0;
      `TEST_TOGGLE_CLK;
    end

    `TEST_DISPLAYF(("\nReads at %0d...", $time));
    data = 0;
    rd = 1;
    for(i=0; i<64; i=i+1) begin
      `TEST_WHEN(omiss==0,
        addr0 = i*8 + 32'h02000000;
        `TEST_EQ(odata, {8{i[7:0]}});
      )
    end
    rd = 0;

    `TEST_DISPLAY("\n Waiting for DMA...");
    `TEST_WHEN(dmaint==1, )

    `TEST_DISPLAY("\n Clear DMA Interrupt");
    //Write the Source Address
    opsz = 2;
    wr = 1;
    data  = 64'hFFFFFFFF;
    addr0 = 32'h10000000;
    `TEST_TOGGLE_CLK;
    data = 0;
    wr = 0;
    `TEST_WHEN(omiss==0,
      `TEST_EQ(dmaint, 0);
    )

    `TEST_DISPLAY("\n>> Test DMA data <<");
    opsz = 2;
    rd = 1;
    for(i=0; i<`DMA_WRITES; i=i+1) begin
      `TEST_WHEN(omiss==0,
        addr0 = i*4;
        `TEST_EQ(odata32, 32'hABCDABCD);
      ) 
    end

    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_END;
    `TEST_DISPLAY("\n>> END TESTS <<\n");

    $finish;
  end
  
  initial begin
    $vcdplusfile("top.dump.vpd");
    $vcdpluson(0, TOP);
    $vcdpluson(0, dcache.wrDataAlign);
    $vcdpluson(0, dcache.wrBAlign);
    $vcdpluson(0, dcache.rdDataAlign);
    $vcdpluson(0, dcache.wrDataOrder);
    $vcdpluson(0, dcache.wrBOrder);
    $vcdpluson(0, dcache.rdAddr);
  end

endmodule
