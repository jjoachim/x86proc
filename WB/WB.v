`ifndef _WB_V_
`define _WB_V_

`include "defines.v"
`include "libSJQ.v"
`include "CommonLogic/Compare.v"

`default_nettype none

// Execute stage
module WB(iClk,     iNSysRst,
          inFlush,  inStall,
          oFlush,   oStall,

          oStat,    oREPE, oEIPN, oBRTK,
          oValE,    oDstR, oRegW, oSegW,
          oAddr,    oUA, oAddrP16, oPCD,
          oMemW,    oWrSz,
          oEFLAGS,  oArchEIP,

          iStat,    iControlStore,
          iRegW,    iSegW, iDstR,
          iValE,    iFLAGSN,
          iAddr,    iUA,   iAddrP16, iPCD,
          iMemW,    iWrSz,
          iEIPN,    iBRTK,
          iCS,      iEIP,  iEIPPLUS);
    
    // I/O
    // System controls
    input           iClk;
    input           iNSysRst;
    input           inFlush;
    input           inStall;
    output          oFlush;
    output          oStall;

    // EXE -> WB
    input   [31:0]  iStat;
    input   [68:0]  iControlStore;
    input           iRegW;
    input           iSegW;
    input   [ 2:0]  iDstR;
    input   [63:0]  iValE;
    input   [31:0]  iFLAGSN;
    input   [31:0]  iAddr;
    input           iUA;
    input   [31:0]  iAddrP16;
    input   [ 1:0]  iPCD;
    input           iMemW;
    input   [ 1:0]  iWrSz;
    input   [31:0]  iEIPN;
    input           iBRTK;
    input   [15:0]  iCS;
    input   [31:0]  iEIP;
    input   [31:0]  iEIPPLUS;

    // WB -> CSR
    output  [31:0]  oStat;
    output          oREPE;
    output  [31:0]  oEIPN;
    output          oBRTK;

    // WB -> RF
    output  [63:0]  oValE;
    output  [ 2:0]  oDstR;
    output          oRegW;
    output          oSegW;

    // WB -> MEM
    output  [31:0]  oAddr;
    output          oUA;
    output  [31:0]  oAddrP16;
    output  [ 1:0]  oPCD;
    output          oMemW;
    output  [ 1:0]  oWrSz;

    // WB -> EXE
    output  [31:0]  oEFLAGS;
	output	[31:0]	oArchEIP;

    // Split control-store signals
    wire            REPEPRE;    // REPE prefix present
    wire            TRAPOP;     // TRAP instruction
    wire            TRAPOPBAR;  // ~TRAPOP
    wire            ROP;        // RISC op
    wire    [ 5:0]  IROP;       // Index into ROP table
    wire    [ 2:0]  FLAGMUX;    // Flag operation

    // Exception mode latch
    wire            EXCLATCHRST;    // Exception mode latch reset
    wire            EXCMODE;        // Exception mode
    wire            EXCMODENOTRAP;  // Exception mode and no TRAP
	wire	[ 7:0]	EXCVECTOR;		// Exception vector latched

    // EFLAGS register
    wire            EFLAGSWE;
    wire    [31:0]  EFLAGS;
    wire    [31:0]  EFLAGSBAR;

    // FLAG bits
    wire            CF;
    wire            SF;
    wire            OF;
    wire            AF;
    wire            PF;
    wire            ZF;
    wire            DF;

    // Split Control-Store
    assign REPEPRE    = iControlStore[`iREPEPRE];
    assign TRAPOP     = iControlStore[`iTRAPOP];
    inv1$ trapop_inv(TRAPOPBAR, TRAPOP);
    assign ROP        = iControlStore[`iROP];
    assign IROP       = iControlStore[`iIROP];
    assign FLAGMUX    = iControlStore[`iFLAGMUX];

    // WB -> CSR
    and2$    latchrst_and(EXCLATCHRST, TRAPOPBAR, iNSysRst);
    latch_sr excmode(EXCMODE,1'b1,iStat[`iEXC],1'b1,EXCLATCHRST);
    mux2$    excmode_nolatch_mux(EXCMODENOTRAP, EXCMODE, 1'b0, TRAPOP);
    assign oStat[`iRSVD]    = iStat[`iRSVD];
    or2$   excMode_or(oStat[`iEXCMODE], iStat[`iEXC], EXCMODENOTRAP);
    assign oStat[`iEXC] = iStat[`iEXC];
    assign oStat[`iINT] = iStat[`iINT];
	regs$  #(8) exvreg(oStat[`iEXCV], iStat[`iEXCV], iStat[`iEXC], iClk, iNSysRst);
    assign oStat[7: 0]    = iStat[7:0];
    assign oREPE    = REPEPRE;
    
    //EIP register
    wire stateWr;
	wire stateWrExc;
	wire fRopWrite;
    or4$ or_stateWr (stateWr, iSegW, iRegW, iMemW, iBRTK);
	//or2$  exc_op    (stateWrExc, stateWr, iStat[`iEXC]);
	mux2$ frop_mux(fRopWrite, stateWr, 1'b0, ROP);

    wire [31:0] iEIPC;
	//wire [31:0]	SELEIP;

    mux2$ mux_EIPC [31:0] (iEIPC, iEIPPLUS, iEIPN, iBRTK);
	//mux2$ seleip_mux(SELEIP, iEIPPLUS, iEIP, 

    wire [31:0] oEIPC;
    regs$ #(32) eip_reg (oEIPC, iEIPC, fRopWrite, iClk, iNSysRst);
	assign oArchEIP = oEIPC;
    assign oEIPN    = iEIPN;


    //This logic starts the machine on startup
    wire brtaken0;
    wire brtaken1;
    regs$ #(1, 0) reg_brtaken0
      (brtaken0, 1'b0, 1'b1, iClk, iNSysRst);
    regs$ #(1, 0) reg_brtaken1
      (brtaken1, brtaken0, 1'b1, iClk, iNSysRst);
    or2$ or_brtk (oBRTK, brtaken1, iBRTK);

    // WB -> RF
    assign oValE    = iValE;
    assign oDstR    = iDstR;
    assign oRegW    = iRegW;
    assign oSegW    = iSegW;

    // WB -> MEM
    assign oAddr    = iAddr;
    assign oUA      = iUA;
    assign oAddrP16 = iAddrP16;
    assign oPCD     = iPCD;
    assign oMemW    = iMemW;
    assign oWrSz    = iWrSz;

    assign oStall   = 1'b0;
    assign oFlush   = oBRTK;

    // WB -> EXE
    or3$ eflagswe_or(EFLAGSWE, FLAGMUX[2], FLAGMUX[1], FLAGMUX[0]);
    reg32e$ gpr_reg(iClk, iFLAGSN, EFLAGS, EFLAGSBAR, iNSysRst, 1'b1, EFLAGSWE); 
    mux2_32$ flagso_mux(oEFLAGS, EFLAGS, iFLAGSN, EFLAGSWE);

    // FLAG bits
    assign ZF = EFLAGS[`iZF];
    assign CF = EFLAGS[`iCF];
    assign SF = EFLAGS[`iSF];
    assign OF = EFLAGS[`iOF];
    assign AF = EFLAGS[`iAF];
    assign PF = EFLAGS[`iPF];
    assign DF = EFLAGS[`iDF];

    //The signal to end simulation
    wire simEnd;
    Compare #(7) simcmp
      (simEnd, {ROP, IROP}, 7'b1000000);

    //Register all architectural committing signals
    wire rsimEnd, rSegW, rRegW, rMemW, rBRTK;
    wire [2:0] rDstR;
    
    wire [31:0] rAddr;
    wire [63:0] rValE;
    wire [1:0] rWrSz;
    regs$ #(106) debug_regs 
      ({rsimEnd, rSegW, rRegW, rMemW, rBRTK, rDstR, rAddr, rValE, rWrSz},
       {simEnd,  iSegW, iRegW, iMemW, iBRTK, iDstR, oAddr, oValE, oWrSz},
       inStall, iClk, iNSysRst);

endmodule

`default_nettype wire

`endif
