`ifndef _DISPIMMDECODER_VH
`define _DISPIMMDECODER_VH

`timescale 1ns/1ps

// Displacement + Immediate + Branch Address Decoder
module DispImmDecoder(DISP, IMM, BRADDR, INST_TAIL, MODRM, SIB, CONTROL_STORE);

    // I/O
    input   [63:0]  INST_TAIL;      // Instruction tail
    input   [ 7:0]  MODRM;          // ModR/M byte
    input   [ 7:0]  SIB;            // SIB byte
    input   [68:0]  CONTROL_STORE;  // Control store
    output  [31:0]  DISP;           // Displacement
    output  [31:0]  IMM;            // Immediate
    output  [47:0]  BRADDR;         // Branch address

    // Organized bytes
    wire    [ 7:0]  ORG_BYTES[0:7];

    // Control-store
    wire            OPSZOVRPRE;
    wire    [ 1:0]  OPSZ;
    wire    [ 1:0]  OPSZOVR;
    wire            OPSZOVRS;
    wire    [ 1:0]  OPSZS;
    wire            USEMODRM;
    wire            USESIB;
    wire            SEXTIMM;
    wire    [ 1:0]  BRTYPE;
    wire            RSTACK;
    wire            WSTACK;
    wire            RWSTACK;

    // Operand Sizes
    wire    [ 1:0]  OPSZA;
    wire    [ 1:0]  OPSZB;

    // ModR/M and SIB values
    wire            MOD00, MOD01, MOD10, MOD11, MOD1XBAR, MODX1BAR;
    wire            RM101, RM1XXBAR, RMXX1BAR;
    wire            BASE101, BASE1XXBAR, BASEXX1BAR;

    // Displacement options
    wire            DISP8MODE;
    wire            DISP32MODE;
    wire            STDDISP32;      // Standard disp32 mode
    wire            MRMDISPO;       // ModR/M displacement-only mode
    wire            SIBDISPO;       // SIB displacement-only mode

    // Potential displacement values
    wire    [31:0]  DISP32;         // disp32
    wire    [31:0]  SEXTDISP8;      // SEXT(disp8)
    wire    [31:0]  SELDISP8;       // DISP8MODE ? SEXTDISP8 : 0

    // Displacement stack adder
    wire    [31:0]  DISPMRM;
    wire    [31:0]  STACKOPMUXOUT;
    wire    [32:0]  DISPPLUSSTACK;

    // Potential immediate values
    wire    [31:0]  SIMM8A;         // SEXT(B0)
    wire    [31:0]  SIMM8B;         // SEXT(B1)
    wire    [31:0]  SIMM8C;         // SEXT(B4)
    wire    [31:0]  SIMM8;          // Sign-extended 8-bit immediate

    wire    [31:0]  ZIMM8A;         // ZEXT(B0)
    wire    [31:0]  ZIMM8B;         // ZEXT(B1)
    wire    [31:0]  ZIMM8C;         // ZEXT(B4)
    wire    [31:0]  IMM16A;         // ZEXT({B1,B0})
    wire    [31:0]  IMM16B;         // ZEXT({B2,B1})
    wire    [31:0]  IMM16C;         // ZEXT({B5, B4})
    wire    [31:0]  IMM32A;         // {B3,B2,B1,B0}
    wire    [31:0]  IMM32B;         // {B4,B3,B2,B1}
    wire    [31:0]  IMM32C;         // {B7,B6,B5,B4}
    wire    [31:0]  ZIMM8;          // 8-bit immediate
    wire    [31:0]  IMM16;          // 16-bit immediate
    wire    [31:0]  IMM32;          // 32-bit immediate
    wire    [31:0]  IMMOP;          // Immediate value of OPSZ

    // Potential branch values
    wire    [31:0]  REL8;           // Short, relative, displacement
    wire    [31:0]  REL16;          // Near, relative, displacement
    wire    [31:0]  REL32;          // Near, relative, displacement
    wire    [47:0]  PTR1616;        // Far, absolute
    wire    [47:0]  PTR1632;        // Far, absolute

    wire    [31:0]  NEARREL;        // Selected near, relative value
    wire    [47:0]  FARABS;         // Selected far, absolute value

    // Organize bytes
    assign ORG_BYTES[0] = INST_TAIL[63:56];
    assign ORG_BYTES[1] = INST_TAIL[55:48];
    assign ORG_BYTES[2] = INST_TAIL[47:40];
    assign ORG_BYTES[3] = INST_TAIL[39:32];
    assign ORG_BYTES[4] = INST_TAIL[31:24];
    assign ORG_BYTES[5] = INST_TAIL[23:16];
    assign ORG_BYTES[6] = INST_TAIL[15: 8];
    assign ORG_BYTES[7] = INST_TAIL[ 7: 0];

    // Control-store values
    assign OPSZOVRPRE = CONTROL_STORE[`iOPSZOVRPRE];
    assign OPSZ       = CONTROL_STORE[`iOPSZ];
    assign OPSZOVR    = CONTROL_STORE[`iOPSZOVR];
    assign OPSZOVRS   = CONTROL_STORE[`iOPSZOVRS];
    assign OPSZS      = CONTROL_STORE[`iOPSZS];
    assign USEMODRM   = CONTROL_STORE[`iUSEMODRM];
    assign USESIB     = CONTROL_STORE[`iUSESIB];
    assign SEXTIMM    = CONTROL_STORE[`iSEXTIMM];
    assign BRTYPE     = CONTROL_STORE[`iBRTYPE];
    assign RSTACK     = CONTROL_STORE[`iRSTACK];
    assign WSTACK     = CONTROL_STORE[`iWSTACK];
    or2$   rwstack_or(RWSTACK, RSTACK, WSTACK);

    // Operand sizes
    mux2_2$ opsza_mux(OPSZA, OPSZ,  OPSZOVR, OPSZOVRPRE);
    mux2_2$ opszb_mux(OPSZB, OPSZA, OPSZS,   OPSZOVRS);

    // ModR/M + SIB values
    inv1$ mod1xbar_inv(MOD1XBAR, MODRM[7]);
    inv1$ modx1bar_inv(MODX1BAR, MODRM[6]);
    nor2$ mod00_nor(MOD00, MODRM[7], MODRM[6]);
    nor2$ mod01_nor(MOD01, MODRM[7], MODX1BAR);
    nor2$ mod10_nor(MOD10, MOD1XBAR, MODRM[6]);
    nor2$ mod11_and(MOD11, MOD1XBAR, MODX1BAR);

    inv1$ rm1xxbar_inv(RM1XXBAR, MODRM[2]);
    inv1$ rmxx1bar_inv(RMXX1BAR, MODRM[0]);
    nor3$ rm101_nor(RM101, RM1XXBAR, MODRM[1], RMXX1BAR);

    inv1$ base1xxbar_inv(BASE1XXBAR, SIB[2]);
    inv1$ basexx1bar_inv(BASEXX1BAR, SIB[0]);
    nor3$ base101_nor(BASE101, BASE1XXBAR, SIB[1], BASEXX1BAR);

    // Generate possible displacements
    assign SEXTDISP8[ 7:0] = ORG_BYTES[0];
    assign SEXTDISP8[31:8] = {24{SEXTDISP8[7]}};
    assign DISP32          = {ORG_BYTES[3], ORG_BYTES[2], ORG_BYTES[1], ORG_BYTES[0]};

    // Determine displacement option
    nand2$ stddisp32_nand(STDDISP32, USEMODRM, MOD10);
    nand3$ mrmdispo_nand(MRMDISPO, USEMODRM, MOD00, RM101);
    nand3$ sibdispo_nand(SIBDISPO, USESIB, MOD00, BASE101);
    nand3$ disp32mode_nand(DISP32MODE, STDDISP32, MRMDISPO, SIBDISPO);
    and2$  disp8mode_and(DISP8MODE, USEMODRM, MOD01);

    // Determine displacement
    mux2_32$ seldisp8_mux(SELDISP8, 32'h0, SEXTDISP8, DISP8MODE);
    mux2_32$ disp_mux(DISPMRM, SELDISP8, DISP32, DISP32MODE);

    // Displacement stack adder
    mux8_32$ stackadder_mux8(STACKOPMUXOUT,
                             DISPMRM,      DISPMRM,      DISPMRM,      DISPMRM,
                             32'h00000000, 32'h00000000, 32'hFFFFFFFE, 32'hFFFFFFFC,
                             {RWSTACK, WSTACK, OPSZA[1]});
    assign DISP = STACKOPMUXOUT;

    // Generate immediate values
    assign SIMM8A = {{24{ORG_BYTES[0][7]}}, ORG_BYTES[0]};
    assign SIMM8B = {{24{ORG_BYTES[1][7]}}, ORG_BYTES[1]};
    assign SIMM8C = {{24{ORG_BYTES[4][7]}}, ORG_BYTES[4]};

    assign ZIMM8A = {24'h000000, ORG_BYTES[0]};
    assign ZIMM8B = {24'h000000, ORG_BYTES[1]};
    assign ZIMM8C = {24'h000000, ORG_BYTES[4]};

    assign IMM16A = {16'h0000, ORG_BYTES[1], ORG_BYTES[0]};
    assign IMM16B = {16'h0000, ORG_BYTES[2], ORG_BYTES[1]};
    assign IMM16C = {16'h0000, ORG_BYTES[5], ORG_BYTES[4]};

    assign IMM32A = {ORG_BYTES[3], ORG_BYTES[2], ORG_BYTES[1], ORG_BYTES[0]};
    assign IMM32B = {ORG_BYTES[4], ORG_BYTES[3], ORG_BYTES[2], ORG_BYTES[1]};
    assign IMM32C = {ORG_BYTES[7], ORG_BYTES[6], ORG_BYTES[5], ORG_BYTES[4]};

    mux4_32$ simm8_mux(SIMM8, SIMM8A, SIMM8B, SIMM8C, 32'h00000000, {DISP32MODE, DISP8MODE});
    mux4_32$ zimm8_mux(ZIMM8, ZIMM8A, ZIMM8B, ZIMM8C, 32'h00000000, {DISP32MODE, DISP8MODE});
    mux4_32$ imm16_mux(IMM16, IMM16A, IMM16B, IMM16C, 32'h00000000, {DISP32MODE, DISP8MODE});
    mux4_32$ imm32_mux(IMM32, IMM32A, IMM32B, IMM32C, 32'h00000000, {DISP32MODE, DISP8MODE});
    mux4_32$ immop_mux(IMMOP, ZIMM8,  IMM16,  IMM32,  32'h00000000, OPSZA);
    mux2_32$ immfn_mux(IMM, IMMOP, SIMM8, SEXTIMM);

    // Generate relative values
    assign REL8[ 7:0]  = ORG_BYTES[0];
    assign REL8[31:8]  = {24{REL8[7]}};
    assign REL16 = {8'h00,        8'h00,        ORG_BYTES[1], ORG_BYTES[0]};
    assign REL32 = {ORG_BYTES[3], ORG_BYTES[2], ORG_BYTES[1], ORG_BYTES[0]};
    // Generate absolute values
    assign PTR1616 = {ORG_BYTES[3], ORG_BYTES[2],
                      8'h00,        8'h00,        ORG_BYTES[1], ORG_BYTES[0]};
    assign PTR1632 = {ORG_BYTES[5], ORG_BYTES[4], 
                      ORG_BYTES[3], ORG_BYTES[2], ORG_BYTES[1], ORG_BYTES[0]};
    mux2_32$ nearrel_mux(NEARREL, REL32,   REL16,   OPSZOVRPRE);
    mux2_48$ farabs_mux( FARABS,  PTR1632, PTR1616, OPSZOVRPRE);
    mux4_48$ braddr_mux(BRADDR,
                        {16'h000,REL8},   {16'h000,NEARREL},
                        48'h000000000000, FARABS,
                        BRTYPE);

endmodule

/*
// DispImmDecoder test bench
module DispImmDecoder_TB;

    // I/O
    reg     [63:0]  INST_TAIL;      // Instruction tail
    reg     [ 7:0]  MODRM;          // ModR/M byte
    reg     [ 7:0]  SIB;            // SIB byte
    reg     [63:0]  CONTROL_STORE;  // Control store
    wire    [31:0]  DISP;           // Displacement
    wire    [31:0]  IMM;            // Immediate
    wire    [47:0]  BRADDR;         // Branch address


    initial begin
        INST_TAIL   <= 64'h0123456789ABCDEF;
        MODRM       <= 8'b010_00_010;
        SIB         <= 8'b10_011_101;
    end

    // UUT
    DispImmDecoder decoder(DISP, IMM, BRADDR,
                           INST_TAIL, MODRM, SIB,
                           CONTROL_STORE);

    // Dump simulation
    initial begin
        $dumpfile("dispImmDecoder.vcd");
        $dumpvars(0, dispImmDecoder_TB);
    end

endmodule
*/

`endif
