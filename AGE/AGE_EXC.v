`ifndef _AGE_EXC_VH
`define _AGE_EXC_VH

`timescale 1ns/1ps

`include "defines.v"

/**********************************************************************************************************
Section 4.3
Intel IA-32 Systems Programming Guide
When the G flag is clear (byte granularity), the effective limit is the value of the 20-bit limit field
in the segment descriptor. Here, the limit ranges from 0 to FFFFFH (1 MByte). When the G flag
is set (4-KByte page granularity), the processor scales the value in the limit field by a factor of
212 (4 KBytes). In this case, the effective limit ranges from FFFH (4 KBytes) to FFFFFFFFH (4
GBytes). Note that when scaling is used (G flag is set), the lower 12 bits of a segment offset
(address) are not checked against the limit; for example, note that if the segment limit is 0,
offsets 0 through FFFH are still valid.

For all types of segments except expand-down data segments, the effective limit is the last
address that is allowed to be accessed in the segment, which is one less than the size, in bytes,
of the segment. The processor causes a general-protection exception any time an attempt is made
to access the following addresses in a segment
**********************************************************************************************************/

// Exception detector of AGE state
module AGE_EXC(oStat,
               iSeg,  iOffset, iLastAddr,
               iCS,   iEIP,    iCSLastAddr,
               iStat, iTRAPOP, iMemR, iMemW);

    // I/O
    input   [15:0]  iSeg;
    input   [31:0]  iOffset;
    input   [31:0]  iLastAddr;
    input   [15:0]  iCS;
    input   [31:0]  iEIP;
    input   [31:0]  iCSLastAddr;
    input   [31:0]  iStat;
    input           iTRAPOP;
    input           iMemR;
    input           iMemW;
    output  [31:0]  oStat;

    // Exception detection logic
    wire            EXC;            // Existing exception

    wire            MEMRW;          // Memory R/W
    wire            UNDERSHOOT;     // Segment undershoot
    wire            OVERSHOOT;      // Segment overshoot?
    wire            NULLSEG;        // Null segment selector?
    wire            BADMEM;         // Bad memory address
    wire            MEMEXC;         // Memory exception

    wire            CSUNDERSHOOT;   // CS undershoot
    wire            CSOVERSHOOT;    // CS overshoot
    wire            NULLCSEG;       // Null code segment selector?
    wire            PCEXC;          // PC exception

	wire 			TRAPOPN;
    wire            EXCDETECT;      // Exception detected         

    /////////////////////////
    // Exception detection //
    /////////////////////////

    // Detect previous exception
    assign EXC = iStat[`iEXC];

    // Detect data memory access exception
    or2$      memrw(MEMRW, iMemR, iMemW);
    assign    UNDERSHOOT = iOffset[31];
    inv1$ ovrshoot_inv(OVERSHOOT, iLastAddr[31]);
	//assign OVERSHOOT = 1'b0;
    redORN16$ redSeg(NULLSEG, {iSeg[15:2],2'b00});
	//assign    NULLSEG = 1'b0;
    or3$      badmem_or(BADMEM, UNDERSHOOT, OVERSHOOT, NULLSEG);
    mux2$     memexc_mux(MEMEXC, 1'b0, BADMEM, MEMRW);

    // Detect instruction memory access exception
	assign CSUNDERSHOOT = 1'b0;
    //assign CSUNDERSHOOT = iEIP[31];
	assign CSOVERSHOOT = 1'b0;
    //inv1$ csovr_inv(CSOVERSHOOT, iCSLastAddr[31]);
    //redORN16$ redCSeg(NULLCSEG, {iCS[15:2],2'b00});
    assign NULLCSEG = 1'b0;
    or3$ pcexc_or(PCEXC, NULLCSEG, CSOVERSHOOT, CSUNDERSHOOT);

    // Detect any exception
	inv1$ trapop_inv(TRAPOPN, iTRAPOP);
    or3$ exc_nor(EXCDETECT, EXC, MEMEXC, PCEXC);

    // Output exception bit
    assign oStat[`iRSVD]    = iStat[`iRSVD];
    assign oStat[`iEXCMODE] = iStat[`iEXCMODE];
    and2$ excdetectf_and(oStat[`iEXC],EXCDETECT,TRAPOPN);
    assign oStat[`iINT]     = iStat[`iINT];
    mux4_8$ excv_mux(oStat[`iEXCV], 8'h00, iStat[`iEXCV], 8'h68, 8'h68, EXC, EXCDETECT);
    assign oStat[`iHEXCV]   = iStat[`iHEXCV];

endmodule

`endif
