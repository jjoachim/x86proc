`ifndef _COUNTER_V_
`define _COUNTER_V_

`include "CommonLogic/RCAdderN.v"
`include "CommonLogic/LibExt.v"

`default_nettype none

/*****************************************************************************/
// Parameterizable Counter
/*****************************************************************************/
module Counter (oVal, iInc, iClk, inReset);
  parameter width = 32;

  output [width-1:0] oVal;
  input iInc, iClk, inReset;

  wire [width-1:0] oAdder, inc, nextState, oRegs;
  wire [(31-width):0] opad, ipad;
  assign ipad = 0;

  //Hold State
  regs$ state [width-1:0]
    (oRegs, nextState, 1'b1, iClk, 1'b1);

  //Zero extend iInc
  assign inc = {{(width-1){1'b0}}, iInc};

  //Increment
  wire oc;
  /*
  RCAdderN #(width) count
    (.oOut(oAdder),
     .oCarry(oc),
     .iA(oVal),
     .iB(inc),
     .iCarry(1'b0)
    );
  */
  generate
    if(width<32) begin
      KSAdder32NC count
        ({opad, oAdder}, {ipad, oVal}, {ipad, inc});
    end else if(width==32) begin
      KSAdder32NC count
        (oAdder, oVal, inc);
    end
  endgenerate

  //Reset Logic
  mux2$ rstmux0 [width-1:0] (nextState, 1'b0, oAdder, inReset);
  mux2$ rstmux1 [width-1:0] (oVal, 1'b0, oRegs, inReset);

endmodule

`default_nettype wire
`endif
