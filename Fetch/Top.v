`timescale 1ns/1ps

`include "Parameters.v"
`include "TestFramework.v"
`include "Fetch/Fetch.v"
`include "Bus/Bus.v"
`include "MainMemory/MainMemory.v"

module TOP;
  reg nreset;

  `TEST_GLOBAL_INIT;

  //Instantiate Bus Logic
  wire [`BUS_MSB:0] bus;
  Bus #(1) bus_logic (bus, `TEST_CLK, nreset);

  //Give Main Memory a bus interface
  wire [31:0] mmbusdata, mmbusaddr, mmdata;
  wire mmvalid;
  wire [3:0] mmbuswr;
  wire mmbusrd;
  BusSlave #(0, 32'h0FFFFFFF) bus_mm
    (bus, mmbusdata, mmbusaddr, mmbuswr, mmbusrd, mmdata, mmvalid);
  MainMemory #(`MEM_BANKS) mm 
    (mmdata, mmvalid, mmbusdata, mmbusaddr, mmbusrd, mmbuswr, `TEST_CLK, nreset);

  //Instantiate Fetch
  wire [127:0] data0, data1;
  wire [31:0] pc, stat, eip;
  wire [15:0] cs;
  wire brTaken, valid0, valid1;

  reg [3:0] CSR_InstSize;
  reg [31:0] WB_EIP;
  reg CSR_NewLineReq, CSR_BufferShift;
  reg WB_BrTaken;
  Fetch F (pc, eip, cs, brTaken, stat, data0, data1, valid0, valid1, bus,
           CSR_NewLineReq, CSR_BufferShift, CSR_InstSize,
           `CS_R, WB_BrTaken, WB_EIP,
           32'b0, 1'b0, `TEST_CLK, nreset);

  //Pull internal signals
  wire loadNextLine;
  assign loadNextLine = F.loadNextLine;

  `TEST_INIT_SEQ(loadNextLine, 1);
  `TEST_INIT_SEQ(pc, 32);
  `TEST_INIT_SEQ(data0, 128);
  `TEST_INIT_SEQ(valid0, 1);
  `TEST_INIT_SEQ(data1, 128);
  `TEST_INIT_SEQ(valid1, 1);
  `TEST_INIT_SEQ(brTaken, 1);

  initial begin
    nreset = 0;
    CSR_NewLineReq = 0;
    CSR_BufferShift = 0;
    CSR_InstSize = 0;
    WB_BrTaken = 0;
    WB_EIP = 0;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    nreset = 1;
    WB_BrTaken = 1;
    WB_EIP = 0;
    `TEST_TOGGLE_CLK;
    WB_BrTaken =  0;

    `TEST_DISPLAY("\n>> BEGIN TESTS <<\n");
    `TEST_WHEN(valid0==1,
      `TEST_EQ(valid1, 1'b0);
      `TEST_EQ(pc, 32'h0);
    )
//NOTE PC is only incremented using multiple of 16, so if inital pc is 4 it goes as follow  4->20->36 or in hex 14 24 34
    CSR_InstSize = 4;              //here instruction size if four, we already have 16B in, so normal processdure
    `TEST_SET(
      `TEST_EQ(valid1, 1'b0);     // Only 16B, so line 1 should not be valid
      `TEST_EQ(pc, 32'h0);        // still on initial line, PC=0
    )

    CSR_InstSize = 6;//10        // Size is 6, (6+4)%16 = 10, no shifting or newline needed
    CSR_NewLineReq = 0;
    `TEST_SET(
      `TEST_EQ(valid0, 1'b1);   //remains the same
      `TEST_EQ(valid1, 1'b0);   //remains the same
      `TEST_EQ(pc, 32'h0);      //remains the same
    )

    CSR_InstSize = 7;//17 committed after shift // Now Decode needs seven 10+7=17, need new line;
    CSR_NewLineReq = 1;   //Decode says line is req
    `TEST_SET(
      `TEST_EQ(valid0, 1'b1);    //right now still same
      `TEST_EQ(valid1, 1'b0);    // still same, cache will churn to get the new line
    )
    `TEST_WHEN(valid1==1,        //Cache found the lin!!!
      `TEST_EQ(valid0, 1'b1);    //just a check to make sure no bug
      `TEST_EQ(pc, 32'h0);       //PC is not incremented since the new line is for the same instruction in prevline
    )				

    `TEST_TOGGLE_CLK;
    CSR_InstSize = 6;           //instruction has size 6, decode has its own shifter to avoid stall here.
    CSR_NewLineReq = 0;         // line1 is no longer requested
    CSR_BufferShift = 1;        //shift the buffer over
    `TEST_SET(
      `TEST_EQ(valid0, 1'b1);  // both line should be valid as shift hasnt happended yet
      `TEST_EQ(valid1, 1'b1);  //...
      `TEST_EQ(pc, 32'h10);    // the shift should incremnt the pc to the next line
    )
    `TEST_TOGGLE_CLK;
    CSR_BufferShift = 0;       //bufffer shift signal goes low in next cycle, shift should be done here

    //`TEST_WHEN(valid1==0,
      `TEST_EQ(valid0, 1'b1);  // its still valid because line1 shifted over to line0
      `TEST_EQ(valid1, 1'b0);  //not valid beacuse line is not in cache,
      `TEST_EQ(pc, 32'h10);    //address is still good
    //)
      `TEST_TOGGLE_CLK;
    `TEST_SET(
    WB_BrTaken = 1;       //gonna branch
    WB_EIP = 32'h77;       //gonna branch
      `TEST_EQ(valid0, 1'b0);  // its still valid because line1 shifted over to line0
      `TEST_EQ(valid1, 1'b0);  //not valid beacuse line is not in cache,
    )
    `TEST_WHEN(valid0==1,
      `TEST_EQ(valid0, 1'b1);  // its still valid because line1 shifted over to line0
      `TEST_EQ(valid1, 1'b0);  //not valid beacuse line is not in cache,
      `TEST_EQ(pc, 32'h77);    //address is still good
    )

    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_END;
    `TEST_DISPLAY("\n>> END TESTS <<\n");

    $finish;
  end
  
  initial begin
    $vcdplusfile("top.dump.vpd");
    $vcdpluson(0, TOP);
  end

endmodule
