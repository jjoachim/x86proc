`timescale 1ns/1ps

`include "Parameters.v"
`include "TestFramework.v"

`include "MainMemory.v"

`define MEM_SZ 8

module TOP;
  wire [31:0] databus;
  wire valid;
  
  reg [31:0] data, mem0, mem1, mem2, mem3, addr;
  reg [3:0] wr;
  reg rd, rst;
  
  `TEST_GLOBAL_INIT;
  MainMemory #(4, 16) mm (databus, valid, data, addr, rd, wr, `TEST_CLK, rst);
  
  always@(*) begin
    mem0 <= mm.bank[0].block[0].mem[0];
    mem1 <= mm.bank[1].block[0].mem[0];
    mem2 <= mm.bank[2].block[0].mem[0];
    mem3 <= mm.bank[3].block[0].mem[0];
  end

  `TEST_INIT_COMB(databus, 32);
  `TEST_INIT_COMB(mem0, 32);
  `TEST_INIT_COMB(mem1, 32);
  `TEST_INIT_COMB(mem2, 32);

  integer i;
  
  initial begin
    //initialize signals
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    data = 0;
    addr = 0;
    rd = 0;
    wr = 0;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    rst = 0;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    rst = 1;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;

    `TEST_DISPLAY("\n>> BEGIN TESTS <<\n");
    //Write to memory
    rd = 0;
    for(i=0; i<`MEM_SZ; i=i+1) begin
      data = 32'hFFFF0000 + i;
      addr = i<<2;
      wr = 4'b1111;
      `TEST_TOGGLE_CLK;
      while(valid != 1) begin
        `TEST_TOGGLE_CLK;
      end
    end
    
    //Read from memory
    wr = 0;
    data = 0;
    for(i=0; i<`MEM_SZ; i=i+1) begin
      addr = i<<2;
      rd = 1;
      `TEST_WHEN(valid,
        `TEST_EQ(databus, 32'hFFFF0000 + i);
      )
    end

    //Write to memory
    rd = 0;
    for(i=0; i<`MEM_SZ; i=i+1) begin
      data = 32'h00FF0000 + i;
      addr = i<<2;
      wr = 4'b1111;
      `TEST_TOGGLE_CLK;
      while(valid != 1) begin
        `TEST_TOGGLE_CLK;
      end
    end
    
    //Read from memory
    wr = 0;
    data = 0;
    for(i=0; i<`MEM_SZ; i=i+1) begin
      addr = i<<2;
      rd = 1;
      `TEST_WHEN(valid,
        `TEST_EQ(databus, 32'h00FF0000 + i);
      )
    end
    `TEST_DISPLAY("\n>> END TESTS <<");
    `TEST_END;

    $finish;
  end
  
  initial begin
    $vcdplusfile("top.dump.vpd");
    $vcdpluson(0, TOP);
    $vcdpluson(0, mm.bank[0].block[0].mem[0]);
    $vcdpluson(0, mm.bank[1].block[0].mem[0]);
    $vcdpluson(0, mm.bank[2].block[0].mem[0]);
    $vcdpluson(0, mm.bank[3].block[0].mem[0]);
  end

endmodule
