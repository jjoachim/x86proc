`ifndef _PARAMETERS_V_
`define _PARAMETERS_V_

`include "defines.v"

/*****************************************************************************/
// Processor Settings
/*****************************************************************************/
//Global Settings
`define CLK_CYCLE 10 //6ns target
`define CLK_GATE_CYCLE (`CLK_CYCLE*5)
`define TEST_CLK_CYCLE `CLK_CYCLE //for Top.v test suites

//Memory Settings
`define MEM_BANKS         8
`define ICACHE_SIZE       512
`define ICACHE_BLOCK_SIZE 16
`define DCACHE_SIZE       512
`define DCACHE_BLOCK_SIZE 16

//Bus Settings
`define BUS_NUM_MASTERS  5
`define ICACHE_MASTER_ID 3 //takes 3 and 4
`define DCACHE_MASTER_ID 1 //takes 1 and 2
`define DMA_MASTER_ID    0 //Lowest Priority

`define BUS_MEM_ADDRL 32'h0
`define BUS_MEM_ADDRH 32'h0FFFFFFF
`define BUS_DMA_ADDRL 32'h10000000
`define BUS_DMA_ADDRH 32'h1FFFFFFF

/*****************************************************************************/
// Initialization
/*****************************************************************************/
//Segment Registers
`define IDT_R 32'h02000000
`define CS_R 16'h0
`define DS_R 16'h0
`define SS_R 16'h0
`define ES_R 16'h0
`define FS_R 16'h0
`define GS_R 16'h0

//Segment Limit Values
`define CS_LIM 20'h04FFF
`define DS_LIM 20'h011FF
`define SS_LIM 20'h04000
`define ES_LIM 20'h003FF
`define FS_LIM 20'h003FF
`define GS_LIM 20'h007FF

//TLB Settings
//V=valid, W=Write allowed, P=Physical (non cache-able)
//sizeof(PPN) = sizeof(VPN) = 20bits
//Format: 00PW_PhysPage_00RV_VirtPage


`define TLB_E0 46'h1_00000_3_00000
`define TLB_E1 46'h0_00002_3_02000
`define TLB_E2 46'h0_00005_3_04000
`define TLB_E3 46'h0_00004_3_0b000


//`define TLB_E4 46'h0_00007_3_0b001   // test 2
`define TLB_E4 46'h0_00007_3_0c000     // test 1,3


`define TLB_E5 46'h0_00005_3_0a000
`define TLB_E6 46'h0_00000_0_00000
`define TLB_E7 46'h2_10000_3_10000

//IDT Entries (in Main Memory)
`define NMI_A 32'h00002010  //IO interrupt
`define NMI_V 32'h00000700

`define GPE_A 32'h00002068
`define GPE_V 32'h00000800

`define PFE_A 32'h00002070
`define PFE_V 32'h00000900

`endif
