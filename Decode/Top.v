`timescale 1ns/1ps

`include "Parameters.v"
`include "TestFramework.v"
`include "Fetch/Fetch.v"
`include "Decode/Decode.v"
`include "Bus/Bus.v"
`include "MainMemory/MainMemory.v"

module TOP;
  `STATIC_ASSERT(`BUS_NUM_MASTERS == 1, bus_masters_not_1);

  reg nreset;

  `TEST_GLOBAL_INIT;

  //Instantiate Bus Logic
  wire [`BUS_MSB:0] bus;
  Bus bus_logic (bus, `TEST_CLK, nreset);

  //Give Main Memory a bus interface
  wire [31:0] mmbusdata, mmbusaddr, mmdata;
  wire mmvalid;
  wire [3:0] mmbuswr;
  wire mmbusrd;
  BusSlave #(0, 32'h0FFFFFFF) bus_mm
    (bus, mmbusdata, mmbusaddr, mmbuswr, mmbusrd, mmdata, mmvalid);
  MainMemory #(`MEM_BANKS) mm 
    (mmdata, mmvalid, mmbusdata, mmbusaddr, mmbusrd, mmbuswr, `TEST_CLK, nreset);

  //Instantiate Fetch
  wire [127:0] F_data0, F_data1;
  reg  [127:0] F_data0_test;
  wire [31:0] F_pc, F_stat, eip;
  wire [15:0] cs;
  wire F_brTaken, F_valid0, F_valid1;

  //reg [3:0] CSR_InstSize;
  reg [31:0] WB_EIP;
  //reg CSR_NewLineReq, CSR_BufferShift;
  reg WB_BrTaken;
   wire [3:0] CSR_InstSize;
   wire  CSR_NewLineReq;
   wire  CSR_BufferShift;
  Fetch F (F_pc, eip, cs, F_brTaken, F_stat, F_data0, F_data1, F_valid0, F_valid1, bus,
           CSR_NewLineReq, CSR_BufferShift, CSR_InstSize,
           `CS_R, WB_BrTaken, WB_EIP,
           32'b0, 1'b0, `TEST_CLK, nreset);

   wire [31:0] CSR_PC,CSR_EIP;
   wire [15:0] CSR_CS;
   wire  [63:0] CSR_RestOfInst;
   wire [31:0] CSR_Stat;
   wire [`CTRLSZ-1:0] CSR_ControlStorebits;
   reg stall_to_CSR,flush_to_CSR;
   reg WB_exceptioin;
   wire [7:0] CSR_MOD,CSR_SIB;
Decode CSR (F_pc,F_valid0,F_valid1,F_brTaken,eip,cs,
	F_data0_test,F_data1, WB_exceptioin, 

	CSR_EIP,CSR_PC,CSR_CS,CSR_InstSize,CSR_NewLineReq,CSR_BufferShift, CSR_RestOfInst, CSR_Stat, CSR_ControlStorebits,
	CSR_MOD,CSR_SIB,
	       `TEST_CLK,nreset,stall_to_CSR,flush_to_CSR
);

  //Pull internal signals
  wire loadNextLine;
  assign loadNextLine = F.loadNextLine;

  `TEST_INIT_SEQ(loadNextLine, 1);
  `TEST_INIT_SEQ(F_pc, 32);
  `TEST_INIT_SEQ(F_data0, 128);
  `TEST_INIT_SEQ(F_valid0, 1);
  `TEST_INIT_SEQ(F_data1, 128);
  `TEST_INIT_SEQ(F_valid1, 1);
  `TEST_INIT_SEQ(F_brTaken, 1);

  initial begin
    nreset = 0;
    WB_BrTaken = 0;
    WB_exceptioin = 0;
    stall_to_CSR = 0;
    flush_to_CSR =0;;
    WB_EIP = 0;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    nreset = 1;
    WB_BrTaken = 1;
    WB_EIP = 0;
    `TEST_TOGGLE_CLK;
    WB_BrTaken =  0;

    `TEST_DISPLAY("\n>> BEGIN TESTS <<\n");
	     F_data0_test=128'hF30F1234ABCD1234ABCD1234ABCD1234;
    `TEST_WHEN(F_valid0==1,

      `TEST_EQ(F_valid1, 1'b0);
      `TEST_EQ(F_pc, 32'h0);
    )
    `TEST_TOGGLE_CLK;
    `TEST_SET(
      `TEST_EQ(F_valid1, 1'b0);     // Only 16B, so line 1 should not be valid
      `TEST_EQ(F_pc, 32'h0);        // still on initial line, PC=0
    )


    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_END;
    `TEST_DISPLAY("\n>> END TESTS <<\n");

    $finish;
  end
  
  initial begin
    $vcdplusfile("top.dump.vpd");
    $vcdpluson(0, TOP);
  end

endmodule
