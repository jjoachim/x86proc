// Module is based on George Nyarangi's 8-bit Kogge-Stone adder
// http://georgeblog.nyarangi.com/2010/07/8-bit-kogge-stone-adder.html

// 16-bit Kogge-Stone Adder
module KSADD16(sum, cout, x, y);

    input   [15:0]  x, y;   // Input vectors
    wire            cin;    // Carry-in
    output  [15:0]  sum;    // Sum 
    output          cout;   // Carry-out

    // Internal generate/propagate signals
    wire    [15:0]  G_Z, P_Z;
    wire    [15:0]  G_A, P_A;
    wire    [15:0]  G_B, P_B;
    wire    [15:0]  G_C, P_C;
    wire    [15:0]  G_D, P_D;
    
    // No carry-in for add
    assign cin = 1'b0;
 
    // Level 1
    gray_cell$  level_00A(cin,     P_Z[ 0], G_Z[ 0],          G_A[ 0]);
    black_cell$ level_01A(G_Z[ 0], P_Z[ 1], G_Z[ 1], P_Z[ 0], G_A[ 1], P_A[ 1]);
    black_cell$ level_02A(G_Z[ 1], P_Z[ 2], G_Z[ 2], P_Z[ 1], G_A[ 2], P_A[ 2]);
    black_cell$ level_03A(G_Z[ 2], P_Z[ 3], G_Z[ 3], P_Z[ 2], G_A[ 3], P_A[ 3]);
    black_cell$ level_04A(G_Z[ 3], P_Z[ 4], G_Z[ 4], P_Z[ 3], G_A[ 4], P_A[ 4]);
    black_cell$ level_05A(G_Z[ 4], P_Z[ 5], G_Z[ 5], P_Z[ 4], G_A[ 5], P_A[ 5]);
    black_cell$ level_06A(G_Z[ 5], P_Z[ 6], G_Z[ 6], P_Z[ 5], G_A[ 6], P_A[ 6]);
    black_cell$ level_07A(G_Z[ 6], P_Z[ 7], G_Z[ 7], P_Z[ 6], G_A[ 7], P_A[ 7]);
    black_cell$ level_08A(G_Z[ 7], P_Z[ 8], G_Z[ 8], P_Z[ 7], G_A[ 8], P_A[ 8]);
    black_cell$ level_09A(G_Z[ 8], P_Z[ 9], G_Z[ 9], P_Z[ 8], G_A[ 9], P_A[ 9]);
    black_cell$ level_10A(G_Z[ 9], P_Z[10], G_Z[10], P_Z[ 9], G_A[10], P_A[10]);
    black_cell$ level_11A(G_Z[10], P_Z[11], G_Z[11], P_Z[10], G_A[11], P_A[11]);
    black_cell$ level_12A(G_Z[11], P_Z[12], G_Z[12], P_Z[11], G_A[12], P_A[12]);
    black_cell$ level_13A(G_Z[12], P_Z[13], G_Z[13], P_Z[12], G_A[13], P_A[13]);
    black_cell$ level_14A(G_Z[13], P_Z[14], G_Z[14], P_Z[13], G_A[14], P_A[14]);
    black_cell$ level_15A(G_Z[14], P_Z[15], G_Z[15], P_Z[14], G_A[15], P_A[15]);

    // Level 2 
    gray_cell$  level_01B(cin,     P_A[ 1], G_A[ 1],          G_B[ 1]);
    gray_cell$  level_02B(G_A[ 0], P_A[ 2], G_A[ 2],          G_B[ 2]);
    black_cell$ level_03B(G_A[ 1], P_A[ 3], G_A[ 3], P_A[ 1], G_B[ 3], P_B[ 3]);
    black_cell$ level_04B(G_A[ 2], P_A[ 4], G_A[ 4], P_A[ 2], G_B[ 4], P_B[ 4]);
    black_cell$ level_05B(G_A[ 3], P_A[ 5], G_A[ 5], P_A[ 3], G_B[ 5], P_B[ 5]);
    black_cell$ level_06B(G_A[ 4], P_A[ 6], G_A[ 6], P_A[ 4], G_B[ 6], P_B[ 6]);
    black_cell$ level_07B(G_A[ 5], P_A[ 7], G_A[ 7], P_A[ 5], G_B[ 7], P_B[ 7]);
    black_cell$ level_08B(G_A[ 6], P_A[ 8], G_A[ 8], P_A[ 6], G_B[ 8], P_B[ 8]);
    black_cell$ level_09B(G_A[ 7], P_A[ 9], G_A[ 9], P_A[ 7], G_B[ 9], P_B[ 9]);
    black_cell$ level_10B(G_A[ 8], P_A[10], G_A[10], P_A[ 8], G_B[10], P_B[10]);
    black_cell$ level_11B(G_A[ 9], P_A[11], G_A[11], P_A[ 9], G_B[11], P_B[11]);
    black_cell$ level_12B(G_A[10], P_A[12], G_A[12], P_A[10], G_B[12], P_B[12]);
    black_cell$ level_13B(G_A[11], P_A[13], G_A[13], P_A[11], G_B[13], P_B[13]);
    black_cell$ level_14B(G_A[12], P_A[14], G_A[14], P_A[12], G_B[14], P_B[14]);
    black_cell$ level_15B(G_A[13], P_A[15], G_A[15], P_A[13], G_B[15], P_B[15]);

    // Level 3
    gray_cell$  level_03C(cin,     P_B[ 3], G_B[ 3],          G_C[ 3]);
    gray_cell$  level_04C(G_A[ 0], P_B[ 4], G_B[ 4],          G_C[ 4]);
    gray_cell$  level_05C(G_B[ 1], P_B[ 5], G_B[ 5],          G_C[ 5]);
    gray_cell$  level_06C(G_B[ 2], P_B[ 6], G_B[ 6],          G_C[ 6]);
    black_cell$ level_07C(G_B[ 3], P_B[ 7], G_B[ 7], P_B[ 3], G_C[ 7], P_C[ 7]);
    black_cell$ level_08C(G_B[ 4], P_B[ 8], G_B[ 8], P_B[ 4], G_C[ 8], P_C[ 8]);
    black_cell$ level_09C(G_B[ 5], P_B[ 9], G_B[ 9], P_B[ 5], G_C[ 9], P_C[ 9]);
    black_cell$ level_10C(G_B[ 6], P_B[10], G_B[10], P_B[ 6], G_C[10], P_C[10]);
    black_cell$ level_11C(G_B[ 7], P_B[11], G_B[11], P_B[ 7], G_C[11], P_C[11]);
    black_cell$ level_12C(G_B[ 8], P_B[12], G_B[12], P_B[ 8], G_C[12], P_C[12]);
    black_cell$ level_13C(G_B[ 9], P_B[13], G_B[13], P_B[ 9], G_C[13], P_C[13]);
    black_cell$ level_14C(G_B[10], P_B[14], G_B[14], P_B[10], G_C[14], P_C[14]);
    black_cell$ level_15C(G_B[11], P_B[15], G_B[15], P_B[11], G_C[15], P_C[15]);

    // Level 4
    gray_cell$  level_07D(cin,     P_C[ 7], G_C[ 7],          G_D[ 7]);
    gray_cell$  level_08D(G_A[ 0], P_C[ 8], G_C[ 8],          G_D[ 8]);
    gray_cell$  level_09D(G_B[ 1], P_C[ 9], G_C[ 9],          G_D[ 9]);
    gray_cell$  level_10D(G_B[ 2], P_C[10], G_C[10],          G_D[10]);
    gray_cell$  level_11D(G_C[ 3], P_C[11], G_C[11],          G_D[11]);
    gray_cell$  level_12D(G_C[ 4], P_C[12], G_C[12],          G_D[12]);
    gray_cell$  level_13D(G_C[ 5], P_C[13], G_C[13],          G_D[13]);
    gray_cell$  level_14D(G_C[ 6], P_C[14], G_C[14],          G_D[14]);
    black_cell$ level_15D(G_C[ 7], P_C[15], G_C[15], P_C[ 7], G_D[15], P_D[15]);

    // Level 5
    gray_cell$  level_15E(cin,     P_D[15], G_D[15],          cout);

    // XOR with AND
    and_xor$ level_Z00(x[ 0], y[ 0], P_Z[ 0], G_Z[ 0]);
    and_xor$ level_Z01(x[ 1], y[ 1], P_Z[ 1], G_Z[ 1]);
    and_xor$ level_Z02(x[ 2], y[ 2], P_Z[ 2], G_Z[ 2]);
    and_xor$ level_Z03(x[ 3], y[ 3], P_Z[ 3], G_Z[ 3]);
    and_xor$ level_Z04(x[ 4], y[ 4], P_Z[ 4], G_Z[ 4]);
    and_xor$ level_Z05(x[ 5], y[ 5], P_Z[ 5], G_Z[ 5]);
    and_xor$ level_Z06(x[ 6], y[ 6], P_Z[ 6], G_Z[ 6]);
    and_xor$ level_Z07(x[ 7], y[ 7], P_Z[ 7], G_Z[ 7]);
    and_xor$ level_Z08(x[ 8], y[ 8], P_Z[ 8], G_Z[ 8]);
    and_xor$ level_Z09(x[ 9], y[ 9], P_Z[ 9], G_Z[ 9]);
    and_xor$ level_Z10(x[10], y[10], P_Z[10], G_Z[10]);
    and_xor$ level_Z11(x[11], y[11], P_Z[11], G_Z[11]);
    and_xor$ level_Z12(x[12], y[12], P_Z[12], G_Z[12]);
    and_xor$ level_Z13(x[13], y[13], P_Z[13], G_Z[13]);
    and_xor$ level_Z14(x[14], y[14], P_Z[14], G_Z[14]);
    and_xor$ level_Z15(x[15], y[15], P_Z[15], G_Z[15]);

    // Outputs
    xor2$ sum00(sum[ 0], cin,     P_Z[ 0]);
    xor2$ sum01(sum[ 1], G_A[ 0], P_Z[ 1]);
    xor2$ sum02(sum[ 2], G_B[ 1], P_Z[ 2]);
    xor2$ sum03(sum[ 3], G_B[ 2], P_Z[ 3]);
    xor2$ sum04(sum[ 4], G_C[ 3], P_Z[ 4]);
    xor2$ sum05(sum[ 5], G_C[ 4], P_Z[ 5]);
    xor2$ sum06(sum[ 6], G_C[ 5], P_Z[ 6]);
    xor2$ sum07(sum[ 7], G_C[ 6], P_Z[ 7]);
    xor2$ sum08(sum[ 8], G_D[ 7], P_Z[ 8]);
    xor2$ sum09(sum[ 9], G_D[ 8], P_Z[ 9]);
    xor2$ sum10(sum[10], G_D[ 9], P_Z[10]);
    xor2$ sum11(sum[11], G_D[10], P_Z[11]);
    xor2$ sum12(sum[12], G_D[11], P_Z[12]);
    xor2$ sum13(sum[13], G_D[12], P_Z[13]);
    xor2$ sum14(sum[14], G_D[13], P_Z[14]);
    xor2$ sum15(sum[15], G_D[14], P_Z[15]);

endmodule

// exhaustive checking of all 256*256*2 possible cases
// testbench for the 8-bit unsigned Kogge-Stone adder
module KS16ADD_TB;

    reg     [15:0]  a, b;           // 16-bit operands
    wire    [15:0]  s;              // 16-bit sum output
    wire            c8;             // carry output
    reg     [16:0]  check;          // 32-bit value used to check correctess
    integer         i, j;           // loop variables
    integer         num_correct;    // counter to keep track of the number correct
    integer         num_wrong;      // counter to keep track of the number wrong

    // instantiate the 8-bit Kogge-Stone adder
    KSADD16 ks1(s, c8, a, b);

    // exhaustive checking
    initial begin
        // initialize the counter variables
        num_correct = 0; num_wrong = 0;

        // loop through all possible cases and record the results
        for (i = 0; i < 100; i = i + 1) begin
            a = {$random} % 65536;
            for (j = 0; j < 100; j = j + 1) begin
                b = {$random} % 65536;
              
                // compute and check the product
                check = a + b;
                #10 if ({c8, s} == check) num_correct = num_correct + 1;
                else                      num_wrong = num_wrong + 1;
                //following line is for debugging
                //$display($time, "  %d + %d = %d (%d)", a, b, {c8, s}, check);
            end
        end
     
        // print the final counter values
        $display("num_correct = %d, num_wrong = %d", num_correct, num_wrong);
    end

endmodule
