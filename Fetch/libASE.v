`ifndef  _ALU_V_
`define  _ALU_V_
`include "../CommonLogic/CommonLogic.v"
`include "../CommonLogic/KSAdder32.v"
`include "Fetch/kogge.v"
`include "Fetch/kogge4.v"
`include "Fetch/all_my_barrels.v"
`include "libext/libSJQ.v"
`include "libext/defines.v"
`include "Bus/Bus.v"


module prefix_check (byte0,byte1,byte2,prefix_out,encoded_rep,encoded_segment,prefix_num);
//time to prefix_num = 1.5
//time to sum = comp+or9+ha= .7+.45+.35
input [7:0] byte0,byte1,byte2;
output [2:0] prefix_out;//[0] is opsize, [1] is seg, [2] is repeat
wire [5:0] nonencoded_segment;//Segment type
output  encoded_rep;// Repeat type; 1 is for rep, 0 for repne
output [1:0] prefix_num; // just a classic binary number
output [2:0] encoded_segment;


compare_8 comp_REPNE(repne0,byte0,8'hF2);
compare_8 comp_REP  (rep0,byte0,8'hF3);

compare_8 comp_CS  (cs0,byte0,8'h2E);
compare_8 comp_SS  (ss0,byte0,8'h36);
compare_8 comp_DS  (ds0,byte0,8'h3E);
compare_8 comp_ES  (es0,byte0,8'h26);
compare_8 comp_FS  (fs0,byte0,8'h64);
compare_8 comp_GS  (gs0,byte0,8'h65);

compare_8 comp_OP  (op0,byte0,8'h66);


compare_8 comp2_REPNE(repne1,byte1,8'hF2);
compare_8 comp2_REP  (rep1,byte1,8'hF3);

compare_8 comp2_CS  (cs1,byte1,8'h2E);
compare_8 comp2_SS  (ss1,byte1,8'h36);
compare_8 comp2_DS  (ds1,byte1,8'h3E);
compare_8 comp2_ES  (es1,byte1,8'h26);
compare_8 comp2_FS  (fs1,byte1,8'h64);
compare_8 comp2_GS  (gs1,byte1,8'h65);

compare_8 comp2_OP  (op1,byte1,8'h66);



compare_8 comp3_REPNE(repne2,byte2,8'hF2);
compare_8 comp3_REP  (rep2,byte2,8'hF3);

compare_8 comp3_CS  (cs2,byte2,8'h2E);
compare_8 comp3_SS  (ss2,byte2,8'h36);
compare_8 comp3_DS  (ds2,byte2,8'h3E);
compare_8 comp3_ES  (es2,byte2,8'h26);
compare_8 comp3_FS  (fs2,byte2,8'h64);
compare_8 comp3_GS  (gs2,byte2,8'h65);

compare_8 comp3_OP  (op2,byte2,8'h66);




// num calc
ase_or9 check_byte0(byte0_check,repne0,rep0,cs0,ss0,ds0,es0,fs0,gs0,op0);
wire byte1_check_maybe,byte2_check_maybe;
ase_or9 check_byte1(byte1_check_maybe,repne1,rep1,cs1,ss1,ds1,es1,fs1,gs1,op1);
and2$ andprefix2 (byte1_check,byte1_check_maybe,byte0_check);
ase_or9 check_byte2(byte2_check_maybe,repne2,rep2,cs2,ss2,ds2,es2,fs2,gs2,op2);
and2$ andprefix3 (byte2_check,byte2_check_maybe,byte1_check);
ase_fa  calc_num_pref(prefix_num[0],prefix_num[1],byte0_check,byte1_check,byte2_check);
// op size flag
wire op1_final,op2_final;
wire cs1_final,ss1_final,ds1_final,es1_final,fs1_final,gs1_final,repne1_final,rep1_final;
wire cs2_final,ss2_final,ds2_final,es2_final,fs2_final,gs2_final,repne2_final,rep2_final;
//need to buffer here
and2$ andgate_final1 [8:0] ({op1_final,cs1_final,ss1_final,ds1_final,es1_final,fs1_final,gs1_final,repne1_final,rep1_final},{op1,cs1,ss1,ds1,es1,fs1,gs1,repne1,rep1},byte0_check);
and2$ andgate_final2 [8:0] ({op2_final,cs2_final,ss2_final,ds2_final,es2_final,fs2_final,gs2_final,repne2_final,rep2_final},{op2,cs2,ss2,ds2,es2,fs2,gs2,repne2,rep2},byte1_check);
or3$  op_prefix_or(prefix_out[0],op0,op1_final,op2_final);
// repeat flag
ase_or6 rep_prefix_or(prefix_out[2],repne0,rep0,repne1_final,rep1_final,rep2_final,repne2_final);
// segment flag
ase_or18 seg_prefix_or(prefix_out[1],cs0,ss0,ds0,es0,fs0,gs0,cs1_final,ss1_final,ds1_final,es1_final,fs1_final,gs1_final,cs2_final,ss2_final,ds2_final,es2_final,fs2_final,gs2_final);
//encoded rep
//not this section can be better with added complexity like muxes to select the appropiate
//prefix, howver assuming the code coming in is perfext, no two contradicting prefixes
// should show
or3$ repttypegate(encoded_rep,rep0,rep1_final,rep2_final);// 1 if rep, repne is default ( change default to get a faster gate??)

//nonencoded segment
or3$ esgate(nonencoded_segment[0],es0,es1_final,es2_final);
or3$ csgate(nonencoded_segment[1],cs0,cs1_final,cs2_final);
or3$ ssgate(nonencoded_segment[2],ss0,ss1_final,ss2_final);
or3$ dsgate(nonencoded_segment[3],ds0,ds1_final,ds2_final);
or3$ fsgate(nonencoded_segment[4],fs0,fs1_final,fs2_final);
or3$ gsgate(nonencoded_segment[5],gs0,gs1_final,gs2_final);
wire valid_segment;
pencoder8_3v$ segment_encoder(1'b0,{2'b0,nonencoded_segment},encoded_segment,valid_segment);


endmodule


module rom128x4(out,in);
input [6:0] in;
//assign byte1=in[7:0];
output [10:0] out;
wire [15:0] outx;
wire [31:0] sizebits0x,sizebits1x,sizebits2x,sizebits3x;


initial begin
	$readmemb("../rom/romadder_size_0.data", firstbyte_rom0.mem);
	$readmemb("../rom/romadder_size_1.data", firstbyte_rom1.mem);
	$readmemb("../rom/romadder_size_2.data", firstbyte_rom2.mem);
	$readmemb("../rom/romadder_size_3.data", firstbyte_rom3.mem);
end


rom32b32w$ firstbyte_rom0(in[4:0],1'b1,sizebits0x);
rom32b32w$ firstbyte_rom1(in[4:0],1'b1,sizebits1x);
rom32b32w$ firstbyte_rom2(in[4:0],1'b1,sizebits2x);
rom32b32w$ firstbyte_rom3(in[4:0],1'b1,sizebits3x);
//need to add buffers here and orangize it. these 2 muxes will fanout to a total of 8.
//a buffer of 4 is already in the mux, thats to buffer delays.
//better to put the buffer outside, so use on 8 buffer instead
//by extrapolation, the higher up u go, the bigger the buffer and the smaller the delay
//therefore just use a big buffer right from the shifter
mux4_16$   firstbyte_mux4_2(outx,sizebits0x[15:0],sizebits1x[15:0],sizebits2x[15:0],sizebits3x[15:0],in[5],in[6]);
assign out= outx[10:0];

endmodule

module sib_check (byte_in,sib_signal);
input [7:0] byte_in;
output  sib_signal;
inv1$ inv1g (b2p,byte_in[2]);
wire sib_signal0,sib_signal2;
nor3$ nor3g (sib_signal0,b2p,byte_in[1],byte_in[0]);
nand2$ asdfasd   (sib_signal2,byte_in[7],byte_in[6]);
and2$  iasdf     (sib_signal,sib_signal0,sib_signal2);
endmodule


module one_modrm (mod_offset,byte_in,mod_csbit_in,sib_check);  // will add other modrm functionality, as in calc
input [7:0] byte_in;
input [1:0] mod_offset;
input  mod_csbit_in;
output sib_check;//might need to add one to it for further logic
sib_check sib_checker(byte_in,sib_match);// can SIB be used without modrm???
and2$ and_check(sib_check,sib_match,mod_csbit_in);
endmodule

//module big_modrm (mod_offset,byte0,byte1,byte2,byte3,mod_csbit_in,sib_check);  // will add other modrm functionality, as in calc
//input [7:0] byte_in;
//input [1:0] mod_offset;
//input  mod_csbit_in;
//output sib_check;//might need to add one to it for further logic
//sib_check sib_checker(byte_in,sib_match);// can SIB be used without modrm???
//and2$ (sib_check,sib_match,mod_csbit_in);
//endmodule
//
//






module accumulator(oOut,iIn,iIn_prev,cout);
output [3:0]oOut;
input [3:0]iIn,iIn_prev;
output cout;
kogge_stone_4 adder_to_accum (oOut,cout,iIn,iIn_prev,1'b0);
endmodule

module ase_or4(out,in1,in2,in3,in4);
// delay =.25+.15=.4   
// it is less than the library or4$ with a delay of .5
input in1,in2,in3,in4;
output out;
inv1$  invgate1 (in1p,in1);
inv1$  invgate2 (in2p,in2);
inv1$  invgate3 (in3p,in3);
inv1$  invgate4 (in4p,in4);
nand4$ nandgate(out,in1p,in2p,in3p,in4p);
endmodule


module compare_8(out,in1,in2);
// delay is .7   
//xnor2+nand4+nor2=.25+.25+.2
input [7:0] in1,in2;
wire [7:0] eq;
wire nand1,nand2;
output out;
// takes two butes and compares them, if equal, result is one
genvar i;
generate
// generate the first stage equal gates
for (i=0; i<8; i=i+1) begin
	xnor2$ eq_q (eq[i],in1[i],in2[i]);
end
endgenerate
//2 4input nand gates 
nand4$ nand1_g (nand1,eq[0],eq[1],eq[2],eq[3]);
nand4$ nand2_g (nand2,eq[4],eq[5],eq[6],eq[7]);
// final 2 input nor
nor2$ nor_g (out,nand1,nand2);

endmodule 


// 16 bit INVERTER
module ase_inv_16(out,in);
input  [15:0] in;
output [15:0] out;
genvar i;
generate
for (i=0; i<16; i=i+1) begin
	inv1$ inv (out[i],in[i]);
end
endgenerate
endmodule

// 16 bit DFF D FLIP FLOP
module ase_dff_16(clk,d,q,qbar,r,s);
input  [15:0] d;
input clk,r,s;
output [15:0] q,qbar;
genvar i;
generate
for (i=0; i<16; i=i+1) begin
	dff$ dff_g (clk,d[i],q[i],qbar[i],r,s);
end
endgenerate
endmodule



// 16 bit AND
module ase_and_16(out,in1,in2);
input  [15:0] in1,in2;
output [15:0] out;
genvar i;
generate
for (i=0; i<16; i=i+1) begin
	and2$ and_g (out[i],in1[i],in2[i]);
end
endgenerate
endmodule


// one bit FA (FULL ADDER)  full
module ase_fa(sum,carry_out,in1,in2,carry_in);
input in1,in2,carry_in;
output sum,carry_out;
nand2$ 	nand1 (j1,in1,in2),
nand2 (j2a,j1,in1),
nand3 (j2b,j1,in2),
nand4 (j3,j2a,j2b),
nand5 (j4b,j3,carry_in),
nand6 (j4a,j4b,carry_in),
nand7 (j5,j3,j4b),
nand8 (sum,j4a,j5),
nand9 (carry_out,j1,j4b);
endmodule
//half adder
module ase_ha(sum,in1,in2);
input in1,in2;
output [1:0] sum;
//nand2$ 	nand1 (j1,in1,in2),
//nand2 (j2a,j1,in1),
//nand3 (j2b,j1,in2),
//nand8 (sum,j2a,j2b),
//nand9 (carry_out,j1,j1);
xor2$ xorgate(sum[0],in1,in2);
and2$ andgate(sum[1],in1,in2);
endmodule

// special adder  2bit+1 bit, to output a max of 3
//delay =nand+inv
module ase_adder_2mod(sum,in1,in2);
input [1:0] in1;
input in2;
wire [1:0] out_ha;
output [1:0] sum;
xor2$ xorgate(sum[0],in1[0],in2);
nand2$ andgate(node1,in1[0],in2);
inv1$ invgate (node2,in1[1]);
nand2$ nandg (sum[1],node1,node2);
endmodule



// 16 bit FA
module ase_rca_fa_16(sum,carry_out,in1,in2,carry_in);
input carry_in;
input  [15:0] in1,in2;
output [15:0] sum;
output carry_out;
wire [14:0] carry;
ase_fa fa_g0 (sum[0],carry[0],in1[0],in2[0],carry_in);
genvar i;
generate
for (i=1; i<15; i=i+1) begin
	ase_fa fa_gg (sum[i],carry[i],in1[i],in2[i],carry[i-1]);
end
endgenerate
ase_fa fa_g15 (sum[15],carry_out,in1[15],in2[15],carry[14]);
endmodule

module neg_overflow (n_of,msb1,msb2,carry_out);
input msb1,msb2,carry_out;
output n_of;
nand2$ nand1 (j1,msb1,msb2);
inv1$ inv1 (j2a,j1);
inv1$ inv2 (j2b,carry_out);
nand2$ nand2 (j3,j2a,j2b);
inv1$ inv3 (n_of,j3);
endmodule


module ase_mux2_64bit(out,in1,in2,control0);
input [63:0]in1,in2;
input  control0;
output [63:0]out;
mux2_16$ mux63_48(out[63:48],in1[63:48],in2[63:48],control0);
mux2_16$ mux47_32(out[47:32],in1[47:32],in2[47:32],control0);
mux2_16$ mux31_16(out[31:16],in1[31:16],in2[31:16],control0);
mux2_16$ mux15_0(out[15:0],in1[15:0],in2[15:0],control0);
endmodule
module ase_mux2_32bit(out,in1,in2,control0);
input [31:0]in1,in2;
input  control0;
output [31:0]out;
mux2_16$ mux31_16(out[31:16],in1[31:16],in2[31:16],control0);
mux2_16$ mux15_0(out[15:0],in1[15:0],in2[15:0],control0);
endmodule

module ase_mux4_32bit(out,in1,in2,in3,in4,control0,control1);
parameter width=32;
input [width-1:0]in1,in2,in3,in4;
input  control0,control1;
output [width-1:0]out;
mux4_16$ mux31_16(out[31:16],in1[31:16],in2[31:16],in3[31:16],in4[31:16],control0,control1);
mux4_16$ mux15_0(out[15:0],in1[15:0],in2[15:0],in3[15:0],in4[15:0],control0,control1);
endmodule

module ase_mux4_64bit(out,in1,in2,in3,in4,control0,control1);
input [63:0]in1,in2,in3,in4;
input  control0,control1;
output [63:0]out;
mux4_16$ mux63_48(out[63:48],in1[63:48],in2[63:48],in3[63:48],in4[63:48],control0,control1);
mux4_16$ mux47_32(out[47:32],in1[47:32],in2[47:32],in3[47:32],in4[47:32],control0,control1);
mux4_16$ mux31_16(out[31:16],in1[31:16],in2[31:16],in3[31:16],in4[31:16],control0,control1);
mux4_16$ mux15_0(out[15:0],in1[15:0],in2[15:0],in3[15:0],in4[15:0],control0,control1);
endmodule
module ase_mux2_128bit(out,in1,in2,control0);
input [127:0]in1,in2;
input  control0;
output [127:0]out;
ase_mux2_64bit mux1(out[127:64],in1[127:64],in2[127:64],control0);
ase_mux2_64bit mux2(out[63:0],in1[63:0],in2[63:0],control0);
endmodule

module ase_mux4_128bit(out,in1,in2,in3,in4,control0,control1);
input [127:0]in1,in2,in3,in4;
input  control0,control1;
output [127:0]out;
ase_mux4_64bit mux1(out[127:64],in1[127:64],in2[127:64],in3[127:64],in4[127:64],control0,control1);
ase_mux4_64bit mux2(out[63:0],in1[63:0],in2[63:0],in3[63:0],in4[63:0],control0,control1);
endmodule



module ase_mux4_4bit(out,in1,in2,in3,in4,control0,control1);
input [3:0]in1,in2,in3,in4;
input  control0,control1;
output [3:0]out;
wire [7:0] out_8;
mux4_8$ mux15_0(out_8,{4'b0,in1},{4'b0,in2},{4'b0,in3},{4'b0,in4},control0,control1);
assign out=out_8[3:0];
endmodule



module ase_mux4_64bit_bufferred(out,in1,in2,in3,in4,control0,control1);
input [63:0]in1,in2,in3,in4;
input  control0,control1;
output [63:0]out;
buffer$ buf_control0(control0_buf,control0);
buffer$ buf_control1(control1_buf,control1);
mux4_16$ mux63_48(out[63:48],in1[63:48],in2[63:48],in3[63:48],in4[63:48],control0_buf,control1_buf);
mux4_16$ mux47_32(out[47:32],in1[47:32],in2[47:32],in3[47:32],in4[47:32],control0_buf,control1_buf);
mux4_16$ mux31_16(out[31:16],in1[31:16],in2[31:16],in3[31:16],in4[31:16],control0_buf,control1_buf);
mux4_16$ mux15_0(out[15:0],in1[15:0],in2[15:0],in3[15:0],in4[15:0],control0_buf,control1_buf);
endmodule

module ase_mux2_16bit(out,in1,in2,control);
input [15:0] in1,in2;
input  control;
output [15:0] out;
genvar i;
generate
for (i=0; i<16;i=i+1) begin
	mux2$  mux_gg (out[i],in1[i],in2[i],control);
end
endgenerate




endmodule
module ase_mux4_16bit(out,in1,in2,in3,in4,control);
input [15:0] in1,in2,in3,in4;
input [1:0] control;
output [15:0] out;
genvar i;
generate
for (i=0; i<16;i=i+1) begin
	mux4$ mux_gg (out[i],in1[i],in2[i],in3[i],in4[i],control[0],control[1]);
end
endgenerate
endmodule




module ase_or9 (out,in1,in2,in3,in4,in5,in6,in7,in8,in9);
// delay is .45
//delay is .25+.2
input in1,in2,in3,in4,in5,in6,in7,in8,in9;
output out;
nor3$ nor1(nor1_o,in1,in2,in3);
nor3$ nor2(nor2_o,in4,in5,in6);
nor3$ nor3(nor3_o,in7,in8,in9);
nand3$ nand1(out,nor1_o,nor2_o,nor3_o);
endmodule


module ase_or6 (out,in1,in2,in3,in4,in5,in6);
// delay is .45
//delay is .25+.2
input in1,in2,in3,in4,in5,in6;
output out;
nor2$ nor1(nor1_o,in1,in2);
nor2$ nor2(nor2_o,in3,in4);
nor2$ nor3(nor3_o,in5,in6);
nand3$ nand1(out,nor1_o,nor2_o,nor3_o);
endmodule




module ase_or12 (out,in1,in2,in3,in4,in5,in6,in7,in8,in9,in10,in11,in12);
// delay is .5
//delay is .25+.25
input in1,in2,in3,in4,in5,in6,in7,in8,in9,in10,in11,in12;
output out;
nor3$ nor1(nor1_o,in1,in2,in3);
nor3$ nor2(nor2_o,in4,in5,in6);
nor3$ nor3(nor3_o,in7,in8,in9);
nor3$ nor4(nor4_o,in10,in11,in12);
nand4$ nand1(out,nor1_o,nor2_o,nor3_o,nor4_o);
endmodule



module ase_or18 (out,in1,in2,in3,in4,in5,in6,in7,in8,in9,in10,in11,in12,in13,in14,in15,in16,in17,in18);
// delay is .85
//delay is .20+.25+.4
input in1,in2,in3,in4,in5,in6,in7,in8,in9,in10,in11,in12,in13,in14,in15,in16,in17,in18;
output out;
nor2$ nor1(nor1_o,in1,in2);
nor2$ nor2(nor2_o,in3,in4);
nor2$ nor3(nor3_o,in5,in6);
nor2$ nor4(nor4_o,in7,in8);
nor2$ nor5(nor5_o,in9,in10);
nor2$ nor6(nor6_o,in11,in12);
nor2$ nor7(nor7_o,in13,in14);
nor2$ nor8(nor8_o,in15,in16);
nor2$ nor9(nor9_o,in17,in18);
nand3$ nand1(nand1out,nor1_o,nor2_o,nor3_o);
nand3$ nand2(nand2out,nor4_o,nor5_o,nor6_o);
nand3$ nand3(nand3out,nor7_o,nor8_o,nor9_o);
or3$ nor3_out(out,nand1out,nand2out,nand3out);
endmodule

module ase_dff_4_with_loop(clk,newd,q,mux,r,s);

input clk,mux,r,s;
input [3:0] newd;
output [3:0] q;
wire [7:0] qx;
wire [7:0] qbar,muxout,oldd;
assign oldd=qx;
mux2_8$ mux32l (muxout,oldd,{4'b0,newd[3:0]},mux);
dff8$ dff32l(clk,muxout,qx,qbar,r,s);
assign q=qx[3:0];
endmodule



module ase_dff_16_with_loop(clk,newd,q,mux,r,s);

input clk,mux,r,s;
input [15:0] newd;
output [15:0] q;
wire [15:0] qbar,muxout,oldd;
assign oldd=q;
mux2_16$ mux32l (muxout[15:0],oldd[15:0],newd[15:0],mux);
dff16$ dff32l(clk,muxout[15:0],q[15:0],qbar[15:0],r,s);
endmodule


module ase_dff_16_with_loop_extra_in(clk,oldd,newd,q,mux,r,s);
parameter width=16;
input clk,mux,r,s;
input [width-1:0] newd;
input [width-1:0] oldd;
output [width-1:0] q;
wire [width-1:0] qbar,muxout;
assign oldd=q;
mux2_16$ mux32l (muxout[width-1:0],oldd[width-1:0],newd[width-1:0],mux);
dff16$ dff32l(clk,muxout[width-1:0],q[width-1:0],qbar[width-1:0],r,s);
endmodule





module ase_dff_32_with_loop(clk,newd,q,mux,r,s);

input clk,mux,r,s;
input [31:0] newd;
inout [31:0] q;
wire [31:0] qbar,muxout,oldd;
assign oldd=q;
mux2_16$ mux32h (muxout[31:16],oldd[31:16],newd[31:16],mux);
mux2_16$ mux32l (muxout[15:0],oldd[15:0],newd[15:0],mux);
dff16$ dff32h(clk,muxout[31:16],q[31:16],qbar[31:16],r,s);
dff16$ dff32l(clk,muxout[15:0],q[15:0],qbar[15:0],r,s);
endmodule



module ase_dff_128_with_loop(clk,newd,q,mux,r,s);

parameter width=128;
parameter step=32;
input clk,mux,r,s;
input [width-1:0] newd;
output [width-1:0] q;
wire [width-1:0] qbar,muxout,oldd;
assign oldd=q;
genvar i;
generate

for (i=0; i<width;i=i+step) begin
	ase_mux2_32bit mux32l (muxout[i+step-1:i],oldd[i+step-1:i],newd[i+step-1:i],mux);
	ase_dff_32_with_loop dff32l(clk,muxout[i+step-1:i],q[i+step-1:i],mux,r,s);
end 
endgenerate
endmodule


module ase_dff_128_with_loop_extra_in(clk,oldd,newd,q,mux,r,s);

parameter width=128;
parameter step=32;
input clk,mux,r,s;
input [width-1:0] newd;
output [width-1:0] q;
wire [width-1:0] qbar,muxout;
input [width-1:0] oldd;
genvar i;
generate

for (i=0; i<width;i=i+step) begin
	ase_mux2_32bit mux32l (muxout[i+step-1:i],oldd[i+step-1:i],newd[i+step-1:i],mux);
	ase_dff_32_with_loop dff32l(clk,muxout[i+step-1:i],q[i+step-1:i],mux,r,s);
end 
endgenerate
endmodule


module ase_dff_1_with_loop_extra_in(clk,oldd,newd,q,mux,r,s);

input clk,mux,r,s;
input  newd;
output  q;
wire  qbar,muxout;
input oldd;
mux2$ mux32l (muxout,oldd,newd,mux);
dff$ dff32l(clk,muxout,q,qbar,r,s);
endmodule




module ase_dff_256_with_loop(clk,newd,q,mux,r,s);

parameter width=256;
parameter step=32;
input clk,mux,r,s;
input [width-1:0] newd;
output [width-1:0] q;
wire [width-1:0] qbar,muxout,oldd;
assign old=q;
genvar i;
generate

for (i=0; i<width;i=i+step) begin
	ase_mux2_32bit mux32l (muxout[i+step-1:i],oldd[i+step-1:i],newd[i+step-1:i],mux);
	ase_dff_32_with_loop dff32l(clk,muxout[i+step-1:i],q[i+step-1:i],mux,r,s);
end 
endgenerate
endmodule

module ase_mux2_2bit(out,in1,in2,control);
input control;
input [1:0]in1,in2;
wire [7:0] outx;
output [1:0]out;
mux2_8$ mux(outx,{6'b0,in1},{6'b0,in2},control);
assign out=outx[1:0];
endmodule


module ase_mux2_4bit(out,in1,in2,control);
input control;
input [3:0]in1,in2;
wire [7:0] outx;
output [3:0]out;
mux2_8$ mux(outx,{4'b0,in1},{4'b0,in2},control);
assign out=outx[3:0];
endmodule
module ase_mux2_5bit(out,in1,in2,control);
input control;
input [4:0]in1,in2;
wire [7:0] outx;
output [4:0]out;
mux2_8$ mux(outx,{3'b0,in1},{3'b0,in2},control);
assign out=outx[4:0];
endmodule


module ase_1to16 (out,in);
parameter width=128;
parameter step=8;
input in;
output [15:0] out;
wire out_1;
bufferH16$ thebuffer(out_1,in);
genvar i;
generate 
for (i=0;i<16;i=i+1) begin
	assign out[i]= out_1;
end
endgenerate

endmodule


module ase_mux2_16_8g_mult_sel(out,in1,in2,control);
parameter width=16;
parameter step=1;
input [width-1:0] in1,in2;
output [width-1:0] out;
input [width/step-1:0] control;
genvar i;
generate
for (i=0; i<width/step;i=i+1) begin
	mux2$ mux(out[i*step+step-1:i*step],in1[i*step+step-1:i*step],in2[i*step+step-1:i*step],control[i]);
end
endgenerate
endmodule



module ase_mux2_128_8g_mult_sel(out,in1,in2,control);
parameter width=128;
parameter step=8;
input [width-1:0] in1,in2;
output [width-1:0] out;
input [width/step-1:0] control;
genvar i;
generate
for (i=0; i<width/step;i=i+1) begin
	mux2_8$ mux(out[i*step+step-1:i*step],in1[i*step+step-1:i*step],in2[i*step+step-1:i*step],control[i]);
end
endgenerate
endmodule

module barrel_128_8_right (out,in1,shift);
parameter width=128;
parameter step=8;
input [width-1:0] in1;
input [3:0] shift;
output [width-1:0] out;
wire [width-1:0] in1_flipped,out_flipped;
genvar i;
generate
for (i=0; i<width/step;i=i+1) begin
	assign in1_flipped[i*step+step-1:i*step]=in1[width-i*step-1:width-i*step-step];
	assign out[i*step+step-1:i*step]=out_flipped[width-i*step-1:width-i*step-step];
end
endgenerate
barrel_128_8 shift_left(out_flipped,in1_flipped,shift);

endmodule


module barrel_16_1_right (out,in1,shift);
parameter width=16;
parameter step=1;
input [width-1:0] in1;
input [3:0] shift;
output [width-1:0] out;
wire [width-1:0] in1_flipped,out_flipped;
genvar i;
generate
for (i=0; i<width/step;i=i+1) begin
	assign in1_flipped[i*step+step-1:i*step]=in1[width-i*step-1:width-i*step-step];
	assign out[i*step+step-1:i*step]=out_flipped[width-i*step-1:width-i*step-step];
end
endgenerate
barrel_16_1 shift_left(out_flipped,in1_flipped,shift);

endmodule


module ase_mux16_1bit(out,in,control);
input [15:0]in;
input [3:0]control;
output out;

wire stage0_0,stage0_1,stage0_2,stage0_3;
mux4$ stage0_0g(stage0_0,in[0],in[1],in[2],in[3],control[0],control[1]);
mux4$ stage0_1g(stage0_1,in[4],in[5],in[6],in[7],control[0],control[1]);
mux4$ stage0_2g(stage0_2,in[8],in[9],in[10],in[11],control[0],control[1]);
mux4$ stage0_3g(stage0_3,in[12],in[13],in[14],in[15],control[0],control[1]);
mux4$ stage1g(out,stage0_0,stage0_1,stage0_2,stage0_3,control[2],control[3]);

endmodule

module ase_mux32_1bit(out,in,control);
input [31:0]in;
input [4:0]control;
output out;
ase_mux16_1bit mux1(outa,in[15:0],control[3:0]);
ase_mux16_1bit mux2(outb,in[31:16],control[3:0]);
mux2$ mux3(out,outa,outb,control[4]);






endmodule






module ase_mux8_32bit(out,in1,in2,in3,in4,in5,in6,in7,in8,control);
input [31:0]in1,in2,in3,in4,in5,in6,in7,in8;
input  [2:0] control;
wire [31:0] outa,outb,outc,outd,out;
output [31:0]out;
mux2_16$ amux31_16(outa[31:16],in1[31:16],in2[31:16],control[0]);
mux2_16$ amux15_0(outa[15:0],in1[15:0],in2[15:0],control[0]);
mux2_16$ bmux31_16(outb[31:16],in3[31:16],in4[31:16],control[0]);
mux2_16$ bmux15_0(outb[15:0],in3[15:0],in4[15:0],control[0]);
mux2_16$ cmux31_16(outc[31:16],in5[31:16],in6[31:16],control[0]);
mux2_16$ cmux15_0(outc[15:0],in5[15:0],in6[15:0],control[0]);
mux2_16$ dmux31_16(outd[31:16],in7[31:16],in8[31:16],control[0]);
mux2_16$ dmux15_0(outd[15:0],in7[15:0],in8[15:0],control[0]);


mux4_16$ emux31_16(out[31:16],outa[31:16],outb[31:16],outc[31:16],outd[31:16],control[1],control[2]);
mux4_16$ emux15_0(out[15:0],outa[15:0],outb[15:0],outc[15:0],outd[15:0],control[1],control[2]);
endmodule


module ase_mux8_64bit(out,in1,in2,in3,in4,in5,in6,in7,in8,control);
input [63:0]in1,in2,in3,in4,in5,in6,in7,in8;
input  [2:0] control;
wire [63:0] outa,outb,outc,outd,out;
output [63:0]out;
ase_mux2_64bit amux31_16(outa,in1,in2,control[0]);
ase_mux2_64bit  bmux31_16(outb,in3,in4,control[0]);
ase_mux2_64bit  cmux31_16(outc,in5,in6,control[0]);
ase_mux2_64bit  dmux31_16(outd,in7,in8,control[0]);

ase_mux4_64bit emux31_16(out,outa,outb,outc,outd,control[1],control[2]);
endmodule

module Bit_Reverse (oOut, iA);
  parameter numBits = 4;
  
  input [numBits-1:0] iA;
  output [numBits-1:0] oOut;

  genvar b;
  generate
    for(b=0; b<numBits; b=b+1) begin : byteReverse
      assign oOut[b] = iA[(numBits-b-1)];
    end
  endgenerate
endmodule



`endif
