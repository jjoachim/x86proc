`ifndef _RF_VH
`define _RF_VH

`timescale 1ns/1ps

`include "defines.v"
`include "libSJQ.v"
`include "RF/BaseGenerator.v"
`include "RF/IndexGenerator.v"
`include "RF/DispImmDecoder.v"
`include "RF/RegisterFile.v"
`include "RF/SegmentGenerator.v"
`include "RF/MemDep.v"
`include "Control/Control.v"
`include "CommonLogic/PipeRegs.v"

// Register file stage
module RF(iClk,    iNSysRst,
          iNFlush, iNStall,
          oFlush,  oStall,

          oCSS,

          oStat,  oControlStore,
          oRegW,  oSegW, oDstR,
          oREG,
          oGPR,   oRM,
          oIndex, oBase, oDisp, oSeg, oSegN, oScale,
          oMemR,  oMemW,
          oAltData,
          oCS,    oEIP,  oEIPPLUS,

          iStat,  iControlStore,
          iModRM, iSIB,  iInstTail,
          iCS,    iEIP,  iISZ,

          iIN,  iWR,  iWE,  iMODEW,
          iINS, iSWR, iSWE);

    // I/O
    // System controls
    input           iClk;
    input           iNSysRst;
    input           iNFlush;
    input           iNStall;
    output          oFlush;
    output          oStall;

    // CSR -> RF
    input   [31:0]  iStat;
    input   [68:0]  iControlStore;
    input   [ 7:0]  iModRM;
    input   [ 7:0]  iSIB;
    input   [63:0]  iInstTail;
    input   [15:0]  iCS;
    input   [31:0]  iEIP;
    input   [ 3:0]  iISZ;

    // WB -> RF
    input   [63:0]  iIN;
    input   [ 2:0]  iWR;
    input           iWE;
    input   [ 1:0]  iMODEW;
    input   [15:0]  iINS;
    input   [ 2:0]  iSWR;
    input           iSWE;

    // RF -> IF
    output  [15:0]  oCSS;

    // RF -> AGE
    output  [31:0]  oStat;
    output  [68:0]  oControlStore;
    output          oRegW;
    output          oSegW;
    output  [ 2:0]  oDstR;
    output  [ 2:0]  oREG;
    output  [63:0]  oGPR;
    output  [63:0]  oRM;
    output  [31:0]  oIndex;
    output  [31:0]  oBase;
    output  [31:0]  oDisp;
    output  [15:0]  oSeg;
    output  [31:0]  oSegN;
    output  [ 1:0]  oScale;
    output          oMemR;
    output          oMemW;
    output  [63:0]  oAltData;
    output  [15:0]  oCS;
    output  [31:0]  oEIP;
    output  [31:0]  oEIPPLUS;

    // Piplene stage inputs
    wire    [31:0]  wStat;
    wire    [68:0]  wControlStore;
    wire            wRegW;
    wire            wSegW;
    wire    [ 2:0]  wDstR;
    wire    [ 2:0]  wREG;
    wire    [63:0]  wGPR;
    wire    [63:0]  wRM;
    wire    [31:0]  wIndex;
    wire    [31:0]  wBase;
    wire    [31:0]  wDisp;
    wire    [15:0]  wSeg;
    wire    [31:0]  wSegN;
    wire    [ 1:0]  wScale;
    wire            wMemR;
    wire            wMemW;
    wire    [63:0]  wAltData;
    wire    [15:0]  wCS;
    wire    [31:0]  wEIP;
    wire    [31:0]  wEIPPLUS;

    // Buffered inputs
    wire    [31:0]  iStatBuf;
    wire    [68:0]  iControlStoreBuf;
    wire    [ 7:0]  iModRMBuf;
    wire    [ 7:0]  iSIBBuf;
    wire    [63:0]  iInstTailBuf;
    wire    [15:0]  iCSBuf;
    wire    [31:0]  iEIPBuf;

    // Split control-store signals
    wire            OPSZOVRPRE; // Operand size override prefix present
    wire            SEGOVRPRE;  // Segment override prefix present
    wire    [ 2:0]  SEGOVR;     // Override segment
    wire    [ 2:0]  RPLUS;      // Lower three bits of opcode
    wire            USESIB;     // Using SIB

    wire            TRAPOP;     // TRAP operation
    wire            ROP;        // ROP
    wire    [ 5:0]  IROP;       // Index of ROP
    wire    [ 1:0]  OPSZ;       // Default operand size
    wire    [ 1:0]  OPSZOVR;    // Overridden operand size
    wire            OPSZOVRS;   // Forced, special operand size override
    wire    [ 1:0]  OPSZS;      // Special operand size
    wire    [ 2:0]  OPERANDS;   // Operand types
    wire            DIR;        // Direction
    wire            DIRBAR;     // ~Direction
    wire            REGMUX;     // REG bits or R+
    wire    [ 2:0]  RSRCMUX;     // 000=REGMUX 001=iEAX 010=iESP 011=iECX
                                // 100=iESI   101=iEDI 110=XXX  111=XXX
    wire            USEREG;     // Using register value
    wire            USEMODRM;   // Using ModR/M
    wire            USEIMM;     // Using immediate
    wire            SEXTIMM;    // Using sign-extended immediate
    wire            SEGOVRDIS;  // Segment override disabled
    wire    [ 2:0]  SEGMUX;     // Opcode-selected segment
    wire            MOVOP;      // MOV operation
    wire            RSTACK;     // Read from stack
    wire            WSTACK;     // Write to stack
    wire            CMPSMUX;    // CMPS operation
    wire            CMPSR;      // 0=ESI 1=EDI
    wire    [ 1:0]  ALTMUX0;    // 00=Immediate 01=SegR 10=EIP 11=BrAddr
    wire            ALTMUX1;    // 0=MEMR.AltData 1=TEMP
    wire            LDTEMP;     // Load TEMP register
    wire            ALUAMUX;    // 0=Register 1=AltData
    wire            ALUBMUX;    // 0=ModR/M   1=AltData
    wire    [ 3:0]  ALUMUX;     // ALU operation
    wire    [ 2:0]  FLAGMUX;    // Flag operation
    wire    [ 2:0]  ALUOUTMUX;  // 0=ALU 1=EFLAGS
    wire            BROP;       // Branch instruction
    wire    [ 1:0]  BRCOND;     // 00=Unconditional  10=~ZF
                                // 10=~CF&~ZF        11=Unused
    wire    [ 1:0]  BRTYPE;     // 00=Short/Relative 01=Near/Realtive
                                // 10=Near/Absolute  11=Far/Absolute
    wire            DISABLECF0; // Disable if ~CF                                
    wire            DISABLEZF0; // Disable if ~ZF
    wire            DISABLEZF1; // Disable if ZF

    // Instruction tail data
    wire    [31:0]  DISP;       // Displacement
    wire    [31:0]  IMM;        // Immediate
    wire    [47:0]  BRADDR;     // Branch Address

    // ModR/M bits
    wire    [ 7:0]  MODRM;
    wire    [ 1:0]  MOD;
    wire            MOD00, MOD01, MOD10, MOD01BAR, MOD11, MOD1XBAR, MODX1BAR, NANDMOD;
    wire            MODREG, MODREGBAR, MODMEM;
    wire            MODREGRD;
    wire    [ 2:0]  REG;
    wire    [ 2:0]  RM;

    // SIB bits
    wire    [ 7:0]  SIB;
    wire    [ 1:0]  SCALE;
    wire    [ 2:0]  INDEX;
    wire    [ 2:0]  BASE;
    wire    [ 1:0]  MSCALE;

    // RF GPR address logic
    wire    [ 2:0]  REGMUXOUT;
    wire    [ 2:0]  RD0;
    wire    [ 2:0]  SIBMUXOUT;
    wire    [ 2:0]  CMPSRMUXOUT;
    wire    [ 2:0]  CMPSMUXOUT;
    wire            STACKRW;
    wire    [ 2:0]  RD2;

    // SEG address logic
    wire    [ 2:0]  SRD0;
    wire    [ 2:0]  SRD1;

    // Operand Sizes
    wire    [ 1:0]  OPSZA;
    wire    [ 1:0]  OPSZB;

    // Register file outputs
    wire    [63:0]  OUT0;
    wire    [63:0]  OUT1;
    wire    [31:0]  OUT2;
    wire    [31:0]  OUT3;
    wire    [15:0]  SGO0;
    wire    [15:0]  SGO1;

    // Forwarded RF outputs
    wire    [63:0]  FOUT0;
    wire    [63:0]  FOUT1;
    wire    [31:0]  FOUT2;
    wire    [31:0]  FOUT3;
    wire    [15:0]  FSGO0;
    wire    [15:0]  FSGO1;

    // Opeands logic
    wire            OPERANDS1XXBAR;
    wire            OPERANDSX1XBAR;
    wire            OPERANDSXX1BAR;
    wire            OPERANDS0X0;
    wire            OPERANDSX10;
    wire            OPERANDSX00;
    wire            OPERANDS101;
    wire            OPERANDS111;
    wire            OPERANDSX11;
    wire            OPERANDS100SEG;
    wire            OPERANDSUSEMRM;

    // SegW logic
    wire            SEGW;

    // RegW logic
    wire            REGWDIRMUX;

    // DstR Logic
    wire    [ 2:0]  DIRMUXOUT;
    wire    [ 2:0]  EAXMUXOUT;

    // MemW logic
    wire            STDMRMWR;
    wire            DIRMRMWR;

    // Segment limits
    wire    [19:0]  CSLIMIT;
    wire    [19:0]  DSLIMIT;
    wire    [19:0]  SSLIMIT;
    wire    [19:0]  ESLIMIT;
    wire    [19:0]  FSLIMIT;
    wire    [19:0]  GSLIMIT;
    wire    [31:0]  CSLIMITNEG;
    wire    [31:0]  DSLIMITNEG;
    wire    [31:0]  SSLIMITNEG;
    wire    [31:0]  ESLIMITNEG;
    wire    [31:0]  FSLIMITNEG;
    wire    [31:0]  GSLIMITNEG;
    wire    [31:0]  SEGLIMMUXOUT;
    wire    [31:0]  NBYTES;
    wire            SEGLIMADDCOUT;

    // Dependency logic
    wire            OPSZB00;
    wire            OPSZB11;
    wire            GPRHMASK;
    wire            RMHMASK;
    wire            DSTRHMASK;
    wire            RFW;
    wire            MEMRW;
    wire            USEBASE;
    wire            ALTMUX0_X1BAR;
    wire            ALTMUX0_01;
    wire            iFlush;
    wire            iStall;
    wire            depStall;
    wire            rfOnlyStall;
    wire            rfFlush;
    wire            rfFlushN;
    wire            rfStallN;
    wire            memBubble;
    wire            stdFlush;
    wire            stdStall;

    // Buffer RF stage inputs
    assign iStatBuf         = iStat;
    assign iControlStoreBuf = iControlStore;
    assign iModRMBuf        = iModRM;
    assign iSIBBuf          = iSIB;
    assign iInstTailBuf     = iInstTail;
    assign iCSBuf           = iCS;
    assign iEIPBuf          = iEIP;

    // Split Control-Store
    assign OPSZOVRPRE = iControlStoreBuf[`iOPSZOVRPRE];
    assign SEGOVRPRE  = iControlStoreBuf[`iSEGOVRPRE];
    assign SEGOVR     = iControlStoreBuf[`iSEGOVR];
    assign RPLUS      = iControlStoreBuf[`iRPLUS];
    assign USESIB     = iControlStoreBuf[`iUSESIB];
    assign TRAPOP     = iControlStoreBuf[`iTRAPOP];
    assign ROP        = iControlStoreBuf[`iROP];
    assign IROP       = iControlStoreBuf[`iIROP];
    assign OPSZ       = iControlStoreBuf[`iOPSZ];
    assign OPSZOVR    = iControlStoreBuf[`iOPSZOVR];
    assign OPSZOVRS   = iControlStoreBuf[`iOPSZOVRS];
    assign OPSZS      = iControlStoreBuf[`iOPSZS];
    assign OPERANDS   = iControlStoreBuf[`iOPERANDS];
    assign DIR        = iControlStoreBuf[`iDIR];
    inv1$  dir_inv(DIRBAR, DIR);
    assign REGMUX     = iControlStoreBuf[`iREGMUX];
    assign RSRCMUX    = iControlStoreBuf[`iRSRCMUX];
    assign USEREG     = iControlStoreBuf[`iUSEREG];
    assign USEMODRM   = iControlStoreBuf[`iUSEMODRM];
    assign USEIMM     = iControlStoreBuf[`iUSEIMM];
    assign SEXTIMM    = iControlStoreBuf[`iSEXTIMM];
    assign SEGOVRDIS  = iControlStoreBuf[`iSEGOVRDIS];
    assign SEGMUX     = iControlStoreBuf[`iSEGMUX];
    assign MOVOP      = iControlStoreBuf[`iMOVOP];
    assign RSTACK     = iControlStoreBuf[`iRSTACK];
    assign WSTACK     = iControlStoreBuf[`iWSTACK];
    assign CMPSMUX    = iControlStoreBuf[`iCMPSMUX];
    assign CMPSR      = iControlStoreBuf[`iCMPSR];
    assign ALTMUX0    = iControlStoreBuf[`iALTMUX0];
    assign ALTMUX1    = iControlStoreBuf[`iALTMUX1];
    assign LDTEMP     = iControlStoreBuf[`iLDTEMP];
    assign ALUAMUX    = iControlStoreBuf[`iALUAMUX];
    assign ALUBMUX    = iControlStoreBuf[`iALUBMUX];
    assign ALUMUX     = iControlStoreBuf[`iALUMUX];
    assign FLAGMUX    = iControlStoreBuf[`iFLAGMUX];
    assign ALUOUTMUX  = iControlStoreBuf[`iALUOUTMUX];
    assign BROP       = iControlStoreBuf[`iBROP];
    assign BRCOND     = iControlStoreBuf[`iBRCOND];
    assign BRTYPE     = iControlStoreBuf[`iBRTYPE];
    assign DISABLECF0 = iControlStoreBuf[`iDISABLECF0];
    assign DISABLEZF0 = iControlStoreBuf[`iDISABLEZF0];
    assign DISABLEZF1 = iControlStoreBuf[`iDISABLEZF1];

    // Split ModR/M
    assign MODRM = iModRMBuf;
    assign MOD   = iModRMBuf[7:6];
    assign REG   = iModRMBuf[5:3];
    assign RM    = iModRMBuf[2:0];

    // Split SIB
    assign SIB   = iSIBBuf;
    assign SCALE = iSIBBuf[7:6];
    assign INDEX = iSIBBuf[5:3];
    assign BASE  = iSIBBuf[2:0];

    // Send stat and control-store through
    assign wStat         = iStatBuf;
    assign wControlStore = iControlStoreBuf;

    // Instruction tail logic
    DispImmDecoder didec(DISP, IMM, BRADDR, iInstTailBuf, MODRM, SIB, iControlStoreBuf);

    // RD0 Logic
    mux2_3$ regmux_mux(REGMUXOUT, REG, RPLUS, REGMUX);
    mux8_3$ rd0_mux(RD0, 
                    REGMUXOUT, 3'b000, 3'b100, 3'b001,
                    3'b110,    3'b111, 3'bZZZ, 3'bZZZ,
                    RSRCMUX);

    // RD2 Logic
    mux2_3$ sibindex_mux(SIBMUXOUT, RM, INDEX, USESIB);
    mux2_3$ cmpsr_mux(CMPSRMUXOUT, 3'b110, 3'b111, CMPSR);
    mux2_3$ cmps_mux(CMPSMUXOUT, SIBMUXOUT, CMPSRMUXOUT, CMPSMUX);
    or2$    stackrw_or(STACKRW, RSTACK, WSTACK);
    mux2_3$ rd2_mux(RD2, CMPSMUXOUT, 3'b100, STACKRW);

    // Operand sizes
    mux2_2$ opsza_mux(OPSZA, OPSZ,  OPSZOVR, OPSZOVRPRE);
    mux2_2$ opszb_mux(OPSZB, OPSZA, OPSZS,   OPSZOVRS);
    and2$   opszb11_and(OPSZB11, OPSZB[0], OPSZB[1]);
    nor2$   opszb00_nor(OPSZB00, OPSZB[0], OPSZB[1]);

    // Operands logic
    inv1$  operands1xxbar_inv(OPERANDS1XXBAR, OPERANDS[2]);
    inv1$  operandsx1xbar_inv(OPERANDSX1XBAR, OPERANDS[1]);
    inv1$  operandsxx1bar_inv(OPERANDSXX1BAR, OPERANDS[0]);
    nor2$  operands0x0_nor(OPERANDS0X0, OPERANDS[2],    OPERANDS[0]);
    nor2$  operandsx10_nor(OPERANDSX10, OPERANDSX1XBAR, OPERANDS[0]);
    nor2$  operandsx00_nor(OPERANDSX00, OPERANDS[1], OPERANDS[0]);
    and3$  operands101_and(OPERANDS101, OPERANDS[2], OPERANDSX1XBAR, OPERANDS[0]);
    and3$  operands111_and(OPERANDS111, OPERANDS[2], OPERANDS[1], OPERANDS[0]);
    and2$  operandsx11_and(OPERANDSX11, OPERANDS[1], OPERANDS[0]);
    and4$  operands100seg_and(OPERANDS100SEG, DIR, OPERANDS[2], OPERANDSX1XBAR, OPERANDSXX1BAR);
    or4$   operandsusemrm_or(OPERANDSUSEMRM,  OPERANDS100SEG, OPERANDS0X0, OPERANDS101, OPERANDS111);

    // MOD values
    inv1$  mod1xbar_inv(MOD1XBAR, MODRM[7]);
    inv1$  modx1bar_inv(MODX1BAR, MODRM[6]);
    nor2$  mod00_nor(MOD00, MODRM[7], MODRM[6]);
    nor2$  mod01_nor(MOD01, MODRM[7], MODX1BAR);
    or2$   mod01bar_or(MOD01BAR, MODRM[7], MODX1BAR);
    nor2$  mod10_nor(MOD10, MOD1XBAR, MODRM[6]);
    nor2$  mod11_and(MOD11, MOD1XBAR, MODX1BAR);
    nand2$ nandmod_nand(NANDMOD, MOD[0], MOD[1]);
    and2$  modreg_and(MODREG, MOD11,   USEMODRM);
    nand2$ modreg_nand(MODREGBAR, MOD11, USEMODRM);
    and2$  modmem_and(MODMEM, NANDMOD, USEMODRM);
    and2$  modregrd_and(MODREGRD, MODREG, OPERANDSUSEMRM);

    // Segment read addresses
    SegmentGenerator segGen0(SRD0, MODRM,  SIB, iControlStoreBuf);
    mux2_3$          segGen1(SRD1, SEGMUX, REG, MOVOP);

    // Register file
    regfilex86 regfile(iIN,   iWR, iWE,

                       RD0,   OUT0,
                       RM,    OUT1,
                       RD2,   OUT2,
                       BASE,  OUT3,

                       iINS,  iSWR, iSWE,
                       SRD0,  SGO0,
                       SRD1,  SGO1,

                       oCSS,

                       OPSZB, 1'b1,     iMODEW,
                       iClk,  iNSysRst, 1'b1);

    // Forwarding logic
    assign FOUT0 = OUT0;
    assign FOUT1 = OUT1;
    assign FOUT2 = OUT2;
    assign FOUT3 = OUT3;
    assign FSGO0 = SGO0;
    assign FSGO1 = SGO1;

    // SCALE logic
    and2$ mscale0_and(MSCALE[0], SCALE[0], USESIB);
    and2$ mscale1_and(MSCALE[1], SCALE[1], USESIB);

    // Output GPR, RM, Index, Base, Disp, Seg, and Scale
    assign wGPR   = FOUT0;
    assign wRM    = FOUT1;
    IndexGenerator indexgen(wIndex, FOUT2, iModRMBuf, iSIBBuf, iControlStoreBuf);
    BaseGenerator basegen(wBase, USEBASE, FOUT3, MODRM, SIB, iControlStoreBuf);
    assign wDisp = DISP;
    assign wSeg = FSGO0;
    assign wScale = MSCALE;

    // SegW logic
    or2$   segw_or(SEGW, OPERANDS111, OPERANDS100SEG);
    assign wSegW = SEGW;

    // RegW logic
    mux2$   regwdirmux_mux(REGWDIRMUX, MODREG, OPERANDS1XXBAR, DIR);
    mux4$   regw_mux(wRegW, REGWDIRMUX, 1'b1, MOD11, OPERANDS1XXBAR, OPERANDS[0], OPERANDS[1]);

    // DstR logic
    mux2_3$ dir_mux(DIRMUXOUT, RM, RD0, DIR);
    mux2_3$ eaxdst_mux(EAXMUXOUT, DIRMUXOUT, 3'b000, OPERANDSX11);
    mux2_3$ dstr_mux(wDstR, EAXMUXOUT, SRD1, SEGW);

    // Pass REG bits through
    assign wREG = REG;

    // MemR logic
    or4$ memr_or(wMemR, STACKRW, CMPSMUX, MODMEM, TRAPOP);

    // MemW logic
    and3$ stdmrmwr_and(STDMRMWR, OPERANDSX10, NANDMOD, USEMODRM);
    and4$ dirmrmwr_and(DIRMRMWR, OPERANDSX00, NANDMOD, USEMODRM, DIRBAR);
    or3$  memw_or(wMemW, STDMRMWR, DIRMRMWR, WSTACK);

    // AltData logic
    mux4_64$ altmux0_mux(wAltData,
                         {32'h00000000,IMM},     {48'h000000000000,FSGO1},
                         {32'h00000000,iEIPBuf}, {16'h0000,BRADDR},
                         ALTMUX0);


    // Segment limits
    assign CSLIMIT    = 20'h04FFF;
    assign DSLIMIT    = 20'h011FF;
    assign SSLIMIT    = 20'h04000;
    assign ESLIMIT    = 20'h003FF;
    assign FSLIMIT    = 20'h003FF;
    assign GSLIMIT    = 20'h007FF;
    
    assign CSLIMITNEG = 32'hFFFFB001;
    assign DSLIMITNEG = 32'hFFFFEE01;
    assign SSLIMITNEG = 32'hFFFFC000;
    assign ESLIMITNEG = 32'hFFFFFC01;
    assign FSLIMITNEG = 32'hFFFFFC01;
    assign GSLIMITNEG = 32'hFFFFF801;

    // Mux adder inputs
    mux4_32$ nbytes_mux(NBYTES,
                        32'hFFFFFFFF, 32'h00000000,
                        32'h00000002, 32'h00000006,
                        OPSZB);
    mux8_32$ nseglm_mux(SEGLIMMUXOUT,
                        ESLIMITNEG, CSLIMITNEG, SSLIMITNEG,   DSLIMITNEG,
                        FSLIMITNEG, GSLIMITNEG, 32'h00000000, 32'h00000000,
                        SRD0);
    // Output biased segment limit
    KSAdder32 segl_add(wSegN, SEGLIMADDCOUT, SEGLIMMUXOUT, NBYTES, 1'b0);
    

    // Transfer the rest of the data
    assign wCS      = iCSBuf;
    assign wEIP     = iEIPBuf;

    // Stall and dependency logic
    mux2$ gprhmask_mux  (GPRHMASK,   RD0[2],   1'b0, OPSZB00);
    mux2$ rmhmask_mux   (RMHMASK,    RM[2],    1'b0, OPSZB00);
    mux2$ dstrhmask_mux (DSTRHMASK,  wDstR[2], 1'b0, OPSZB00);
    or2$  rfw_or(RFW, wRegW, wSegW);
    or2$  memrw_or(MEMRW, wMemR, wMemW);
    inv1$ altmux0_x1_inv(ALTMUX0_X1BAR, ALTMUX0[1]);
    nor2$ altmux0_01_nor(ALTMUX0_01, ALTMUX0[1], ALTMUX0_X1BAR);
    inv1$ iflush_inv(iFlush, iNFlush);
    inv1$ istall_inv(iStall, iNStall);
    
    MemDep  mem_dep(memBubble, wMemR, wMemW, iNStall, iNFlush, iClk, iNSysRst);
    BasicDepTrack #(5,6,1) depTracker(depStall, 
                                      {{1'b0,OPSZB11,GPRHMASK,RD0[1:0]}, {1'b0,OPSZB11,RMHMASK,RM[1:0]}, {2'b00,RD2}, {2'b00,BASE}, {2'b10,SRD0}, {2'b10,SRD1}},
                                      {             USEREG,                            MODREGRD,             MEMRW,     USEBASE,       MEMRW,      ALTMUX0_01 },
                                      {wSegW,OPSZB11,DSTRHMASK,wDstR[1:0]}, RFW, 
                                      iNStall, iNFlush, iClk, iNSysRst);
    //and2$ rfonlystall_and (rfOnlyStall,  depStall, iNStall);
    //or2$ rfflush_or (rfFlush, rfOnlyStall, iFlush);
    //nor2$ rfflushN_nor(rfFlushN, rfOnlyStall, iFlush);
    //nor2$ rfStallN_nor(rfStallN, depStall, iStall);
    
    assign oFlush = 0;
    or3$   stdflush_or (stdFlush, memBubble, depStall, iFlush);
    nand2$ rfFlushN_nand(rfFlushN, iNStall, stdFlush);
    or2$   stall_or    (stdStall,   depStall,  memBubble       );
    and2$  stall_and   (oStall, stdStall, iNFlush);

    // Next instruction EIP adder
    KSAdder32NC eipplust_add(wEIPPLUS, wEIP, {28'h0000000,iISZ});

    // Pipeline register
    PipeRegs #(529) rf2age ({oStat,oControlStore,oRegW,oSegW,oDstR,
                             oREG,oGPR,oRM,
                             oIndex,oBase,oDisp,oSeg,oSegN,oScale,
                             oMemR,oMemW,oAltData,oCS,oEIP,oEIPPLUS},

                            {wStat,wControlStore,wRegW,wSegW,wDstR,
                             wREG,wGPR,wRM,
                             wIndex,wBase,wDisp,wSeg,wSegN,wScale,
                             wMemR,wMemW,wAltData,wCS,wEIP,wEIPPLUS},

                             /*rfStallN*/ iNStall, rfFlushN,
                             iClk,     iNSysRst);

endmodule

`endif
