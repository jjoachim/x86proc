`ifndef _BYTEREVERSE_V_
`define _BYTEREVERSE_V_

`default_nettype none

/*****************************************************************************/
// Parameterizable Byte Reverse
/*****************************************************************************/
module ByteReverse (oOut, iA);
  parameter numBytes = 4;
  
  input [numBytes*8-1:0] iA;
  output [numBytes*8-1:0] oOut;

  genvar b;
  generate
    for(b=0; b<numBytes; b=b+1) begin : byteReverse
      assign oOut[b*8 +: 8] = iA[(numBytes-b-1)*8 +: 8];
    end
  endgenerate
endmodule

`default_nettype wire
`endif
