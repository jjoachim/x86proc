// Module is based on George Nyarangi's 8-bit Kogge-Stone adder
// http://georgeblog.nyarangi.com/2010/07/8-bit-kogge-stone-adder.html

// 32-bit Kogge-Stone Adder
module KSADD32(sum, cout, x, y);

    input   [31:0]  x, y;   // Input vectors
    wire            cin;    // Carry-in
    output  [31:0]  sum;    // Sum 
    output          cout;   // Carry-out

    // Internal generate/propagate signals
    wire    [31:0]  G_Z, P_Z;
    wire    [31:0]  G_A, P_A;
    wire    [31:0]  G_B, P_B;
    wire    [31:0]  G_C, P_C;
    wire    [31:0]  G_D, P_D;
    wire    [31:0]  G_E, P_E;
    
    // No carry-in for add
    assign cin = 1'b0;
 
    // Level 1
    gray_cell$  level_00A(cin,     P_Z[ 0], G_Z[ 0],          G_A[ 0]);
    black_cell$ level_01A(G_Z[ 0], P_Z[ 1], G_Z[ 1], P_Z[ 0], G_A[ 1], P_A[ 1]);
    black_cell$ level_02A(G_Z[ 1], P_Z[ 2], G_Z[ 2], P_Z[ 1], G_A[ 2], P_A[ 2]);
    black_cell$ level_03A(G_Z[ 2], P_Z[ 3], G_Z[ 3], P_Z[ 2], G_A[ 3], P_A[ 3]);
    black_cell$ level_04A(G_Z[ 3], P_Z[ 4], G_Z[ 4], P_Z[ 3], G_A[ 4], P_A[ 4]);
    black_cell$ level_05A(G_Z[ 4], P_Z[ 5], G_Z[ 5], P_Z[ 4], G_A[ 5], P_A[ 5]);
    black_cell$ level_06A(G_Z[ 5], P_Z[ 6], G_Z[ 6], P_Z[ 5], G_A[ 6], P_A[ 6]);
    black_cell$ level_07A(G_Z[ 6], P_Z[ 7], G_Z[ 7], P_Z[ 6], G_A[ 7], P_A[ 7]);
    black_cell$ level_08A(G_Z[ 7], P_Z[ 8], G_Z[ 8], P_Z[ 7], G_A[ 8], P_A[ 8]);
    black_cell$ level_09A(G_Z[ 8], P_Z[ 9], G_Z[ 9], P_Z[ 8], G_A[ 9], P_A[ 9]);
    black_cell$ level_10A(G_Z[ 9], P_Z[10], G_Z[10], P_Z[ 9], G_A[10], P_A[10]);
    black_cell$ level_11A(G_Z[10], P_Z[11], G_Z[11], P_Z[10], G_A[11], P_A[11]);
    black_cell$ level_12A(G_Z[11], P_Z[12], G_Z[12], P_Z[11], G_A[12], P_A[12]);
    black_cell$ level_13A(G_Z[12], P_Z[13], G_Z[13], P_Z[12], G_A[13], P_A[13]);
    black_cell$ level_14A(G_Z[13], P_Z[14], G_Z[14], P_Z[13], G_A[14], P_A[14]);
    black_cell$ level_15A(G_Z[14], P_Z[15], G_Z[15], P_Z[14], G_A[15], P_A[15]);
    black_cell$ level_16A(G_Z[15], P_Z[16], G_Z[16], P_Z[15], G_A[16], P_A[16]);
    black_cell$ level_17A(G_Z[16], P_Z[17], G_Z[17], P_Z[16], G_A[17], P_A[17]);
    black_cell$ level_18A(G_Z[17], P_Z[18], G_Z[18], P_Z[17], G_A[18], P_A[18]);
    black_cell$ level_19A(G_Z[18], P_Z[19], G_Z[19], P_Z[18], G_A[19], P_A[19]);
    black_cell$ level_20A(G_Z[19], P_Z[20], G_Z[20], P_Z[19], G_A[20], P_A[20]);
    black_cell$ level_21A(G_Z[20], P_Z[21], G_Z[21], P_Z[20], G_A[21], P_A[21]);
    black_cell$ level_22A(G_Z[21], P_Z[22], G_Z[22], P_Z[21], G_A[22], P_A[22]);
    black_cell$ level_23A(G_Z[22], P_Z[23], G_Z[23], P_Z[22], G_A[23], P_A[23]);
    black_cell$ level_24A(G_Z[23], P_Z[24], G_Z[24], P_Z[23], G_A[24], P_A[24]);
    black_cell$ level_25A(G_Z[24], P_Z[25], G_Z[25], P_Z[24], G_A[25], P_A[25]);
    black_cell$ level_26A(G_Z[25], P_Z[26], G_Z[26], P_Z[25], G_A[26], P_A[26]);
    black_cell$ level_27A(G_Z[26], P_Z[27], G_Z[27], P_Z[26], G_A[27], P_A[27]);
    black_cell$ level_28A(G_Z[27], P_Z[28], G_Z[28], P_Z[27], G_A[28], P_A[28]);
    black_cell$ level_29A(G_Z[28], P_Z[29], G_Z[29], P_Z[28], G_A[29], P_A[29]);
    black_cell$ level_30A(G_Z[29], P_Z[30], G_Z[30], P_Z[29], G_A[30], P_A[30]);
    black_cell$ level_31A(G_Z[30], P_Z[31], G_Z[31], P_Z[30], G_A[31], P_A[31]);

    // Level 2 
    gray_cell$  level_01B(cin,     P_A[ 1], G_A[ 1],          G_B[ 1]);
    gray_cell$  level_02B(G_A[ 0], P_A[ 2], G_A[ 2],          G_B[ 2]);
    black_cell$ level_03B(G_A[ 1], P_A[ 3], G_A[ 3], P_A[ 1], G_B[ 3], P_B[ 3]);
    black_cell$ level_04B(G_A[ 2], P_A[ 4], G_A[ 4], P_A[ 2], G_B[ 4], P_B[ 4]);
    black_cell$ level_05B(G_A[ 3], P_A[ 5], G_A[ 5], P_A[ 3], G_B[ 5], P_B[ 5]);
    black_cell$ level_06B(G_A[ 4], P_A[ 6], G_A[ 6], P_A[ 4], G_B[ 6], P_B[ 6]);
    black_cell$ level_07B(G_A[ 5], P_A[ 7], G_A[ 7], P_A[ 5], G_B[ 7], P_B[ 7]);
    black_cell$ level_08B(G_A[ 6], P_A[ 8], G_A[ 8], P_A[ 6], G_B[ 8], P_B[ 8]);
    black_cell$ level_09B(G_A[ 7], P_A[ 9], G_A[ 9], P_A[ 7], G_B[ 9], P_B[ 9]);
    black_cell$ level_10B(G_A[ 8], P_A[10], G_A[10], P_A[ 8], G_B[10], P_B[10]);
    black_cell$ level_11B(G_A[ 9], P_A[11], G_A[11], P_A[ 9], G_B[11], P_B[11]);
    black_cell$ level_12B(G_A[10], P_A[12], G_A[12], P_A[10], G_B[12], P_B[12]);
    black_cell$ level_13B(G_A[11], P_A[13], G_A[13], P_A[11], G_B[13], P_B[13]);
    black_cell$ level_14B(G_A[12], P_A[14], G_A[14], P_A[12], G_B[14], P_B[14]);
    black_cell$ level_15B(G_A[13], P_A[15], G_A[15], P_A[13], G_B[15], P_B[15]);
    black_cell$ level_16B(G_A[14], P_A[16], G_A[16], P_A[14], G_B[16], P_B[16]);
    black_cell$ level_17B(G_A[15], P_A[17], G_A[17], P_A[15], G_B[17], P_B[17]);
    black_cell$ level_18B(G_A[16], P_A[18], G_A[18], P_A[16], G_B[18], P_B[18]);
    black_cell$ level_19B(G_A[17], P_A[19], G_A[19], P_A[17], G_B[19], P_B[19]);
    black_cell$ level_20B(G_A[18], P_A[20], G_A[20], P_A[18], G_B[20], P_B[20]);
    black_cell$ level_21B(G_A[19], P_A[21], G_A[21], P_A[19], G_B[21], P_B[21]);
    black_cell$ level_22B(G_A[20], P_A[22], G_A[22], P_A[20], G_B[22], P_B[22]);
    black_cell$ level_23B(G_A[21], P_A[23], G_A[23], P_A[21], G_B[23], P_B[23]);
    black_cell$ level_24B(G_A[22], P_A[24], G_A[24], P_A[22], G_B[24], P_B[24]);
    black_cell$ level_25B(G_A[23], P_A[25], G_A[25], P_A[23], G_B[25], P_B[25]);
    black_cell$ level_26B(G_A[24], P_A[26], G_A[26], P_A[24], G_B[26], P_B[26]);
    black_cell$ level_27B(G_A[25], P_A[27], G_A[27], P_A[25], G_B[27], P_B[27]);
    black_cell$ level_28B(G_A[26], P_A[28], G_A[28], P_A[26], G_B[28], P_B[28]);
    black_cell$ level_29B(G_A[27], P_A[29], G_A[29], P_A[27], G_B[29], P_B[29]);
    black_cell$ level_30B(G_A[28], P_A[30], G_A[30], P_A[28], G_B[30], P_B[30]);
    black_cell$ level_31B(G_A[29], P_A[31], G_A[31], P_A[29], G_B[31], P_B[31]);

    // Level 3
    gray_cell$  level_03C(cin,     P_B[ 3], G_B[ 3],          G_C[ 3]);
    gray_cell$  level_04C(G_A[ 0], P_B[ 4], G_B[ 4],          G_C[ 4]);
    gray_cell$  level_05C(G_B[ 1], P_B[ 5], G_B[ 5],          G_C[ 5]);
    gray_cell$  level_06C(G_B[ 2], P_B[ 6], G_B[ 6],          G_C[ 6]);
    black_cell$ level_07C(G_B[ 3], P_B[ 7], G_B[ 7], P_B[ 3], G_C[ 7], P_C[ 7]);
    black_cell$ level_08C(G_B[ 4], P_B[ 8], G_B[ 8], P_B[ 4], G_C[ 8], P_C[ 8]);
    black_cell$ level_09C(G_B[ 5], P_B[ 9], G_B[ 9], P_B[ 5], G_C[ 9], P_C[ 9]);
    black_cell$ level_10C(G_B[ 6], P_B[10], G_B[10], P_B[ 6], G_C[10], P_C[10]);
    black_cell$ level_11C(G_B[ 7], P_B[11], G_B[11], P_B[ 7], G_C[11], P_C[11]);
    black_cell$ level_12C(G_B[ 8], P_B[12], G_B[12], P_B[ 8], G_C[12], P_C[12]);
    black_cell$ level_13C(G_B[ 9], P_B[13], G_B[13], P_B[ 9], G_C[13], P_C[13]);
    black_cell$ level_14C(G_B[10], P_B[14], G_B[14], P_B[10], G_C[14], P_C[14]);
    black_cell$ level_15C(G_B[11], P_B[15], G_B[15], P_B[11], G_C[15], P_C[15]);
    black_cell$ level_16C(G_B[12], P_B[16], G_B[16], P_B[12], G_C[16], P_C[16]);
    black_cell$ level_17C(G_B[13], P_B[17], G_B[17], P_B[13], G_C[17], P_C[17]);
    black_cell$ level_18C(G_B[14], P_B[18], G_B[18], P_B[14], G_C[18], P_C[18]);
    black_cell$ level_19C(G_B[15], P_B[19], G_B[19], P_B[15], G_C[19], P_C[19]);
    black_cell$ level_20C(G_B[16], P_B[20], G_B[20], P_B[16], G_C[20], P_C[20]);
    black_cell$ level_21C(G_B[17], P_B[21], G_B[21], P_B[17], G_C[21], P_C[21]);
    black_cell$ level_22C(G_B[18], P_B[22], G_B[22], P_B[18], G_C[22], P_C[22]);
    black_cell$ level_23C(G_B[19], P_B[23], G_B[23], P_B[19], G_C[23], P_C[23]);
    black_cell$ level_24C(G_B[20], P_B[24], G_B[24], P_B[20], G_C[24], P_C[24]);
    black_cell$ level_25C(G_B[21], P_B[25], G_B[25], P_B[21], G_C[25], P_C[25]);
    black_cell$ level_26C(G_B[22], P_B[26], G_B[26], P_B[22], G_C[26], P_C[26]);
    black_cell$ level_27C(G_B[23], P_B[27], G_B[27], P_B[23], G_C[27], P_C[27]);
    black_cell$ level_28C(G_B[24], P_B[28], G_B[28], P_B[24], G_C[28], P_C[28]);
    black_cell$ level_29C(G_B[25], P_B[29], G_B[29], P_B[25], G_C[29], P_C[29]);
    black_cell$ level_30C(G_B[26], P_B[30], G_B[30], P_B[26], G_C[30], P_C[30]);
    black_cell$ level_31C(G_B[27], P_B[31], G_B[31], P_B[27], G_C[31], P_C[31]);

    // Level 4
    gray_cell$  level_07D(cin,     P_C[ 7], G_C[ 7],          G_D[ 7]);
    gray_cell$  level_08D(G_A[ 0], P_C[ 8], G_C[ 8],          G_D[ 8]);
    gray_cell$  level_09D(G_B[ 1], P_C[ 9], G_C[ 9],          G_D[ 9]);
    gray_cell$  level_10D(G_B[ 2], P_C[10], G_C[10],          G_D[10]);
    gray_cell$  level_11D(G_C[ 3], P_C[11], G_C[11],          G_D[11]);
    gray_cell$  level_12D(G_C[ 4], P_C[12], G_C[12],          G_D[12]);
    gray_cell$  level_13D(G_C[ 5], P_C[13], G_C[13],          G_D[13]);
    gray_cell$  level_14D(G_C[ 6], P_C[14], G_C[14],          G_D[14]);
    black_cell$ level_15D(G_C[ 7], P_C[15], G_C[15], P_C[ 7], G_D[15], P_D[15]);
    black_cell$ level_16D(G_C[ 8], P_C[16], G_C[16], P_C[ 8], G_D[16], P_D[16]);
    black_cell$ level_17D(G_C[ 9], P_C[17], G_C[17], P_C[ 9], G_D[17], P_D[17]);
    black_cell$ level_18D(G_C[10], P_C[18], G_C[18], P_C[10], G_D[18], P_D[18]);
    black_cell$ level_19D(G_C[11], P_C[19], G_C[19], P_C[11], G_D[19], P_D[19]);
    black_cell$ level_20D(G_C[12], P_C[20], G_C[20], P_C[12], G_D[20], P_D[20]);
    black_cell$ level_21D(G_C[13], P_C[21], G_C[21], P_C[13], G_D[21], P_D[21]);
    black_cell$ level_22D(G_C[14], P_C[22], G_C[22], P_C[14], G_D[22], P_D[22]);
    black_cell$ level_23D(G_C[15], P_C[23], G_C[23], P_C[15], G_D[23], P_D[23]);
    black_cell$ level_24D(G_C[16], P_C[24], G_C[24], P_C[16], G_D[24], P_D[24]);
    black_cell$ level_25D(G_C[17], P_C[25], G_C[25], P_C[17], G_D[25], P_D[25]);
    black_cell$ level_26D(G_C[18], P_C[26], G_C[26], P_C[18], G_D[26], P_D[26]);
    black_cell$ level_27D(G_C[19], P_C[27], G_C[27], P_C[19], G_D[27], P_D[27]);
    black_cell$ level_28D(G_C[20], P_C[28], G_C[28], P_C[20], G_D[28], P_D[28]);
    black_cell$ level_29D(G_C[21], P_C[29], G_C[29], P_C[21], G_D[29], P_D[29]);
    black_cell$ level_30D(G_C[22], P_C[30], G_C[30], P_C[22], G_D[30], P_D[30]);
    black_cell$ level_31D(G_C[23], P_C[31], G_C[31], P_C[23], G_D[31], P_D[31]);

    // Level 5
    gray_cell$  level_15E(cin,     P_D[15], G_D[15],          G_E[15]);
    gray_cell$  level_16E(G_A[ 0], P_D[16], G_D[16],          G_E[16]);
    gray_cell$  level_17E(G_B[ 1], P_D[17], G_D[17],          G_E[17]);
    gray_cell$  level_18E(G_B[ 2], P_D[18], G_D[18],          G_E[18]);
    gray_cell$  level_19E(G_C[ 3], P_D[19], G_D[19],          G_E[19]);
    gray_cell$  level_20E(G_C[ 4], P_D[20], G_D[20],          G_E[20]);
    gray_cell$  level_21E(G_C[ 5], P_D[21], G_D[21],          G_E[21]);
    gray_cell$  level_22E(G_C[ 6], P_D[22], G_D[22],          G_E[22]);
    gray_cell$  level_23E(G_D[ 7], P_D[23], G_D[23],          G_E[23]);
    gray_cell$  level_24E(G_D[ 8], P_D[24], G_D[24],          G_E[24]);
    gray_cell$  level_25E(G_D[ 9], P_D[25], G_D[25],          G_E[25]);
    gray_cell$  level_26E(G_D[10], P_D[26], G_D[26],          G_E[26]);
    gray_cell$  level_27E(G_D[11], P_D[27], G_D[27],          G_E[27]);
    gray_cell$  level_28E(G_D[12], P_D[28], G_D[28],          G_E[28]);
    gray_cell$  level_29E(G_D[13], P_D[29], G_D[29],          G_E[29]);
    gray_cell$  level_30E(G_D[14], P_D[30], G_D[30],          G_E[30]);
    black_cell$ level_31E(G_D[15], P_D[31], G_D[31], P_D[15], G_E[31], P_E[31]);

    // Level 6
    gray_cell$  level_31F(cin,     P_E[31], G_E[31],          cout);

    // XOR with AND
    and_xor$ level_Z00(x[ 0], y[ 0], P_Z[ 0], G_Z[ 0]);
    and_xor$ level_Z01(x[ 1], y[ 1], P_Z[ 1], G_Z[ 1]);
    and_xor$ level_Z02(x[ 2], y[ 2], P_Z[ 2], G_Z[ 2]);
    and_xor$ level_Z03(x[ 3], y[ 3], P_Z[ 3], G_Z[ 3]);
    and_xor$ level_Z04(x[ 4], y[ 4], P_Z[ 4], G_Z[ 4]);
    and_xor$ level_Z05(x[ 5], y[ 5], P_Z[ 5], G_Z[ 5]);
    and_xor$ level_Z06(x[ 6], y[ 6], P_Z[ 6], G_Z[ 6]);
    and_xor$ level_Z07(x[ 7], y[ 7], P_Z[ 7], G_Z[ 7]);
    and_xor$ level_Z08(x[ 8], y[ 8], P_Z[ 8], G_Z[ 8]);
    and_xor$ level_Z09(x[ 9], y[ 9], P_Z[ 9], G_Z[ 9]);
    and_xor$ level_Z10(x[10], y[10], P_Z[10], G_Z[10]);
    and_xor$ level_Z11(x[11], y[11], P_Z[11], G_Z[11]);
    and_xor$ level_Z12(x[12], y[12], P_Z[12], G_Z[12]);
    and_xor$ level_Z13(x[13], y[13], P_Z[13], G_Z[13]);
    and_xor$ level_Z14(x[14], y[14], P_Z[14], G_Z[14]);
    and_xor$ level_Z15(x[15], y[15], P_Z[15], G_Z[15]);
    and_xor$ level_Z16(x[16], y[16], P_Z[16], G_Z[16]);
    and_xor$ level_Z17(x[17], y[17], P_Z[17], G_Z[17]);
    and_xor$ level_Z18(x[18], y[18], P_Z[18], G_Z[18]);
    and_xor$ level_Z19(x[19], y[19], P_Z[19], G_Z[19]);
    and_xor$ level_Z20(x[20], y[20], P_Z[20], G_Z[20]);
    and_xor$ level_Z21(x[21], y[21], P_Z[21], G_Z[21]);
    and_xor$ level_Z22(x[22], y[22], P_Z[22], G_Z[22]);
    and_xor$ level_Z23(x[23], y[23], P_Z[23], G_Z[23]);
    and_xor$ level_Z24(x[24], y[24], P_Z[24], G_Z[24]);
    and_xor$ level_Z25(x[25], y[25], P_Z[25], G_Z[25]);
    and_xor$ level_Z26(x[26], y[26], P_Z[26], G_Z[26]);
    and_xor$ level_Z27(x[27], y[27], P_Z[27], G_Z[27]);
    and_xor$ level_Z28(x[28], y[28], P_Z[28], G_Z[28]);
    and_xor$ level_Z29(x[29], y[29], P_Z[29], G_Z[29]);
    and_xor$ level_Z30(x[30], y[30], P_Z[30], G_Z[30]);
    and_xor$ level_Z31(x[31], y[31], P_Z[31], G_Z[31]);

    // Outputs
    xor2$ sum00(sum[ 0], cin,     P_Z[ 0]);
    xor2$ sum01(sum[ 1], G_A[ 0], P_Z[ 1]);
    xor2$ sum02(sum[ 2], G_B[ 1], P_Z[ 2]);
    xor2$ sum03(sum[ 3], G_B[ 2], P_Z[ 3]);
    xor2$ sum04(sum[ 4], G_C[ 3], P_Z[ 4]);
    xor2$ sum05(sum[ 5], G_C[ 4], P_Z[ 5]);
    xor2$ sum06(sum[ 6], G_C[ 5], P_Z[ 6]);
    xor2$ sum07(sum[ 7], G_C[ 6], P_Z[ 7]);
    xor2$ sum08(sum[ 8], G_D[ 7], P_Z[ 8]);
    xor2$ sum09(sum[ 9], G_D[ 8], P_Z[ 9]);
    xor2$ sum10(sum[10], G_D[ 9], P_Z[10]);
    xor2$ sum11(sum[11], G_D[10], P_Z[11]);
    xor2$ sum12(sum[12], G_D[11], P_Z[12]);
    xor2$ sum13(sum[13], G_D[12], P_Z[13]);
    xor2$ sum14(sum[14], G_D[13], P_Z[14]);
    xor2$ sum15(sum[15], G_D[14], P_Z[15]);
    xor2$ sum16(sum[16], G_E[15], P_Z[16]);
    xor2$ sum17(sum[17], G_E[16], P_Z[17]);
    xor2$ sum18(sum[18], G_E[17], P_Z[18]);
    xor2$ sum19(sum[19], G_E[18], P_Z[19]);
    xor2$ sum20(sum[20], G_E[19], P_Z[20]);
    xor2$ sum21(sum[21], G_E[20], P_Z[21]);
    xor2$ sum22(sum[22], G_E[21], P_Z[22]);
    xor2$ sum23(sum[23], G_E[22], P_Z[23]);
    xor2$ sum24(sum[24], G_E[23], P_Z[24]);
    xor2$ sum25(sum[25], G_E[24], P_Z[25]);
    xor2$ sum26(sum[26], G_E[25], P_Z[26]);
    xor2$ sum27(sum[27], G_E[26], P_Z[27]);
    xor2$ sum28(sum[28], G_E[27], P_Z[28]);
    xor2$ sum29(sum[29], G_E[28], P_Z[29]);
    xor2$ sum30(sum[30], G_E[29], P_Z[30]);
    xor2$ sum31(sum[31], G_E[30], P_Z[31]);

endmodule

// exhaustive checking of all 256*256*2 possible cases
// testbench for the 8-bit unsigned Kogge-Stone adder
module KS32ADD_TB;

    reg     [31:0]  a, b;           // 32-bit operands
    wire    [31:0]  s;              // 32-bit sum output
    wire            c8;             // carry output
    reg     [32:0]  check;          // 33-bit value used to check correctess
    integer         i, j;           // loop variables
    integer         num_correct;    // counter to keep track of the number correct
    integer         num_wrong;      // counter to keep track of the number wrong

    // instantiate the 8-bit Kogge-Stone adder
    KSADD32 ks1(s, c8, a, b);

    // exhaustive checking
    initial begin
        // initialize the counter variables
        num_correct = 0; num_wrong = 0;

        // loop through all possible cases and record the results
        for (i = 0; i < 100; i = i + 1) begin
            a = {$random} % 4294967296;
            for (j = 0; j < 100; j = j + 1) begin
                b = {$random} % 4294967296;
              
                // compute and check the product
                check = a + b;
                #10 if ({c8, s} == check) num_correct = num_correct + 1;
                else                      num_wrong = num_wrong + 1;
                //following line is for debugging
                //$display($time, "  %d + %d = %d (%d)", a, b, {c8, s}, check);
            end
        end
     
        // print the final counter values
        $display("num_correct = %d, num_wrong = %d", num_correct, num_wrong);
    end

endmodule
