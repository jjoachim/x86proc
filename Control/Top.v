`timescale 1ns/1ps

`include "Parameters.v"
`include "TestFramework.v"

`include "Control/Control.v"

module TOP;
  `TEST_GLOBAL_INIT;
  reg nstall, nreset;
  reg [2:0] src [5:0];
  reg [5:0] srcV;
  reg [2:0] dst [1:0];
  reg [1:0] dstV;
  wire bubble;

  wire [17:0] srcCat;
  assign srcCat = {src[5], src[4], src[3], src[2], src[1], src[0]};

  wire [5:0] dstCat;
  assign dstCat = {dst[1], dst[0]};

  //object addresses are 3 bits, 6 source ports, 2 dest ports
  BasicDepTrack #(3, 6, 2) bubbler
    (bubble, srcCat, srcV, dstCat, dstV, nstall, `TEST_CLK, nreset);

  `TEST_INIT_UCOMB(bubble, 1);

  initial begin
    nreset = 0;
    src[5] = 0;
    src[4] = 0;
    src[3] = 0;
    src[2] = 0;
    src[1] = 0;
    src[0] = 0;
    srcV = 0;
    dst[1] = 0;
    dst[0] = 0;
    dstV = 0;
    nstall = 1;
    `TEST_TOGGLE_CLK;
    nreset = 1;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;

    `TEST_FORMAT(`TEST_FMT_HEX);
    `TEST_DISPLAY("\n>> BEGIN TESTS <<\n");

    `TEST_SET(
      src[0] = 0;
      src[1] = 1;
      srcV = 3;
      dst[0] = 2;
      dstV = 1;
      nstall = 1;
      `TEST_EQ(bubble, 1'b0);
     );

    `TEST_SET(
      src[0] = 0;
      src[1] = 1;
      srcV = 3;
      dst[0] = 2;
      dstV = 1;
      nstall = 1;
      `TEST_EQ(bubble, 1'b0);
     );

    `TEST_SET(
      src[0] = 0;
      src[1] = 1;
      srcV = 3;
      dst[0] = 3;
      dst[1] = 3;
      dstV = 3;
      `TEST_EQ(bubble, 1'b0);
     );

    `TEST_SET(
      src[0] = 0;
      src[1] = 1;
      srcV = 3;
      dst[0] = 4;
      dst[1] = 5;
      dstV = 3;
      `TEST_EQ(bubble, 1'b0);
     );

    `TEST_SET(
      src[0] = 0;
      src[1] = 1;
      srcV = 3;
      dst[0] = 6;
      dst[1] = 7;
      dstV = 3;
      `TEST_EQ(bubble, 1'b0);
     );

    `TEST_SET(
      src[0] = 3;
      src[1] = 2;
      srcV = 3;
      dst[1] = 0;
      dstV = 2;
      `TEST_EQ(bubble, 1'b1);
     );
    
    `TEST_SET(
      src[0] = 3;
      src[1] = 4;
      src[2] = 5;
      src[3] = 6;
      src[4] = 7;
      srcV = 31;
      dst[0] = 0;
      dstV = 1;
      `TEST_EQ(bubble, 1'b1);
     );

    `TEST_SET(
      src[0] = 3;
      src[1] = 4;
      src[2] = 5;
      src[3] = 6;
      src[4] = 7;
      srcV = 31;
      dst[0] = 0;
      dstV = 1;
      `TEST_EQ(bubble, 1'b1);
     );

    `TEST_SET(
      src[0] = 3;
      src[1] = 4;
      src[2] = 5;
      src[3] = 6;
      src[4] = 7;
      srcV = 31;
      dst[0] = 0;
      dstV = 1;
      nstall = 0;
      `TEST_EQ(bubble, 1'b1);
     );

    `TEST_SET(
      src[0] = 3;
      src[1] = 4;
      src[2] = 5;
      src[3] = 6;
      src[4] = 7;
      srcV = 31;
      dst[0] = 0;
      dstV = 1;
      nstall = 1;
      `TEST_EQ(bubble, 1'b1);
     );

    `TEST_SET(
      src[0] = 3;
      src[1] = 4;
      src[2] = 5;
      src[3] = 6;
      src[4] = 7;
      srcV = 31;
      dst[0] = 0;
      dstV = 1;
      `TEST_EQ(bubble, 1'b0);
     );

    `TEST_END;
    `TEST_DISPLAY("\n>> END TESTS <<\n");

    $finish;
  end

  initial begin
    $vcdplusfile("top.dump.vpd");
    $vcdpluson(0, TOP);
    $vcdpluson(0, src);
    $vcdpluson(0, dst);
  end

endmodule
