`ifndef _CONTROL_V_
`define _CONTROL_V_

`include "Parameters.v"
`include "CommonLogic/CommonLogic.v"

`default_nettype none

module BasicDepTrack (oBubble, 
                      iSrc, iSrcV,
                      iDest, iDestV, 
                      inStall, inFlush, iClk, inReset);

  parameter addrSize = 5;
  parameter srcPorts = 6;
  parameter dstPorts = 2;

  parameter numObjs = 1<<addrSize;

  output oBubble;
  input [srcPorts*addrSize-1:0] iSrc;
  input [srcPorts-1:0] iSrcV;
  input [dstPorts*addrSize-1:0] iDest;
  input [dstPorts-1:0] iDestV;
  input iClk, inReset, inStall, inFlush;

  wire [srcPorts-1:0] srcBusy;
  wire nBubble;

  genvar i, p;
  generate
    for(i=0; i<numObjs; i=i+1) begin : obj
      //Object address
      wire [addrSize-1:0] myAddr;
      assign myAddr = i;

      //Object destination addr match
      wire [dstPorts-1:0] daddrcmp;
      for(p=0; p<dstPorts; p=p+1) begin : wrPort
        Compare #(addrSize+1) cmp_daddr
          (daddrcmp[p], 
           {1'b1, myAddr},
           {iDestV[p], iDest[p*addrSize +: addrSize]}
          );
      end
      wire destValid;
      ReduceOR #(dstPorts) reduce_dvalid (destValid, daddrcmp);

      //Assume a bubble when applicable
      wire destActive; //Don't launch destination during a bubble!
      and2$ launch_dest (destActive, destValid, nBubble);

      //Track Object Progress
      wire oAge;
      PipeRegs AGE //1 reg, reset to 0, invert WrEnable
        (oAge, destActive, inStall, inFlush, iClk, inReset);
      wire oMem;
      PipeRegs MEM //1 reg, reset to 0, invert WrEnable
        (oMem, oAge, inStall, inFlush, iClk, inReset);
      wire oExe;
      PipeRegs EXE //1 reg, reset to 0, invert WrEnable
        (oExe, oMem, inStall, inFlush, iClk, inReset);
      wire oWb;
      PipeRegs WB //1 reg, reset to 0, invert WrEnable
        (oWb, oExe, inStall, inFlush, iClk, inReset);

      //Object busy
      wire busy;
      or4$ or_busy (busy, oAge, oMem, oExe, oWb);

      //Object source addr match
      for(p=0; p<srcPorts; p=p+1) begin : rdPort
        wire nsaddrcmp;
        NCompare #(addrSize) cmp_saddr
          (nsaddrcmp, myAddr, iSrc[p*addrSize +: addrSize]);

        //We only report busy if this source was requested
        wire busyMask;
        and2$ and_busyMask (busyMask, busy, iSrcV[p]);
      
        //Power output port
        tristateL$ tri_objBusy
          (.out(srcBusy[p]), .in(busyMask), .enbar(nsaddrcmp));
      end
    end
  endgenerate

  //Bubble if any source is busy
  ReduceOR #(srcPorts) reduce_bubble (oBubble, srcBusy);
  not1$ inv_bubble (nBubble, oBubble);
endmodule

`default_nettype wire
`endif
