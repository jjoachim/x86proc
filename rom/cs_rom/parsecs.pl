use strict;
 my $linsize=64;
 my $file= "Control_Store.csv";
#my $file=$ARGV[0];
my $new_fileext="control_store.rom";
open(my $in_fh, "<", "$file")
or die "cannot open > output.txt: $!";
open(my $OUTFILE, ">", $new_fileext);
my $start=0;

while (<$in_fh>){
	my $line=$_;
	$line=~s/^\s+//;
	$line=~s/\s+$//;
	chomp($line);
	unless ($start==0){
		$line =~ /(.*?),(.*)/;
		my $bits=$2;
		$bits=~s/[,\s+]//g;
		if ($bits eq ""){
			print $OUTFILE "0" x $linsize,"\n";
		}else{
			print $OUTFILE "$bits","0" x ($linsize-length($bits)),"\n";
		}
	}else{
		$start=1;
	}

}
close ($in_fh);
close ($OUTFILE);
`chmod -R 777 *`;
` perl ../cutrom.pl control_store.rom`;
