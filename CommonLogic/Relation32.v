`ifndef _RELATION32_V_
`define _RELATION32_V_

module Relation32 (equal, nequal, gt, gte, lt, lte, in1, in2);
parameter size=32;
input [size-1:0] in1,in2;
wire [size-1:0] sum;
wire cout;
output equal,gt,gte,lt,lte,nequal;
wire [size-1:0] in2_p;
ase_inv_32 inverter_all (in2_p,in2);
KSAdder32 adder(sum,cout,in1,in2_p,1'b1);
ase_equal eqaul_gate(equal,nequal,in1,in2);
assign lt=sum[size-1];
inv1$ gte_inv(gte,lt);
nor2$ gt_nor(gt,lt,equal);
//the follwoing if on crit path can be optimized by modifying the equal gate to ouput ne as well the use gte with ne 
inv1$ lte_inv(lte,gt);


endmodule

module ase_inv_32 (out,in1);
parameter size=32;
input [size-1:0] in1;
output [size-1:0] out;
	
inv1$ inv [size-1:0] (out,in1);

endmodule

module ase_equal (equal_high,equal_low,in1,in2);
input [31:0]in1,in2;
output equal_high,equal_low;
wire [31:0] eq;
wire [7:0] nand_1;
wire [3:0] nor_2;
genvar i;
generate  
for (i=0;i<32;i=i+1) begin : loop0
	xnor2$ eq_g(eq[i],in1[i],in2[i]);
end 
endgenerate

generate  
for (i=0;i<8;i=i+1) begin : loop1
	nand4$ nand1_g(nand_1[i],eq[4*i],eq[4*i+1],eq[4*i+2],eq[4*i+3]);
end 
endgenerate

generate  
for (i=0;i<4;i=i+1) begin : loop2
	nor2$ nor1_g(nor_2[i],nand_1[2*i],nand_1[2*i+1]);
end 
endgenerate
and4$ and_g(equal_high,nor_2[0],nor_2[1],nor_2[2],nor_2[3]);
nand4$ nand_g_(equal_low,nor_2[0],nor_2[1],nor_2[2],nor_2[3]);

endmodule

`endif
