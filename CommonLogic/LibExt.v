`ifndef _LIBEXT_V_
`define _LIBEXT_V_

`default_nettype none

/*****************************************************************************/
// Static Assert
/*****************************************************************************/
`define STATIC_ASSERT(X, Y)             \
  generate                              \
    if((X) == 0) begin                  \
      STATIC_ASSERT_FAILED Y(1'b0);     \
    end                                 \
  endgenerate                           \
  parameter STATIC_ASSERT_TRUE_``Y = 1

/*****************************************************************************/
// SRAM Initialization
/*****************************************************************************/
`define MEMI_INIT(F, BANKS, BPB, M)                         \
  genvar F``k, F``b;                                        \
  generate                                                  \
    for(F``k=0; F``k<(BANKS); F``k=F``k+1) begin : F``_BANK \
      for(F``b=0; F``b<(BPB); F``b=F``b+1) begin : F``_BPB  \
        initial begin                                       \
          $readmemh(                                        \
            $sformatf("%s_%0d_%0d.mem",`"F`",F``k,F``b),    \
            M``.bank[F``k].block[F``b].mem);                \
        end                                                 \
      end                                                   \
    end                                                     \
  endgenerate                                               \
  genvar F``_MEMI_INIT_DONE

/*****************************************************************************/
// Common Modules (library extensions)
/*****************************************************************************/
module not1$ (oOut, iA);
  input iA;
  output oOut;

  inv1$ the_nand (oOut, iA);
endmodule

module regs$(oData, iData, iWrEn, iClk, inReset);
  parameter width = 1;
  parameter rstVal = 0;
  parameter nwre = 0;

  input [width-1:0] iData;
  input iWrEn, iClk, inReset;
  output [width-1:0] oData;
  wire [width-1:0] oDatab, dffData;

  generate
    //WrEnable mux
    if(nwre==0) begin
      mux2$ mux_wrEn [width-1:0]
        (dffData, oData, iData, iWrEn);
    end else if(nwre == 1) begin //inWrEn
      mux2$ mux_wrEn [width-1:0]
        (dffData, iData, oData, iWrEn);
    end else begin
      assign dffData = iData;
    end

    //Set/Reset dffs
    if(rstVal==0) begin
      dff$ flops [width-1:0] 
        (iClk, dffData, oData, oDatab, inReset, 1'b1);
    end else begin
      dff$ flops [width-1:0] 
        (iClk, dffData, oData, oDatab, 1'b1, inReset);
    end
  endgenerate

endmodule

`default_nettype wire
`endif
