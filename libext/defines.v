`ifndef _PIPE_DEFINES_V
`define _PIPE_DEFINES_V

// GPR indices
`define iAL     3'b000;
`define iCL     3'b001;
`define iDL     3'b010;
`define iBL     3'b011;
`define iAH     3'b100;
`define iCH     3'b101;
`define iDH     3'b110;
`define iBH     3'b111;

`define iAX     3'b000;
`define iCX     3'b001;
`define iDX     3'b010;
`define iBX     3'b011;
`define iSP     3'b100;
`define iBP     3'b101;
`define iSI     3'b110;
`define iDI     3'b111;

`define iEAX    3'b000;
`define iECX    3'b001;
`define iEDX    3'b010;
`define iEBX    3'b011;
`define iESP    3'b100;
`define iEBP    3'b101;
`define iESI    3'b110;
`define iEDI    3'b111;

// MMX register indices
`define iMM0    3'b000;
`define iMM1    3'b001;
`define iMM2    3'b010;
`define iMM3    3'b011;
`define iMM4    3'b100;
`define iMM5    3'b101;
`define iMM6    3'b110;
`define iMM7    3'b111;

// Segment register indices
`define iES     3'b000;
`define iCS     3'b001;
`define iSS     3'b010;
`define iDS     3'b011;
`define iFS     3'b100;
`define iGS     3'b101;

// EFLAGS register bits
`define iID     21
`define iVIP    20
`define iVIF    19
`define iAC     18
`define iVM     17
`define iRF     16
`define iNT     14
`define iIOPL   13:12
`define iOF     11      // Overflow flag
`define iDF     10      // Direction flag
`define iIF     9
`define iTF     8
`define iSF     7       // Sign Flag
`define iZF     6       // Zero flag
`define iAF     4       // Auxillary flag
`define iPF     2       // Protection flag
`define iCF     0       // Carry flag

// Stat indices
`define STATSZ          32
`define iRSVD           31:19
`define iEXCMODE        18
`define iEXC            17
`define iINT            16
`define iEXCV           15:8
`define iHEXCV          7:0

// Exception vectors
`define INTEXCV         8'h10         // Interrupt exception vector
`define GPREXCV         8'h68         // General protection exception vector
`define PFEXCV          8'h70         // Page fault exception vector

// Control-Store indices
`define CTRLSZ          69
`define CSSZ            59

`define iOPSZOVRPRE     68      // Operand size override prefix present
`define iSEGOVRPRE      67      // Segment override prefix present
`define iREPEPRE        66      // REPE prefix present
`define iSEGOVR         65:63   // Override segment
`define iRPLUS          62:60   // Lower three bits of opcode
`define iUSESIB         59      // Using SIB

`define iTRAPOP         58      // TRAP operation
`define iROP            57      // ROP
`define iIROP           56:51   // Index of ROP
`define iOPSZ           50:49   // Default operand size
`define iOPSZOVR        48:47   // Overridden operand size
`define iOPSZOVRS       46      // Forced, special operand size override
`define iOPSZS          45:44   // Special operand size
`define iOPERANDS       43:41   // Operand types
`define iDIR            40      // Direction
`define iREGMUX         39      // REG bits or R+
`define iRSRCMUX        38:36   // 000=REGMUX 001=iEAX 010=iESP 011=iECX
                                // 100=iESI   101=iEDI 110=XXX  111=XXX
`define iUSEREG         35      // Using register value
`define iUSEMODRM       34      // Using ModR/M
`define iUSEIMM         33      // Using immediate
`define iSEXTIMM        32      // Using sign-extended immediate
`define iSEGOVRDIS      31      // Segment override disabled
`define iSEGMUX         30:28   // Opcode-selected segment
`define iMOVOP          27      // MOV operation
`define iRSTACK         26      // Read from stack
`define iWSTACK         25      // Write to stack
`define iCMPSMUX        24      // CMPS operation
`define iCMPSR          23      // 0=ESI 1=EDI
`define iALTMUX0        22:21   // 00=Immediate 01=SegR 10=EIP 11=BrAddr
`define iALTMUX1        20      // 0=MEMR.AltData 1=TEMP
`define iLDTEMP         19      // Load TEMP register
`define iALUAMUX        18      // 0=Register 1=AltData
`define iALUBMUX        17      // 0=ModR/M   1=AltData
`define iALUMUX         16:13   // ALU operation
`define iFLAGMUX        12:10   // Flag operation
`define iALUOUTMUX      9:8     // 00=ALU 01=EFLAGS 10=CS 11=EIP
`define iBROP           7       // Branch instruction
`define iBRCOND         6:5     // 00=Unconditional  10=~ZF
                                // 10=~CF&~ZF        11=Unused
`define iBRTYPE         4:3     // 00=Short/Relative 01=Near/Realtive
                                // 10=Near/Absolute  11=Far/Absolute
`define iDISABLECF0     2       // Disable if ~CF
`define iDISABLEZF0     1       // Disable if ~ZF
`define iDISABLEZF1     0       // Disable if ZF

`endif
