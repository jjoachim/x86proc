`ifndef _EXE_VH
`define _EXE_VH

`include "defines.v"
`include "libSJQ.v"
`include "EXE/ALU.v"
`include "EXE/PCGEN.v"
`include "EXE/FLAGSGEN.v"
`include "CommonLogic/PipeRegs.v"

// Execute stage
module EXE(iClk,    iNSysRst,     
           inFlush, inStall,
           oFlush,  oStall,

           oStat,   oControlStore,
           oRegW,   oSegW, oDstR,
           oValE,   oFLAGSN,
           oAddr,   oUA,   oAddrP16, oPCD,
           oMemW,   oWrSz,
           oEIPN,   oBRTK,
           oCS,     oEIP,  oEIPPLUS,

           iStat,   iControlStore,
           iRegW,   iSegW, iDstR,
           iGPR,    iRM,
           iREG,
           iAddr,   iUA,   iAddrP16, iPCD,
           iMemW,
           iAltData,
           iEFLAGS, iArchEIP,
           iCS,     iEIP,  iEIPPLUS);
    
    // I/O
    // System controls
    input           iClk;
    input           iNSysRst;
    input           inFlush;
    input           inStall;
    output          oFlush;
    output          oStall;

    // MEM -> EXE
    input   [31:0]  iStat;
    input   [68:0]  iControlStore;
    input           iRegW;
    input           iSegW;
    input   [ 2:0]  iDstR;
    input   [63:0]  iGPR;
    input   [63:0]  iRM;
    input   [ 2:0]  iREG;
    input   [31:0]  iAddr;
    input           iUA;
    input   [31:0]  iAddrP16;
    input   [ 1:0]  iPCD;
    input           iMemW;
    input   [63:0]  iAltData;
    input   [15:0]  iCS;
    input   [31:0]  iEIP;
    input   [31:0]  iEIPPLUS;

    // WB -> EXE
    input   [31:0]  iEFLAGS;
	input 	[31:0]	iArchEIP;

    // EXE -> WB
    output  [31:0]  oStat;
    output  [68:0]  oControlStore;
    output          oRegW;
    output          oSegW;
    output  [ 2:0]  oDstR;
    output  [63:0]  oValE;
    output  [31:0]  oFLAGSN;
    output  [31:0]  oAddr;
    output          oUA;
    output  [31:0]  oAddrP16;
    output  [ 1:0]  oPCD;
    output          oMemW;
    output  [ 1:0]  oWrSz;
    output  [31:0]  oEIPN;
    output          oBRTK;
    output  [15:0]  oCS;
    output  [31:0]  oEIP;
    output  [31:0]  oEIPPLUS;

    // Piplene stage inputs
    wire    [31:0]  wStat;
    wire    [68:0]  wControlStore;
    wire            wRegW;
    wire            wSegW;
    wire    [ 2:0]  wDstR;
    wire    [63:0]  wValE;
    wire    [31:0]  wFLAGSN;
    wire    [31:0]  wAddr;
    wire            wUA;
    wire    [31:0]  wAddrP16;
    wire    [ 1:0]  wPCD;
    wire            wMemW;
    wire    [ 1:0]  wWrSz;
    wire    [31:0]  wEIPN;
    wire            wBRTK;
    wire    [15:0]  wCS;
    wire    [31:0]  wEIP;
    wire    [31:0]  wEIPPLUS;

    // Buffer stat, control store, and flags
    wire    [31:0]  STATBUF;
    wire    [68:0]  CSBUF;
    wire    [31:0]  FLAGSBUF;

    // Split control-store signals
    wire            OPSZOVRPRE; // Operand size override prefix present
    wire            REPEPRE;    // REPE prefix present
    wire            TRAPOP;     // TRAP operation
    wire    [ 1:0]  OPSZ;       // Default operand size
    wire    [ 1:0]  OPSZOVR;    // Overridden operand size
    wire            OPSZOVRS;   // Forced, special operand size override
    wire    [ 1:0]  OPSZS;      // Special operand size
    wire            ALUAMUX;    // 0=Register 1=AltData
    wire            ALUBMUX;    // 0=ModR/M   1=AltData
    wire    [ 3:0]  ALUMUX;     // ALU operation
    wire    [ 2:0]  FLAGMUX;    // Flag operation
    wire    [ 1:0]  ALUOUTMUX;  // 00=ALU 01=EFLAGS 10=CS 11=EIP
    wire            BROP;       // Branch instruction
    wire    [ 1:0]  BRCOND;     // 00=Unconditional  10=~ZF
                                // 10=~CF&~ZF        11=Unused
    wire    [ 1:0]  BRTYPE;     // 00=Short/Relative 01=Near/Realtive
                                // 10=Near/Absolute  11=Far/Absolute
    wire            DISABLECF0; // Disable if ~CF                                
    wire            DISABLEZF0; // Disable if ~ZF
    wire            DISABLEZF1; // Disable if ZF

    // Operand Sizes
    wire    [ 1:0]  OPSZA;
    wire    [ 1:0]  OPSZB;

    // Forwarding logic
    wire    [63:0]  FGPR;
    wire    [63:0]  FRM;
    wire    [63:0]  FALT;

    // ALU I/O
    wire    [63:0]  ALUAMUXOUT;
    wire    [63:0]  ALUBMUXOUT;
    wire    [63:0]  ALUOUT;
    wire    [31:0]  SELEIP;
    wire    [ 7:0]  ALUOUTFLAGS;

    // PCGEN I/O
	wire 			BRCOND11;
	wire 	[63:0] 	PCGENRM;
    wire    [15:0]  CSN;
    wire            CSOverwrite;
    wire    [15:0]  CSSEL;

    // ALUOUT Multiplexer
    wire    [ 1:0]  ALUOUTMUXSEL;

    // Disables
    wire            CF0;
    wire            DISCF0;
    wire            ZF0;
    wire            DISZF0;
    wire            DISZF1;
    wire            DISABLE;

    // Control signal buffering
    bufferH16$ stat_buf [31:0] (STATBUF,  iStat);
    bufferH16$ cs_buf   [68:0] (CSBUF,    iControlStore);
    bufferH16$ flag_buf [31:0] (FLAGSBUF, iEFLAGS);

    // Split Control-Store
    assign OPSZOVRPRE = CSBUF[`iOPSZOVRPRE];
    assign REPEPRE    = CSBUF[`iREPEPRE];
    assign TRAPOP     = CSBUF[`iTRAPOP];
    assign OPSZ       = CSBUF[`iOPSZ];
    assign OPSZOVR    = CSBUF[`iOPSZOVR];
    assign OPSZOVRS   = CSBUF[`iOPSZOVRS];
    assign OPSZS      = CSBUF[`iOPSZS];
    assign ALUAMUX    = CSBUF[`iALUAMUX];
    assign ALUBMUX    = CSBUF[`iALUBMUX];
    assign ALUMUX     = CSBUF[`iALUMUX];
    assign FLAGMUX    = CSBUF[`iFLAGMUX];
    assign ALUOUTMUX  = CSBUF[`iALUOUTMUX];
    assign BROP       = CSBUF[`iBROP];
    assign BRCOND     = CSBUF[`iBRCOND];
    assign BRTYPE     = CSBUF[`iBRTYPE];
    assign DISABLECF0 = CSBUF[`iDISABLECF0];
    assign DISABLEZF0 = CSBUF[`iDISABLEZF0];
    assign DISABLEZF1 = CSBUF[`iDISABLEZF1];

    // Operand sizes
    mux2_2$ opsza_mux(OPSZA, OPSZ,  OPSZOVR, OPSZOVRPRE);
    mux2_2$ opszb_mux(OPSZB, OPSZA, OPSZS,   OPSZOVRS);

    // Forwarding logic
    assign FGPR   = iGPR;
    assign FRM    = iRM;
    assign FALT   = iAltData;

    // ALU Muxes
    mux2_64$ alua_mux(ALUAMUXOUT, FGPR, FALT, ALUAMUX);
    mux2_64$ alub_mux(ALUBMUXOUT, FRM,  FALT, ALUBMUX);

    // ALU
    ALU alu(ALUOUT,     ALUOUTFLAGS,
            ALUAMUXOUT, ALUBMUXOUT,  OPSZA, FLAGSBUF,
            ALUMUX,     iREG,        OPSZB);

    // FLAGSGEN
    FLAGSGEN flagsgen(wFLAGSN, 
                      ALUOUT, ALUOUTFLAGS, FLAGSBUF,
                      FLAGMUX);

	// PCGEN RM input
	and2$ brcond11_and(BRCOND11, BRCOND[1], BRCOND[0]);
	mux2_64$ pcgen_rm_mux(PCGENRM, FRM, FALT, BRCOND11);

    // PC Generator
    PCGEN pcgen(CSN,     CSOverwrite,
                wEIPN,   wBRTK,
                PCGENRM, FALT, FLAGSBUF,
                iCS,     iEIP, iEIPPLUS,
                iSegW,   iDstR,
                STATBUF, CSBUF);

    // ALUOUTMUX
    mux2_16$ cssel_mux(CSSEL, iCS, CSN, CSOverwrite);
    mux2_2$  aluoutmuxsel_mux(ALUOUTMUXSEL, ALUOUTMUX, 2'b10, CSOverwrite);
    mux2_32$ eip2alu_mux(SELEIP, iEIPPLUS, iArchEIP, STATBUF[`iEXCMODE]);
    mux4_64$ aluoutmux(wValE,
                       ALUOUT,                   {32'h00000000,FLAGSBUF},
                       {48'h000000000000,CSSEL}, {32'h00000000,SELEIP},
                       ALUOUTMUXSEL);

    // Disables
    inv1$ cf0_inv(CF0, FLAGSBUF[`iCF]);
    and2$ discf0_and(DISCF0, CF0, DISABLECF0);
    inv1$ zf0_inv(ZF0, FLAGSBUF[`iZF]);
    and2$ diszf0_and(DISZF0, ZF0, DISABLEZF0);
    and2$ diszf1_and(DISZF1, FLAGSBUF[`iZF], DISABLEZF1);
    or4$  disable_or(DISABLE, DISCF0, DISZF0, DISZF1, STATBUF[`iEXC]);

    // Pass rest of signals through
    assign wStat[31:18]         = iStat[31:18];
	mux2$  statexc_mux(wStat[`iEXC], iStat[`iEXC], 1'b0, iStat[`iEXCMODE]);
	assign wStat[16:0]          = iStat[16:0];
    assign wControlStore[68:13] = iControlStore[68:13];
    mux2$  csflags_mux12(wControlStore[12], iControlStore[12], 1'b0, STATBUF[`iEXC]);
    mux2$  csflags_mux11(wControlStore[11], iControlStore[11], 1'b0, STATBUF[`iEXC]);
    mux2$  csflags_mux10(wControlStore[10], iControlStore[10], 1'b0, STATBUF[`iEXC]);
    assign wControlStore[ 9: 0] = iControlStore[ 9: 0];
    mux2$  regwExc_mux(wRegW, iRegW, 1'b0, DISABLE);
    or2$   segwExc_or(wSegW, iSegW, CSOverwrite);
    mux2_3$ dstr_mux(wDstR, iDstR, 3'b001, CSOverwrite);
    assign wAddr         = iAddr;
    assign wUA           = iUA;
    assign wAddrP16      = iAddrP16;
    assign wPCD          = iPCD;
    mux2$  memwExc_mux(wMemW, iMemW, 1'b0, DISABLE);
    assign wWrSz         = OPSZB;
    assign wCS           = iCS;
    assign wEIP          = iEIP;
    assign wEIPPLUS      = iEIPPLUS;

    assign oFlush = 0;
    assign oStall = 0;

    // Pipeline register
    PipeRegs #(385) exe2wb({oStat,oControlStore,oRegW,oSegW,oDstR,
                            oValE,oFLAGSN,
                            oAddr,oUA,oAddrP16,oPCD,oMemW,oWrSz,
                            oEIPN,oBRTK,oCS,oEIP,oEIPPLUS},

                           {wStat,wControlStore,wRegW,wSegW,wDstR,
                            wValE,wFLAGSN,
                            wAddr,wUA,wAddrP16,wPCD,wMemW,wWrSz,
                            wEIPN,wBRTK,wCS,wEIP,wEIPPLUS},

                            inStall, inFlush,
                            iClk,   iNSysRst);
endmodule

`endif
