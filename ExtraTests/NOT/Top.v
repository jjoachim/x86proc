`timescale 1ns/1ps

`include "Parameters.v"
`include "TestFramework.v"

`include "Processor/Processor.v"

module TOP;
  `STATIC_ASSERT(`BUS_NUM_MASTERS == 5, bus_masters_not_5);

  reg nreset;

  `TEST_GLOBAL_INIT;

  Processor proc (`TEST_CLK, nreset);
  `MEMI_INIT(memfile,`MEM_BANKS,256/(`MEM_BANKS),proc.mm);

  wire [31:0] eip;
  assign eip = proc.pipe.wb.oEIPC; 

  `TEST_INIT_COMB(eip, 32);
  
  wire [31:0] eax, ebx, ecx, edx, 
              esi, edi, ebp, esp;
  assign eax = proc.pipe.rf.regfile.GPR[0];
  assign ecx = proc.pipe.rf.regfile.GPR[1];
  assign edx = proc.pipe.rf.regfile.GPR[2];
  assign ebx = proc.pipe.rf.regfile.GPR[3];
  assign esp = proc.pipe.rf.regfile.GPR[4];
  assign ebp = proc.pipe.rf.regfile.GPR[5];
  assign esi = proc.pipe.rf.regfile.GPR[6];
  assign edi = proc.pipe.rf.regfile.GPR[7];

  `TEST_INIT_COMB(eax, 32);
  `TEST_INIT_COMB(ebx, 32);
  `TEST_INIT_COMB(ecx, 32);
  `TEST_INIT_COMB(edx, 32);
  `TEST_INIT_COMB(esi, 32);
  `TEST_INIT_COMB(edi, 32);
  `TEST_INIT_COMB(ebp, 32);
  `TEST_INIT_COMB(esp, 32);

  wire [15:0] es, cs, ss, ds, fs, gs;
  assign es = proc.pipe.rf.regfile.SEG[0];
  assign cs = proc.pipe.rf.regfile.SEG[1];
  assign ss = proc.pipe.rf.regfile.SEG[2];
  assign ds = proc.pipe.rf.regfile.SEG[3];
  assign fs = proc.pipe.rf.regfile.SEG[4];
  assign gs = proc.pipe.rf.regfile.SEG[5];

  `TEST_INIT_COMB(es, 16);
  `TEST_INIT_COMB(cs, 16);
  `TEST_INIT_COMB(ss, 16);
  `TEST_INIT_COMB(ds, 16);
  `TEST_INIT_COMB(fs, 16);
  `TEST_INIT_COMB(gs, 16);

  wire [63:0] mmx0, mmx1, mmx2, mmx3,
              mmx4, mmx5, mmx6, mmx7;
  assign mmx0 = proc.pipe.rf.regfile.MMX[0];
  assign mmx1 = proc.pipe.rf.regfile.MMX[1];
  assign mmx2 = proc.pipe.rf.regfile.MMX[2];
  assign mmx3 = proc.pipe.rf.regfile.MMX[3];
  assign mmx4 = proc.pipe.rf.regfile.MMX[4];
  assign mmx5 = proc.pipe.rf.regfile.MMX[5];
  assign mmx6 = proc.pipe.rf.regfile.MMX[6];
  assign mmx7 = proc.pipe.rf.regfile.MMX[7];

  `TEST_INIT_SEQ(mmx0, 64);
  `TEST_INIT_SEQ(mmx1, 64);
  `TEST_INIT_SEQ(mmx2, 64);
  `TEST_INIT_SEQ(mmx3, 64);
  `TEST_INIT_SEQ(mmx4, 64);
  `TEST_INIT_SEQ(mmx5, 64);
  `TEST_INIT_SEQ(mmx6, 64);
  `TEST_INIT_SEQ(mmx7, 64);
  
  wire SimEnd, SegW, RegW, MemW, BrTk;
  assign SimEnd = proc.pipe.wb.rsimEnd;
  assign SegW = proc.pipe.wb.rSegW;
  assign RegW = proc.pipe.wb.rRegW;
  assign MemW = proc.pipe.wb.rMemW;
  assign BrTk = proc.pipe.wb.rBRTK;

  wire InstCommit;
  assign InstCommit = SegW | RegW | MemW | BrTk;

  wire [2:0] DstR;
  assign DstR = proc.pipe.wb.rDstR;

  initial begin
    #`CLK_CYCLE; //let opening messages print
    $display("\nAsserting Reset...");
    `TEST_TOGGLE_CLK;
    nreset = 0;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    `TEST_TOGGLE_CLK;
    nreset = 1;
    `TEST_TOGGLE_CLK;
    
    $display("Starting processor at %0d", $time);

    //Initialization
    `TEST_WHEN(RegW==1,
      `TEST_EQ(ebx, 32'h00000200);
    )
    `TEST_WHEN(SegW==1,
      `TEST_EQ(ds, 16'h0200);
    )
    `TEST_WHEN(RegW==1,
      `TEST_EQ(ebx, 32'h00000000);
    )
    `TEST_WHEN(RegW==1, 
      `TEST_EQ(eax, 32'hffffffff);
    )

    //Begin!
    `TEST_WHEN(RegW==1, 
      `TEST_EQ(eax, 32'hffffff00);
    )
    `TEST_WHEN(RegW==1, 
      `TEST_EQ(eax, 32'hffff00ff);
    )
    `TEST_WHEN(RegW==1, 
      `TEST_EQ(eax, 32'h0000ff00);
    )
    `TEST_WHEN(RegW==1, 
      `TEST_EQ(eax, 32'hffffffff);
    )

    `TEST_WHEN(SimEnd==1, )

    `TEST_END;
    `TEST_DISPLAY("\n>> END TESTS <<\n");

    $finish;
  end
  
  initial begin
    $vcdplusfile("top.dump.vpd");
    $vcdpluson(0, TOP);
  end

endmodule
