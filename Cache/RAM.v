`ifndef _RAM_V_
`define _RAM_V_

`include "Parameters.v"
`include "CommonLogic/CommonLogic.v"

`default_nettype none

/*****************************************************************************/
// RAM module
/*****************************************************************************/
module RAM (oData, iAddr, iData, iWrE, iClk);
  parameter size = 512;
  parameter numLineBytes = 16;

  //totalSize has to be divisible by blockSize
  `STATIC_ASSERT((size % numLineBytes) == 0, size_divisible_by_blockSize);

  parameter numLineBits = numLineBytes*8;
  parameter numLines = size/numLineBytes;

  //numLines must be divisible by 16.  This will ensure that at least 2
  //ram modules are instantiated, which will in turn assure that
  //chunkAddrSize >= 1.
  `STATIC_ASSERT((numLines % 16) == 0, numLines_divisible_by_16);

  parameter chunkAddrSize = $clog2(numLines/8); //div8 because ramNb8w$
  parameter chunkAddrLSB = 0;
  parameter chunkAddrMSB = chunkAddrLSB + chunkAddrSize - 1;
  
  parameter lineAddrSize = $clog2(8); //ramNb8w
  parameter lineAddrLSB = chunkAddrMSB + 1;
  parameter lineAddrMSB = lineAddrLSB + lineAddrSize - 1;

  parameter addrSize = chunkAddrSize + lineAddrSize;

  parameter skewWr = 0;

  // I/O
  output [numLineBits-1:0] oData;
  input [addrSize-1:0] iAddr;
  
  input [numLineBits-1:0] iData;
  input [numLineBytes-1:0] iWrE;

  input iClk;

  wire wrClk;
  Skew #(skewWr) clkSkew (wrClk, iClk);

  wire wrEnBool;
  ReduceOR #(numLineBytes) orWrEnbool
    (wrEnBool, iWrE);

  genvar i, j, n;
  generate
    //for each 8-line chunk
    for(i=0; i<(numLines/8); i=i+1) begin : chunk
      wire [chunkAddrSize-1:0] cIndex;
      assign cIndex = i;
  
      //Are we reading/writing this chunk?
      wire cEn;
      Compare #(chunkAddrSize) ccmp0
        (cEn, iAddr[chunkAddrMSB:chunkAddrLSB], cIndex);

      //Compute read/write address
      wire [lineAddrSize-1:0] cAddrMask;
      and2$ maskAddr [lineAddrSize-1:0]
        (cAddrMask, iAddr[lineAddrMSB:lineAddrLSB], cEn);

      wire [lineAddrSize-1:0] cAddr;
      or2$ cAddrcomb [lineAddrSize-1:0]
        (cAddr, cAddrMask[0], cAddrMask[1]);

      //Compute write bytes
      wire [numLineBytes-1:0] cnWrEn;
      nand2$ maskWrEn [numLineBytes-1:0] //port1 write bytes
        (cnWrEn, cEn, iWrE);

      wire [numLineBytes-1:0] cnWrEnDelay;
      or2$ or_cnWrEnDelay [numLineBytes-1:0] //delay write enable signals
        (cnWrEnDelay, cnWrEn, wrClk);

      //instantiate memory for this chunk
      wire [numLineBits-1:0] cDataR;
      for(j=0; j<numLineBytes; j=j+1) begin : memArray
        ram8b8w$ mem
          (cAddr, iData[j*8 +: 8], 1'b0, cnWrEnDelay[j], cDataR[j*8 +: 8]);
        
        initial begin
          memArray[j].mem.mem[0] = 0;
          memArray[j].mem.mem[1] = 0;
          memArray[j].mem.mem[2] = 0;
          memArray[j].mem.mem[3] = 0;
          memArray[j].mem.mem[4] = 0;
          memArray[j].mem.mem[5] = 0;
          memArray[j].mem.mem[6] = 0;
          memArray[j].mem.mem[7] = 0;
        end
      end
      
      //attach to output databus
      wire ncEn; //read bus tristate buffer enables
      not1$ notcEn
        (ncEn, cEn);
      tristateL$ tbData0 [numLineBits-1:0]
        (.out(oData), .in(cDataR), .enbar(ncEn));
    end
      
  endgenerate
endmodule

`default_nettype wire
`endif
