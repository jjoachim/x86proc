`ifndef _PIPEREGS_V_
`define _PIPEREGS_V_

module PipeRegs (oData, iData, inStall, inFlush, iClk, inReset);
  parameter width = 1;

  output [width-1:0] oData;
  input [width-1:0] iData;
  input inStall, inFlush, iClk, inReset;
  
  //Delay flush signal for next cycle
  /*
  wire nFlush, nresets;
  regs$ #(1, 1) flushreg //resets to 1
    (nFlush, inFlush, 1'b1, iClk, inReset);
  */

  wire [width-1:0] iFData;
  mux2$ mux_flush [width-1:0] 
    (iFData, 1'b0, iData, inFlush);

  wire iFStall;
  mux2$ mux_stall
    (iFStall, 1'b1, inStall, inFlush);

  regs$ #(width) piperegs
    (oData, iFData, iFStall, iClk, inReset);

endmodule

`endif
