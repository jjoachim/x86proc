`ifndef _PCGEN_VH
`define _PCGEN_VH

// PC generator
module PCGEN(oCSN,  oCSOverwrite,
             oEIPN, oBRTK,
             iRM,   iAltData, iEFLAGS,
             iCS,   iEIP, iEIPPLUS,
             iSegW, iDstR,
             iStat, iControlStore);

    // I/O
    input   [63:0]  iRM;            // Near pointer, or exception far pointer
    input   [63:0]  iAltData;       // Branch address or offset
    input   [31:0]  iEFLAGS;        // Condition codes

    input   [15:0]  iCS;            // Current code segment
    input   [31:0]  iEIP;           // Current EIP
    input   [31:0]  iEIPPLUS;       // Next EIP

    input           iSegW;          // Segment register write
    input   [ 2:0]  iDstR;          // Destination register

    input   [31:0]  iStat;          // Status of instruction
    input   [68:0]  iControlStore;  // Control store of instruction

    output  [15:0]  oCSN;           // New code segment
    output          oCSOverwrite;   // Overwrite CS
    output  [31:0]  oEIPN;          // New EIP
    output          oBRTK;          // Overwrite EIP

    // Split control-store signals
    wire            OPSZOVRPRE; // Operand size override prefix present
    wire            TRAPOP;     // TRAP operation
    wire    [ 1:0]  OPSZ;       // Default operand size
    wire    [ 1:0]  OPSZOVR;    // Overridden operand size
    wire            OPSZOVRS;   // Forced, special operand size override
    wire    [ 1:0]  OPSZS;      // Special operand size
    wire            BROP;       // Branch instruction
    wire    [ 1:0]  BRCOND;     // 00=Unconditional  10=~ZF
                                // 10=~CF&~ZF        11=Unused
    wire    [ 1:0]  BRTYPE;     // 00=Short/Relative 01=Near/Realtive
                                // 10=Near/Absolute  11=Far/Absolute
    wire            DISABLECF0; // Disable if ~CF                                
    wire            DISABLEZF0; // Disable if ~ZF
    wire            DISABLEZF1; // Disable if ZF

    // Operand Sizes
    wire    [ 1:0]  OPSZA;
    wire    [ 1:0]  OPSZB;

    // Branch addresses
    wire    [31:0]  SHORTREL;   // Short, relative displacement
    wire    [31:0]  NEARREL;    // Near, relative displacement
    wire    [31:0]  NEARABS;    // Near, absolute
    wire    [47:0]  FARABS;     // Far, absolute
    wire    [47:0]  FARTRAP;    // Far, absolute, trap

    // Branch address adder output
    wire            SHORTRELCY;
    wire    [31:0]  NEARRELADD;
    wire            NEARRELCY;

    // EIPN generation
    wire    [31:0]  EIPN_BRTYPEMUXOUT;
    wire    [31:0]  EIPN_TRAPOPMUXOUT;

    // BRTK generation
    wire            ZF0;
    wire            CFZF0;
    wire            BRTK_BRCONDMUXOUT;
    wire            BRTK_BROPMUXOUT;
    wire            BRTK_TRAPMUXOUT;

    // CSN generation
    wire    [15:0]  CSN_BRTYPEMUXOUT;
    wire    [15:0]  CSN_TRAPOPMUXOUT;

    // CSOverwrite generation
    wire            CSOVR_BRCONDMUXOUT;
    wire            CSOVR_BRTYPEMUXOUT;
    wire            CSOVR_BROPMUXOUT;
    wire            CSOVR_TRAPMUXOUT;

    // Split Control-Store
    assign OPSZOVRPRE = iControlStore[`iOPSZOVRPRE];
    assign TRAPOP     = iControlStore[`iTRAPOP];
    assign OPSZ       = iControlStore[`iOPSZ];
    assign OPSZOVR    = iControlStore[`iOPSZOVR];
    assign OPSZOVRS   = iControlStore[`iOPSZOVRS];
    assign OPSZS      = iControlStore[`iOPSZS];
    assign ALUAMUX    = iControlStore[`iALUAMUX];
    assign ALUBMUX    = iControlStore[`iALUBMUX];
    assign ALUMUX     = iControlStore[`iALUMUX];
    assign FLAGMUX    = iControlStore[`iFLAGMUX];
    assign ALUOUTMUX  = iControlStore[`iALUOUTMUX];
    assign BROP       = iControlStore[`iBROP];
    assign BRCOND     = iControlStore[`iBRCOND];
    assign BRTYPE     = iControlStore[`iBRTYPE];
    assign DISABLECF0 = iControlStore[`iDISABLECF0];
    assign DISABLEZF0 = iControlStore[`iDISABLEZF0];
    assign DISABLEZF1 = iControlStore[`iDISABLEZF1];

    // Operand sizes
    mux2_2$ opsza_mux(OPSZA, OPSZ,  OPSZOVR, OPSZOVRPRE);
    mux2_2$ opszb_mux(OPSZB, OPSZA, OPSZS,   OPSZOVRS);

    // Branch addresses
    KSAdder32 shortrel_adder(SHORTREL,  SHORTRELCY,iEIPPLUS,iAltData[31:0],1'b0);
    KSAdder32 nearrel_adder (NEARRELADD,NEARRELCY, iEIPPLUS,iAltData[31:0],1'b0);
    mux2_32$  nearrel_mux(NEARREL, {16'h0000,NEARRELADD[15:0]},NEARRELADD,OPSZB[1]);
    mux2_32$  nearabs_mux(NEARABS, {16'h0000,iRM[15:0]},       iRM[31:0], OPSZB[1]);
    assign FARABS  = iAltData[47:0];
    assign FARTRAP = {iRM[31:16],
                      iRM[63:48],iRM[15:0]};

    // EIPN muxes
    mux4_32$ eipn_brtype_mux(EIPN_BRTYPEMUXOUT,
                             SHORTREL, NEARREL, NEARABS, FARABS[31:0],
                             BRTYPE);
    mux2_32$ eipn_trap_mux(EIPN_TRAPOPMUXOUT,
                           EIPN_BRTYPEMUXOUT, FARTRAP[31:0],
                           TRAPOP);
    mux2_32$ eipn_exc_mux(oEIPN,
                          EIPN_TRAPOPMUXOUT, iEIP,
                          iStat[`iEXC]);

    // BRTK muxes
    nor2$ cfzf0_nor(CFZF0, iEFLAGS[`iCF], iEFLAGS[`iZF]);
    inv1$ zf0_inv(ZF0, iEFLAGS[`iZF]);
    mux4$ brtk_brcond_mux(BRTK_BRCONDMUXOUT,
                          1'b1, ZF0, CFZF0, 1'b1,
                          BRCOND[0], BRCOND[1]);
    mux2$ brtk_brop_mux(BRTK_BROPMUXOUT,
                        1'b0,BRTK_BRCONDMUXOUT,
                        BROP);
    mux2$ brtk_trap_mux(BRTK_TRAPMUXOUT,
                        BRTK_BROPMUXOUT, 1'b1,
                        TRAPOP);
    mux2$ brtk_exc_mux(oBRTK,
                       BRTK_TRAPMUXOUT, 1'b0,
                       iStat[`iEXC]);

    // CSN muxes
    mux4_16$ csn_brtype_mux(CSN_BRTYPEMUXOUT,
                            iCS, iCS, iCS, FARABS[47:32],
                            BRTYPE[0], BRTYPE[1]);
    mux2_16$ csn_trap_mux(CSN_TRAPOPMUXOUT,
                          CSN_BRTYPEMUXOUT, FARTRAP[47:32],
                          TRAPOP);
    mux2_16$ csn_exc_mux(oCSN,
                         CSN_TRAPOPMUXOUT, iCS,
                         iStat[`iEXC]);

    // CSOverwrite muxes
    mux4$ csovr_brcond_mux(CSOVR_BRCONDMUXOUT,
                           1'b1, ZF0, CFZF0, 1'b0,
                           BRCOND[0], BRCOND[1]);
    mux4$ csovr_brtype_mux(CSOVR_BRTYPEMUXOUT,
                           1'b0, 1'b0, 1'b0, CSOVR_BRCONDMUXOUT,
                           BRTYPE[0], BRTYPE[1]);
    mux2$ csovr_brop_mux(CSOVR_BROPMUXOUT,
                         1'b0,CSOVR_BRTYPEMUXOUT,
                         BROP);
    mux2$ csovr_trap_mux(CSOVR_TRAPMUXOUT,
                         CSOVR_BROPMUXOUT, 1'b1,
                         TRAPOP);
    mux2$ csovr_exc_mux(oCSOverwrite,
                        CSOVR_TRAPMUXOUT, 1'b0,
                        iStat[`iEXC]);

endmodule

`endif
